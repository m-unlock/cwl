{
    "$graph": [
        {
            "class": "CommandLineTool",
            "label": "Concatenate multiple files",
            "baseCommand": [
                "cat"
            ],
            "stdout": "$(inputs.outname)",
            "hints": [
                {
                    "dockerPull": "debian:buster",
                    "class": "DockerRequirement"
                }
            ],
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#concatenate.cwl/infiles"
                },
                {
                    "type": "string",
                    "id": "#concatenate.cwl/outname"
                }
            ],
            "id": "#concatenate.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2021-00-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential",
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outname)"
                    },
                    "id": "#concatenate.cwl/output"
                }
            ]
        },
        {
            "class": "CommandLineTool",
            "label": "compress a file multithreaded with pigz",
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/pigz:2.8",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.8"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/pigz"
                            ],
                            "package": "pigz"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "pigz",
                "-c"
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.inputfile)"
                }
            ],
            "stdout": "$(inputs.inputfile.basename).gz",
            "inputs": [
                {
                    "type": "File",
                    "id": "#pigz.cwl/inputfile"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "-p"
                    },
                    "id": "#pigz.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.inputfile.basename).gz"
                    },
                    "id": "#pigz.cwl/outfile"
                }
            ],
            "id": "#pigz.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Filter from reads",
            "doc": "Filter reads using BBmaps bbduk tool (paired-end only)\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/bbmap:39.06",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "39.06"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/bbmap"
                            ],
                            "package": "bbmap"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "bbduk.sh"
            ],
            "inputs": [
                {
                    "type": "File",
                    "inputBinding": {
                        "prefix": "in=",
                        "separate": false
                    },
                    "id": "#bbduk_filter.cwl/forward_reads"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#bbduk_filter.cwl/identifier"
                },
                {
                    "type": "int",
                    "inputBinding": {
                        "prefix": "k=",
                        "separate": false
                    },
                    "default": 31,
                    "id": "#bbduk_filter.cwl/kmersize"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 8000,
                    "id": "#bbduk_filter.cwl/memory"
                },
                {
                    "doc": "Reference contamination fasta file (can be compressed)",
                    "label": "Reference contamination file",
                    "type": [
                        "null",
                        "string"
                    ],
                    "inputBinding": {
                        "prefix": "ref=",
                        "separate": false
                    },
                    "id": "#bbduk_filter.cwl/reference"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "prefix": "in2=",
                        "separate": false
                    },
                    "id": "#bbduk_filter.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "threads=",
                        "separate": false
                    },
                    "id": "#bbduk_filter.cwl/threads"
                }
            ],
            "stderr": "$(inputs.identifier)_bbduk-summary.txt",
            "arguments": [
                {
                    "prefix": "-Xmx",
                    "separate": false,
                    "valueFrom": "$(inputs.memory)M"
                },
                {
                    "prefix": "out=",
                    "separate": false,
                    "valueFrom": "$(inputs.identifier)_1.fq.gz"
                },
                {
                    "prefix": "out2=",
                    "separate": false,
                    "valueFrom": "$(inputs.identifier)_2.fq.gz"
                },
                {
                    "prefix": "stats=",
                    "separate": false,
                    "valueFrom": "$(inputs.identifier)_bbduk-stats.txt"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_1.fq.gz"
                    },
                    "id": "#bbduk_filter.cwl/out_forward_reads"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_2.fq.gz"
                    },
                    "id": "#bbduk_filter.cwl/out_reverse_reads"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_bbduk-stats.txt"
                    },
                    "id": "#bbduk_filter.cwl/stats_file"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_bbduk-summary.txt"
                    },
                    "id": "#bbduk_filter.cwl/summary"
                }
            ],
            "id": "#bbduk_filter.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/dateModified": "2023-02-10",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/bbmap:39.06",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "39.06"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/bbmap"
                            ],
                            "package": "bbmap"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "label": "BBMap",
            "doc": "Read filtering using BBMap against a (contamination) reference genome\n",
            "inputs": [
                {
                    "type": "File",
                    "inputBinding": {
                        "position": 1,
                        "prefix": "in=",
                        "separate": false
                    },
                    "id": "#bbmap_filter-reads.cwl/forward_reads"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#bbmap_filter-reads.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "maximum memory usage in megabytes",
                    "label": "memory usage (mb)",
                    "default": 8000,
                    "id": "#bbmap_filter-reads.cwl/memory"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": false,
                    "id": "#bbmap_filter-reads.cwl/output_mapped"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "position": 3,
                        "prefix": "ref=",
                        "separate": false
                    },
                    "id": "#bbmap_filter-reads.cwl/reference"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "inputBinding": {
                        "position": 2,
                        "prefix": "in2=",
                        "separate": false
                    },
                    "id": "#bbmap_filter-reads.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "number of threads to use for computational processes",
                    "label": "number of threads",
                    "inputBinding": {
                        "prefix": "threads=",
                        "separate": false
                    },
                    "default": 2,
                    "id": "#bbmap_filter-reads.cwl/threads"
                }
            ],
            "stderr": "$(inputs.identifier)_BBMap_log.txt",
            "outputs": [
                {
                    "label": "Coverage per contig",
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_BBMap_covstats.txt"
                    },
                    "id": "#bbmap_filter-reads.cwl/covstats"
                },
                {
                    "label": "BBMap log output",
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_BBMap_log.txt"
                    },
                    "id": "#bbmap_filter-reads.cwl/log"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_filtered_1.fq.gz"
                    },
                    "id": "#bbmap_filter-reads.cwl/out_forward_reads"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_filtered_2.fq.gz"
                    },
                    "id": "#bbmap_filter-reads.cwl/out_reverse_reads"
                },
                {
                    "label": "Mapping statistics",
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_BBMap_stats.txt"
                    },
                    "id": "#bbmap_filter-reads.cwl/stats"
                }
            ],
            "baseCommand": [
                "bbmap.sh"
            ],
            "arguments": [
                "-Xmx$(inputs.memory)M",
                "printunmappedcount",
                "overwrite=true",
                "statsfile=$(inputs.identifier)_BBMap_stats.txt",
                "covstats=$(inputs.identifier)_BBMap_covstats.txt",
                "${\n  if (inputs.output_mapped){\n    return 'outm1='+inputs.identifier+'_filtered_1.fq.gz \\\n            outm2='+inputs.identifier+'_filtered_2.fq.gz';\n  } else {\n    return 'outu1='+inputs.identifier+'_filtered_1.fq.gz \\\n            outu2='+inputs.identifier+'_filtered_2.fq.gz';\n  }\n}\n"
            ],
            "id": "#bbmap_filter-reads.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/dateModified": "2022-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Prepare fasta DB",
            "doc": "Prepares fasta file for so it does not contain duplicate fasta headers.\nOnly looks at the first part of the header before any whitespace.\nAdds and incremental number in the header.\n\nExpects fasta file(s) or plaintext fasta(s). Not mixed!    \n",
            "requirements": [
                {
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "prepare_fasta_db",
                            "writable": true
                        },
                        {
                            "entryname": "script.sh",
                            "entry": "#!/bin/bash\necho -e \"\\\n#/usr/bin/python3\nimport sys\\n\\\nheaders = set()\\n\\\nc = 0\\n\\\nfor line in sys.stdin:\\n\\\n  splitline = line.split()\\n\\\n  if line[0] == '>':    \\n\\\n    if splitline[0] in headers:\\n\\\n      c += 1\\n\\\n      print(splitline[0]+'.x'+str(c)+' '+' '.join(splitline[1:]))\\n\\\n    else:\\n\\\n      print(line.strip())\\n\\\n    headers.add(splitline[0])\\n\\\n  else:\\n\\\n    print(line.strip())\" > ./dup.py\nout_name=$1\nshift\n\n# python container does not have the command 'file' anymore\n# if file $@ | grep gzip; then\nif [[ $@ == *.gz ]]; then\n  zcat $@ | python3 ./dup.py | gzip > $out_name\nelse\n  cat $@ | python3 ./dup.py | gzip > $out_name\nfi"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "python:3.10-bookworm",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "3.10.6"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/python"
                            ],
                            "package": "python3"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "bash",
                "script.sh"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "label": "fasta files",
                    "doc": "Fasta file(s) to be the prepared. Can also be gzipped (not mixed!)",
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#prepare_fasta_db.cwl/fasta_files"
                },
                {
                    "type": "string",
                    "label": "Output outfile",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#prepare_fasta_db.cwl/output_file_name"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_file_name)"
                    },
                    "id": "#prepare_fasta_db.cwl/fasta_db"
                }
            ],
            "id": "#prepare_fasta_db.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-07-00",
            "https://schema.org/dateModified": "2023-01-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Bracken",
            "doc": "Bayesian Reestimation of Abundance with KrakEN.\nBracken is a highly accurate statistical method that computes the abundance of species in DNA sequences from a metagenomics sample.\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "InitialWorkDirRequirement",
                    "listing": [
                        {
                            "entry": "$(inputs.kraken_report)",
                            "writable": true
                        }
                    ]
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/bracken:2.9--py39h1f90b4d_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.9"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/bracken",
                                "file:///home/bart/git/cwl/tools/bracken/doi.org/10.7717/peerj-cs.104"
                            ],
                            "package": "bracken"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "bracken"
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.identifier+\"_\"+inputs.database.path.split( '/' ).pop()+\"_bracken_\"+inputs.level+\".txt\")",
                    "prefix": "-o"
                }
            ],
            "inputs": [
                {
                    "type": "Directory",
                    "label": "Database",
                    "doc": "Database location of kraken2",
                    "inputBinding": {
                        "prefix": "-d"
                    },
                    "id": "#bracken.cwl/database"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset",
                    "label": "identifier used",
                    "id": "#bracken.cwl/identifier"
                },
                {
                    "type": "File",
                    "label": "Kraken report",
                    "doc": "Kraken REPORT file to use for abundance estimation",
                    "inputBinding": {
                        "prefix": "-i"
                    },
                    "id": "#bracken.cwl/kraken_report"
                },
                {
                    "type": "string",
                    "label": "Level",
                    "doc": "Level to estimate abundance at. option [D,P,C,O,F,G,S,S1]. Default Species; 'S'",
                    "inputBinding": {
                        "prefix": "-l"
                    },
                    "default": "S",
                    "id": "#bracken.cwl/level"
                },
                {
                    "type": "int",
                    "label": "Read length",
                    "doc": "Read length to get all classification. Default 100",
                    "inputBinding": {
                        "prefix": "-r"
                    },
                    "default": 100,
                    "id": "#bracken.cwl/read_length"
                },
                {
                    "type": "int",
                    "label": "threshold",
                    "doc": "Number of reads required PRIOR to abundance estimation to perform reestimation. Default 0",
                    "inputBinding": {
                        "prefix": "-t"
                    },
                    "default": 0,
                    "id": "#bracken.cwl/threshold"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier+\"_\"+inputs.database.path.split( '/' ).pop()+\"_bracken_\"+inputs.level+\".txt\")"
                    },
                    "id": "#bracken.cwl/output_report"
                }
            ],
            "id": "#bracken.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2024-04-15",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "label": "Convert an array of 1 file to a file object",
            "doc": "Converts the array and returns the first file in the array. \nShould only be used when 1 file is in the array.\n",
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "id": "#array_to_file_tool.cwl/files"
                }
            ],
            "baseCommand": [
                "mv"
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.files[0].path)",
                    "position": 1
                },
                {
                    "valueFrom": "./",
                    "position": 2
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.files[0].basename)"
                    },
                    "id": "#array_to_file_tool.cwl/file"
                }
            ],
            "id": "#array_to_file_tool.cwl"
        },
        {
            "class": "ExpressionTool",
            "doc": "Transforms the input files to a mentioned directory\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "string",
                    "id": "#files_to_folder.cwl/destination"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "id": "#files_to_folder.cwl/files"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "Directory"
                        }
                    ],
                    "id": "#files_to_folder.cwl/folders"
                }
            ],
            "expression": "${\n  var array = []\n  if (inputs.files != null) {\n    array = array.concat(inputs.files)\n  }\n  if (inputs.folders != null) {\n    array = array.concat(inputs.folders)\n  }\n  var r = {\n     'results':\n       { \"class\": \"Directory\",\n         \"basename\": inputs.destination,\n         \"listing\": array\n       } \n     };\n   return r; \n }\n",
            "outputs": [
                {
                    "type": "Directory",
                    "id": "#files_to_folder.cwl/results"
                }
            ],
            "id": "#files_to_folder.cwl",
            "http://schema.org/citation": "https://m-unlock.nl",
            "http://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "http://schema.org/dateModified": "2024-10-07",
            "http://schema.org/dateCreated": "2020-00-00",
            "http://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "http://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "doc": "Modified from https://github.com/ambarishK/bio-cwl-tools/blob/release/fastp/fastp.cwl\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/fastp:0.23.4--hadf994f_3",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "0.23.4"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/fastp",
                                "file:///home/bart/git/cwl/tools/fastp/doi.org/10.1002/imt2.107"
                            ],
                            "package": "fastp"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": true,
                    "inputBinding": {
                        "prefix": "--correction"
                    },
                    "id": "#fastp.cwl/base_correction"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": false,
                    "inputBinding": {
                        "prefix": "--dedup"
                    },
                    "id": "#fastp.cwl/deduplicate"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": true,
                    "inputBinding": {
                        "prefix": "--disable_trim_poly_g"
                    },
                    "id": "#fastp.cwl/disable_trim_poly_g"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--trim_poly_g"
                    },
                    "id": "#fastp.cwl/force_polyg_tail_trimming"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "prefix": "--in1"
                    },
                    "id": "#fastp.cwl/forward_reads"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#fastp.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": false,
                    "inputBinding": {
                        "prefix": "--merge"
                    },
                    "id": "#fastp.cwl/merge_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 50,
                    "inputBinding": {
                        "prefix": "--length_required"
                    },
                    "id": "#fastp.cwl/min_length_required"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 20,
                    "inputBinding": {
                        "prefix": "--qualified_quality_phred"
                    },
                    "id": "#fastp.cwl/qualified_phred_quality"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "prefix": "--in2"
                    },
                    "id": "#fastp.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "--thread"
                    },
                    "id": "#fastp.cwl/threads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 20,
                    "inputBinding": {
                        "prefix": "--unqualified_percent_limit"
                    },
                    "id": "#fastp.cwl/unqualified_phred_quality"
                }
            ],
            "arguments": [
                {
                    "prefix": "--out1",
                    "valueFrom": "$(inputs.identifier)_fastp_1.fq.gz"
                },
                "${\n  if (inputs.reverse_reads){\n    return '--out2';\n  } else {\n    return '';\n  }\n}\n",
                "${\n  if (inputs.reverse_reads){\n    return inputs.identifier + \"_fastp_2.fq.gz\";\n  } else {\n    return '';\n  }\n}\n",
                "${\n  if (inputs.reverse_reads_path){\n    return '--out2';\n  } else {\n    return '';\n  }\n}\n",
                "${\n  if (inputs.reverse_reads_path){\n    return inputs.identifier + \"_fastp_2.fq.gz\";\n  } else {\n    return '';\n  }\n}\n",
                "${\n  if (inputs.merge_reads){\n    return '--merged_out';\n  } else {\n    return '';\n  }\n}\n",
                "${\n  if (inputs.merge_reads){\n    return inputs.identifier + \"merged_fastp.fq.gz\";\n  } else {\n    return '';\n  }\n}\n",
                {
                    "prefix": "-h",
                    "valueFrom": "$(inputs.identifier)_fastp.html"
                },
                {
                    "prefix": "-j",
                    "valueFrom": "$(inputs.identifier)_fastp.json"
                }
            ],
            "baseCommand": [
                "fastp"
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_fastp.html"
                    },
                    "id": "#fastp.cwl/html_report"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_fastp.json"
                    },
                    "id": "#fastp.cwl/json_report"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_merged_fastp.fq.gz"
                    },
                    "id": "#fastp.cwl/merged_reads"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_fastp_1.fq.gz"
                    },
                    "id": "#fastp.cwl/out_forward_reads"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_fastp_2.fq.gz"
                    },
                    "id": "#fastp.cwl/out_reverse_reads"
                }
            ],
            "id": "#fastp.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/dateModified": "2022-02-22",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "baseCommand": [
                "fastqc"
            ],
            "label": "FASTQC",
            "doc": "Performs quality control on FASTQ files\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "InitialWorkDirRequirement",
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "FASTQC",
                            "writable": true
                        }
                    ]
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/fastqc:0.12.1--hdfd78af_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "0.12.1"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/fastqc"
                            ],
                            "package": "fastqc"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "arguments": [
                "--outdir",
                "FASTQC"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "doc": "FastQ file list",
                    "label": "FASTQ file list",
                    "inputBinding": {
                        "position": 100
                    },
                    "id": "#fastqc.cwl/fastq"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "FastQ files list",
                    "label": "FASTQ files list",
                    "inputBinding": {
                        "position": 101,
                        "prefix": "--nano"
                    },
                    "id": "#fastqc.cwl/nanopore_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "--threads"
                    },
                    "id": "#fastqc.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "outputBinding": {
                        "glob": "FASTQC/*.html"
                    },
                    "id": "#fastqc.cwl/html_files"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "outputBinding": {
                        "glob": "FASTQC/*.zip"
                    },
                    "id": "#fastqc.cwl/zip_files"
                }
            ],
            "id": "#fastqc.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0002-5516-8391",
                    "https://schema.org/email": "mailto:german.royvalgarcia@wur.nl",
                    "https://schema.org/name": "Germ\u00e1n Royval"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2021-11-26",
            "https://schema.org/dateModified": "2022-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "baseCommand": [
                "kraken2"
            ],
            "label": "Kraken2",
            "doc": "Kraken2 metagenomics taxomic read classification.\n\nUpdated databases available at: https://benlangmead.github.io/aws-indexes/k2 (e.g. PlusPF-8)\nOriginal db: https://ccb.jhu.edu/software/kraken2/index.shtml?t=downloads\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/kraken2:2.1.3--pl5321hdcf5f25_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.1.3"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/kraken2",
                                "file:///home/bart/git/cwl/tools/kraken2/doi.org/10.1186/s13059-019-1891-0"
                            ],
                            "package": "kraken2"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.identifier)_$(inputs.database.path.split( '/' ).pop())_kraken2.txt",
                    "prefix": "--output"
                },
                {
                    "valueFrom": "$(inputs.identifier)_$(inputs.database.path.split( '/' ).pop())_kraken2_report.txt",
                    "prefix": "--report"
                },
                "--report-zero-counts",
                "--use-names",
                "--memory-mapping"
            ],
            "inputs": [
                {
                    "type": "boolean",
                    "doc": "input data is gzip compressed",
                    "inputBinding": {
                        "position": 3,
                        "prefix": "--bzip2-compressed"
                    },
                    "default": false,
                    "id": "#kraken2.cwl/bzip2"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Confidence",
                    "doc": "Confidence score threshold (default 0.0) must be in [0, 1]",
                    "inputBinding": {
                        "position": 4,
                        "prefix": "--confidence"
                    },
                    "id": "#kraken2.cwl/confidence"
                },
                {
                    "type": "Directory",
                    "label": "Database",
                    "doc": "Database location of kraken2",
                    "inputBinding": {
                        "prefix": "--db"
                    },
                    "id": "#kraken2.cwl/database"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Forward reads",
                    "doc": "Illumina forward read file",
                    "inputBinding": {
                        "position": 100
                    },
                    "id": "#kraken2.cwl/forward_reads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "input data is gzip compressed",
                    "inputBinding": {
                        "position": 3,
                        "prefix": "--gzip-compressed"
                    },
                    "default": false,
                    "id": "#kraken2.cwl/gzip"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#kraken2.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Nanopore reads",
                    "doc": "Oxford Nanopore Technologies reads in FASTQ",
                    "inputBinding": {
                        "position": 102
                    },
                    "id": "#kraken2.cwl/nanopore_reads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Paired end",
                    "doc": "Data is paired end (separate files)",
                    "inputBinding": {
                        "position": 2,
                        "prefix": "--paired"
                    },
                    "default": false,
                    "id": "#kraken2.cwl/paired_end"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Reverse reads",
                    "doc": "Illumina reverse read file",
                    "inputBinding": {
                        "position": 101
                    },
                    "id": "#kraken2.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "--threads"
                    },
                    "id": "#kraken2.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_$(inputs.database.path.split( '/' ).pop())_kraken2_report.txt"
                    },
                    "id": "#kraken2.cwl/sample_report"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_$(inputs.database.path.split( '/' ).pop())_kraken2.txt"
                    },
                    "id": "#kraken2.cwl/standard_report"
                }
            ],
            "id": "#kraken2.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0002-5516-8391",
                    "https://schema.org/email": "mailto:german.royvalgarcia@wur.nl",
                    "https://schema.org/name": "Germ\u00e1n Royval"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2021-11-25",
            "https://schema.org/dateModified": "2021-11-04",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "kreport2krona.py",
            "doc": "This program takes a Kraken report file and prints out a krona-compatible TEXT file\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/krakentools:1.2--pyh5e36f6f_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.2"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/krakentools"
                            ],
                            "package": "kronatools"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "kreport2krona.py"
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.report.nameroot)_krona.txt",
                    "prefix": "--output"
                }
            ],
            "inputs": [
                {
                    "type": "boolean",
                    "label": "Intermediate Ranks",
                    "doc": "Include non-standard levels. Default false",
                    "inputBinding": {
                        "prefix": "--intermediate-ranks"
                    },
                    "default": false,
                    "id": "#kreport2krona.cwl/intermediate-ranks"
                },
                {
                    "type": "boolean",
                    "label": "No Intermediate Ranks",
                    "doc": "only output standard levels [D,P,C,O,F,G,S]. Default true",
                    "inputBinding": {
                        "prefix": "--no-intermediate-ranks"
                    },
                    "default": true,
                    "id": "#kreport2krona.cwl/no-intermediate-ranks"
                },
                {
                    "type": "File",
                    "label": "Report",
                    "doc": "Kraken report file",
                    "inputBinding": {
                        "prefix": "--report"
                    },
                    "id": "#kreport2krona.cwl/report"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.report.nameroot)_krona.txt"
                    },
                    "id": "#kreport2krona.cwl/krona_txt"
                }
            ],
            "id": "#kreport2krona.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2024-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/krona:2.8.1--pl5321hdfd78af_1",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.8.1"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/krona",
                                "file:///home/bart/git/cwl/tools/krona/doi.org/10.1186/1471-2105-12-385"
                            ],
                            "package": "krona"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "ktImportText"
            ],
            "label": "Krona ktImportText",
            "doc": "Creates a Krona chart from text files listing quantities and lineages.\ntext  Tab-delimited text file. Each line should be a number followed by a list of wedges to contribute to (starting from the highest level). \nIf no wedges are listed (and just a quantity is given), it will contribute to the top level. \nIf the same lineage is listed more than once, the values will be added. Quantities can be omitted if -q is specified.\nLines beginning with \"#\" will be ignored. By default, separate datasets will be created for each input.\n",
            "arguments": [
                {
                    "prefix": "-o",
                    "valueFrom": "$(inputs.input.nameroot).html"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Highest level",
                    "doc": "Name of the highest level. Default 'all'",
                    "inputBinding": {
                        "position": 1,
                        "prefix": "-n"
                    },
                    "id": "#krona_ktImportText.cwl/highest_level"
                },
                {
                    "type": "File",
                    "label": "Tab-delimited text file",
                    "inputBinding": {
                        "position": 10
                    },
                    "id": "#krona_ktImportText.cwl/input"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "No quantity",
                    "doc": "Fields do not have a field for quantity. Default false",
                    "inputBinding": {
                        "position": 2,
                        "prefix": "-q"
                    },
                    "default": false,
                    "id": "#krona_ktImportText.cwl/no_quantity"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.input.nameroot).html"
                    },
                    "id": "#krona_ktImportText.cwl/krona_html"
                }
            ],
            "id": "#krona_ktImportText.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2024-04-10",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "ScatterFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                },
                {
                    "class": "SubworkflowFeatureRequirement"
                }
            ],
            "label": "Illumina read quality control, trimming and contamination filter.",
            "doc": "**Workflow for Illumina paired read quality control, trimming and filtering.**<br />\nMultiple paired datasets will be merged into single paired dataset.<br />\nSummary:\n- FastQC on raw data files<br />\n- fastp for read quality trimming<br />\n- BBduk for phiX and (optional) rRNA filtering<br />\n- Kraken2 for taxonomic classification of reads (optional)<br />\n- Bracken Bayesian Reestimation of Abundance with KrakEN (optional)<br />\n- BBmap for (contamination) filtering using given references (optional)<br />\n- FastQC on filtered (merged) data<br />\n\nOther UNLOCK workflows on WorkflowHub: https://workflowhub.eu/projects/16/workflows?view=default<br><br>\n\n**All tool CWL files and other workflows can be found here:**<br>\n  Tools: https://gitlab.com/m-unlock/cwl<br>\n  Workflows: https://gitlab.com/m-unlock/cwl/workflows<br>\n\n**How to setup and use an UNLOCK workflow:**<br>\nhttps://m-unlock.gitlab.io/docs/setup/setup.html<br>\n",
            "outputs": [
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Filtered forward read",
                    "doc": "Filtered forward read",
                    "outputSource": "#main/out_fwd_reads/fwd_out",
                    "id": "#main/QC_forward_reads"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Filtered reverse read",
                    "doc": "Filtered reverse read",
                    "outputSource": "#main/out_rev_reads/rev_out",
                    "id": "#main/QC_reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "Kraken2 folder",
                    "doc": "Folder with Kraken2 output files",
                    "outputSource": "#main/kraken2_files_to_folder/results",
                    "id": "#main/kraken2_folder"
                },
                {
                    "type": "Directory",
                    "label": "Filtering reports folder",
                    "doc": "Folder containing all reports of filtering and quality control",
                    "outputSource": "#main/reports_files_to_folder/results",
                    "id": "#main/reports_folder"
                }
            ],
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "string"
                    },
                    "label": "Bracken levels",
                    "doc": "Taxonomy levels in bracken estimate abundances on. Default runs through; [D,P,C,O,F,G,S,S1]",
                    "default": [
                        "P",
                        "C",
                        "O",
                        "F",
                        "G",
                        "S"
                    ],
                    "id": "#main/bracken_levels"
                },
                {
                    "type": "int",
                    "label": "Input read length",
                    "doc": "Read length Needed for Bracken analysis. Need to be available in kraken2 database folder. (default 150)",
                    "default": 150,
                    "id": "#main/bracken_read_length"
                },
                {
                    "type": "boolean",
                    "doc": "Remove exact duplicate reads with fastp. (default false)",
                    "label": "Deduplicate reads",
                    "default": false,
                    "id": "#main/deduplicate"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Output Destination",
                    "doc": "Optional output destination only used for cwl-prov reporting.",
                    "id": "#main/destination"
                },
                {
                    "type": "boolean",
                    "label": "Don't output reads.",
                    "doc": "Do not output filtered reads. (default false)",
                    "default": false,
                    "id": "#main/do_not_output_filtered_reads"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "doc": "References fasta file(s) for filtering",
                    "label": "Filter reference file(s)",
                    "loadListing": "no_listing",
                    "id": "#main/filter_references"
                },
                {
                    "type": "boolean",
                    "doc": "Optionally remove rRNA sequences from the reads (default false)",
                    "label": "filter rRNA",
                    "default": false,
                    "id": "#main/filter_rrna"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "doc": "Forward sequence fastq file(s) locally",
                    "label": "Forward reads",
                    "loadListing": "no_listing",
                    "id": "#main/forward_reads"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow.",
                    "label": "identifier used",
                    "id": "#main/identifier"
                },
                {
                    "type": "boolean",
                    "doc": "Keep with reads mapped to the given reference. Default false",
                    "label": "Keep mapped reads",
                    "default": false,
                    "id": "#main/keep_reference_mapped_reads"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Kraken2 confidence threshold",
                    "doc": "Confidence score threshold must be between [0, 1]. (default 0.0)",
                    "id": "#main/kraken2_confidence"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "Directory"
                        }
                    ],
                    "label": "Kraken2 database",
                    "doc": "Kraken2 database location, multiple databases is possible",
                    "default": [],
                    "loadListing": "no_listing",
                    "id": "#main/kraken2_database"
                },
                {
                    "type": "int",
                    "doc": "Maximum memory usage in MegaBytes. (default 8000)",
                    "label": "Maximum memory in MB",
                    "default": 8000,
                    "id": "#main/memory"
                },
                {
                    "type": "boolean",
                    "label": "Kraken2 standard report",
                    "doc": "Also output Kraken2 standard report with per read classification. These can be large. (default false)",
                    "default": false,
                    "id": "#main/output_kraken2_standard_report"
                },
                {
                    "type": "boolean",
                    "doc": "Prepare references to a single fasta file and unique headers.\nWhen false a single fasta file as reference is expected with unique headers. (default true)\n",
                    "label": "Prepare references",
                    "default": true,
                    "id": "#main/prepare_reference"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "doc": "Reverse sequence fastq file(s) locally",
                    "label": "Reverse reads",
                    "loadListing": "no_listing",
                    "id": "#main/reverse_reads"
                },
                {
                    "type": "boolean",
                    "label": "Run Bracken",
                    "doc": "Skip Bracken analysis. (default false)",
                    "default": false,
                    "id": "#main/skip_bracken"
                },
                {
                    "type": "boolean",
                    "doc": "Skip kraken2 on filtered data. (default false)",
                    "label": "Skip kraken2 filtered",
                    "default": false,
                    "id": "#main/skip_kraken2_filtered"
                },
                {
                    "type": "boolean",
                    "doc": "Skip kraken2 on unfiltered data. (default false)",
                    "label": "Skip kraken2 unfiltered",
                    "default": false,
                    "id": "#main/skip_kraken2_unfiltered"
                },
                {
                    "type": "boolean",
                    "doc": "Skip FastQC analyses of filtered input reads (default false)",
                    "label": "Skip QC filtered",
                    "default": false,
                    "id": "#main/skip_qc_filtered"
                },
                {
                    "type": "boolean",
                    "doc": "Skip FastQC analyses of raw input reads (default false)",
                    "label": "Skip QC unfiltered",
                    "default": false,
                    "id": "#main/skip_qc_unfiltered"
                },
                {
                    "label": "Input URLs used for this run",
                    "doc": "A provenance element to capture the original source of the input data",
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "string"
                        }
                    ],
                    "id": "#main/source"
                },
                {
                    "type": "int",
                    "doc": "Number of threads to use for computational processes. (default 2)",
                    "label": "Number of threads",
                    "default": 2,
                    "id": "#main/threads"
                }
            ],
            "steps": [
                {
                    "label": "fastp",
                    "doc": "Read quality filtering and (barcode) trimming.",
                    "run": "#fastp.cwl",
                    "in": [
                        {
                            "source": "#main/deduplicate",
                            "id": "#main/fastp/deduplicate"
                        },
                        {
                            "source": [
                                "#main/fastq_merge_fwd/output",
                                "#main/fastq_fwd_array_to_file/file"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#main/fastp/forward_reads"
                        },
                        {
                            "source": "#main/identifier",
                            "id": "#main/fastp/identifier"
                        },
                        {
                            "source": [
                                "#main/fastq_merge_rev/output",
                                "#main/fastq_rev_array_to_file/file"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#main/fastp/reverse_reads"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/fastp/threads"
                        }
                    ],
                    "out": [
                        "#main/fastp/out_forward_reads",
                        "#main/fastp/out_reverse_reads",
                        "#main/fastp/html_report",
                        "#main/fastp/json_report"
                    ],
                    "id": "#main/fastp"
                },
                {
                    "label": "Fwd reads array to file",
                    "doc": "Forward file of single file array to file object",
                    "when": "$(inputs.forward_reads.length === 1)",
                    "run": "#array_to_file_tool.cwl",
                    "in": [
                        {
                            "source": "#main/forward_reads",
                            "id": "#main/fastq_fwd_array_to_file/files"
                        },
                        {
                            "source": "#main/forward_reads",
                            "id": "#main/fastq_fwd_array_to_file/forward_reads"
                        }
                    ],
                    "out": [
                        "#main/fastq_fwd_array_to_file/file"
                    ],
                    "id": "#main/fastq_fwd_array_to_file"
                },
                {
                    "label": "Merge forward reads",
                    "doc": "Merge multiple forward fastq reads to a single file",
                    "when": "$(inputs.forward_reads.length > 1)",
                    "run": "#concatenate.cwl",
                    "in": [
                        {
                            "source": "#main/forward_reads",
                            "id": "#main/fastq_merge_fwd/forward_reads"
                        },
                        {
                            "source": "#main/forward_reads",
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/fastq_merge_fwd/infiles"
                        },
                        {
                            "source": "#main/identifier",
                            "valueFrom": "$(self)_illumina_merged_1.fq.gz",
                            "id": "#main/fastq_merge_fwd/outname"
                        }
                    ],
                    "out": [
                        "#main/fastq_merge_fwd/output"
                    ],
                    "id": "#main/fastq_merge_fwd"
                },
                {
                    "label": "Merge reverse reads",
                    "doc": "Merge multiple reverse fastq reads to a single file",
                    "when": "$(inputs.reverse_reads.length > 1)",
                    "run": "#concatenate.cwl",
                    "in": [
                        {
                            "source": "#main/reverse_reads",
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/fastq_merge_rev/infiles"
                        },
                        {
                            "source": "#main/identifier",
                            "valueFrom": "$(self)_illumina_merged_2.fq.gz",
                            "id": "#main/fastq_merge_rev/outname"
                        },
                        {
                            "source": "#main/reverse_reads",
                            "id": "#main/fastq_merge_rev/reverse_reads"
                        }
                    ],
                    "out": [
                        "#main/fastq_merge_rev/output"
                    ],
                    "id": "#main/fastq_merge_rev"
                },
                {
                    "label": "Rev reads array to file",
                    "doc": "Forward file of single file array to file object",
                    "when": "$(inputs.reverse_reads.length === 1)",
                    "run": "#array_to_file_tool.cwl",
                    "in": [
                        {
                            "source": "#main/reverse_reads",
                            "id": "#main/fastq_rev_array_to_file/files"
                        },
                        {
                            "source": "#main/reverse_reads",
                            "id": "#main/fastq_rev_array_to_file/reverse_reads"
                        }
                    ],
                    "out": [
                        "#main/fastq_rev_array_to_file/file"
                    ],
                    "id": "#main/fastq_rev_array_to_file"
                },
                {
                    "label": "FastQC after",
                    "doc": "Quality assessment and report of reads",
                    "run": "#fastqc.cwl",
                    "when": "$(inputs.skip_qc_filtered == false)",
                    "in": [
                        {
                            "source": [
                                "#main/phix_filter/out_forward_reads",
                                "#main/phix_filter/out_reverse_reads"
                            ],
                            "id": "#main/fastqc_illumina_after/fastq"
                        },
                        {
                            "source": "#main/skip_qc_filtered",
                            "id": "#main/fastqc_illumina_after/skip_qc_filtered"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/fastqc_illumina_after/threads"
                        }
                    ],
                    "out": [
                        "#main/fastqc_illumina_after/html_files",
                        "#main/fastqc_illumina_after/zip_files"
                    ],
                    "id": "#main/fastqc_illumina_after"
                },
                {
                    "label": "FastQC before",
                    "doc": "Quality assessment and report of reads",
                    "run": "#fastqc.cwl",
                    "when": "$(inputs.skip_qc_unfiltered == false)",
                    "in": [
                        {
                            "source": [
                                "#main/forward_reads",
                                "#main/reverse_reads"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/fastqc_illumina_before/fastq"
                        },
                        {
                            "source": "#main/skip_qc_unfiltered",
                            "id": "#main/fastqc_illumina_before/skip_qc_unfiltered"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/fastqc_illumina_before/threads"
                        }
                    ],
                    "out": [
                        "#main/fastqc_illumina_before/html_files",
                        "#main/fastqc_illumina_before/zip_files"
                    ],
                    "id": "#main/fastqc_illumina_before"
                },
                {
                    "label": "Kraken2 unfiltered",
                    "doc": "Taxonomic classification on unfiltered files",
                    "when": "$(!inputs.skip_kraken2_filtered && inputs.kraken2_database !== null && inputs.kraken2_database.length !== 0)",
                    "run": "#workflow_kraken2-bracken.cwl",
                    "scatter": "#main/illumina_kraken2_filtered/kraken2_database",
                    "in": [
                        {
                            "source": "#main/bracken_levels",
                            "id": "#main/illumina_kraken2_filtered/bracken_levels"
                        },
                        {
                            "source": "#main/identifier",
                            "valueFrom": "$(self+\"_illumina_filtered\")",
                            "id": "#main/illumina_kraken2_filtered/identifier"
                        },
                        {
                            "source": "#main/phix_filter/out_forward_reads",
                            "id": "#main/illumina_kraken2_filtered/illumina_forward_reads"
                        },
                        {
                            "source": "#main/phix_filter/out_reverse_reads",
                            "id": "#main/illumina_kraken2_filtered/illumina_reverse_reads"
                        },
                        {
                            "source": "#main/kraken2_confidence",
                            "id": "#main/illumina_kraken2_filtered/kraken2_confidence"
                        },
                        {
                            "source": "#main/kraken2_database",
                            "id": "#main/illumina_kraken2_filtered/kraken2_database"
                        },
                        {
                            "source": "#main/output_kraken2_standard_report",
                            "id": "#main/illumina_kraken2_filtered/output_standard_report"
                        },
                        {
                            "source": "#main/bracken_read_length",
                            "id": "#main/illumina_kraken2_filtered/read_length"
                        },
                        {
                            "source": "#main/skip_kraken2_filtered",
                            "id": "#main/illumina_kraken2_filtered/skip_kraken2_filtered"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/illumina_kraken2_filtered/threads"
                        }
                    ],
                    "out": [
                        "#main/illumina_kraken2_filtered/kraken2_folder",
                        "#main/illumina_kraken2_filtered/bracken_folder"
                    ],
                    "id": "#main/illumina_kraken2_filtered"
                },
                {
                    "label": "Kraken2 unfiltered",
                    "doc": "Taxonomic classification on unfiltered files",
                    "when": "$(!inputs.skip_kraken2_unfiltered && inputs.kraken2_database !== null && inputs.kraken2_database.length !== 0)",
                    "run": "#workflow_kraken2-bracken.cwl",
                    "scatter": "#main/illumina_kraken2_unfiltered/kraken2_database",
                    "in": [
                        {
                            "source": "#main/bracken_levels",
                            "id": "#main/illumina_kraken2_unfiltered/bracken_levels"
                        },
                        {
                            "source": "#main/identifier",
                            "valueFrom": "$(self+\"_illumina_unfiltered\")",
                            "id": "#main/illumina_kraken2_unfiltered/identifier"
                        },
                        {
                            "source": [
                                "#main/rrna_filter/out_forward_reads",
                                "#main/fastp/out_forward_reads"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#main/illumina_kraken2_unfiltered/illumina_forward_reads"
                        },
                        {
                            "source": [
                                "#main/rrna_filter/out_reverse_reads",
                                "#main/fastp/out_reverse_reads"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#main/illumina_kraken2_unfiltered/illumina_reverse_reads"
                        },
                        {
                            "source": "#main/kraken2_confidence",
                            "id": "#main/illumina_kraken2_unfiltered/kraken2_confidence"
                        },
                        {
                            "source": "#main/kraken2_database",
                            "id": "#main/illumina_kraken2_unfiltered/kraken2_database"
                        },
                        {
                            "source": "#main/output_kraken2_standard_report",
                            "id": "#main/illumina_kraken2_unfiltered/output_standard_report"
                        },
                        {
                            "source": "#main/bracken_read_length",
                            "id": "#main/illumina_kraken2_unfiltered/read_length"
                        },
                        {
                            "source": "#main/skip_bracken",
                            "id": "#main/illumina_kraken2_unfiltered/skip_bracken"
                        },
                        {
                            "source": "#main/skip_kraken2_unfiltered",
                            "id": "#main/illumina_kraken2_unfiltered/skip_kraken2_unfiltered"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/illumina_kraken2_unfiltered/threads"
                        }
                    ],
                    "out": [
                        "#main/illumina_kraken2_unfiltered/kraken2_folder",
                        "#main/illumina_kraken2_unfiltered/bracken_folder"
                    ],
                    "id": "#main/illumina_kraken2_unfiltered"
                },
                {
                    "label": "Kraken2 folder",
                    "doc": "Kraken2 files to single folder",
                    "when": "$((!inputs.skip_kraken2_unfiltered || !inputs.skip_kraken2_filtered) && (!inputs.kraken2_database !== null && inputs.kraken2_database.length !== 0))",
                    "in": [
                        {
                            "default": "Illumina_Kraken2-Bracken",
                            "id": "#main/kraken2_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/illumina_kraken2_unfiltered/kraken2_folder",
                                "#main/illumina_kraken2_unfiltered/bracken_folder",
                                "#main/illumina_kraken2_filtered/kraken2_folder",
                                "#main/illumina_kraken2_filtered/bracken_folder"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/kraken2_files_to_folder/folders"
                        },
                        {
                            "source": "#main/kraken2_database",
                            "id": "#main/kraken2_files_to_folder/kraken2_database"
                        },
                        {
                            "source": "#main/skip_kraken2_filtered",
                            "id": "#main/kraken2_files_to_folder/skip_kraken2_filtered"
                        },
                        {
                            "source": "#main/skip_kraken2_unfiltered",
                            "id": "#main/kraken2_files_to_folder/skip_kraken2_unfiltered"
                        }
                    ],
                    "run": "#files_to_folder.cwl",
                    "out": [
                        "#main/kraken2_files_to_folder/results"
                    ],
                    "id": "#main/kraken2_files_to_folder"
                },
                {
                    "label": "Output fwd reads",
                    "doc": "Step for to output filtered reads because that is an option.",
                    "when": "$(!inputs.do_not_output_filtered_reads)",
                    "run": {
                        "class": "ExpressionTool",
                        "requirements": [
                            {
                                "class": "InlineJavascriptRequirement"
                            }
                        ],
                        "inputs": [
                            {
                                "type": "File",
                                "id": "#main/out_fwd_reads/run/fwd_in"
                            }
                        ],
                        "outputs": [
                            {
                                "type": "File",
                                "id": "#main/out_fwd_reads/run/fwd_out"
                            }
                        ],
                        "expression": "${ return {'fwd_out': inputs.fwd_in}; }\n"
                    },
                    "in": [
                        {
                            "source": "#main/do_not_output_filtered_reads",
                            "id": "#main/out_fwd_reads/do_not_output_filtered_reads"
                        },
                        {
                            "source": "#main/phix_filter/out_forward_reads",
                            "id": "#main/out_fwd_reads/fwd_in"
                        }
                    ],
                    "out": [
                        "#main/out_fwd_reads/fwd_out"
                    ],
                    "id": "#main/out_fwd_reads"
                },
                {
                    "label": "Output rev reads",
                    "doc": "Step to output filtered reads because that is an option.",
                    "when": "$(!inputs.do_not_output_filtered_reads)",
                    "run": {
                        "class": "ExpressionTool",
                        "requirements": [
                            {
                                "class": "InlineJavascriptRequirement"
                            }
                        ],
                        "inputs": [
                            {
                                "type": "File",
                                "id": "#main/out_rev_reads/run/rev_in"
                            }
                        ],
                        "outputs": [
                            {
                                "type": "File",
                                "id": "#main/out_rev_reads/run/rev_out"
                            }
                        ],
                        "expression": "${ return {'rev_out': inputs.rev_in}; }\n"
                    },
                    "in": [
                        {
                            "source": "#main/do_not_output_filtered_reads",
                            "id": "#main/out_rev_reads/do_not_output_filtered_reads"
                        },
                        {
                            "source": "#main/phix_filter/out_reverse_reads",
                            "id": "#main/out_rev_reads/rev_in"
                        }
                    ],
                    "out": [
                        "#main/out_rev_reads/rev_out"
                    ],
                    "id": "#main/out_rev_reads"
                },
                {
                    "label": "PhiX filter (bbduk)",
                    "doc": "Filters illumina spike-in PhiX sequences from reads using bbduk",
                    "run": "#bbduk_filter.cwl",
                    "in": [
                        {
                            "source": [
                                "#main/reference_filter_illumina/out_forward_reads",
                                "#main/rrna_filter/out_forward_reads",
                                "#main/fastp/out_forward_reads"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#main/phix_filter/forward_reads"
                        },
                        {
                            "source": "#main/identifier",
                            "valueFrom": "$(self+\"_illumina_filtered\")",
                            "id": "#main/phix_filter/identifier"
                        },
                        {
                            "source": "#main/memory",
                            "id": "#main/phix_filter/memory"
                        },
                        {
                            "valueFrom": "/opt/conda/opt/bbmap-39.06-0/resources/phix174_ill.ref.fa.gz",
                            "id": "#main/phix_filter/reference"
                        },
                        {
                            "source": [
                                "#main/reference_filter_illumina/out_reverse_reads",
                                "#main/rrna_filter/out_reverse_reads",
                                "#main/fastp/out_reverse_reads"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#main/phix_filter/reverse_reads"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/phix_filter/threads"
                        }
                    ],
                    "out": [
                        "#main/phix_filter/out_forward_reads",
                        "#main/phix_filter/out_reverse_reads",
                        "#main/phix_filter/summary",
                        "#main/phix_filter/stats_file"
                    ],
                    "id": "#main/phix_filter"
                },
                {
                    "label": "Prepare references",
                    "doc": "Prepare references to a single fasta file and unique headers",
                    "when": "$(inputs.prepare_reference && inputs.fasta_input !== null && inputs.fasta_input.length !== 0)",
                    "run": "#workflow_prepare_fasta_db.cwl",
                    "in": [
                        {
                            "source": "#main/filter_references",
                            "id": "#main/prepare_fasta_db/fasta_input"
                        },
                        {
                            "default": true,
                            "id": "#main/prepare_fasta_db/make_headers_unique"
                        },
                        {
                            "source": "#main/identifier",
                            "id": "#main/prepare_fasta_db/output_name"
                        },
                        {
                            "source": "#main/prepare_reference",
                            "id": "#main/prepare_fasta_db/prepare_reference"
                        }
                    ],
                    "out": [
                        "#main/prepare_fasta_db/fasta_db"
                    ],
                    "id": "#main/prepare_fasta_db"
                },
                {
                    "label": "Reference array to file",
                    "doc": "Array to file object when the reference does not need to be prepared",
                    "when": "$(inputs.prepare_reference == false && inputs.files.length !== 0)",
                    "run": "#array_to_file_tool.cwl",
                    "in": [
                        {
                            "source": "#main/filter_references",
                            "id": "#main/reference_array_to_file/files"
                        },
                        {
                            "source": "#main/prepare_reference",
                            "id": "#main/reference_array_to_file/prepare_reference"
                        }
                    ],
                    "out": [
                        "#main/reference_array_to_file/file"
                    ],
                    "id": "#main/reference_array_to_file"
                },
                {
                    "label": "Reference read mapping",
                    "doc": "Map reads against references using BBMap",
                    "when": "$(inputs.filter_references !== null && inputs.filter_references.length !== 0)",
                    "run": "#bbmap_filter-reads.cwl",
                    "in": [
                        {
                            "source": "#main/filter_references",
                            "id": "#main/reference_filter_illumina/filter_references"
                        },
                        {
                            "source": [
                                "#main/rrna_filter/out_forward_reads",
                                "#main/fastp/out_forward_reads"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#main/reference_filter_illumina/forward_reads"
                        },
                        {
                            "source": "#main/identifier",
                            "valueFrom": "$(self+\"_ref-filter\")",
                            "id": "#main/reference_filter_illumina/identifier"
                        },
                        {
                            "source": "#main/memory",
                            "id": "#main/reference_filter_illumina/memory"
                        },
                        {
                            "source": "#main/reference_array_to_file/file",
                            "id": "#main/reference_filter_illumina/non_prepared_reference"
                        },
                        {
                            "source": "#main/keep_reference_mapped_reads",
                            "id": "#main/reference_filter_illumina/output_mapped"
                        },
                        {
                            "source": "#main/prepare_fasta_db/fasta_db",
                            "id": "#main/reference_filter_illumina/prepared_reference"
                        },
                        {
                            "valueFrom": "${ var ref = null; if (inputs.prepared_reference) { ref = inputs.prepared_reference; } else if (inputs.non_prepared_reference) { ref = inputs.non_prepared_reference; } console.log(ref); return ref; }",
                            "id": "#main/reference_filter_illumina/reference"
                        },
                        {
                            "source": [
                                "#main/rrna_filter/out_reverse_reads",
                                "#main/fastp/out_reverse_reads"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#main/reference_filter_illumina/reverse_reads"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/reference_filter_illumina/threads"
                        }
                    ],
                    "out": [
                        "#main/reference_filter_illumina/out_forward_reads",
                        "#main/reference_filter_illumina/out_reverse_reads",
                        "#main/reference_filter_illumina/log",
                        "#main/reference_filter_illumina/stats",
                        "#main/reference_filter_illumina/covstats"
                    ],
                    "id": "#main/reference_filter_illumina"
                },
                {
                    "label": "Reports to folder",
                    "doc": "Preparation of fastp output files to a specific output folder",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "default": "Illumina_quality_reports",
                            "id": "#main/reports_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/fastqc_illumina_before/html_files",
                                "#main/fastqc_illumina_before/zip_files",
                                "#main/fastqc_illumina_after/html_files",
                                "#main/fastqc_illumina_after/zip_files",
                                "#main/fastp/html_report",
                                "#main/fastp/json_report",
                                "#main/reference_filter_illumina/stats",
                                "#main/reference_filter_illumina/covstats",
                                "#main/reference_filter_illumina/log",
                                "#main/phix_filter/summary",
                                "#main/phix_filter/stats_file",
                                "#main/rrna_filter/summary",
                                "#main/rrna_filter/stats_file"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/reports_files_to_folder/files"
                        }
                    ],
                    "out": [
                        "#main/reports_files_to_folder/results"
                    ],
                    "id": "#main/reports_files_to_folder"
                },
                {
                    "label": "rRNA filter (bbduk)",
                    "doc": "Filters rRNA sequences from reads using bbduk",
                    "when": "$(inputs.filter_rrna)",
                    "run": "#bbduk_filter.cwl",
                    "in": [
                        {
                            "source": "#main/filter_rrna",
                            "id": "#main/rrna_filter/filter_rrna"
                        },
                        {
                            "source": "#main/fastp/out_forward_reads",
                            "id": "#main/rrna_filter/forward_reads"
                        },
                        {
                            "source": "#main/identifier",
                            "valueFrom": "$(self+\"_rRNA-filter\")",
                            "id": "#main/rrna_filter/identifier"
                        },
                        {
                            "source": "#main/memory",
                            "id": "#main/rrna_filter/memory"
                        },
                        {
                            "valueFrom": "/opt/conda/opt/bbmap-39.06-0/resources/riboKmers.fa.gz",
                            "id": "#main/rrna_filter/reference"
                        },
                        {
                            "source": "#main/fastp/out_reverse_reads",
                            "id": "#main/rrna_filter/reverse_reads"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/rrna_filter/threads"
                        }
                    ],
                    "out": [
                        "#main/rrna_filter/out_forward_reads",
                        "#main/rrna_filter/out_reverse_reads",
                        "#main/rrna_filter/summary",
                        "#main/rrna_filter/stats_file"
                    ],
                    "id": "#main/rrna_filter"
                }
            ],
            "id": "#main",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-06-14",
            "https://schema.org/dateModified": "2024-05-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "ScatterFeatureRequirement"
                }
            ],
            "label": "Kracken2 + Bracken",
            "doc": "Run Kraken2 Analysis + Krona visualization followed by Bracken. Currently only on illumina reads.\n",
            "outputs": [
                {
                    "type": "Directory",
                    "label": "Bracken folder",
                    "doc": "Folder with Bracken output files",
                    "outputSource": "#workflow_kraken2-bracken.cwl/files_to_folder_bracken/results",
                    "id": "#workflow_kraken2-bracken.cwl/bracken_folder"
                },
                {
                    "type": "Directory",
                    "label": "Kraken2 folder",
                    "doc": "Folder with Kraken2 output files",
                    "outputSource": "#workflow_kraken2-bracken.cwl/files_to_folder_kraken/results",
                    "id": "#workflow_kraken2-bracken.cwl/kraken2_folder"
                }
            ],
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "string"
                    },
                    "label": "Bracken levels",
                    "doc": "Taxonomy levels in bracken estimate abundances on. Default runs through; [P,C,O,F,G,S]",
                    "default": [
                        "P",
                        "C",
                        "O",
                        "F",
                        "G",
                        "S"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/bracken_levels"
                },
                {
                    "type": "int",
                    "label": "Bracken reads threshold",
                    "doc": "Number of reads required PRIOR to abundance estimation to perform reestimation in bracken. Default 0",
                    "default": 0,
                    "id": "#workflow_kraken2-bracken.cwl/bracken_reads_threshold"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Output Destination",
                    "doc": "Optional output destination only used for cwl-prov reporting.",
                    "id": "#workflow_kraken2-bracken.cwl/destination"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#workflow_kraken2-bracken.cwl/identifier"
                },
                {
                    "type": "File",
                    "doc": "Forward sequence fastq file(s) locally",
                    "label": "Forward reads",
                    "loadListing": "no_listing",
                    "id": "#workflow_kraken2-bracken.cwl/illumina_forward_reads"
                },
                {
                    "type": "File",
                    "doc": "Reverse sequence fastq file(s) locally",
                    "label": "Reverse reads",
                    "loadListing": "no_listing",
                    "id": "#workflow_kraken2-bracken.cwl/illumina_reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Kraken2 confidence threshold",
                    "doc": "Confidence score threshold (default 0.0) must be between [0, 1]",
                    "id": "#workflow_kraken2-bracken.cwl/kraken2_confidence"
                },
                {
                    "type": "Directory",
                    "label": "Kraken2 database",
                    "doc": "Kraken2 database location",
                    "loadListing": "no_listing",
                    "id": "#workflow_kraken2-bracken.cwl/kraken2_database"
                },
                {
                    "type": "boolean",
                    "label": "Kraken2 standard report",
                    "doc": "Also output Kraken2 standard report with per read classification. These can be large. (default true)",
                    "default": true,
                    "id": "#workflow_kraken2-bracken.cwl/output_standard_report"
                },
                {
                    "type": "int",
                    "label": "Read length",
                    "doc": "Read length to use in bracken",
                    "id": "#workflow_kraken2-bracken.cwl/read_length"
                },
                {
                    "type": "boolean",
                    "label": "Run Bracken",
                    "doc": "Skip Bracken analysis. Default false.",
                    "default": false,
                    "id": "#workflow_kraken2-bracken.cwl/skip_bracken"
                },
                {
                    "label": "Input URLs used for this run",
                    "doc": "A provenance element to capture the original source of the input data",
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "string"
                        }
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/source"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Number of threads to use for computational processes",
                    "label": "Number of threads",
                    "default": 2,
                    "id": "#workflow_kraken2-bracken.cwl/threads"
                }
            ],
            "steps": [
                {
                    "label": "Bracken folder",
                    "doc": "Bracken files to single folder",
                    "in": [
                        {
                            "default": "Bracken_Illumina",
                            "id": "#workflow_kraken2-bracken.cwl/files_to_folder_bracken/destination"
                        },
                        {
                            "source": [
                                "#workflow_kraken2-bracken.cwl/illumina_bracken/output_report"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_kraken2-bracken.cwl/files_to_folder_bracken/files"
                        }
                    ],
                    "run": "#files_to_folder.cwl",
                    "out": [
                        "#workflow_kraken2-bracken.cwl/files_to_folder_bracken/results"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/files_to_folder_bracken"
                },
                {
                    "label": "Kraken2 folder",
                    "doc": "Kraken2 files to single folder",
                    "in": [
                        {
                            "default": "Kraken2_Illumina",
                            "id": "#workflow_kraken2-bracken.cwl/files_to_folder_kraken/destination"
                        },
                        {
                            "source": [
                                "#workflow_kraken2-bracken.cwl/illumina_kraken2/sample_report",
                                "#workflow_kraken2-bracken.cwl/illumina_kraken2/sample_report",
                                "#workflow_kraken2-bracken.cwl/kraken2krona_txt/krona_txt",
                                "#workflow_kraken2-bracken.cwl/krona/krona_html",
                                "#workflow_kraken2-bracken.cwl/kraken2_compress/outfile"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_kraken2-bracken.cwl/files_to_folder_kraken/files"
                        }
                    ],
                    "run": "#files_to_folder.cwl",
                    "out": [
                        "#workflow_kraken2-bracken.cwl/files_to_folder_kraken/results"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/files_to_folder_kraken"
                },
                {
                    "label": "Illumina bracken",
                    "doc": "Bracken runs on Illumina reads",
                    "run": "#bracken.cwl",
                    "when": "$(!inputs.skip_bracken)",
                    "scatter": "#workflow_kraken2-bracken.cwl/illumina_bracken/level",
                    "in": [
                        {
                            "source": "#workflow_kraken2-bracken.cwl/kraken2_database",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_bracken/database"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/identifier",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_bracken/identifier"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/illumina_kraken2/sample_report",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_bracken/kraken_report"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/bracken_levels",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_bracken/level"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/read_length",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_bracken/read_length"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/skip_bracken",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_bracken/skip_bracken"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/bracken_reads_threshold",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_bracken/threshold"
                        }
                    ],
                    "out": [
                        "#workflow_kraken2-bracken.cwl/illumina_bracken/output_report"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/illumina_bracken"
                },
                {
                    "label": "Kraken2",
                    "doc": "bla",
                    "run": "#kraken2.cwl",
                    "in": [
                        {
                            "source": "#workflow_kraken2-bracken.cwl/kraken2_confidence",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2/confidence"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/kraken2_database",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2/database"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/illumina_forward_reads",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2/forward_reads"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/identifier",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2/identifier"
                        },
                        {
                            "default": true,
                            "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2/paired_end"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/illumina_reverse_reads",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2/reverse_reads"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/threads",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2/threads"
                        }
                    ],
                    "out": [
                        "#workflow_kraken2-bracken.cwl/illumina_kraken2/sample_report",
                        "#workflow_kraken2-bracken.cwl/illumina_kraken2/standard_report"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2"
                },
                {
                    "label": "Compress kraken2",
                    "doc": "Compress large kraken2 report file",
                    "when": "$(inputs.kraken2_standard_report)",
                    "run": "#pigz.cwl",
                    "in": [
                        {
                            "source": "#workflow_kraken2-bracken.cwl/illumina_kraken2/standard_report",
                            "id": "#workflow_kraken2-bracken.cwl/kraken2_compress/inputfile"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/output_standard_report",
                            "id": "#workflow_kraken2-bracken.cwl/kraken2_compress/kraken2_standard_report"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/threads",
                            "id": "#workflow_kraken2-bracken.cwl/kraken2_compress/threads"
                        }
                    ],
                    "out": [
                        "#workflow_kraken2-bracken.cwl/kraken2_compress/outfile"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/kraken2_compress"
                },
                {
                    "label": "kraken2krona",
                    "doc": "Convert kraken2 report to krona format",
                    "run": "#kreport2krona.cwl",
                    "in": [
                        {
                            "source": "#workflow_kraken2-bracken.cwl/illumina_kraken2/sample_report",
                            "id": "#workflow_kraken2-bracken.cwl/kraken2krona_txt/report"
                        }
                    ],
                    "out": [
                        "#workflow_kraken2-bracken.cwl/kraken2krona_txt/krona_txt"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/kraken2krona_txt"
                },
                {
                    "label": "Krona",
                    "doc": "Krona visualization of Kraken2",
                    "run": "#krona_ktImportText.cwl",
                    "in": [
                        {
                            "source": "#workflow_kraken2-bracken.cwl/kraken2krona_txt/krona_txt",
                            "id": "#workflow_kraken2-bracken.cwl/krona/input"
                        }
                    ],
                    "out": [
                        "#workflow_kraken2-bracken.cwl/krona/krona_html"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/krona"
                }
            ],
            "id": "#workflow_kraken2-bracken.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2024-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                }
            ],
            "label": "Prepare (multiple) fasta files to one file.",
            "doc": "Prepare (multiple) fasta files to one file. \nWith option to make unique headers to avoid same fasta headers, which can break some tools.\n",
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "label": "Fasta input",
                    "doc": "Fasta file(s) to prepare",
                    "id": "#workflow_prepare_fasta_db.cwl/fasta_input"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Make headers unique",
                    "doc": "Make fasta headers unique avoiding same fasta headers, which can break some tools.",
                    "default": false,
                    "id": "#workflow_prepare_fasta_db.cwl/make_headers_unique"
                },
                {
                    "type": "string",
                    "doc": "Output name for this dataset used",
                    "label": "identifier used",
                    "id": "#workflow_prepare_fasta_db.cwl/output_name"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "label": "Prepared fasta file",
                    "doc": "Prepared fasta file",
                    "outputSource": [
                        "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/file",
                        "#workflow_prepare_fasta_db.cwl/merge_input/output",
                        "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/fasta_db"
                    ],
                    "pickValue": "first_non_null",
                    "id": "#workflow_prepare_fasta_db.cwl/fasta_db"
                }
            ],
            "steps": [
                {
                    "label": "Array to file",
                    "doc": "Pick first file of filter_reference when make_headers_unique input is false",
                    "when": "$(inputs.make_headers_unique === false && inputs.fasta_input.length === 1)",
                    "run": "#array_to_file_tool.cwl",
                    "in": [
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/fasta_input"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/files"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/make_headers_unique",
                            "id": "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/make_headers_unique"
                        }
                    ],
                    "out": [
                        "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/file"
                    ],
                    "id": "#workflow_prepare_fasta_db.cwl/fasta_array_to_file"
                },
                {
                    "label": "Merge reference files",
                    "doc": "Only merge input when make unique is false.",
                    "when": "$(inputs.make_headers_unique === false && inputs.fasta_input.length > 1)",
                    "run": "#concatenate.cwl",
                    "in": [
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/fasta_input"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/infiles"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/make_headers_unique",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/make_headers_unique"
                        },
                        {
                            "valueFrom": "$(inputs.output_name)_filter-reference_merged.fa",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/outname"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/output_name",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/output_name"
                        }
                    ],
                    "out": [
                        "#workflow_prepare_fasta_db.cwl/merge_input/output"
                    ],
                    "id": "#workflow_prepare_fasta_db.cwl/merge_input"
                },
                {
                    "label": "Prepare references",
                    "doc": "Prepare references to a single fasta file and unique headers",
                    "when": "$(inputs.make_headers_unique)",
                    "run": "#prepare_fasta_db.cwl",
                    "in": [
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/fasta_files"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/fasta_input"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/make_headers_unique",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/make_headers_unique"
                        },
                        {
                            "valueFrom": "$(inputs.output_name)_filter-reference_uniq.fa.gz",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/output_file_name"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/output_name",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/output_name"
                        }
                    ],
                    "out": [
                        "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/fasta_db"
                    ],
                    "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db"
                }
            ],
            "id": "#workflow_prepare_fasta_db.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2023-01-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        }
    ],
    "cwlVersion": "v1.2",
    "$namespaces": {
        "s": "https://schema.org/"
    }
}
