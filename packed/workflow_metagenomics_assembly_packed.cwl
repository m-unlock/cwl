{
    "$graph": [
        {
            "class": "CommandLineTool",
            "id": "#bakta.cwl",
            "label": "Bakta: rapid & standardized annotation of bacterial genomes, MAGs & plasmids",
            "doc": "The software and documentation can be found here:\nhttps://github.com/oschwengers/bakta\n\nNecessary database files can be found here:\nhttps://doi.org/10.5281/zenodo.4247252\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "ramMin": 4096,
                    "coresMin": 1,
                    "class": "ResourceRequirement"
                }
            ],
            "hints": [
                {
                    "class": "LoadListingRequirement",
                    "loadListing": "deep_listing"
                },
                {
                    "class": "NetworkAccess",
                    "networkAccess": true
                },
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/bakta:1.9.4-db-light5.1",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.9.4"
                            ],
                            "specs": [
                                "https://github.com/oschwengers/bakta",
                                "doi.org/10.1099/mgen.0.000685"
                            ],
                            "package": "bakta"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "arguments": [
                {
                    "prefix": "--db",
                    "valueFrom": "${\n  // check if custom profile directory is used\n  if (inputs.db){\n    return inputs.db.path\n  }\n  // else use default built-in bakta db-light database\n  else { \n    return \"/bakta-db_v5.1-light\";\n  }\n}\n"
                }
            ],
            "inputs": [
                {
                    "doc": "Genome assembly in Fasta format",
                    "id": "#bakta.cwl/bakta/fasta_file",
                    "inputBinding": {
                        "position": 100
                    },
                    "type": "File"
                },
                {
                    "doc": "Database path (default = build-in light database)",
                    "id": "#bakta.cwl/bakta/db",
                    "type": [
                        "null",
                        "Directory"
                    ]
                },
                {
                    "doc": "Minimum contig size (default = 1; 200 in compliant mode)",
                    "id": "#bakta.cwl/bakta/min_contig_length",
                    "inputBinding": {
                        "prefix": "--min-contig-length"
                    },
                    "type": [
                        "null",
                        "int"
                    ]
                },
                {
                    "doc": "Prefix for output files",
                    "id": "#bakta.cwl/bakta/prefix",
                    "inputBinding": {
                        "prefix": "--prefix"
                    },
                    "type": [
                        "null",
                        "string"
                    ]
                },
                {
                    "doc": "Output directory (default = current working directory)",
                    "id": "#bakta.cwl/bakta/output",
                    "inputBinding": {
                        "prefix": "--output"
                    },
                    "type": [
                        "null",
                        "Directory"
                    ]
                },
                {
                    "doc": "Force overwriting existing output folder (except for current working directory)",
                    "id": "#bakta.cwl/bakta/force",
                    "inputBinding": {
                        "prefix": "--force"
                    },
                    "default": true,
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Genus name",
                    "id": "#bakta.cwl/bakta/genus",
                    "inputBinding": {
                        "prefix": "--genus"
                    },
                    "type": [
                        "null",
                        "string"
                    ]
                },
                {
                    "doc": "Species name",
                    "id": "#bakta.cwl/bakta/species",
                    "inputBinding": {
                        "prefix": "--species"
                    },
                    "type": [
                        "null",
                        "string"
                    ]
                },
                {
                    "doc": "Strain name",
                    "id": "#bakta.cwl/bakta/strain",
                    "inputBinding": {
                        "prefix": "--strain"
                    },
                    "type": [
                        "null",
                        "string"
                    ]
                },
                {
                    "doc": "Plasmid name",
                    "id": "#bakta.cwl/bakta/plasmid",
                    "inputBinding": {
                        "prefix": "--plasmid"
                    },
                    "type": [
                        "null",
                        "string"
                    ]
                },
                {
                    "doc": "All sequences are complete replicons (chromosome/plasmid[s])",
                    "id": "#bakta.cwl/bakta/complete",
                    "inputBinding": {
                        "prefix": "--complete"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Prodigal training file for CDS prediction",
                    "id": "#bakta.cwl/bakta/prodigal_tf_file",
                    "inputBinding": {
                        "prefix": "--prodigal-tf"
                    },
                    "type": [
                        "null",
                        "File"
                    ]
                },
                {
                    "doc": "Translation table 11/4 (default = 11)",
                    "id": "#bakta.cwl/bakta/translation_table",
                    "inputBinding": {
                        "prefix": "--translation-table"
                    },
                    "type": [
                        "null",
                        "int"
                    ]
                },
                {
                    "doc": "Gram type (default = ?)",
                    "id": "#bakta.cwl/bakta/gram",
                    "inputBinding": {
                        "prefix": "--gram"
                    },
                    "type": [
                        "null",
                        "string"
                    ]
                },
                {
                    "doc": "Locus prefix (default = contig)",
                    "id": "#bakta.cwl/bakta/locus",
                    "inputBinding": {
                        "prefix": "--locus"
                    },
                    "type": [
                        "null",
                        "string"
                    ]
                },
                {
                    "doc": "Locus tag prefix (default = autogenerated)",
                    "id": "#bakta.cwl/bakta/locus_tag",
                    "inputBinding": {
                        "prefix": "--locus-tag"
                    },
                    "type": [
                        "null",
                        "string"
                    ]
                },
                {
                    "doc": "Keep original contig headers",
                    "id": "#bakta.cwl/bakta/keep_contig_headers",
                    "inputBinding": {
                        "prefix": "--keep-contig-headers"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Force Genbank/ENA/DDJB compliance",
                    "id": "#bakta.cwl/bakta/compliant",
                    "inputBinding": {
                        "prefix": "--compliant"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Replicon information table (tsv/csv)",
                    "id": "#bakta.cwl/bakta/replicons",
                    "inputBinding": {
                        "prefix": "--replicons"
                    },
                    "type": [
                        "null",
                        "File"
                    ]
                },
                {
                    "doc": "Genbank/GFF3 file of trusted regions for pre-detected feature coordinates",
                    "id": "#bakta.cwl/bakta/regions",
                    "inputBinding": {
                        "prefix": "--regions"
                    },
                    "type": [
                        "null",
                        "File"
                    ]
                },
                {
                    "doc": "Fasta file of trusted protein sequences for CDS annotation",
                    "id": "#bakta.cwl/bakta/proteins",
                    "inputBinding": {
                        "prefix": "--proteins"
                    },
                    "type": [
                        "null",
                        "File"
                    ]
                },
                {
                    "doc": "Run in metagenome mode",
                    "id": "#bakta.cwl/bakta/meta",
                    "inputBinding": {
                        "prefix": "--meta"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Skip tRNA detection & annotation",
                    "id": "#bakta.cwl/bakta/skip_tRNA",
                    "inputBinding": {
                        "prefix": "--skip-trna"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Skip tmRNA detection & annotation",
                    "id": "#bakta.cwl/bakta/skip_tmrna",
                    "inputBinding": {
                        "prefix": "--skip-tmrna"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Skip rRNA detection & annotation",
                    "id": "#bakta.cwl/bakta/skip_rrna",
                    "inputBinding": {
                        "prefix": "--skip-rrna"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Skip ncRNA detection & annotation",
                    "id": "#bakta.cwl/bakta/skip_ncrna",
                    "inputBinding": {
                        "prefix": "--skip-ncrna"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Skip ncRNA region detection & annotation",
                    "id": "#bakta.cwl/bakta/skip_ncrna_region",
                    "inputBinding": {
                        "prefix": "--skip-ncrna-region"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Skip CRISPR detection & annotation",
                    "id": "#bakta.cwl/bakta/skip_crispr",
                    "inputBinding": {
                        "prefix": "--skip-crispr"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Skip CDS detection & annotation",
                    "id": "#bakta.cwl/bakta/skip_cds",
                    "inputBinding": {
                        "prefix": "--skip-cds"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Skip Pseudogene detection & annotation",
                    "id": "#bakta.cwl/bakta/skip_pseudo",
                    "inputBinding": {
                        "prefix": "--skip-pseudo"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Skip sORF detection & annotation",
                    "id": "#bakta.cwl/bakta/skip_sorf",
                    "inputBinding": {
                        "prefix": "--skip-sorf"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Skip gap detection & annotation",
                    "id": "#bakta.cwl/bakta/skip_gap",
                    "inputBinding": {
                        "prefix": "--skip-gap"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Skip ori detection & annotation",
                    "id": "#bakta.cwl/bakta/skip_ori",
                    "inputBinding": {
                        "prefix": "--skip-ori"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Skip genome plotting",
                    "id": "#bakta.cwl/bakta/skip_plot",
                    "inputBinding": {
                        "prefix": "--skip-plot"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Print verbose information",
                    "id": "#bakta.cwl/bakta/verbose",
                    "inputBinding": {
                        "prefix": "--verbose"
                    },
                    "default": true,
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Run Bakta in debug mode",
                    "id": "#bakta.cwl/bakta/debug",
                    "inputBinding": {
                        "prefix": "--debug"
                    },
                    "type": [
                        "null",
                        "boolean"
                    ]
                },
                {
                    "doc": "Threads",
                    "id": "#bakta.cwl/bakta/threads",
                    "inputBinding": {
                        "prefix": "--threads"
                    },
                    "type": [
                        "null",
                        "int"
                    ]
                },
                {
                    "doc": "Directory for temporary files (default = system dependent auto detection)",
                    "id": "#bakta.cwl/bakta/tmp_dir",
                    "inputBinding": {
                        "prefix": "--tmp-dir"
                    },
                    "type": [
                        "null",
                        "Directory"
                    ]
                }
            ],
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0003-4216-2721",
                    "https://schema.org/email": "mailto:oliver.schwengers@computational.bio.uni-giessen.de",
                    "https://schema.org/name": "Oliver Schwengers"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/dateModified": "2024-07-08",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential",
            "outputs": [
                {
                    "doc": "Hypothetical CDS AA sequences as Fasta",
                    "id": "#bakta.cwl/bakta/hypo_sequences_cds",
                    "type": "File",
                    "format": "http://edamontology.org/format_2200",
                    "outputBinding": {
                        "glob": "*.hypotheticals.faa"
                    }
                },
                {
                    "doc": "Information on hypothetical CDS as TSV",
                    "id": "#bakta.cwl/bakta/hypo_annotation_tsv",
                    "type": "File",
                    "format": "http://edamontology.org/format_3475",
                    "outputBinding": {
                        "glob": "*.hypotheticals.tsv"
                    }
                },
                {
                    "doc": "Annotation as TSV",
                    "id": "#bakta.cwl/bakta/annotation_tsv",
                    "type": "File",
                    "format": "http://edamontology.org/format_3475",
                    "outputBinding": {
                        "glob": "${ if (inputs.prefix !== null) { return inputs.prefix + '.tsv'; } else{ return inputs.fasta_file.basename.replace(/\\.[^/.]+$/, '') + '.tsv'; } }"
                    }
                },
                {
                    "doc": "Annotation summary as txt",
                    "id": "#bakta.cwl/bakta/summary_txt",
                    "type": "File",
                    "format": "http://edamontology.org/format_2330",
                    "outputBinding": {
                        "glob": "*.txt"
                    }
                },
                {
                    "doc": "Annotation as JSON",
                    "id": "#bakta.cwl/bakta/annotation_json",
                    "type": "File",
                    "format": "http://edamontology.org/format_3464",
                    "outputBinding": {
                        "glob": "*.json"
                    }
                },
                {
                    "doc": "Annotation as GFF3",
                    "id": "#bakta.cwl/bakta/annotation_gff3",
                    "type": "File",
                    "format": "http://edamontology.org/format_1939",
                    "outputBinding": {
                        "glob": "*.gff3"
                    }
                },
                {
                    "doc": "Annotation as GenBank",
                    "id": "#bakta.cwl/bakta/annotation_gbff",
                    "type": "File",
                    "format": "http://edamontology.org/format_1936",
                    "outputBinding": {
                        "glob": "*.gbff"
                    }
                },
                {
                    "doc": "Annotation as EMBL",
                    "id": "#bakta.cwl/bakta/annotation_embl",
                    "type": "File",
                    "format": "http://edamontology.org/format_1927",
                    "outputBinding": {
                        "glob": "*.embl"
                    }
                },
                {
                    "doc": "Genome Sequences as Fasta",
                    "id": "#bakta.cwl/bakta/sequences_fna",
                    "type": "File",
                    "format": "http://edamontology.org/format_2200",
                    "outputBinding": {
                        "glob": "*.fna"
                    }
                },
                {
                    "doc": "Gene DNA sequences as Fasta",
                    "id": "#bakta.cwl/bakta/sequences_ffn",
                    "type": "File",
                    "format": "http://edamontology.org/format_2200",
                    "outputBinding": {
                        "glob": "*.ffn"
                    }
                },
                {
                    "doc": "CDS AA sequences as Fasta",
                    "id": "#bakta.cwl/bakta/sequences_cds",
                    "type": "File",
                    "format": "http://edamontology.org/format_2200",
                    "outputBinding": {
                        "glob": "${ if (inputs.prefix !== null) { return inputs.prefix + '.faa'; } else{ return inputs.fasta_file.basename.replace(/\\.[^/.]+$/, '') + '.faa'; } }"
                    }
                },
                {
                    "doc": "Circular genome plot as PNG",
                    "id": "#bakta.cwl/bakta/plot_png",
                    "type": [
                        "null",
                        "File"
                    ],
                    "format": "http://edamontology.org/format_3603",
                    "outputBinding": {
                        "glob": "*.png"
                    }
                },
                {
                    "doc": "Circular genome plot as SVG",
                    "id": "#bakta.cwl/bakta/plot_svg",
                    "type": [
                        "null",
                        "File"
                    ],
                    "format": "http://edamontology.org/format_3604",
                    "outputBinding": {
                        "glob": "*.svg"
                    }
                }
            ]
        },
        {
            "class": "CommandLineTool",
            "label": "Compress a directory (tar)",
            "hints": [
                {
                    "dockerPull": "debian:buster",
                    "class": "DockerRequirement"
                }
            ],
            "baseCommand": [
                "tar",
                "czfh"
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.indir.basename).tar.gz"
                },
                {
                    "valueFrom": "-C"
                },
                {
                    "valueFrom": "$(inputs.indir.path)/.."
                },
                {
                    "valueFrom": "$(inputs.indir.basename)"
                }
            ],
            "inputs": [
                {
                    "type": "Directory",
                    "id": "#compress_directory.cwl/indir"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.indir.basename).tar.gz"
                    },
                    "id": "#compress_directory.cwl/outfile"
                }
            ],
            "id": "#compress_directory.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2021-00-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Concatenate multiple files",
            "baseCommand": [
                "cat"
            ],
            "stdout": "$(inputs.outname)",
            "hints": [
                {
                    "dockerPull": "debian:buster",
                    "class": "DockerRequirement"
                }
            ],
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#concatenate.cwl/infiles"
                },
                {
                    "type": "string",
                    "id": "#concatenate.cwl/outname"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outname)"
                    },
                    "id": "#concatenate.cwl/output"
                }
            ],
            "id": "#concatenate.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2021-00-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "compress a file multithreaded with pigz",
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/pigz:2.8",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.8"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/pigz"
                            ],
                            "package": "pigz"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "pigz",
                "-c"
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.inputfile)"
                }
            ],
            "stdout": "$(inputs.inputfile.basename).gz",
            "inputs": [
                {
                    "type": "File",
                    "id": "#pigz.cwl/inputfile"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "-p"
                    },
                    "id": "#pigz.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.inputfile.basename).gz"
                    },
                    "id": "#pigz.cwl/outfile"
                }
            ],
            "id": "#pigz.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Filter from reads",
            "doc": "Filter reads using BBmaps bbduk tool (paired-end only)\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/bbmap:39.06",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "39.06"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/bbmap"
                            ],
                            "package": "bbmap"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "bbduk.sh"
            ],
            "inputs": [
                {
                    "type": "File",
                    "inputBinding": {
                        "prefix": "in=",
                        "separate": false
                    },
                    "id": "#bbduk_filter.cwl/forward_reads"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#bbduk_filter.cwl/identifier"
                },
                {
                    "type": "int",
                    "inputBinding": {
                        "prefix": "k=",
                        "separate": false
                    },
                    "default": 31,
                    "id": "#bbduk_filter.cwl/kmersize"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 8000,
                    "id": "#bbduk_filter.cwl/memory"
                },
                {
                    "doc": "Reference contamination fasta file (can be compressed)",
                    "label": "Reference contamination file",
                    "type": [
                        "null",
                        "string"
                    ],
                    "inputBinding": {
                        "prefix": "ref=",
                        "separate": false
                    },
                    "id": "#bbduk_filter.cwl/reference"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "prefix": "in2=",
                        "separate": false
                    },
                    "id": "#bbduk_filter.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "threads=",
                        "separate": false
                    },
                    "id": "#bbduk_filter.cwl/threads"
                }
            ],
            "stderr": "$(inputs.identifier)_bbduk-summary.txt",
            "arguments": [
                {
                    "prefix": "-Xmx",
                    "separate": false,
                    "valueFrom": "$(inputs.memory)M"
                },
                {
                    "prefix": "out=",
                    "separate": false,
                    "valueFrom": "$(inputs.identifier)_1.fq.gz"
                },
                {
                    "prefix": "out2=",
                    "separate": false,
                    "valueFrom": "$(inputs.identifier)_2.fq.gz"
                },
                {
                    "prefix": "stats=",
                    "separate": false,
                    "valueFrom": "$(inputs.identifier)_bbduk-stats.txt"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_1.fq.gz"
                    },
                    "id": "#bbduk_filter.cwl/out_forward_reads"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_2.fq.gz"
                    },
                    "id": "#bbduk_filter.cwl/out_reverse_reads"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_bbduk-stats.txt"
                    },
                    "id": "#bbduk_filter.cwl/stats_file"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_bbduk-summary.txt"
                    },
                    "id": "#bbduk_filter.cwl/summary"
                }
            ],
            "id": "#bbduk_filter.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/dateModified": "2023-02-10",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/bbmap:39.06",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "39.06"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/bbmap"
                            ],
                            "package": "bbmap"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "label": "BBMap",
            "doc": "Read filtering using BBMap against a (contamination) reference genome\n",
            "baseCommand": [
                "bbmap.sh"
            ],
            "arguments": [
                "-Xmx$(inputs.memory)M",
                "printunmappedcount",
                "overwrite=true",
                "statsfile=$(inputs.identifier)_BBMap_stats.txt",
                "covstats=$(inputs.identifier)_BBMap_covstats.txt",
                "out=$(inputs.identifier)_BBMap.sam"
            ],
            "inputs": [
                {
                    "type": "boolean",
                    "label": "fast mode",
                    "doc": "Sets other BBMap paramters to run faster, at reduced sensitivity. Bad for RNA-seq.",
                    "inputBinding": {
                        "prefix": "fast=t"
                    },
                    "default": true,
                    "id": "#bbmap.cwl/fast"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "position": 1,
                        "prefix": "in=",
                        "separate": false
                    },
                    "id": "#bbmap.cwl/forward_reads"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#bbmap.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "maximum memory usage in megabytes",
                    "label": "memory usage (mb)",
                    "default": 8000,
                    "id": "#bbmap.cwl/memory"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "position": 3,
                        "prefix": "ref=",
                        "separate": false
                    },
                    "id": "#bbmap.cwl/reference"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "inputBinding": {
                        "position": 2,
                        "prefix": "in2=",
                        "separate": false
                    },
                    "id": "#bbmap.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "number of threads to use for computational processes",
                    "label": "number of threads",
                    "inputBinding": {
                        "prefix": "threads=",
                        "separate": false
                    },
                    "default": 2,
                    "id": "#bbmap.cwl/threads"
                }
            ],
            "stderr": "$(inputs.identifier)_BBMap_log.txt",
            "outputs": [
                {
                    "label": "Coverage per contig",
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_BBMap_covstats.txt"
                    },
                    "id": "#bbmap.cwl/covstats"
                },
                {
                    "label": "BBMap log output",
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_BBMap_log.txt"
                    },
                    "id": "#bbmap.cwl/log"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_BBMap.sam"
                    },
                    "id": "#bbmap.cwl/sam"
                },
                {
                    "label": "Mapping statistics",
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_BBMap_stats.txt"
                    },
                    "id": "#bbmap.cwl/stats"
                }
            ],
            "id": "#bbmap.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/dateModified": "2022-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/bbmap:39.06",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "39.06"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/bbmap"
                            ],
                            "package": "bbmap"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "label": "BBMap",
            "doc": "Read filtering using BBMap against a (contamination) reference genome\n",
            "inputs": [
                {
                    "type": "File",
                    "inputBinding": {
                        "position": 1,
                        "prefix": "in=",
                        "separate": false
                    },
                    "id": "#bbmap_filter-reads.cwl/forward_reads"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#bbmap_filter-reads.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "maximum memory usage in megabytes",
                    "label": "memory usage (mb)",
                    "default": 8000,
                    "id": "#bbmap_filter-reads.cwl/memory"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": false,
                    "id": "#bbmap_filter-reads.cwl/output_mapped"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "position": 3,
                        "prefix": "ref=",
                        "separate": false
                    },
                    "id": "#bbmap_filter-reads.cwl/reference"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "inputBinding": {
                        "position": 2,
                        "prefix": "in2=",
                        "separate": false
                    },
                    "id": "#bbmap_filter-reads.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "number of threads to use for computational processes",
                    "label": "number of threads",
                    "inputBinding": {
                        "prefix": "threads=",
                        "separate": false
                    },
                    "default": 2,
                    "id": "#bbmap_filter-reads.cwl/threads"
                }
            ],
            "stderr": "$(inputs.identifier)_BBMap_log.txt",
            "outputs": [
                {
                    "label": "Coverage per contig",
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_BBMap_covstats.txt"
                    },
                    "id": "#bbmap_filter-reads.cwl/covstats"
                },
                {
                    "label": "BBMap log output",
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_BBMap_log.txt"
                    },
                    "id": "#bbmap_filter-reads.cwl/log"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_filtered_1.fq.gz"
                    },
                    "id": "#bbmap_filter-reads.cwl/out_forward_reads"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_filtered_2.fq.gz"
                    },
                    "id": "#bbmap_filter-reads.cwl/out_reverse_reads"
                },
                {
                    "label": "Mapping statistics",
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_BBMap_stats.txt"
                    },
                    "id": "#bbmap_filter-reads.cwl/stats"
                }
            ],
            "baseCommand": [
                "bbmap.sh"
            ],
            "arguments": [
                "-Xmx$(inputs.memory)M",
                "printunmappedcount",
                "overwrite=true",
                "statsfile=$(inputs.identifier)_BBMap_stats.txt",
                "covstats=$(inputs.identifier)_BBMap_covstats.txt",
                "${\n  if (inputs.output_mapped){\n    return 'outm1='+inputs.identifier+'_filtered_1.fq.gz \\\n            outm2='+inputs.identifier+'_filtered_2.fq.gz';\n  } else {\n    return 'outu1='+inputs.identifier+'_filtered_1.fq.gz \\\n            outu2='+inputs.identifier+'_filtered_2.fq.gz';\n  }\n}\n"
            ],
            "id": "#bbmap_filter-reads.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/dateModified": "2022-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Prepare fasta DB",
            "doc": "Prepares fasta file for so it does not contain duplicate fasta headers.\nOnly looks at the first part of the header before any whitespace.\nAdds and incremental number in the header.\n\nExpects fasta file(s) or plaintext fasta(s). Not mixed!    \n",
            "requirements": [
                {
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "prepare_fasta_db",
                            "writable": true
                        },
                        {
                            "entryname": "script.sh",
                            "entry": "#!/bin/bash\necho -e \"\\\n#/usr/bin/python3\nimport sys\\n\\\nheaders = set()\\n\\\nc = 0\\n\\\nfor line in sys.stdin:\\n\\\n  splitline = line.split()\\n\\\n  if line[0] == '>':    \\n\\\n    if splitline[0] in headers:\\n\\\n      c += 1\\n\\\n      print(splitline[0]+'.x'+str(c)+' '+' '.join(splitline[1:]))\\n\\\n    else:\\n\\\n      print(line.strip())\\n\\\n    headers.add(splitline[0])\\n\\\n  else:\\n\\\n    print(line.strip())\" > ./dup.py\nout_name=$1\nshift\n\n# python container does not have the command 'file' anymore\n# if file $@ | grep gzip; then\nif [[ $@ == *.gz ]]; then\n  zcat $@ | python3 ./dup.py | gzip > $out_name\nelse\n  cat $@ | python3 ./dup.py | gzip > $out_name\nfi"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "python:3.10-bookworm",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "3.10.6"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/python"
                            ],
                            "package": "python3"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "bash",
                "script.sh"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "label": "fasta files",
                    "doc": "Fasta file(s) to be the prepared. Can also be gzipped (not mixed!)",
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#prepare_fasta_db.cwl/fasta_files"
                },
                {
                    "type": "string",
                    "label": "Output outfile",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#prepare_fasta_db.cwl/output_file_name"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_file_name)"
                    },
                    "id": "#prepare_fasta_db.cwl/fasta_db"
                }
            ],
            "id": "#prepare_fasta_db.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-07-00",
            "https://schema.org/dateModified": "2023-01-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "BinSpreader",
            "doc": "BinSPreader is a tool to refine metagenome-assembled genomes (MAGs) obtained from existing tools. BinSPreader exploits the assembly graph topology and other connectivity information, such as paired-end and Hi-C reads, to refine the existing binning, correct binning errors, propagate binning from longer contigs to shorter contigs and infer contigs belonging to multiple bins.",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "networkAccess": true,
                    "class": "NetworkAccess"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/binspreader:3.16.0.dev--h95f258a_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "3.16.0.dev"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/binspreader",
                                "file:///home/bart/git/cwl/tools/binspreader/doi.org/10.1016/j.isci.2022.104770"
                            ],
                            "package": "binspreader"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "bin-refine"
            ],
            "inputs": [
                {
                    "type": "File",
                    "doc": "Input assembly graph from assembler outputs in GFA format",
                    "label": "assembly graph",
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#binspreader.cwl/assembly_graph"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Load binary-converted reads from tmpdir",
                    "label": "Load binary-converted reads from tmpdir",
                    "inputBinding": {
                        "prefix": "--bin-load",
                        "position": 24
                    },
                    "id": "#binspreader.cwl/bin_load"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "doc": "Reads bin weight threshold (default 0.1)",
                    "label": "Reads bin weight threshold",
                    "inputBinding": {
                        "prefix": "-b",
                        "position": 23
                    },
                    "id": "#binspreader.cwl/bin_weight"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "doc": "simple maximum or maximum likelihood binning assignment strategy (default -Smax) or -Smle",
                    "label": "Binning assignment strategy",
                    "default": "-Smax",
                    "inputBinding": {
                        "position": 10
                    },
                    "id": "#binspreader.cwl/binning_assignment_strategy"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "use CAMI bioboxes binning format",
                    "label": "Use CAMI bioboxes binning format",
                    "inputBinding": {
                        "prefix": "--cami",
                        "position": 12
                    },
                    "id": "#binspreader.cwl/cami_binning_format"
                },
                {
                    "type": "File",
                    "doc": "Contig-to-bin mapping tsv file",
                    "label": "Contig-to-bin mapping file",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#binspreader.cwl/contig2bin"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "doc": "convergence relative tolerance threshold (default 1e-5)",
                    "label": "Convergence relative tolerance threshold",
                    "inputBinding": {
                        "prefix": "-e",
                        "position": 7
                    },
                    "id": "#binspreader.cwl/convergence"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "Dataset in YAML format describing Hi-C and paired-end reads",
                    "label": "Dataset in YAML format",
                    "inputBinding": {
                        "prefix": "--dataset",
                        "position": 4
                    },
                    "id": "#binspreader.cwl/dataset"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "produce lots of debug data, set to true by default here",
                    "label": "Produce lots of debug data",
                    "inputBinding": {
                        "prefix": "--debug",
                        "position": 25
                    },
                    "default": true,
                    "id": "#binspreader.cwl/debug"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Binning will not be propagated further than bound from initially binned edges",
                    "label": "Binning will not be propagated further than bound from initially binned edges",
                    "inputBinding": {
                        "prefix": "-db",
                        "position": 21
                    },
                    "id": "#binspreader.cwl/distance_bound"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "emit zero bin for unbinned sequences",
                    "label": "Emit zero bin for unbinned sequences",
                    "inputBinding": {
                        "prefix": "--zero-bin",
                        "position": 13
                    },
                    "id": "#binspreader.cwl/emit_zero_bin"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "estimate pairwise bin distance (could be slow on large graphs!)",
                    "label": "Estimate pairwise bin distance",
                    "inputBinding": {
                        "prefix": "--bin-dist",
                        "position": 15
                    },
                    "id": "#binspreader.cwl/estimate_pairwise_bin_distance"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#binspreader.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "maximum number of iterations (default 5000)",
                    "label": "Maximum number of iterations",
                    "inputBinding": {
                        "prefix": "-n",
                        "position": 8
                    },
                    "id": "#binspreader.cwl/iterations"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "doc": "labels correction regularization parameter for labeled data (default 0.6)",
                    "label": "Labels correction regularization parameter",
                    "inputBinding": {
                        "prefix": "-la",
                        "position": 16
                    },
                    "id": "#binspreader.cwl/labels_correction_regularization_parameter"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Binning will not be propagated to edges longer than threshold",
                    "label": "Binning will not be propagated to edges longer than threshold",
                    "inputBinding": {
                        "prefix": "-lt",
                        "position": 20
                    },
                    "id": "#binspreader.cwl/length_threshold"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Library index (0-based, default 0). Only the library specified by this index will be used.",
                    "label": "Library index",
                    "inputBinding": {
                        "prefix": "-l",
                        "position": 5
                    },
                    "id": "#binspreader.cwl/library_index"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "doc": "Regularization parameter for sparse propagation procedure. Increase/decrease for more agressive/conservative refining (default 0.6)",
                    "label": "Regularization parameter for sparse propagation procedure",
                    "inputBinding": {
                        "prefix": "-ma",
                        "position": 19
                    },
                    "id": "#binspreader.cwl/metaalpha"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "allow multiple bin assignment (default false)",
                    "label": "Allow multiple bin assignment",
                    "inputBinding": {
                        "prefix": "-m",
                        "position": 9
                    },
                    "id": "#binspreader.cwl/multiple_bin_assignment"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Do not create a special bin for unbinned contigs. More agressive strategy.",
                    "label": "Do not create a special bin for unbinned contigs",
                    "inputBinding": {
                        "prefix": "--no-unbinned-bin",
                        "position": 18
                    },
                    "id": "#binspreader.cwl/no_unbinned_bin"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "provide contigs paths from file separately from GFA. We recommend using scaffolds for metagenome binning and further analysis. If there is a specific demand to use contigs.fasta, their paths though the assembly graph should be additionally specified.",
                    "label": "contigs paths",
                    "inputBinding": {
                        "prefix": "--paths",
                        "position": 3
                    },
                    "id": "#binspreader.cwl/paths"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "doc": "Select propagation or correction mode (default Rcorr) or Rprop",
                    "label": "Propagation or correction mode",
                    "default": "-Rcorr",
                    "inputBinding": {
                        "position": 11
                    },
                    "id": "#binspreader.cwl/propagation_correction_mode"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "doc": "scratch directory to use",
                    "label": "Scratch directory",
                    "inputBinding": {
                        "prefix": "--tmp-dir",
                        "position": 26
                    },
                    "default": "/tmp/binspreader",
                    "id": "#binspreader.cwl/scratch_directory"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Gradually reduce regularization parameter from binned to unbinned edges. Recommended for sparse binnings with low assembly fraction.",
                    "label": "Sparse propagation",
                    "inputBinding": {
                        "prefix": "--sparse-propagation",
                        "position": 17
                    },
                    "id": "#binspreader.cwl/sparse_propagation"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Split reads according to binning. Can be used for reassembly.",
                    "label": "Split reads according to binning",
                    "inputBinding": {
                        "prefix": "-r",
                        "position": 22
                    },
                    "id": "#binspreader.cwl/split_reads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "use tall table for multiple binning result",
                    "label": "Use tall table for multiple binning result",
                    "inputBinding": {
                        "prefix": "--tall-multi",
                        "position": 14
                    },
                    "id": "#binspreader.cwl/tall_table"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "number of threads to use (default 1/2 of available threads)",
                    "label": "Number of threads",
                    "inputBinding": {
                        "prefix": "-t",
                        "position": 6
                    },
                    "id": "#binspreader.cwl/threads"
                }
            ],
            "arguments": [
                {
                    "valueFrom": "BinSpreader",
                    "position": 2
                }
            ],
            "outputs": [
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "Refined bin distance matrix (if --bin-dist was used)",
                    "label": "Refined bin distance matrix",
                    "outputBinding": {
                        "glob": "BinSpreader/bin_dist.tsv"
                    },
                    "id": "#binspreader.cwl/bin_dist"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "Read set for bin labeled by bin_label (if --reads was used)",
                    "label": "Read set for bin labeled by bin_label",
                    "outputBinding": {
                        "glob": "BinSpreader/bin_label_1.fastq"
                    },
                    "id": "#binspreader.cwl/bin_label_1"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "Read set for bin labeled by bin_label (if --reads was used)",
                    "label": "Read set for bin labeled by bin_label",
                    "outputBinding": {
                        "glob": "BinSpreader/bin_label_2.fastq"
                    },
                    "id": "#binspreader.cwl/bin_label_2"
                },
                {
                    "type": "File",
                    "doc": "Various per-bin statistics",
                    "label": "Various per-bin statistics",
                    "outputBinding": {
                        "glob": "BinSpreader/bin_stats.tsv"
                    },
                    "id": "#binspreader.cwl/bin_stats"
                },
                {
                    "type": "File",
                    "doc": "Soft bin weights per contig",
                    "label": "Soft bin weights per contig",
                    "outputBinding": {
                        "glob": "BinSpreader/bin_weights.tsv"
                    },
                    "id": "#binspreader.cwl/bin_weights"
                },
                {
                    "type": "File",
                    "doc": "Soft bin weights per edge",
                    "label": "Soft bin weights per edge",
                    "outputBinding": {
                        "glob": "BinSpreader/edge_weights.tsv"
                    },
                    "id": "#binspreader.cwl/edge_weights"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "List of graph links between assembly graph edges with weights (if --debug was used)",
                    "label": "List of graph links between assembly graph edges with weights",
                    "outputBinding": {
                        "glob": "BinSpreader/graph_links.tsv"
                    },
                    "id": "#binspreader.cwl/graph_links"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "List of paired-end links between assembly graph edges with weights (if --debug was used)",
                    "label": "List of paired-end links between assembly graph edges with weights",
                    "outputBinding": {
                        "glob": "BinSpreader/pe_links.tsv"
                    },
                    "id": "#binspreader.cwl/pe_links"
                },
                {
                    "type": "File",
                    "doc": "Refined binning in .tsv format",
                    "label": "Refined binning",
                    "outputBinding": {
                        "glob": "BinSpreader/binning.tsv"
                    },
                    "id": "#binspreader.cwl/refined_contig2bin"
                }
            ],
            "id": "#binspreader.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2024-07-04",
            "https://schema.org/dateModified": "2024-07-04",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Bracken",
            "doc": "Bayesian Reestimation of Abundance with KrakEN.\nBracken is a highly accurate statistical method that computes the abundance of species in DNA sequences from a metagenomics sample.\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "InitialWorkDirRequirement",
                    "listing": [
                        {
                            "entry": "$(inputs.kraken_report)",
                            "writable": true
                        }
                    ]
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/bracken:2.9--py39h1f90b4d_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.9"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/bracken",
                                "file:///home/bart/git/cwl/tools/bracken/doi.org/10.7717/peerj-cs.104"
                            ],
                            "package": "bracken"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "bracken"
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.identifier+\"_\"+inputs.database.path.split( '/' ).pop()+\"_bracken_\"+inputs.level+\".txt\")",
                    "prefix": "-o"
                }
            ],
            "inputs": [
                {
                    "type": "Directory",
                    "label": "Database",
                    "doc": "Database location of kraken2",
                    "inputBinding": {
                        "prefix": "-d"
                    },
                    "id": "#bracken.cwl/database"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset",
                    "label": "identifier used",
                    "id": "#bracken.cwl/identifier"
                },
                {
                    "type": "File",
                    "label": "Kraken report",
                    "doc": "Kraken REPORT file to use for abundance estimation",
                    "inputBinding": {
                        "prefix": "-i"
                    },
                    "id": "#bracken.cwl/kraken_report"
                },
                {
                    "type": "string",
                    "label": "Level",
                    "doc": "Level to estimate abundance at. option [D,P,C,O,F,G,S,S1]. Default Species; 'S'",
                    "inputBinding": {
                        "prefix": "-l"
                    },
                    "default": "S",
                    "id": "#bracken.cwl/level"
                },
                {
                    "type": "int",
                    "label": "Read length",
                    "doc": "Read length to get all classification. Default 100",
                    "inputBinding": {
                        "prefix": "-r"
                    },
                    "default": 100,
                    "id": "#bracken.cwl/read_length"
                },
                {
                    "type": "int",
                    "label": "threshold",
                    "doc": "Number of reads required PRIOR to abundance estimation to perform reestimation. Default 0",
                    "inputBinding": {
                        "prefix": "-t"
                    },
                    "default": 0,
                    "id": "#bracken.cwl/threshold"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier+\"_\"+inputs.database.path.split( '/' ).pop()+\"_bracken_\"+inputs.level+\".txt\")"
                    },
                    "id": "#bracken.cwl/output_report"
                }
            ],
            "id": "#bracken.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2024-04-15",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "BUSCO",
            "doc": "Based on evolutionarily-informed expectations of gene content of near-universal single-copy orthologs, \nBUSCO metric is complementary to technical metrics like N50.\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "networkAccess": "$(inputs.busco_data !== undefined)",
                    "class": "NetworkAccess"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/busco:5.7.0--pyhdfd78af_1",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "5.7.0"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/busco",
                                "file:///home/bart/git/cwl/tools/busco/doi.org/10.1093/molbev/msab199"
                            ],
                            "package": "busco"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "busco"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Auto-lineage detection",
                    "doc": "Run auto-lineage to find optimum lineage path",
                    "inputBinding": {
                        "prefix": "--auto-lineage"
                    },
                    "id": "#busco.cwl/auto-lineage"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Eukaryote auto-lineage detection",
                    "doc": "Run auto-placement just on eukaryote tree to find optimum lineage path.",
                    "inputBinding": {
                        "prefix": "--auto-lineage-euk"
                    },
                    "id": "#busco.cwl/auto-lineage-euk"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Prokaryote auto-lineage detection",
                    "doc": "Run auto-lineage just on non-eukaryote trees to find optimum lineage path.",
                    "inputBinding": {
                        "prefix": "--auto-lineage-prok"
                    },
                    "id": "#busco.cwl/auto-lineage-prok"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "Dataset location",
                    "doc": "This assumes --offline mode. Specify local filepath for finding BUSCO dataset downloads",
                    "inputBinding": {
                        "prefix": "--download_path"
                    },
                    "id": "#busco.cwl/busco_data"
                },
                {
                    "type": "string",
                    "label": "Name of the output file",
                    "doc": "Give your analysis run a recognisable short name. Output folders and files will be labelled with this name.",
                    "inputBinding": {
                        "prefix": "--out"
                    },
                    "id": "#busco.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Lineage",
                    "doc": "Specify the name of the BUSCO lineage to be used.",
                    "inputBinding": {
                        "prefix": "--lineage_dataset"
                    },
                    "id": "#busco.cwl/lineage"
                },
                {
                    "type": "string",
                    "label": "Input molecule type",
                    "doc": "Specify which BUSCO analysis mode to run.\nThere are three valid modes:\n- geno or genome, for genome assemblies (DNA)\n- tran or transcriptome, for transcriptome assemblies (DNA)\n- prot or proteins, for annotated gene sets (protein)\n",
                    "inputBinding": {
                        "prefix": "--mode"
                    },
                    "id": "#busco.cwl/mode"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Input fasta file",
                    "doc": "Input sequence file in FASTA format. Can be an assembled genome or transcriptome (DNA), or protein sequences from an annotated gene set. Also possible to use a path to a directory containing multiple input files.",
                    "inputBinding": {
                        "prefix": "--in"
                    },
                    "id": "#busco.cwl/sequence_file"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "Input folder",
                    "doc": "Input folder with sequence files in FASTA format. Can be an assembled genome or transcriptome (DNA), or protein sequences from an annotated gene set. Also possible to use a path to a directory containing multiple input files.",
                    "inputBinding": {
                        "prefix": "--in"
                    },
                    "id": "#busco.cwl/sequence_folder"
                },
                {
                    "type": "boolean",
                    "label": "Skip BBTools",
                    "doc": "Skip BBTools steps",
                    "default": false,
                    "inputBinding": {
                        "prefix": "--skip_bbtools"
                    },
                    "id": "#busco.cwl/skip_bbtools"
                },
                {
                    "type": "boolean",
                    "label": "Compress output",
                    "doc": "Compress some subdirectories with many files to save space",
                    "default": true,
                    "inputBinding": {
                        "prefix": "--tar"
                    },
                    "id": "#busco.cwl/tar_output"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Number of threads",
                    "default": 1,
                    "inputBinding": {
                        "prefix": "--cpu"
                    },
                    "id": "#busco.cwl/threads"
                }
            ],
            "arguments": [
                "${\n  if (inputs.busco_data){\n    return '--offline';\n  } else {\n    return null;\n  }\n}\n"
            ],
            "outputs": [
                {
                    "label": "Batch summary",
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "Summary file when input is multiple files",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)/batch_summary.txt"
                    },
                    "id": "#busco.cwl/batch_summary"
                },
                {
                    "label": "BUSCO logs folder",
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.identifier)/logs"
                    },
                    "id": "#busco.cwl/logs"
                },
                {
                    "label": "BUSCO short summary files",
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.identifier)/short_summary.*"
                    },
                    "id": "#busco.cwl/short_summaries"
                }
            ],
            "id": "#busco.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0002-5516-8391",
                    "https://schema.org/email": "mailto:german.royvalgarcia@wur.nl",
                    "https://schema.org/name": "Germ\u00e1n Royval"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-01-01",
            "https://schema.org/dateModified": "2022-02-28",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "baseCommand": [
                "bash",
                "-x",
                "script.sh"
            ],
            "label": "CarveMe GEMstats",
            "doc": "Small summary of a list of CarveMe genome-scale metabolic models in sbml-fbc2 format\nContains; number of mets,reactions and genes\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "InitialWorkDirRequirement",
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "gemstats_output",
                            "writable": true
                        },
                        {
                            "entryname": "script.sh",
                            "entry": "#!/bin/bash\nidentifier=$1\nshift;\necho \"Model Mets Reactions Genes\" > $identifier\\_CarveMe_GEMstats.tsv\nfor file in \"$@\"\ndo\n  bash /unlock/infrastructure/scripts/GEMstats.sh $file\ndone >> $identifier\\_CarveMe_GEMstats.tsv"
                        }
                    ]
                }
            ],
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "label": "CarveMe GEMs",
                    "doc": "List of CarveMe metabolic models in sbml-fbc2 format.",
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#GEMstats.cwl/carveme_gems"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#GEMstats.cwl/identifier"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_CarveMe_GEMstats.tsv"
                    },
                    "id": "#GEMstats.cwl/carveme_GEMstats"
                }
            ],
            "id": "#GEMstats.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2022-01-01",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "CarveMe",
            "doc": "CarveMe is a python-based tool for genome-scale metabolic model reconstruction.\n(Workflow will finish as successful even though no model can be created. Check messages!)\n    \n",
            "requirements": [
                {
                    "listing": [
                        "$(inputs.protein_file)"
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/carveme:1.6.1",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.6.1"
                            ],
                            "specs": [
                                "https://pypi.org/project/carveme",
                                "file:///home/bart/git/cwl/tools/carveme/doi.org/10.1093/nar/gky537"
                            ],
                            "package": "python3"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "carve"
            ],
            "outputs": [
                {
                    "label": "CarveMe GEM",
                    "doc": "CarveMe metabolic model Output SBML in sbml-fbc2 format",
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.protein_file.nameroot).GEM.xml"
                    },
                    "id": "#carveme.cwl/carveme_gem"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "carveme_solver",
                    "doc": "Solver to use for optimization, by default cplex which is properly configured in the docker image. Alternatively, you can use the open source SCIP solver. This will be slower (expect at least 10 min per execution).",
                    "inputBinding": {
                        "prefix": "--solver"
                    },
                    "id": "#carveme.cwl/carveme_solver"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Gap fill",
                    "doc": "Gap fill model for given media",
                    "inputBinding": {
                        "prefix": "--gapfill"
                    },
                    "id": "#carveme.cwl/gapfill"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Initial media",
                    "doc": "Initialize model with given medium",
                    "inputBinding": {
                        "prefix": "--init"
                    },
                    "id": "#carveme.cwl/init"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Media database",
                    "doc": "Media database file",
                    "inputBinding": {
                        "prefix": "--mediadb"
                    },
                    "id": "#carveme.cwl/mediadb"
                },
                {
                    "type": "File",
                    "label": "Input fasta file",
                    "doc": "Proteins sequence file in FASTA format.",
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#carveme.cwl/protein_file"
                }
            ],
            "arguments": [
                "--fbc2",
                {
                    "prefix": "--output",
                    "valueFrom": "$(inputs.protein_file.nameroot).GEM.xml"
                }
            ],
            "id": "#carveme.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-06-00",
            "https://schema.org/dateModified": "2023-02-20",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "CheckM",
            "doc": "CheckM provides a set of tools for assessing the quality of genomes recovered from isolates, single cells, or metagenomes\n",
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/checkm-genome:1.2.2",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.2.2"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/checkm-genome",
                                "file:///home/bart/git/cwl/tools/checkm/doi.org/10.1101/gr.186072.114"
                            ],
                            "package": "checkm-genome"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "requirements": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/checkm-genome:1.2.2",
                    "class": "DockerRequirement"
                },
                {
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "/checkm_data",
                            "writable": true
                        },
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "checkm_output",
                            "writable": true
                        },
                        {
                            "entryname": "script.sh",
                            "entry": "# !/bin/bash\nexport CHECKM_DATA_PATH=/venv/checkm_data\ncheckm lineage_wf $@"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "arguments": [
                {
                    "position": 51,
                    "valueFrom": "--reduced_tree"
                },
                {
                    "position": 52,
                    "prefix": "-f",
                    "valueFrom": "$(inputs.identifier)_CheckM_report.txt"
                },
                {
                    "position": 54,
                    "valueFrom": "$(inputs.identifier)_checkm"
                }
            ],
            "baseCommand": [
                "bash",
                "-x",
                "script.sh"
            ],
            "inputs": [
                {
                    "type": "Directory",
                    "doc": "folder containing bins in fasta format from metagenomic binning",
                    "label": "bins folder",
                    "inputBinding": {
                        "position": 53
                    },
                    "id": "#checkm_lineagewf.cwl/bin_dir"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "fasta file extension",
                    "inputBinding": {
                        "position": 5,
                        "prefix": "-x"
                    },
                    "default": "fa",
                    "id": "#checkm_lineagewf.cwl/fasta_extension"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#checkm_lineagewf.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Number of threads to use",
                    "default": 8,
                    "inputBinding": {
                        "position": 4,
                        "prefix": "-t"
                    },
                    "id": "#checkm_lineagewf.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_checkm"
                    },
                    "id": "#checkm_lineagewf.cwl/checkm_out_folder"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_CheckM_report.txt"
                    },
                    "id": "#checkm_lineagewf.cwl/checkm_out_table"
                }
            ],
            "id": "#checkm_lineagewf.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-07-1",
            "https://schema.org/dateModified": "2024-07-04",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "DAS Tool",
            "doc": "Recovery of genomes from metagenomes via a dereplication, aggregation and scoring strategy.",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/das_tool:1.1.5",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.1.5"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/das_tool",
                                "file:///home/bart/git/cwl/tools/das_tool/doi.org/10.1038/s41564-018-0171-1"
                            ],
                            "package": "dastool"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "DAS_Tool"
            ],
            "inputs": [
                {
                    "type": "File",
                    "doc": "Input assembly in fasta format",
                    "label": "Input assembly",
                    "inputBinding": {
                        "prefix": "--contigs"
                    },
                    "id": "#das_tool.cwl/assembly"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "doc": "Comma separated list of tab separated contigs to bin tables.",
                    "label": "Bin-Contig tables",
                    "inputBinding": {
                        "itemSeparator": ",",
                        "prefix": "--bins"
                    },
                    "id": "#das_tool.cwl/bin_tables"
                },
                {
                    "type": "string",
                    "doc": "Comma separated list of the binning prediction tool names.",
                    "label": "Binner labels",
                    "inputBinding": {
                        "prefix": "--labels"
                    },
                    "id": "#das_tool.cwl/binner_labels"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#das_tool.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Number of threads to use",
                    "default": 1,
                    "inputBinding": {
                        "position": 0,
                        "prefix": "--threads"
                    },
                    "id": "#das_tool.cwl/threads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Export bins as fasta files.",
                    "label": "Write bins",
                    "inputBinding": {
                        "prefix": "--write_bins"
                    },
                    "default": true,
                    "id": "#das_tool.cwl/write_bins"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Export unbinned contigs as fasta file",
                    "label": "Write unbinned",
                    "inputBinding": {
                        "prefix": "--write_unbinned"
                    },
                    "default": true,
                    "id": "#das_tool.cwl/write_unbinned"
                }
            ],
            "arguments": [
                {
                    "prefix": "-o",
                    "valueFrom": "$(inputs.identifier)"
                }
            ],
            "outputs": [
                {
                    "type": "Directory",
                    "label": "Bins",
                    "doc": "Bin fasta files.",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_DASTool_bins"
                    },
                    "id": "#das_tool.cwl/bin_dir"
                },
                {
                    "type": "File",
                    "label": "Contig to bin",
                    "doc": "Contigs to bin file table",
                    "outputBinding": {
                        "glob": "*_DASTool_contig2bin.tsv"
                    },
                    "id": "#das_tool.cwl/contig2bin"
                },
                {
                    "type": "File",
                    "label": "Log",
                    "doc": "DASTool log file",
                    "outputBinding": {
                        "glob": "*_DASTool.log"
                    },
                    "id": "#das_tool.cwl/log"
                },
                {
                    "type": "File",
                    "label": "DAS Tool run summary",
                    "doc": "Summary",
                    "outputBinding": {
                        "glob": "*_DASTool_summary.tsv"
                    },
                    "id": "#das_tool.cwl/summary"
                }
            ],
            "id": "#das_tool.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2022-09-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Fasta_to_Scaffolds2Bin",
            "doc": "Converts genome bins in fasta format to scaffolds-to-bin table. (DAS Tool helper script)",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/das_tool:1.1.4",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.1.4"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/das_tool",
                                "file:///home/bart/git/cwl/tools/das_tool/doi.org/10.1038/s41564-018-0171-1"
                            ],
                            "package": "dastool"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "Fasta_to_Contig2Bin.sh"
            ],
            "inputs": [
                {
                    "type": "Directory",
                    "doc": "Input assembly in fasta format",
                    "label": "Input assembly",
                    "inputBinding": {
                        "prefix": "--input_folder"
                    },
                    "id": "#fasta_to_contig2bin.cwl/bin_folder"
                },
                {
                    "type": "string",
                    "doc": "Binner name used to create the bins",
                    "label": "Binner name",
                    "id": "#fasta_to_contig2bin.cwl/binner_name"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "doc": "Extension of fasta files. (default fasta)",
                    "label": "Fasta extension",
                    "inputBinding": {
                        "prefix": "--extension"
                    },
                    "id": "#fasta_to_contig2bin.cwl/extension"
                }
            ],
            "stdout": "$(inputs.binner_name)_Contig2Bin.tsv",
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.binner_name)_Contig2Bin.tsv"
                    },
                    "id": "#fasta_to_contig2bin.cwl/table"
                }
            ],
            "id": "#fasta_to_contig2bin.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2022-09-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "eggNOG",
            "doc": "eggNOG is a public resource that provides Orthologous Groups (OGs)\nof proteins at different taxonomic levels, each with integrated and summarized functional annotations.\n",
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/eggnog-mapper:2.1.12--pyhdfd78af_0",
                    "class": "DockerRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.1.12"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/eggnog-mapper",
                                "https://doi.org/10.1101/2021.06.03.446934"
                            ],
                            "package": "eggnog"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "emapper.py"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--cpu"
                    },
                    "default": 4,
                    "id": "#eggnog-mapper.cwl/cpu"
                },
                {
                    "type": "boolean",
                    "inputBinding": {
                        "prefix": "--dbmem"
                    },
                    "doc": "Use this option to allocate the whole eggnog.db DB in memory. Database will be unloaded after execution. (default false)",
                    "default": false,
                    "id": "#eggnog-mapper.cwl/dbmem"
                },
                {
                    "type": [
                        {
                            "type": "record",
                            "name": "#eggnog-mapper.cwl/eggnog_dbs/eggnog",
                            "fields": [
                                {
                                    "type": "Directory",
                                    "inputBinding": {
                                        "prefix": "--data_dir"
                                    },
                                    "doc": "Directory containing all data files for the eggNOG database.",
                                    "name": "#eggnog-mapper.cwl/eggnog_dbs/eggnog/data_dir"
                                },
                                {
                                    "type": "File",
                                    "inputBinding": {
                                        "prefix": "--database"
                                    },
                                    "doc": "eggNOG database file",
                                    "name": "#eggnog-mapper.cwl/eggnog_dbs/eggnog/db"
                                },
                                {
                                    "type": "File",
                                    "inputBinding": {
                                        "prefix": "--dmnd_db"
                                    },
                                    "doc": "eggNOG database file for diamond blast search",
                                    "name": "#eggnog-mapper.cwl/eggnog_dbs/eggnog/diamond_db"
                                }
                            ]
                        }
                    ],
                    "id": "#eggnog-mapper.cwl/eggnog_dbs"
                },
                {
                    "type": [
                        {
                            "type": "enum",
                            "symbols": [
                                "#eggnog-mapper.cwl/go_evidence/experimental",
                                "#eggnog-mapper.cwl/go_evidence/non-electronic",
                                "#eggnog-mapper.cwl/go_evidence/all"
                            ],
                            "inputBinding": {
                                "prefix": "--go_evidence"
                            }
                        }
                    ],
                    "default": "non-electronic",
                    "doc": "Defines what type of GO terms should be used for annotation. \nexperimental = Use only terms inferred from experimental evidence. \nnon-electronic = Use only non-electronically curated terms.\nDefault non-electronic.\n",
                    "id": "#eggnog-mapper.cwl/go_evidence"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "separate": true,
                        "prefix": "-i"
                    },
                    "label": "Input FASTA file containing protein sequences",
                    "id": "#eggnog-mapper.cwl/input_fasta"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "enum",
                            "symbols": [
                                "#eggnog-mapper.cwl/mode/diamond",
                                "#eggnog-mapper.cwl/mode/hmmer"
                            ],
                            "inputBinding": {
                                "prefix": "-m"
                            }
                        }
                    ],
                    "default": "diamond",
                    "doc": "Run with hmmer or diamond. Default diamond",
                    "id": "#eggnog-mapper.cwl/mode"
                }
            ],
            "arguments": [
                {
                    "position": 2,
                    "prefix": "--annotate_hits_table",
                    "valueFrom": "$(inputs.input_fasta.nameroot)"
                },
                {
                    "position": 3,
                    "prefix": "-o",
                    "valueFrom": "$(inputs.input_fasta.nameroot)"
                }
            ],
            "outputs": [
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "format": "http://edamontology.org/format_3475",
                    "outputBinding": {
                        "glob": "$(inputs.input_fasta.nameroot)*annotations*"
                    },
                    "id": "#eggnog-mapper.cwl/output_annotations"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "format": "http://edamontology.org/format_3475",
                    "outputBinding": {
                        "glob": "$(inputs.input_fasta.nameroot)*orthologs*"
                    },
                    "id": "#eggnog-mapper.cwl/output_orthologs"
                }
            ],
            "id": "#eggnog-mapper.cwl",
            "http://schema.org/author": [
                {
                    "class": "http://schema.org/Person",
                    "http://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "http://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "http://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "http://schema.org/Person",
                    "http://schema.org/author": "Ekaterina Sakharova"
                },
                {
                    "class": "http://schema.org/Person",
                    "http://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "http://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "http://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "http://schema.org/Person",
                    "http://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "http://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "http://schema.org/name": "Bart Nijsse"
                }
            ],
            "http://schema.org/copyrightHolder": "EMBL - European Bioinformatics Institute",
            "http://schema.org/license": "https://www.apache.org/licenses/LICENSE-2.0",
            "http://schema.org/dateCreated": "2019-06-14",
            "http://schema.org/dateModified": "2024-10-03"
        },
        {
            "class": "CommandLineTool",
            "label": "EukRep",
            "doc": "EukRep, Classification of Eukaryotic and Prokaryotic sequences from metagenomic datasets",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/eukrep:0.6.7",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "0.6.7"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/eukrep",
                                "file:///home/bart/git/cwl/tools/eukrep/doi.org/10.1101/171355"
                            ],
                            "package": "diamond"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "EukRep"
            ],
            "inputs": [
                {
                    "type": "File",
                    "doc": "Input assembly in fasta format",
                    "label": "Input assembly",
                    "inputBinding": {
                        "prefix": "-i"
                    },
                    "id": "#eukrep.cwl/assembly"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#eukrep.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Minumum contig length",
                    "doc": "Minimum sequence length cutoff for sequences to be included in prediction. Default is 3kb",
                    "id": "#eukrep.cwl/min_contig_size"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "doc": "{strict,balanced,lenient} Default is balanced.\nHow stringent the algorithm is in identifying eukaryotic scaffolds. Strict has a lower false positive rate and true positive rate; vice verso for leneient.\n",
                    "label": "Algorithm stringency",
                    "inputBinding": {
                        "prefix": "-m"
                    },
                    "id": "#eukrep.cwl/stringency"
                }
            ],
            "arguments": [
                {
                    "prefix": "-o",
                    "valueFrom": "$(inputs.identifier)_EukRep.fasta"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_EukRep.fasta"
                    },
                    "id": "#eukrep.cwl/euk_fasta_out"
                }
            ],
            "id": "#eukrep.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2022-06-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "label": "Convert an array of 1 file to a file object",
            "doc": "Converts the array and returns the first file in the array. \nShould only be used when 1 file is in the array.\n",
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "id": "#array_to_file_tool.cwl/files"
                }
            ],
            "baseCommand": [
                "mv"
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.files[0].path)",
                    "position": 1
                },
                {
                    "valueFrom": "./",
                    "position": 2
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.files[0].basename)"
                    },
                    "id": "#array_to_file_tool.cwl/file"
                }
            ],
            "id": "#array_to_file_tool.cwl"
        },
        {
            "class": "ExpressionTool",
            "label": "File from folder regex",
            "doc": "Expression to filter a single file from a directory. Return nothing with no hit.\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "loadListing": "shallow_listing",
                    "class": "LoadListingRequirement"
                }
            ],
            "inputs": [
                {
                    "label": "Input folder",
                    "doc": "Folder with only files",
                    "type": "Directory",
                    "id": "#file_from_folder_regex.cwl/folder"
                },
                {
                    "label": "Output name",
                    "doc": "Rename output file.",
                    "type": [
                        "null",
                        "string"
                    ],
                    "id": "#file_from_folder_regex.cwl/output_file_name"
                },
                {
                    "type": "string",
                    "label": "Regex (JS)",
                    "doc": "JavaScript regular expression to be used on the filenames\nfor example: \".*unbinned.*\"\n",
                    "id": "#file_from_folder_regex.cwl/regex"
                }
            ],
            "expression": "${\n  var regex = new RegExp(inputs.regex);\n  var outfile;\n  for (var i = 0; i < inputs.folder.listing.length; i++) {\n      if (regex.test(inputs.folder.listing[i].location)){\n          outfile = inputs.folder.listing[i]; \n          if (inputs.output_file_name) {\n              outfile.basename = inputs.output_file_name\n          }\n      }\n  }\n  if (outfile){\n      return { \"out_file\": outfile };\n  } else {\n      return { \"out_file\": null };\n  }    \n}\n",
            "outputs": [
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "id": "#file_from_folder_regex.cwl/out_file"
                }
            ],
            "id": "#file_from_folder_regex.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2024-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "ExpressionTool",
            "doc": "Transforms the input files to a mentioned directory\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "string",
                    "id": "#files_to_folder.cwl/destination"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "id": "#files_to_folder.cwl/files"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "Directory"
                        }
                    ],
                    "id": "#files_to_folder.cwl/folders"
                }
            ],
            "expression": "${\n  var array = []\n  if (inputs.files != null) {\n    array = array.concat(inputs.files)\n  }\n  if (inputs.folders != null) {\n    array = array.concat(inputs.folders)\n  }\n  var r = {\n     'results':\n       { \"class\": \"Directory\",\n         \"basename\": inputs.destination,\n         \"listing\": array\n       } \n     };\n   return r; \n }\n",
            "outputs": [
                {
                    "type": "Directory",
                    "id": "#files_to_folder.cwl/results"
                }
            ],
            "id": "#files_to_folder.cwl",
            "http://schema.org/citation": "https://m-unlock.nl",
            "http://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "http://schema.org/dateModified": "2024-10-07",
            "http://schema.org/dateCreated": "2020-00-00",
            "http://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "http://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "ExpressionTool",
            "doc": "Expression to filter files (by name) in a directory using a regular expression.\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "loadListing": "shallow_listing",
                    "class": "LoadListingRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "boolean",
                    "label": "Reverse",
                    "doc": "Exclude files with regex. (default false)",
                    "default": false,
                    "id": "#folder_file_regex.cwl/exclude"
                },
                {
                    "label": "Input folder",
                    "doc": "Folder with only files",
                    "type": "Directory",
                    "id": "#folder_file_regex.cwl/folder"
                },
                {
                    "type": "boolean",
                    "label": "Output as folder",
                    "doc": "Output files in folder when true. (default false)",
                    "default": false,
                    "id": "#folder_file_regex.cwl/output_as_folder"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Output folder name",
                    "doc": "Output folder name. When output folder is true. (default 'filtered')",
                    "default": "filtered",
                    "id": "#folder_file_regex.cwl/output_folder_name"
                },
                {
                    "label": "Regex (JS)",
                    "doc": "JavaScript regular expression to be used on the filenames\nMetaBAT2 example: \"bin\\.[0-9]+\\.fa\"\n",
                    "type": "string",
                    "id": "#folder_file_regex.cwl/regex"
                }
            ],
            "expression": "${\n  var regex = new RegExp(inputs.regex);\n  var array = [];\n  if (inputs.exclude == false) {\n    for (var i = 0; i < inputs.folder.listing.length; i++) {\n      if (regex.test(inputs.folder.listing[i].location)){\n        array = array.concat(inputs.folder.listing[i]);\n      }\n    }\n  } else {\n    for (var i = 0; i < inputs.folder.listing.length; i++) {\n      if (!regex.test(inputs.folder.listing[i].location)){\n        array = array.concat(inputs.folder.listing[i]);\n      }  \n    }    \n  }\n  if (inputs.output_as_folder) {\n        var r = {\n     'output_folder':\n       { \"class\": \"Directory\",\n         \"basename\": inputs.output_folder_name,\n         \"listing\": array\n       }\n     };\n  } else {\n    r = array;\n  }\n   return r;\n}\n",
            "outputs": [
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "id": "#folder_file_regex.cwl/output_files"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "id": "#folder_file_regex.cwl/output_folder"
                }
            ],
            "id": "#folder_file_regex.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-10-00",
            "https://schema.org/dateModified": "2022-10-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "ExpressionTool",
            "doc": "Transforms the input folder to an array of files\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "loadListing": "shallow_listing",
                    "class": "LoadListingRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "Directory",
                    "id": "#folder_to_files.cwl/folder"
                }
            ],
            "expression": "${\n  var files = [];\n  for (var i = 0; i < inputs.folder.listing.length; i++) {\n    files.push(inputs.folder.listing[i]);\n  }\n  return {\"files\": files};\n}  \n",
            "outputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "id": "#folder_to_files.cwl/files"
                }
            ],
            "id": "#folder_to_files.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2022-10-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "ExpressionTool",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "label": "Merge file arrays",
            "doc": "Merges arrays of files in an array to a array of files\n",
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": {
                            "type": "array",
                            "items": "File"
                        }
                    },
                    "id": "#merge_file_arrays.cwl/input"
                }
            ],
            "outputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "id": "#merge_file_arrays.cwl/output"
                }
            ],
            "expression": "${\n  var output = [];\n  for (var i = 0; i < inputs.input.length; i++) {\n    var readgroup_array = inputs.input[i];\n    for (var j = 0; j < readgroup_array.length; j++) {\n      var readgroup = readgroup_array[j];\n      output.push(readgroup);\n    }\n  }\n  return {'output': output}\n}",
            "id": "#merge_file_arrays.cwl"
        },
        {
            "class": "CommandLineTool",
            "doc": "Modified from https://github.com/ambarishK/bio-cwl-tools/blob/release/fastp/fastp.cwl\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/fastp:0.23.4--hadf994f_3",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "0.23.4"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/fastp",
                                "file:///home/bart/git/cwl/tools/fastp/doi.org/10.1002/imt2.107"
                            ],
                            "package": "fastp"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": true,
                    "inputBinding": {
                        "prefix": "--correction"
                    },
                    "id": "#fastp.cwl/base_correction"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": false,
                    "inputBinding": {
                        "prefix": "--dedup"
                    },
                    "id": "#fastp.cwl/deduplicate"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": true,
                    "inputBinding": {
                        "prefix": "--disable_trim_poly_g"
                    },
                    "id": "#fastp.cwl/disable_trim_poly_g"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--trim_poly_g"
                    },
                    "id": "#fastp.cwl/force_polyg_tail_trimming"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "prefix": "--in1"
                    },
                    "id": "#fastp.cwl/forward_reads"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#fastp.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": false,
                    "inputBinding": {
                        "prefix": "--merge"
                    },
                    "id": "#fastp.cwl/merge_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 50,
                    "inputBinding": {
                        "prefix": "--length_required"
                    },
                    "id": "#fastp.cwl/min_length_required"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 20,
                    "inputBinding": {
                        "prefix": "--qualified_quality_phred"
                    },
                    "id": "#fastp.cwl/qualified_phred_quality"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "prefix": "--in2"
                    },
                    "id": "#fastp.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "--thread"
                    },
                    "id": "#fastp.cwl/threads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 20,
                    "inputBinding": {
                        "prefix": "--unqualified_percent_limit"
                    },
                    "id": "#fastp.cwl/unqualified_phred_quality"
                }
            ],
            "arguments": [
                {
                    "prefix": "--out1",
                    "valueFrom": "$(inputs.identifier)_fastp_1.fq.gz"
                },
                "${\n  if (inputs.reverse_reads){\n    return '--out2';\n  } else {\n    return '';\n  }\n}\n",
                "${\n  if (inputs.reverse_reads){\n    return inputs.identifier + \"_fastp_2.fq.gz\";\n  } else {\n    return '';\n  }\n}\n",
                "${\n  if (inputs.reverse_reads_path){\n    return '--out2';\n  } else {\n    return '';\n  }\n}\n",
                "${\n  if (inputs.reverse_reads_path){\n    return inputs.identifier + \"_fastp_2.fq.gz\";\n  } else {\n    return '';\n  }\n}\n",
                "${\n  if (inputs.merge_reads){\n    return '--merged_out';\n  } else {\n    return '';\n  }\n}\n",
                "${\n  if (inputs.merge_reads){\n    return inputs.identifier + \"merged_fastp.fq.gz\";\n  } else {\n    return '';\n  }\n}\n",
                {
                    "prefix": "-h",
                    "valueFrom": "$(inputs.identifier)_fastp.html"
                },
                {
                    "prefix": "-j",
                    "valueFrom": "$(inputs.identifier)_fastp.json"
                }
            ],
            "baseCommand": [
                "fastp"
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_fastp.html"
                    },
                    "id": "#fastp.cwl/html_report"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_fastp.json"
                    },
                    "id": "#fastp.cwl/json_report"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_merged_fastp.fq.gz"
                    },
                    "id": "#fastp.cwl/merged_reads"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_fastp_1.fq.gz"
                    },
                    "id": "#fastp.cwl/out_forward_reads"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_fastp_2.fq.gz"
                    },
                    "id": "#fastp.cwl/out_reverse_reads"
                }
            ],
            "id": "#fastp.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/dateModified": "2022-02-22",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "baseCommand": [
                "fastqc"
            ],
            "label": "FASTQC",
            "doc": "Performs quality control on FASTQ files\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "InitialWorkDirRequirement",
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "FASTQC",
                            "writable": true
                        }
                    ]
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/fastqc:0.12.1--hdfd78af_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "0.12.1"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/fastqc"
                            ],
                            "package": "fastqc"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "arguments": [
                "--outdir",
                "FASTQC"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "doc": "FastQ file list",
                    "label": "FASTQ file list",
                    "inputBinding": {
                        "position": 100
                    },
                    "id": "#fastqc.cwl/fastq"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "FastQ files list",
                    "label": "FASTQ files list",
                    "inputBinding": {
                        "position": 101,
                        "prefix": "--nano"
                    },
                    "id": "#fastqc.cwl/nanopore_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "--threads"
                    },
                    "id": "#fastqc.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "outputBinding": {
                        "glob": "FASTQC/*.html"
                    },
                    "id": "#fastqc.cwl/html_files"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "outputBinding": {
                        "glob": "FASTQC/*.zip"
                    },
                    "id": "#fastqc.cwl/zip_files"
                }
            ],
            "id": "#fastqc.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0002-5516-8391",
                    "https://schema.org/email": "mailto:german.royvalgarcia@wur.nl",
                    "https://schema.org/name": "Germ\u00e1n Royval"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2021-11-26",
            "https://schema.org/dateModified": "2022-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Filtlong",
            "doc": "Filtlong is a tool for filtering long reads by quality. It can take a set of long reads and produce a smaller, better subset. \nIt uses both read length (longer is better) and read identity (higher is better) when choosing which reads pass the filter.\n",
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/filtlong:0.2.1--hd03093a_1",
                    "class": "DockerRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "filtlong",
                            "writable": true
                        },
                        {
                            "entryname": "script.sh",
                            "entry": "#!/bin/bash\noutname=$1\nlongreads=$2\nshift;shift;\nfiltlong $longreads $@ 2> >(tee -a $outname.filtlong.log>&2) | gzip > $outname.fastq.gz"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "baseCommand": [
                "bash",
                "-x",
                "script.sh"
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_filename).filtlong.log"
                    },
                    "id": "#filtlong.cwl/log"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_filename).fastq.gz"
                    },
                    "id": "#filtlong.cwl/output_reads"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Reference assembly",
                    "doc": "Reference assembly in FASTA format",
                    "inputBinding": {
                        "prefix": "--assembly",
                        "position": 13
                    },
                    "id": "#filtlong.cwl/assembly"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Forward reads",
                    "doc": "Forward reference Illumina reads in FASTQ format",
                    "inputBinding": {
                        "prefix": "-illumina_1",
                        "position": 11
                    },
                    "id": "#filtlong.cwl/forward_reads"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Keep percentage",
                    "doc": "Keep only this percentage of the best reads (measured by bases)",
                    "inputBinding": {
                        "prefix": "--keep_percent",
                        "position": 4
                    },
                    "id": "#filtlong.cwl/keep_percent"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Length weight",
                    "doc": "Weight given to the length score (default; 1)",
                    "inputBinding": {
                        "prefix": "--length_weight",
                        "position": 14
                    },
                    "id": "#filtlong.cwl/length_weight"
                },
                {
                    "type": "File",
                    "label": "Long reads",
                    "doc": "Long reads in fastq format",
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#filtlong.cwl/long_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Maximum length",
                    "doc": "Maximum read length threshold",
                    "inputBinding": {
                        "prefix": "--max_length",
                        "position": 6
                    },
                    "id": "#filtlong.cwl/maximum_length"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Mean quality weight",
                    "doc": "Weight given to the mean quality score (default; 1)",
                    "inputBinding": {
                        "prefix": "--mean_q_weight",
                        "position": 15
                    },
                    "id": "#filtlong.cwl/mean_q_weight"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Minimum mean quality",
                    "doc": "Minimum mean quality threshold",
                    "inputBinding": {
                        "prefix": "--min_mean_q",
                        "position": 7
                    },
                    "id": "#filtlong.cwl/min_mean_q"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Minimum window quality",
                    "doc": "Minimum window quality threshold",
                    "inputBinding": {
                        "prefix": "--min_window_q",
                        "position": 8
                    },
                    "id": "#filtlong.cwl/min_window_q"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Minimum length",
                    "doc": "Minimum read length threshold",
                    "inputBinding": {
                        "prefix": "--min_length",
                        "position": 5
                    },
                    "id": "#filtlong.cwl/minimum_length"
                },
                {
                    "type": "string",
                    "label": "Output filename",
                    "doc": "Output filename (fastq.gz will be added by default)",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#filtlong.cwl/output_filename"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Reverse reads",
                    "doc": "Reverse reference Illumina reads in FASTQ format",
                    "inputBinding": {
                        "prefix": "-illumina_2",
                        "position": 12
                    },
                    "id": "#filtlong.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Split",
                    "doc": "Split reads at this many (or more) consecutive non-k-mer-matching bases",
                    "inputBinding": {
                        "prefix": "--trim",
                        "position": 10
                    },
                    "id": "#filtlong.cwl/split"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Target bases",
                    "doc": "Keep only the best reads up to this many total bases",
                    "inputBinding": {
                        "prefix": "--target_bases",
                        "position": 3
                    },
                    "id": "#filtlong.cwl/target_bases"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Trim",
                    "doc": "Trim non-k-mer-matching bases from start/end of reads",
                    "inputBinding": {
                        "prefix": "--trim",
                        "position": 9
                    },
                    "id": "#filtlong.cwl/trim"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Mean window weight",
                    "doc": "Weight given to the window quality score (default; 1)",
                    "inputBinding": {
                        "prefix": "--window_q_weight",
                        "position": 16
                    },
                    "id": "#filtlong.cwl/window_q_weight"
                }
            ],
            "id": "#filtlong.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2023-01-03",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/flye:2.9.3--py310h2b6aa90_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.9.3"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/flye",
                                "file:///home/bart/git/cwl/tools/flye/doi.org/10.1038/s41592-020-00971-x"
                            ],
                            "package": "flye"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "flye"
            ],
            "label": "Flye",
            "doc": "Flye De novo assembler for single molecule sequencing reads, with a focus in Oxford Nanopore Technologies reads",
            "inputs": [
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Debug mode",
                    "doc": "Set to true to display debug output while running",
                    "default": false,
                    "inputBinding": {
                        "prefix": "--debug"
                    },
                    "id": "#flye.cwl/debug_mode"
                },
                {
                    "type": "boolean",
                    "label": "deterministic",
                    "doc": "Perform disjointig assembly single-threaded. Default false",
                    "inputBinding": {
                        "prefix": "--deterministic"
                    },
                    "id": "#flye.cwl/deterministic"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Genome size",
                    "doc": "Estimated genome size (for example, 5m or 2.6g)",
                    "inputBinding": {
                        "prefix": "--genome-size"
                    },
                    "id": "#flye.cwl/genome_size"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Metagenome",
                    "doc": "Set to true if assembling a metagenome",
                    "default": false,
                    "inputBinding": {
                        "prefix": "--meta"
                    },
                    "id": "#flye.cwl/metagenome"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "ONT corrected",
                    "doc": "ONT reads in FASTQ format that were corrected with other methods (<3% error)",
                    "inputBinding": {
                        "prefix": "--nano-corr"
                    },
                    "id": "#flye.cwl/nano_corrected"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "ONT high quality",
                    "doc": "ONT high-quality reads in FASTQ format, Guppy5 SUP or Q20 (<5% error)",
                    "inputBinding": {
                        "prefix": "--nano-hq"
                    },
                    "id": "#flye.cwl/nano_high_quality"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "ONT reads raw",
                    "doc": "ONT regular reads in FASTQ format, pre-Guppy5 (<20% error)",
                    "inputBinding": {
                        "prefix": "--nano-raw"
                    },
                    "id": "#flye.cwl/nano_raw"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Output folder name",
                    "doc": "Output folder name",
                    "inputBinding": {
                        "prefix": "--out-dir"
                    },
                    "default": "flye_output",
                    "id": "#flye.cwl/output_folder_name"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "PacBio reads corrected",
                    "doc": "PacBio  reads in FASTQ format, that were corrected with other methods (<3% error)",
                    "inputBinding": {
                        "prefix": "--pacbio-corr"
                    },
                    "id": "#flye.cwl/pacbio_corrected"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "PacBio HiFi reads",
                    "doc": "PacBio HiFi  reads in FASTQ format, (<1% error)",
                    "inputBinding": {
                        "prefix": "--pacbio-hifi"
                    },
                    "id": "#flye.cwl/pacbio_hifi"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "PacBio reads raw",
                    "doc": "PacBio regular CLR  reads in FASTQ format, (<20% error)",
                    "inputBinding": {
                        "prefix": "--pacbio-raw"
                    },
                    "id": "#flye.cwl/pacbio_raw"
                },
                {
                    "label": "Flye will carry out polishing multiple times as determined here",
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "--iterations"
                    },
                    "id": "#flye.cwl/polishing_iterations"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "--threads"
                    },
                    "id": "#flye.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.output_folder_name)/00-assembly"
                    },
                    "id": "#flye.cwl/00_assembly"
                },
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.output_folder_name)/10-consensus"
                    },
                    "id": "#flye.cwl/10_consensus"
                },
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.output_folder_name)/20-repeat"
                    },
                    "id": "#flye.cwl/20_repeat"
                },
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.output_folder_name)/30-contigger"
                    },
                    "id": "#flye.cwl/30_contigger"
                },
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.output_folder_name)/40-polishing"
                    },
                    "id": "#flye.cwl/40_polishing"
                },
                {
                    "label": "Polished assembly created by flye, main output for after polishing with next tool",
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_folder_name)/assembly.fasta"
                    },
                    "id": "#flye.cwl/assembly"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_folder_name)/assembly_graph.gfa"
                    },
                    "id": "#flye.cwl/assembly_graph"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_folder_name)/assembly_info.txt"
                    },
                    "id": "#flye.cwl/assembly_info"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_folder_name)/flye.log"
                    },
                    "id": "#flye.cwl/flye_log"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_folder_name)/params.json"
                    },
                    "id": "#flye.cwl/params"
                }
            ],
            "id": "#flye.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0002-5516-8391",
                    "https://schema.org/email": "mailto:german.royvalgarcia@wur.nl",
                    "https://schema.org/name": "Germ\u00e1n Royval"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2021-11-29",
            "https://schema.org/dateModified": "2023-01-10",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "GTDBTK Classify Workflow",
            "doc": "Taxonomic genome classification workflow with GTDBTK. \n",
            "baseCommand": [
                "bash",
                "script.sh"
            ],
            "requirements": [
                {
                    "listing": [
                        {
                            "entryname": "script.sh",
                            "entry": "#!/bin/bash\nexport GTDBTK_DATA_PATH=$1\nshift;\ngtdbtk classify_wf $@"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/gtdbtk:2.4.0--pyhdfd78af_1",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.4.0"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/gtdbtk",
                                "file:///home/bart/git/cwl/tools/gtdbtk/doi.org/10.1093/bioinformatics/btac672"
                            ],
                            "package": "gtdbtk"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "Directory",
                    "doc": "Directory containing bins in fasta format from metagenomic binning",
                    "label": "bins with directory",
                    "inputBinding": {
                        "position": 2,
                        "prefix": "--genome_dir"
                    },
                    "id": "#gtdbtk_classify_wf.cwl/bin_dir"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "fasta file extension",
                    "inputBinding": {
                        "position": 2,
                        "prefix": "--extension"
                    },
                    "default": "fa",
                    "id": "#gtdbtk_classify_wf.cwl/fasta_extension"
                },
                {
                    "type": "Directory",
                    "doc": "Directory containing the GTDBTK repository",
                    "label": "gtdbtk data directory",
                    "loadListing": "no_listing",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#gtdbtk_classify_wf.cwl/gtdbtk_data"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#gtdbtk_classify_wf.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "skip ani screen, or specify a --mash_db location",
                    "inputBinding": {
                        "position": 2,
                        "prefix": "--skip_ani_screen"
                    },
                    "default": true,
                    "id": "#gtdbtk_classify_wf.cwl/skip_ani_screen"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Number of threads to use",
                    "default": 8,
                    "inputBinding": {
                        "position": 2,
                        "prefix": "--cpus"
                    },
                    "id": "#gtdbtk_classify_wf.cwl/threads"
                }
            ],
            "arguments": [
                {
                    "valueFrom": "--force",
                    "position": 10
                },
                {
                    "prefix": "--prefix",
                    "valueFrom": "$(inputs.identifier).gtdbtk",
                    "position": 11
                },
                {
                    "prefix": "--out_dir",
                    "position": 12,
                    "valueFrom": "$(inputs.identifier)_GTDB-Tk"
                }
            ],
            "stdout": "$(inputs.identifier)_GTDB-Tk.stdout.log",
            "outputs": [
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_GTDB-Tk"
                    },
                    "id": "#gtdbtk_classify_wf.cwl/gtdbtk_out_folder"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_GTDB-Tk/classify/$(inputs.identifier).gtdbtk.bac120.summary.tsv"
                    },
                    "id": "#gtdbtk_classify_wf.cwl/gtdbtk_summary"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_GTDB-Tk.stdout.log"
                    },
                    "id": "#gtdbtk_classify_wf.cwl/stdout_out"
                }
            ],
            "id": "#gtdbtk_classify_wf.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2021-00-00",
            "https://schema.org/dateModified": "2022-02-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "InterProScan",
            "doc": "InterProScan is the software package that allows sequences to be scanned  against InterPro's signatures. Signatures are predictive models,  provided by several different databases, that make up the InterPro consortium.\nYou need to to download a copy of InterProScan v5 here:  https://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/\nBefore you run InterProScan 5 for the first time, you should run the command: > python3 setup.py -f interproscan.properties\nDocumentation on InterProScan can be found here: https://interproscan-docs.readthedocs.io",
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/interproscan_v5:base",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "5"
                            ],
                            "specs": [
                                "https://doi.org/10.1093/bioinformatics/btu031"
                            ],
                            "package": "interproscan"
                        },
                        {
                            "version": [
                                "21.0.2"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/openjdk"
                            ],
                            "package": "openjdk"
                        },
                        {
                            "version": [
                                "5.32"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/perl"
                            ],
                            "package": "perl"
                        },
                        {
                            "version": [
                                "3.12.2"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/python"
                            ],
                            "package": "python"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "networkAccess": true,
                    "class": "NetworkAccess"
                }
            ],
            "baseCommand": [
                "bash",
                "-x"
            ],
            "arguments": [
                {
                    "position": 1,
                    "valueFrom": "$(inputs.interproscan_directory.path)/interproscan.sh"
                },
                {
                    "prefix": "--output-file-base",
                    "position": 9,
                    "valueFrom": "$(inputs.protein_fasta.nameroot).interproscan"
                }
            ],
            "inputs": [
                {
                    "type": "string",
                    "inputBinding": {
                        "position": 4,
                        "prefix": "--applications"
                    },
                    "default": "Pfam,SFLD,SMART,AntiFam,NCBIfam",
                    "label": "Applications",
                    "doc": "Comma separated list of analyses:\nFunFam,SFLD,PANTHER,Gene3D,Hamap,PRINTS,ProSiteProfiles,Coils,SUPERFAMILY,SMART,CDD,PIRSR,ProSitePatterns,AntiFam,Pfam,MobiDBLite,PIRSF,NCBIfam\ndefault Pfam,SFLD,SMART,AntiFam,NCBIfam\n",
                    "id": "#interproscan_v5.cwl/applications"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": true,
                    "inputBinding": {
                        "position": 7,
                        "prefix": "--goterms"
                    },
                    "label": "GOterms",
                    "doc": "Lookup of corresponding Gene Ontology annotation (IMPLIES -iprlookup option). Default true",
                    "id": "#interproscan_v5.cwl/goterms"
                },
                {
                    "inputBinding": {
                        "position": 1
                    },
                    "type": "Directory",
                    "label": "IPR directory",
                    "doc": "InterProScan (full) program directory path",
                    "id": "#interproscan_v5.cwl/interproscan_directory"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": true,
                    "inputBinding": {
                        "position": 6,
                        "prefix": "--iprlookup"
                    },
                    "label": "IPR lookup",
                    "doc": "Also include lookup of corresponding InterPro annotation in the TSV and GFF3 output formats. Default true",
                    "id": "#interproscan_v5.cwl/ipr_lookup"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "TSV,JSON",
                    "inputBinding": {
                        "position": 5,
                        "prefix": "--formats"
                    },
                    "label": "Output format",
                    "doc": "Optional, case-insensitive, comma separated list of output formats. Supported formats are TSV, XML, JSON, and GFF3. Default  JSON,TSV",
                    "id": "#interproscan_v5.cwl/output_formats"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": true,
                    "inputBinding": {
                        "position": 8,
                        "prefix": "--pathways"
                    },
                    "label": "Pathways",
                    "doc": "Lookup of corresponding Pathway annotation (IMPLIES -iprlookup option). Default true",
                    "id": "#interproscan_v5.cwl/pathways"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "position": 2,
                        "prefix": "--input"
                    },
                    "label": "Input protein fasta file path",
                    "id": "#interproscan_v5.cwl/protein_fasta"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "position": 3,
                        "prefix": "--cpu"
                    },
                    "id": "#interproscan_v5.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "*.gff3"
                    },
                    "id": "#interproscan_v5.cwl/gff3_annotations"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "*.json"
                    },
                    "id": "#interproscan_v5.cwl/json_annotations"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "*.tsv"
                    },
                    "id": "#interproscan_v5.cwl/tsv_annotations"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "*.xml"
                    },
                    "id": "#interproscan_v5.cwl/xml_annotations"
                }
            ],
            "id": "#interproscan_v5.cwl",
            "http://schema.org/author": [
                {
                    "class": "http://schema.org/Person",
                    "http://schema.org/author": "Michael Crusoe, Aleksandra Ola Tarkowska, Maxim Scheremetjew, Ekaterina Sakharova"
                },
                {
                    "class": "http://schema.org/Person",
                    "http://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "http://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "http://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "http://schema.org/Person",
                    "http://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "http://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "http://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "http://schema.org/Person",
                    "http://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "http://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "http://schema.org/name": "Changlin Ke"
                }
            ],
            "http://schema.org/copyrightHolder": "EMBL - European Bioinformatics Institute",
            "http://schema.org/license": "https://www.apache.org/licenses/LICENSE-2.0",
            "http://schema.org/dateCreated": "2019-06-14",
            "http://schema.org/dateModified": "2024-03-00",
            "http://schema.org/citation": "https://m-unlock.nl",
            "http://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "KofamScan",
            "doc": "KofamScan / KofamKOALA assigns K numbers to the user's sequence data by HMMER/HMMSEARCH against KOfam, a customized HMM database of KEGG Orthologs (KOs). \nK number assignments with scores above the predefined thresholds for individual KOs are more reliable than other proposed assignments. \nSuch high score assignments are highlighted with asterisks '*' in the output. \nThe K number assignments facilitate the interpretation of the annotation results by linking the user's sequence data to the KEGG pathways and EC numbers..\n",
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/kofamscan:1.3.0-db.2024-01-01",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "3.2.1"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/hmmer",
                                "file:///home/bart/git/cwl/tools/kofamscan/doi.org/10.1093/nar/gkr367"
                            ],
                            "package": "hmmer"
                        },
                        {
                            "version": [
                                "1.3.0"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/kofamscan",
                                "file:///home/bart/git/cwl/tools/kofamscan/doi.org/10.1093/bioinformatics/btu031"
                            ],
                            "package": "kofamscan"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "baseCommand": [
                "exec_annotation"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        {
                            "type": "enum",
                            "symbols": [
                                "#kofamscan.cwl/format/detail",
                                "#kofamscan.cwl/format/detail-tsv",
                                "#kofamscan.cwl/format/mapper",
                                "#kofamscan.cwl/format/mapper-one-line"
                            ],
                            "inputBinding": {
                                "prefix": "--format"
                            }
                        }
                    ],
                    "default": "detail-tsv",
                    "doc": "Format of the output. (default detail-tsv)\ndetail:          Detail for each hits (including hits below threshold)\ndetail-tsv:      Tab separeted values for detail format\nmapper:          KEGG Mapper compatible format\nmapper-one-line: Similar to mapper, but all hit KOs are listed in one line\n",
                    "id": "#kofamscan.cwl/format"
                },
                {
                    "type": "File",
                    "doc": "Protein sequences in fasta file format",
                    "label": "Protein fasta file",
                    "inputBinding": {
                        "position": 100
                    },
                    "id": "#kofamscan.cwl/input_fasta"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "id": "#kofamscan.cwl/ko_list"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "Profile directory",
                    "doc": "Use a custom profile directory instead of the build-in version.",
                    "id": "#kofamscan.cwl/profile_directory"
                },
                {
                    "label": "Profile organism",
                    "type": [
                        "null",
                        "string"
                    ],
                    "doc": "Specify organism profile; 'eukaryote' and 'prokaryote' are valid choices for the build-in database. Takes the \".hal\" file in the profile directory with given name.",
                    "id": "#kofamscan.cwl/profile_organism"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 3,
                    "inputBinding": {
                        "prefix": "--cpu"
                    },
                    "id": "#kofamscan.cwl/threads"
                }
            ],
            "arguments": [
                {
                    "prefix": "-f",
                    "valueFrom": "detail-tsv"
                },
                {
                    "prefix": "-o",
                    "valueFrom": "$(inputs.input_fasta.nameroot).KofamKOALA.txt"
                },
                {
                    "prefix": "--profile",
                    "valueFrom": "${\n  // set profile organism subset (.hal) file\n  var po = \"\";\n  if (inputs.profile_organism){\n    po = \"/\"+inputs.profile_organism+\".hal\" ;\n  }\n  // check if custom profile directory is used\n  if (inputs.profile_directory){\n    return inputs.profile_directory.path+po\n  }\n  // else use default built-in kofam database\n  else { \n    return \"/profiles\"+po;\n  }\n}\n"
                },
                {
                    "prefix": "--ko-list",
                    "valueFrom": "${\n  if (inputs.ko_list){\n    return inputs.ko_list;\n  } else {\n    return '/ko_list';\n  }\n}\n"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.input_fasta.nameroot).KofamKOALA.txt"
                    },
                    "id": "#kofamscan.cwl/output"
                }
            ],
            "id": "#kofamscan.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-00-00",
            "https://schema.org/dateModified": "2024-03-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "baseCommand": [
                "kraken2"
            ],
            "label": "Kraken2",
            "doc": "Kraken2 metagenomics taxomic read classification.\n\nUpdated databases available at: https://benlangmead.github.io/aws-indexes/k2 (e.g. PlusPF-8)\nOriginal db: https://ccb.jhu.edu/software/kraken2/index.shtml?t=downloads\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/kraken2:2.1.3--pl5321hdcf5f25_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.1.3"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/kraken2",
                                "file:///home/bart/git/cwl/tools/kraken2/doi.org/10.1186/s13059-019-1891-0"
                            ],
                            "package": "kraken2"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.identifier)_$(inputs.database.path.split( '/' ).pop())_kraken2.txt",
                    "prefix": "--output"
                },
                {
                    "valueFrom": "$(inputs.identifier)_$(inputs.database.path.split( '/' ).pop())_kraken2_report.txt",
                    "prefix": "--report"
                },
                "--report-zero-counts",
                "--use-names",
                "--memory-mapping"
            ],
            "inputs": [
                {
                    "type": "boolean",
                    "doc": "input data is gzip compressed",
                    "inputBinding": {
                        "position": 3,
                        "prefix": "--bzip2-compressed"
                    },
                    "default": false,
                    "id": "#kraken2.cwl/bzip2"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Confidence",
                    "doc": "Confidence score threshold (default 0.0) must be in [0, 1]",
                    "inputBinding": {
                        "position": 4,
                        "prefix": "--confidence"
                    },
                    "id": "#kraken2.cwl/confidence"
                },
                {
                    "type": "Directory",
                    "label": "Database",
                    "doc": "Database location of kraken2",
                    "inputBinding": {
                        "prefix": "--db"
                    },
                    "id": "#kraken2.cwl/database"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Forward reads",
                    "doc": "Illumina forward read file",
                    "inputBinding": {
                        "position": 100
                    },
                    "id": "#kraken2.cwl/forward_reads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "input data is gzip compressed",
                    "inputBinding": {
                        "position": 3,
                        "prefix": "--gzip-compressed"
                    },
                    "default": false,
                    "id": "#kraken2.cwl/gzip"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#kraken2.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Nanopore reads",
                    "doc": "Oxford Nanopore Technologies reads in FASTQ",
                    "inputBinding": {
                        "position": 102
                    },
                    "id": "#kraken2.cwl/nanopore_reads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Paired end",
                    "doc": "Data is paired end (separate files)",
                    "inputBinding": {
                        "position": 2,
                        "prefix": "--paired"
                    },
                    "default": false,
                    "id": "#kraken2.cwl/paired_end"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Reverse reads",
                    "doc": "Illumina reverse read file",
                    "inputBinding": {
                        "position": 101
                    },
                    "id": "#kraken2.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "--threads"
                    },
                    "id": "#kraken2.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_$(inputs.database.path.split( '/' ).pop())_kraken2_report.txt"
                    },
                    "id": "#kraken2.cwl/sample_report"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_$(inputs.database.path.split( '/' ).pop())_kraken2.txt"
                    },
                    "id": "#kraken2.cwl/standard_report"
                }
            ],
            "id": "#kraken2.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0002-5516-8391",
                    "https://schema.org/email": "mailto:german.royvalgarcia@wur.nl",
                    "https://schema.org/name": "Germ\u00e1n Royval"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2021-11-25",
            "https://schema.org/dateModified": "2021-11-04",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "kreport2krona.py",
            "doc": "This program takes a Kraken report file and prints out a krona-compatible TEXT file\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/krakentools:1.2--pyh5e36f6f_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.2"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/krakentools"
                            ],
                            "package": "kronatools"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "kreport2krona.py"
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.report.nameroot)_krona.txt",
                    "prefix": "--output"
                }
            ],
            "inputs": [
                {
                    "type": "boolean",
                    "label": "Intermediate Ranks",
                    "doc": "Include non-standard levels. Default false",
                    "inputBinding": {
                        "prefix": "--intermediate-ranks"
                    },
                    "default": false,
                    "id": "#kreport2krona.cwl/intermediate-ranks"
                },
                {
                    "type": "boolean",
                    "label": "No Intermediate Ranks",
                    "doc": "only output standard levels [D,P,C,O,F,G,S]. Default true",
                    "inputBinding": {
                        "prefix": "--no-intermediate-ranks"
                    },
                    "default": true,
                    "id": "#kreport2krona.cwl/no-intermediate-ranks"
                },
                {
                    "type": "File",
                    "label": "Report",
                    "doc": "Kraken report file",
                    "inputBinding": {
                        "prefix": "--report"
                    },
                    "id": "#kreport2krona.cwl/report"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.report.nameroot)_krona.txt"
                    },
                    "id": "#kreport2krona.cwl/krona_txt"
                }
            ],
            "id": "#kreport2krona.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2024-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/krona:2.8.1--pl5321hdfd78af_1",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.8.1"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/krona",
                                "file:///home/bart/git/cwl/tools/krona/doi.org/10.1186/1471-2105-12-385"
                            ],
                            "package": "krona"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "ktImportText"
            ],
            "label": "Krona ktImportText",
            "doc": "Creates a Krona chart from text files listing quantities and lineages.\ntext  Tab-delimited text file. Each line should be a number followed by a list of wedges to contribute to (starting from the highest level). \nIf no wedges are listed (and just a quantity is given), it will contribute to the top level. \nIf the same lineage is listed more than once, the values will be added. Quantities can be omitted if -q is specified.\nLines beginning with \"#\" will be ignored. By default, separate datasets will be created for each input.\n",
            "arguments": [
                {
                    "prefix": "-o",
                    "valueFrom": "$(inputs.input.nameroot).html"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Highest level",
                    "doc": "Name of the highest level. Default 'all'",
                    "inputBinding": {
                        "position": 1,
                        "prefix": "-n"
                    },
                    "id": "#krona_ktImportText.cwl/highest_level"
                },
                {
                    "type": "File",
                    "label": "Tab-delimited text file",
                    "inputBinding": {
                        "position": 10
                    },
                    "id": "#krona_ktImportText.cwl/input"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "No quantity",
                    "doc": "Fields do not have a field for quantity. Default false",
                    "inputBinding": {
                        "position": 2,
                        "prefix": "-q"
                    },
                    "default": false,
                    "id": "#krona_ktImportText.cwl/no_quantity"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.input.nameroot).html"
                    },
                    "id": "#krona_ktImportText.cwl/krona_html"
                }
            ],
            "id": "#krona_ktImportText.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2024-04-10",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "MaxBin2",
            "doc": "MaxBin2 is a software for binning assembled metagenomic sequences based on an Expectation-Maximization algorithm.",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/maxbin2:2.2.7",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.2.7"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/maxbin2",
                                "file:///home/bart/git/cwl/tools/maxbin2/doi.org/10.1093/bioinformatics/btv638"
                            ],
                            "package": "maxbin2"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "run_MaxBin.pl"
            ],
            "inputs": [
                {
                    "type": "File",
                    "doc": "Abundances file",
                    "label": "Abundances",
                    "inputBinding": {
                        "prefix": "-abund"
                    },
                    "id": "#maxbin2.cwl/abundances"
                },
                {
                    "type": "File",
                    "doc": "Input assembly in fasta format",
                    "label": "Input assembly",
                    "inputBinding": {
                        "prefix": "-contig"
                    },
                    "id": "#maxbin2.cwl/contigs"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#maxbin2.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Number of threads to use",
                    "default": 1,
                    "inputBinding": {
                        "position": 0,
                        "prefix": "-thread"
                    },
                    "id": "#maxbin2.cwl/threads"
                }
            ],
            "arguments": [
                {
                    "prefix": "-out",
                    "valueFrom": "$(inputs.identifier)_MaxBin2.bin"
                }
            ],
            "stdout": "$(inputs.identifier)_MaxBin2.log",
            "outputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "label": "Bins",
                    "doc": "Bin fasta files. The XX bin. XX are numbers, e.g. out.001.fasta",
                    "outputBinding": {
                        "glob": "*.fasta"
                    },
                    "id": "#maxbin2.cwl/bins"
                },
                {
                    "type": "File",
                    "label": "Log",
                    "doc": "Log file recording the core steps of MaxBin algorithm",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_MaxBin2.log"
                    },
                    "id": "#maxbin2.cwl/log"
                },
                {
                    "type": "File",
                    "label": "Markers",
                    "doc": "Marker gene presence numbers for each bin. This table is ready to be plotted by R or other 3rd-party software.",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_MaxBin2.bin.marker"
                    },
                    "id": "#maxbin2.cwl/markers"
                },
                {
                    "type": "File",
                    "label": "MaxBin2 Summary",
                    "doc": "Summary file describing which contigs are being classified into which bin.",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_MaxBin2.bin.summary"
                    },
                    "id": "#maxbin2.cwl/summary"
                }
            ],
            "id": "#maxbin2.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2022-08-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Polishing of assembly created from ONT nanopore long reads",
            "doc": "Uses Medaka to polish an assembly constructed from ONT nanopore reads\n",
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/medaka:1.11.3",
                    "class": "DockerRequirement"
                },
                {
                    "networkAccess": true,
                    "class": "NetworkAccess"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.11.3"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/medaka"
                            ],
                            "package": "medaka"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "medaka.py"
            ],
            "requirements": [
                {
                    "listing": [
                        "$(inputs.draft_assembly)"
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "inputs": [
                {
                    "label": "Basecalling model that was used by guppy.",
                    "doc": "Please consult https://github.com/nanoporetech/medaka for detailed information.\nChoice of medaka model depends on how basecalling was performed. Run \"medaka tools list\\_models\".\n{pore}_{device}_{caller variant}_{caller version}\nAvailable models: {r103_fast_g507, r103_fast_snp_g507, r103_fast_variant_g507, r103_hac_g507, r103_hac_snp_g507, r103_hac_variant_g507, r103_min_high_g345, r103_min_high_g360, r103_prom_high_g360, r103_prom_snp_g3210, r103_prom_variant_g3210,\n                  r103_sup_g507, r103_sup_snp_g507, r103_sup_variant_g507, r1041_e82_400bps_fast_g615, r1041_e82_400bps_fast_variant_g615, r1041_e82_400bps_hac_g615, r1041_e82_400bps_hac_variant_g615, r1041_e82_400bps_sup_g615, r1041_e82_400bps_sup_variant_g615,\n                  r104_e81_fast_g5015, r104_e81_fast_variant_g5015, r104_e81_hac_g5015, r104_e81_hac_variant_g5015, r104_e81_sup_g5015, r104_e81_sup_g610, r104_e81_sup_variant_g610, r10_min_high_g303, r10_min_high_g340, r941_e81_fast_g514, r941_e81_fast_variant_g514,\n                  r941_e81_hac_g514, r941_e81_hac_variant_g514, r941_e81_sup_g514, r941_e81_sup_variant_g514, r941_min_fast_g303, r941_min_fast_g507, r941_min_fast_snp_g507, r941_min_fast_variant_g507, r941_min_hac_g507, r941_min_hac_snp_g507,\n                  r941_min_hac_variant_g507, r941_min_high_g303, r941_min_high_g330, r941_min_high_g340_rle, r941_min_high_g344, r941_min_high_g351, r941_min_high_g360, r941_min_sup_g507, r941_min_sup_snp_g507, r941_min_sup_variant_g507, r941_prom_fast_g303,\n                  r941_prom_fast_g507, r941_prom_fast_snp_g507, r941_prom_fast_variant_g507, r941_prom_hac_g507, r941_prom_hac_snp_g507, r941_prom_hac_variant_g507, r941_prom_high_g303, r941_prom_high_g330, r941_prom_high_g344, r941_prom_high_g360,\n                  r941_prom_high_g4011, r941_prom_snp_g303, r941_prom_snp_g322, r941_prom_snp_g360, r941_prom_sup_g507, r941_prom_sup_snp_g507, r941_prom_sup_variant_g507, r941_prom_variant_g303, r941_prom_variant_g322, r941_prom_variant_g360, r941_sup_plant_g610,\n                  r941_sup_plant_variant_g610}\nFor basecalling with guppy version >= v3.0.3, select model based on pore name and whether high or fast basecalling was used.\nFor flip flop basecalling with v3.03 > guppy version => v2.3.5 select r941_flip235.\nFor flip flop basecalling with v2.3.5 > guppy version >= 2.1.3 select r941_flip213.\nFor transducer basecaling using Albacore or non-flip-flop guppy basecalling, select r941_trans.\n\nFor test set (https://denbi-nanopore-training-course.readthedocs.io/en/latest/basecalling/basecalling.html?highlight=flowcell),\nuse \"r941_min_hac_g507\" according to the list of available models.\n",
                    "type": "string",
                    "inputBinding": {
                        "prefix": "--model"
                    },
                    "id": "#medaka_consensus_py.cwl/basecall_model"
                },
                {
                    "label": "Assembly that medaka will try to polish.",
                    "type": "File",
                    "inputBinding": {
                        "prefix": "-r"
                    },
                    "id": "#medaka_consensus_py.cwl/draft_assembly"
                },
                {
                    "label": "Basecalled ONT nanopore reads in fastq format.",
                    "type": "File",
                    "inputBinding": {
                        "prefix": "-i"
                    },
                    "id": "#medaka_consensus_py.cwl/reads"
                },
                {
                    "label": "Number of CPU threads used by tool.",
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "-t"
                    },
                    "id": "#medaka_consensus_py.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "polished_*bed"
                    },
                    "id": "#medaka_consensus_py.cwl/gaps_in_draft_coords"
                },
                {
                    "label": "draft assembly polished by medaka.",
                    "type": "File",
                    "outputBinding": {
                        "glob": "polished_*fasta"
                    },
                    "id": "#medaka_consensus_py.cwl/polished_assembly"
                }
            ],
            "id": "#medaka_consensus_py.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0002-5516-8391",
                    "https://schema.org/email": "mailto:german.royvalgarcia@wur.nl",
                    "https://schema.org/name": "Germ\u00e1n Royval"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2021-11-29",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "MEMOTE",
            "doc": "MEMOTE, short for metabolic model testing",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/memote:0.17.0--pyhdfd78af_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "0.17.0"
                            ],
                            "specs": [
                                "https://pypi.org/project/memote"
                            ],
                            "package": "python3"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "memote"
            ],
            "inputs": [
                {
                    "type": "File",
                    "doc": "Metabolic model (xml format)",
                    "label": "Metabolic model",
                    "inputBinding": {
                        "position": 100
                    },
                    "id": "#memote.cwl/GEM"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Report",
                    "doc": "Take a snapshot of a model's state and generate a report.",
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#memote.cwl/report_snapshot"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Run",
                    "doc": "Run the test suite on a single model and collect results.",
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#memote.cwl/run"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Skip test; find incorrect thermodynamic reversibility",
                    "label": "Skip test; find incorrect thermodynamic reversibility",
                    "inputBinding": {
                        "prefix": "--skip",
                        "valueFrom": "skip_test_find_incorrect_thermodynamic_reversibility",
                        "position": 15
                    },
                    "id": "#memote.cwl/skip_test_find_incorrect_thermodynamic_reversibility"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Skip test; Find metabolites consumed with closed bounds",
                    "label": "Skip test; Find metabolites consumed with closed bounds",
                    "inputBinding": {
                        "prefix": "--skip",
                        "valueFrom": "test_find_metabolites_consumed_with_closed_bounds",
                        "position": 12
                    },
                    "id": "#memote.cwl/skip_test_find_metabolites_consumed_with_closed_bounds"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Skip test; Find metabolites not consumedwith open_bounds",
                    "label": "Skip test; Find metabolites not consumedwith open_bounds",
                    "inputBinding": {
                        "prefix": "--skip",
                        "valueFrom": "test_find_metabolites_not_consumed_with_open_bounds",
                        "position": 14
                    },
                    "id": "#memote.cwl/skip_test_find_metabolites_not_consumed_with_open_bounds"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Skip test; Find metabolites not produced with open bounds",
                    "label": "Skip test; Find metabolites not produced with open bounds",
                    "inputBinding": {
                        "prefix": "--skip",
                        "valueFrom": "test_find_metabolites_not_produced_with_open_bounds",
                        "position": 13
                    },
                    "id": "#memote.cwl/skip_test_find_metabolites_not_produced_with_open_bounds"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Skip test; find metabolites produced with closed bounds",
                    "label": "Skip test; find metabolites produced with closed bounds",
                    "inputBinding": {
                        "prefix": "--skip",
                        "valueFrom": "test_find_metabolites_produced_with_closed_bounds",
                        "position": 11
                    },
                    "id": "#memote.cwl/skip_test_find_metabolites_produced_with_closed_bounds"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "solver",
                    "doc": "Set the solver to be used. [cplex|glpk|gurobi|glpk_exact]. default; glpk",
                    "inputBinding": {
                        "prefix": "--solver",
                        "position": 3
                    },
                    "id": "#memote.cwl/solver"
                }
            ],
            "arguments": [
                {
                    "valueFrom": "${ if (inputs.run){ return ['run', '--filename', inputs.GEM.basename + '_MEMOTE.json.gz']; } else { return []; } }",
                    "prefix": ""
                },
                {
                    "valueFrom": "${ if (inputs.report_snapshot){ return ['report', 'snapshot', '--filename', inputs.GEM.basename + '_MEMOTE.html']; } else { return []; } }",
                    "prefix": ""
                }
            ],
            "outputs": [
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.GEM.basename)_MEMOTE.html"
                    },
                    "id": "#memote.cwl/report_html"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.GEM.basename)_MEMOTE.json.gz"
                    },
                    "id": "#memote.cwl/run_json"
                }
            ],
            "id": "#memote.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-06-00",
            "https://schema.org/dateModified": "2023-02-20",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "aggregateBinDepths",
            "doc": "Aggregate bin depths using MetaBat2 using the script aggregateBinDepths.pl\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/metabat2:2.15--h4da6f23_2",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.15"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/metabat2",
                                "file:///home/bart/git/cwl/tools/metabat2/doi.org/10.7717%2Fpeerj.7359"
                            ],
                            "package": "metabat2"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "perl",
                "/usr/local/bin/aggregateBinDepths.pl"
            ],
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "doc": "Bin fasta files",
                    "label": "Bin fasta files",
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#aggregateBinDepths.cwl/bins"
                },
                {
                    "type": "string",
                    "doc": "Name of the output file",
                    "label": "output file name",
                    "id": "#aggregateBinDepths.cwl/identifier"
                },
                {
                    "type": "File",
                    "doc": "Contig depths files obtained from metabat2 script jgi_summarize_bam_contig_depths",
                    "label": "contigs depths",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#aggregateBinDepths.cwl/metabatdepthsFile"
                }
            ],
            "stdout": "$(inputs.identifier)_binDepths.tsv",
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_binDepths.tsv"
                    },
                    "id": "#aggregateBinDepths.cwl/binDepths"
                }
            ],
            "id": "#aggregateBinDepths.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2021-00-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "MetaBAT2 binning",
            "doc": "Metagenome Binning based on Abundance and Tetranucleotide frequency (MetaBat2)\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/metabat2:2.15--h4da6f23_2",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.15"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/metabat2",
                                "file:///home/bart/git/cwl/tools/metabat2/doi.org/10.7717%2Fpeerj.7359"
                            ],
                            "package": "metabat2"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "metabat2"
            ],
            "arguments": [
                {
                    "prefix": "--outFile",
                    "valueFrom": "MetaBAT2_bins/$(inputs.identifier)_MetaBAT2_bin"
                }
            ],
            "inputs": [
                {
                    "type": "File",
                    "label": "The input assembly in fasta format",
                    "inputBinding": {
                        "position": 4,
                        "prefix": "--inFile"
                    },
                    "id": "#metabat2.cwl/assembly"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "position": 5,
                        "prefix": "--abdFile"
                    },
                    "id": "#metabat2.cwl/depths"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#metabat2.cwl/identifier"
                },
                {
                    "type": "int",
                    "label": "Number of threads to use",
                    "default": 1,
                    "inputBinding": {
                        "position": 0,
                        "prefix": "--numThreads"
                    },
                    "id": "#metabat2.cwl/threads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Export unbinned contigs as fasta file",
                    "label": "Write unbinned",
                    "inputBinding": {
                        "prefix": "--unbinned"
                    },
                    "id": "#metabat2.cwl/write_unbinned"
                }
            ],
            "stdout": "$(inputs.identifier)_MetaBAT2.log",
            "outputs": [
                {
                    "type": "Directory",
                    "label": "Bin directory",
                    "doc": "Bin directory",
                    "outputBinding": {
                        "glob": "MetaBAT2_bins"
                    },
                    "id": "#metabat2.cwl/bin_dir"
                },
                {
                    "type": "File",
                    "label": "Log",
                    "doc": "MetaBat2 log file",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_MetaBAT2.log"
                    },
                    "id": "#metabat2.cwl/log"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Unbinned contigs",
                    "doc": "Unbinned contig fasta files",
                    "outputBinding": {
                        "glob": "MetaBAT2_bins/$(inputs.identifier)_MetaBAT2_bin.unbinned.fa"
                    },
                    "id": "#metabat2.cwl/unbinned"
                }
            ],
            "id": "#metabat2.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2022-10-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "jgi_summarize_bam_contig_depths",
            "doc": "Summarize contig read depth from bam file for metabat2 binning.\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/metabat2:2.15--h4da6f23_2",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.15"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/metabat2",
                                "file:///home/bart/git/cwl/tools/metabat2/doi.org/10.7717%2Fpeerj.7359"
                            ],
                            "package": "metabat2"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "jgi_summarize_bam_contig_depths"
            ],
            "arguments": [
                {
                    "position": 1,
                    "prefix": "--outputDepth",
                    "valueFrom": "$(inputs.identifier)_contigDepths.tsv"
                }
            ],
            "inputs": [
                {
                    "type": "File",
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#metabatContigDepths.cwl/bamFile"
                },
                {
                    "type": "string",
                    "doc": "Name of the output file",
                    "label": "output file name",
                    "id": "#metabatContigDepths.cwl/identifier"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_contigDepths.tsv"
                    },
                    "id": "#metabatContigDepths.cwl/depths"
                }
            ],
            "id": "#metabatContigDepths.cwl",
            "http://schema.org/author": [
                {
                    "class": "http://schema.org/Person",
                    "http://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "http://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "http://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "http://schema.org/Person",
                    "http://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "http://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "http://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "http://schema.org/Person",
                    "http://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "http://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "http://schema.org/name": "Bart Nijsse"
                }
            ],
            "http://schema.org/citation": "https://m-unlock.nl",
            "http://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "http://schema.org/dateCreated": "2020-00-00",
            "http://schema.org/dateModified": "2023-04-17",
            "http://schema.org/license": "https://spdx.org/licenses/CC0-1.0.html",
            "http://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Bin read mapping stats",
            "doc": "Table of general read mapping statistics of the bins and assembly\n\nID\nReads\nAssembly size\nContigs\nn50\nLargest contig\nMapped reads\nBins\nTotal bin size\nBinned\nReads mapped to bins\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/scripts:1.0.3",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "0.20.0"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/pysam"
                            ],
                            "package": "pysam"
                        },
                        {
                            "version": [
                                "3.10.6"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/python"
                            ],
                            "package": "python3"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "python3",
                "/scripts/metagenomics/assembly_bins_readstats.py"
            ],
            "inputs": [
                {
                    "type": "File",
                    "doc": "Assembly in fasta format",
                    "label": "Assembly",
                    "inputBinding": {
                        "prefix": "--assembly"
                    },
                    "id": "#assembly_bins_readstats.cwl/assembly"
                },
                {
                    "type": "File",
                    "doc": "BAM file with reads mapped to the assembly",
                    "label": "BAM file",
                    "inputBinding": {
                        "prefix": "--bam"
                    },
                    "id": "#assembly_bins_readstats.cwl/bam_file"
                },
                {
                    "type": "File",
                    "doc": "File containing bins names and their respective (assembly) contigs. Format contig<tab>bin_name",
                    "label": "binContigs file",
                    "inputBinding": {
                        "prefix": "--binContigs"
                    },
                    "id": "#assembly_bins_readstats.cwl/binContigs"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "inputBinding": {
                        "prefix": "--identifier"
                    },
                    "id": "#assembly_bins_readstats.cwl/identifier"
                }
            ],
            "stdout": "$(inputs.identifier)_binReadStats.tsv",
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_binReadStats.tsv"
                    },
                    "id": "#assembly_bins_readstats.cwl/binReadStats"
                }
            ],
            "id": "#assembly_bins_readstats.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2022-12-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Bin summary",
            "doc": "Creates a summary table of the bins and their quality and taxonomy.\n\nColumns are:\nBin\nContigs\nSize\nLargest_contig\nN50\nGC\navgDepth\nGTDB-Tk_taxonomy\nBUSCO_Taxonomy\nBUSCO_score\nCheckM_Completeness\nCheckM_Contamination\nCheckM_Strain-heterogeneity    \n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/scripts:1.0.3",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.5.0"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/pandas",
                                "file:///home/bart/git/cwl/tools/metagenomics/doi.org/10.5281/zenodo.3509134"
                            ],
                            "package": "pandas"
                        },
                        {
                            "version": [
                                "3.10.6"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/python"
                            ],
                            "package": "python3"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "python3",
                "/scripts/metagenomics/bins_summary.py"
            ],
            "inputs": [
                {
                    "type": "File",
                    "doc": "MetaBAT2 aggregateDepths file",
                    "label": "bin depths",
                    "inputBinding": {
                        "prefix": "--bin_depths"
                    },
                    "id": "#bins_summary.cwl/bin_depths"
                },
                {
                    "type": "Directory",
                    "doc": "Directory containing bins in fasta format from metagenomic binning",
                    "label": "Bins directory",
                    "inputBinding": {
                        "prefix": "--bin_dir"
                    },
                    "id": "#bins_summary.cwl/bin_dir"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "Directory containing BUSCO reports",
                    "label": "BUSCO folder",
                    "inputBinding": {
                        "prefix": "--busco_batch"
                    },
                    "id": "#bins_summary.cwl/busco_batch"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "CheckM report file",
                    "label": "CheckM report",
                    "inputBinding": {
                        "prefix": "--checkm"
                    },
                    "id": "#bins_summary.cwl/checkm"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "CheckM report file",
                    "label": "CheckM report",
                    "inputBinding": {
                        "prefix": "--gtdbtk"
                    },
                    "id": "#bins_summary.cwl/gtdbtk"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#bins_summary.cwl/identifier"
                }
            ],
            "arguments": [
                {
                    "prefix": "--output",
                    "valueFrom": "$(inputs.identifier)"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_binContigs.tsv"
                    },
                    "id": "#bins_summary.cwl/bin_contigs"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_binSummary.tsv"
                    },
                    "id": "#bins_summary.cwl/bins_summary_table"
                }
            ],
            "id": "#bins_summary.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2021-00-00",
            "https://schema.org/dateModified": "2022-12-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Minimap2 to (un)mapped long reads",
            "doc": "Get unmapped or mapped long reads in fastq.gz format using minimap2 and samtools. Mainly used for contamination removal.\n - requires pigz!\nminimap2 | samtools | pigz\n",
            "requirements": [
                {
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "minimap_run",
                            "writable": true
                        },
                        {
                            "entryname": "script.sh",
                            "entry": "#!/bin/bash\n#   $1 = mapped/unmapped (-F -f)\n# 1 $2 = ref\n# 2 $3 = fastq\n# 3 $4 = preset (map-ont)\n# 4 $5 = threads\n# 5 $6 = identifier\n\nminimap2 -a -t $5 -x $4 $2 $3 | samtools fastq -@ $5 -n $1 4 | pigz -p $5 > $6_filtered.fastq.gz"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "baseCommand": [
                "bash",
                "-x",
                "script.sh"
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/minimap2:2.28",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.28"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/minimap2",
                                "file:///home/bart/git/cwl/tools/minimap2/doi.org/10.1093/bioinformatics/bty191"
                            ],
                            "package": "minimap2"
                        },
                        {
                            "version": [
                                "2.8"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/pigz"
                            ],
                            "package": "pigz"
                        },
                        {
                            "version": [
                                "1.19.2"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/samtools",
                                "file:///home/bart/git/cwl/tools/minimap2/doi.org/10.1093/gigascience/giab008"
                            ],
                            "package": "samtools"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "arguments": [
                "${\n  if (inputs.output_mapped){\n    return '-F';\n  } else {\n    return '-f';\n  }\n}\n"
            ],
            "inputs": [
                {
                    "type": "string",
                    "doc": "Output prefix (_filtered.fastq.gz will be added)",
                    "label": "identifier",
                    "inputBinding": {
                        "position": 5
                    },
                    "id": "#minimap2_to_fastq.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Keep only reads mapped to the reference (default = false / output only unmapped reads)",
                    "label": "Keep mapped",
                    "default": false,
                    "inputBinding": {
                        "position": 6
                    },
                    "id": "#minimap2_to_fastq.cwl/output_mapped"
                },
                {
                    "type": "string",
                    "doc": "- map-pb/map-ont - PacBio CLR/Nanopore vs reference mapping\n- map-hifi - PacBio HiFi reads vs reference mapping\n- ava-pb/ava-ont - PacBio/Nanopore read overlap\n- asm5/asm10/asm20 - asm-to-ref mapping, for ~0.1/1/5% sequence divergence\n- splice/splice:hq - long-read/Pacbio-CCS spliced alignment\n- sr - genomic short-read mapping\n",
                    "label": "Read type",
                    "inputBinding": {
                        "position": 3
                    },
                    "id": "#minimap2_to_fastq.cwl/preset"
                },
                {
                    "type": "File",
                    "doc": "Query sequence in FASTQ/FASTA format (can be gzipped).",
                    "label": "Reads",
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#minimap2_to_fastq.cwl/reads"
                },
                {
                    "type": "File",
                    "doc": "Target sequence in FASTQ/FASTA format (can be gzipped).",
                    "label": "Reference",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#minimap2_to_fastq.cwl/reference"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Maximum threads to use",
                    "label": "Threads",
                    "default": 4,
                    "inputBinding": {
                        "position": 4
                    },
                    "id": "#minimap2_to_fastq.cwl/threads"
                }
            ],
            "stderr": "$(inputs.identifier)_minimap2.log",
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_filtered.fastq.gz"
                    },
                    "id": "#minimap2_to_fastq.cwl/fastq"
                },
                {
                    "type": "File",
                    "id": "#minimap2_to_fastq.cwl/log",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_minimap2.log"
                    }
                }
            ],
            "id": "#minimap2_to_fastq.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0002-5516-8391",
                    "https://schema.org/email": "mailto:german.royvalgarcia@wur.nl",
                    "https://schema.org/name": "Germ\u00e1n Royval"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-03-00",
            "https://schema.org/dateModified": "2022-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Minimap2 to (un)mapped long reads",
            "doc": "Get unmapped or mapped long reads reads in fastq.gz format using minimap2 and samtools. Mainly used for contamination removal.\n - requires pigz!\nminimap2 | samtools | pigz\n",
            "requirements": [
                {
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "minimap_run",
                            "writable": true
                        },
                        {
                            "entryname": "script.sh",
                            "entry": "#!/bin/bash\n# 1 $1 = ref\n# 2 $2 = forward_reads\n# 3 $3 = forward_reads\n# 4 $4 = threads\n# 5 $5 = identifier\n# 6 $6 = preset\nminimap2 --split-prefix temp -a -t $4 -x $6 $1 $2 $3 | samtools view -@ $4 -hu - | samtools sort -@ $4 -o $5_sorted.bam"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "baseCommand": [
                "bash",
                "-x",
                "script.sh"
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/minimap2:2.28",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.28"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/minimap2",
                                "file:///home/bart/git/cwl/tools/minimap2/doi.org/10.1093/bioinformatics/bty191"
                            ],
                            "package": "minimap2"
                        },
                        {
                            "version": [
                                "1.19.2"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/samtools",
                                "file:///home/bart/git/cwl/tools/minimap2/doi.org/10.1093/gigascience/giab008"
                            ],
                            "package": "samtools"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "File",
                    "doc": "Forward sequence in FASTQ/FASTA format (can be gzipped).",
                    "label": "Forward reads",
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#minimap2_to_sorted-bam_PE.cwl/forward_reads"
                },
                {
                    "type": "string",
                    "doc": "Output prefix (_filtered.fastq.gz will be added)",
                    "label": "identifier",
                    "inputBinding": {
                        "position": 5
                    },
                    "id": "#minimap2_to_sorted-bam_PE.cwl/identifier"
                },
                {
                    "type": "string",
                    "doc": "- map-pb/map-ont - PacBio CLR/Nanopore vs reference mapping\n- map-hifi - PacBio HiFi reads vs reference mapping\n- ava-pb/ava-ont - PacBio/Nanopore read overlap\n- asm5/asm10/asm20 - asm-to-ref mapping, for ~0.1/1/5% sequence divergence\n- splice/splice:hq - long-read/Pacbio-CCS spliced alignment\n- sr - genomic short-read mapping\n",
                    "label": "Read type",
                    "inputBinding": {
                        "position": 6
                    },
                    "id": "#minimap2_to_sorted-bam_PE.cwl/preset"
                },
                {
                    "type": "File",
                    "doc": "Target sequence in FASTQ/FASTA format (can be gzipped).",
                    "label": "Reference",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#minimap2_to_sorted-bam_PE.cwl/reference"
                },
                {
                    "type": "File",
                    "doc": "Reverse sequence in FASTQ/FASTA format (can be gzipped).",
                    "label": "Reverse reads",
                    "inputBinding": {
                        "position": 3
                    },
                    "id": "#minimap2_to_sorted-bam_PE.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Maximum threads to use",
                    "label": "Threads",
                    "default": 4,
                    "inputBinding": {
                        "position": 4
                    },
                    "id": "#minimap2_to_sorted-bam_PE.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_sorted.bam"
                    },
                    "id": "#minimap2_to_sorted-bam_PE.cwl/sorted_bam"
                }
            ],
            "id": "#minimap2_to_sorted-bam_PE.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0002-5516-8391",
                    "https://schema.org/email": "mailto:german.royvalgarcia@wur.nl",
                    "https://schema.org/name": "Germ\u00e1n Royval"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2024-05-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "NanoPlot",
            "doc": "Plotting suite for long read sequencing data and alignments\n\nModified from:\n  https://github.com/common-workflow-library/bio-cwl-tools/blob/release/nanoplot/nanoplot.cwl\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "networkAccess": true,
                    "class": "NetworkAccess"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/nanoplot:1.43.0--pyhdfd78af_1",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "specs": [
                                "https://github.com/wdecoster/NanoPlot/releases",
                                "file:///home/bart/git/cwl/tools/nanoplot/doi.org/10.1093/bioinformatics/btad311"
                            ],
                            "version": [
                                "1.43.0"
                            ],
                            "package": "NanoPlot"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": "NanoPlot",
            "inputs": [
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--alength"
                    },
                    "id": "#nanoplot.cwl/aligned_length"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "format": "http://edamontology.org/format_2572",
                    "inputBinding": {
                        "prefix": "--bam"
                    },
                    "id": "#nanoplot.cwl/bam_files"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--barcoded"
                    },
                    "id": "#nanoplot.cwl/barcoded"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "inputBinding": {
                        "prefix": "--color"
                    },
                    "id": "#nanoplot.cwl/color"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "inputBinding": {
                        "prefix": "--colormap"
                    },
                    "id": "#nanoplot.cwl/colormap"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "format": "http://edamontology.org/format_3462",
                    "inputBinding": {
                        "prefix": "--cram"
                    },
                    "id": "#nanoplot.cwl/cram_files"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--downsample"
                    },
                    "id": "#nanoplot.cwl/downsample"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--dpi"
                    },
                    "id": "#nanoplot.cwl/dpi"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--drop_outliers"
                    },
                    "id": "#nanoplot.cwl/drop_outliers"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "format": "http://edamontology.org/format_1931",
                    "inputBinding": {
                        "prefix": "--fasta"
                    },
                    "id": "#nanoplot.cwl/fasta_files"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "inputBinding": {
                        "prefix": "--fastq"
                    },
                    "id": "#nanoplot.cwl/fastq_files"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "inputBinding": {
                        "prefix": "--prefix"
                    },
                    "default": "",
                    "id": "#nanoplot.cwl/file_prefix"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "inputBinding": {
                        "prefix": "--font_scale"
                    },
                    "id": "#nanoplot.cwl/font_scale"
                },
                {
                    "type": [
                        {
                            "type": "enum",
                            "symbols": [
                                "#nanoplot.cwl/format/eps",
                                "#nanoplot.cwl/format/jpeg",
                                "#nanoplot.cwl/format/jpg",
                                "#nanoplot.cwl/format/pdf",
                                "#nanoplot.cwl/format/pgf",
                                "#nanoplot.cwl/format/png",
                                "#nanoplot.cwl/format/ps",
                                "#nanoplot.cwl/format/raw",
                                "#nanoplot.cwl/format/rgba",
                                "#nanoplot.cwl/format/svg",
                                "#nanoplot.cwl/format/svgz",
                                "#nanoplot.cwl/format/tif",
                                "#nanoplot.cwl/format/tiff"
                            ]
                        },
                        "null"
                    ],
                    "inputBinding": {
                        "prefix": "--format"
                    },
                    "id": "#nanoplot.cwl/format"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--no-N50"
                    },
                    "id": "#nanoplot.cwl/hide_n50"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--hide_stats"
                    },
                    "id": "#nanoplot.cwl/hide_stats"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Add NanoPlot run info in the report.",
                    "inputBinding": {
                        "prefix": "--info_in_report"
                    },
                    "id": "#nanoplot.cwl/info_in_report"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--listcolormaps"
                    },
                    "id": "#nanoplot.cwl/listcolormaps"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--listcolors"
                    },
                    "id": "#nanoplot.cwl/listcolors"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--loglength"
                    },
                    "id": "#nanoplot.cwl/log_length"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--maxlength"
                    },
                    "id": "#nanoplot.cwl/max_length"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--minlength"
                    },
                    "id": "#nanoplot.cwl/min_length"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--minqual"
                    },
                    "id": "#nanoplot.cwl/min_quality"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "inputBinding": {
                        "prefix": "--fastq_minimal"
                    },
                    "id": "#nanoplot.cwl/minimal_fastq_files"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--percentqual"
                    },
                    "id": "#nanoplot.cwl/percent_quality"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "inputBinding": {
                        "prefix": "--title"
                    },
                    "id": "#nanoplot.cwl/plot_title"
                },
                {
                    "type": [
                        {
                            "type": "array",
                            "items": {
                                "type": "enum",
                                "symbols": [
                                    "#nanoplot.cwl/plots/kde",
                                    "#nanoplot.cwl/plots/hex",
                                    "#nanoplot.cwl/plots/dot"
                                ]
                            }
                        },
                        "null"
                    ],
                    "inputBinding": {
                        "prefix": "--plots"
                    },
                    "id": "#nanoplot.cwl/plots"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "enum",
                            "symbols": [
                                "#nanoplot.cwl/read_type/1D",
                                "#nanoplot.cwl/read_type/2D",
                                "#nanoplot.cwl/read_type/1D2"
                            ]
                        }
                    ],
                    "inputBinding": {
                        "prefix": "--readtype"
                    },
                    "id": "#nanoplot.cwl/read_type"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "inputBinding": {
                        "prefix": "--fastq_rich"
                    },
                    "id": "#nanoplot.cwl/rich_fastq_files"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--runtime_until"
                    },
                    "id": "#nanoplot.cwl/run_until"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--N50"
                    },
                    "id": "#nanoplot.cwl/show_n50"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Store the extracted data in a pickle file for future plotting.",
                    "inputBinding": {
                        "prefix": "--store"
                    },
                    "id": "#nanoplot.cwl/store_pickle"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Store the extracted data in tab separated file.",
                    "inputBinding": {
                        "prefix": "--raw"
                    },
                    "id": "#nanoplot.cwl/store_raw"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "inputBinding": {
                        "prefix": "--summary"
                    },
                    "id": "#nanoplot.cwl/summary_files"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--threads"
                    },
                    "id": "#nanoplot.cwl/threads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Output the stats file as a properly formatted TSV.",
                    "inputBinding": {
                        "prefix": "--tsv_stats"
                    },
                    "id": "#nanoplot.cwl/tsv_stats"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "inputBinding": {
                        "prefix": "--ubam"
                    },
                    "id": "#nanoplot.cwl/ubam_files"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--pickle"
                    },
                    "id": "#nanoplot.cwl/use_pickle_file"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--verbose"
                    },
                    "default": true,
                    "id": "#nanoplot.cwl/verbose"
                }
            ],
            "outputs": [
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)ActivePores_Over_Time.*"
                    },
                    "id": "#nanoplot.cwl/ActivePores_Over_Time"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)ActivityMap_ReadsPerChannel.*"
                    },
                    "id": "#nanoplot.cwl/ActivityMap_ReadsPerChannel"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)CumulativeYieldPlot_Gigabases.*"
                    },
                    "id": "#nanoplot.cwl/CumulativeYieldPlot_Gigabases"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)CumulativeYieldPlot_NumberOfReads.*"
                    },
                    "id": "#nanoplot.cwl/CumulativeYieldPlot_NumberOfReads"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)LengthvsQualityScatterPlot_*.*"
                    },
                    "id": "#nanoplot.cwl/LengthvsQualityScatterPlot"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)Non_weightedHistogramReadlength.*"
                    },
                    "id": "#nanoplot.cwl/Non_weightedHistogramReadlength"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)Non_weightedLogTransformed_HistogramReadlength.*"
                    },
                    "id": "#nanoplot.cwl/Non_weightedLogTransformed_HistogramReadlength"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)NumberOfReads_Over_Time.*"
                    },
                    "id": "#nanoplot.cwl/NumberOfReads_Over_Time"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)TimeLengthViolinPlot.*"
                    },
                    "id": "#nanoplot.cwl/TimeLengthViolinPlot"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)TimeQualityViolinPlot.*"
                    },
                    "id": "#nanoplot.cwl/TimeQualityViolinPlot"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)WeightedHistogramReadlength.*"
                    },
                    "id": "#nanoplot.cwl/WeightedHistogramReadlength"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)WeightedLogTransformed_HistogramReadlength.*"
                    },
                    "id": "#nanoplot.cwl/WeightedLogTransformed_HistogramReadlength"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)Yield_By_Length.*"
                    },
                    "id": "#nanoplot.cwl/Yield_By_Length"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "*.log"
                    },
                    "id": "#nanoplot.cwl/log"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)NanoPlot_*.log"
                    },
                    "id": "#nanoplot.cwl/logfile"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)NanoStats.txt"
                    },
                    "id": "#nanoplot.cwl/nanostats"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)NanoPlot-data.pickle"
                    },
                    "id": "#nanoplot.cwl/pickle"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)NanoPlot-data.tsv.gz"
                    },
                    "id": "#nanoplot.cwl/raw_data"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)NanoPlot-report.html"
                    },
                    "id": "#nanoplot.cwl/report"
                }
            ],
            "id": "#nanoplot.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0002-2703-8936",
                    "https://schema.org/name": "Miguel Boland"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/name": "Michael R. Crusoe"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-04-04",
            "https://schema.org/dateModified": "2023-04-03",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Pilon",
            "doc": "\"https://github.com/broadinstitute/pilon\n Pilon is a software tool which can be used to:\n   Automatically improve draft assemblies\n   Find variation among strains, including large event detection\"\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/pilon:1.24--hdfd78af_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.24"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/pilon",
                                "file:///home/bart/git/cwl/tools/pilon/doi.org/10.1371/journal.pone.0112963"
                            ],
                            "package": "pilon"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "File",
                    "label": "Assembly",
                    "doc": "Draft assembly fasta file",
                    "inputBinding": {
                        "prefix": "--genome"
                    },
                    "id": "#pilon.cwl/assembly"
                },
                {
                    "type": "File",
                    "label": "Bam file",
                    "doc": "Indexed sorted bam file with mapped reads to draft assembly",
                    "inputBinding": {
                        "prefix": "--frags"
                    },
                    "id": "#pilon.cwl/bam_file"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Fix List",
                    "doc": "A comma-separated list of categories of issues to try to fix:\n  \"snps\": try to fix individual base errors;\n  \"indels\": try to fix small indels;\n  \"gaps\": try to fill gaps;\n  \"local\": try to detect and fix local misassemblies;\n  \"all\": all of the above (default);\n  \"bases\": shorthand for \"snps\" and \"indels\" (for back compatibility);\n",
                    "inputBinding": {
                        "prefix": "--fix"
                    },
                    "id": "#pilon.cwl/fixlist"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#pilon.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Memory usage in megabytes",
                    "label": "memory usage (MB)",
                    "default": 8000,
                    "id": "#pilon.cwl/memory"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "number of threads to use for computational processes",
                    "label": "number of threads",
                    "inputBinding": {
                        "prefix": "--threads"
                    },
                    "default": 2,
                    "id": "#pilon.cwl/threads"
                },
                {
                    "label": "vcf output",
                    "type": "boolean",
                    "default": true,
                    "inputBinding": {
                        "prefix": "--vcf"
                    },
                    "id": "#pilon.cwl/vcf"
                }
            ],
            "baseCommand": [
                "java"
            ],
            "arguments": [
                "-jar",
                "-Xmx$(inputs.memory)M",
                "/usr/local/share/pilon-1.24-0/pilon.jar",
                {
                    "valueFrom": "$(inputs.identifier)_pilon_polished",
                    "prefix": "--output"
                }
            ],
            "stdout": "$(inputs.identifier)_pilon.log",
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_pilon.log"
                    },
                    "id": "#pilon.cwl/pilon_log"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_pilon_polished.fasta"
                    },
                    "id": "#pilon.cwl/pilon_polished_assembly"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_pilon_polished.vcf"
                    },
                    "id": "#pilon.cwl/pilon_vcf"
                }
            ],
            "id": "#pilon.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2022-02-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Prodigal",
            "doc": "Prokaryotic gene prediction using Prodigal with compressed input fasta (gzip) files.",
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/prodigal:2.6.3",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.6.3"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/prodigal",
                                "file:///home/bart/git/cwl/tools/prodigal/doi.org/10.1186/1471-2105-11-119"
                            ],
                            "package": "prodigal"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "prodigal_run",
                            "writable": true
                        },
                        {
                            "entryname": "script.sh",
                            "entry": "#!/bin/bash\nif [[ $1 == *.gz ]]; then\n  gunzip -c $1 > $(inputs.identifier).fasta;\nelse\n  cp $1 $(inputs.identifier).fasta;\nfi\nshift;\nprodigal $@ < $(inputs.identifier).fasta;"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "baseCommand": [
                "bash",
                "-x",
                "script.sh"
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.input_fasta.path)",
                    "shellQuote": false
                },
                {
                    "valueFrom": "$(inputs.identifier).prodigal.gff",
                    "prefix": "-o"
                },
                {
                    "valueFrom": "$(inputs.identifier).prodigal.ffn",
                    "prefix": "-d"
                },
                {
                    "valueFrom": "$(inputs.identifier).prodigal.faa",
                    "prefix": "-a"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier).prodigal.faa"
                    },
                    "id": "#prodigal.cwl/predicted_proteins_faa"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier).prodigal.ffn"
                    },
                    "id": "#prodigal.cwl/predicted_proteins_ffn"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier).prodigal.gff"
                    },
                    "id": "#prodigal.cwl/predicted_proteins_out"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Codon table to use",
                    "default": 11,
                    "inputBinding": {
                        "prefix": "-g"
                    },
                    "id": "#prodigal.cwl/codon_table"
                },
                {
                    "type": "string",
                    "doc": "Identifier for the output files",
                    "id": "#prodigal.cwl/identifier"
                },
                {
                    "type": "File",
                    "id": "#prodigal.cwl/input_fasta"
                },
                {
                    "doc": "Input is a meta-genome or an isolate genome",
                    "type": [
                        "null",
                        {
                            "type": "enum",
                            "symbols": [
                                "#prodigal.cwl/mode/single",
                                "#prodigal.cwl/mode/meta"
                            ]
                        }
                    ],
                    "inputBinding": {
                        "prefix": "-p"
                    },
                    "id": "#prodigal.cwl/mode"
                }
            ],
            "id": "#prodigal.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-6867-2039",
                    "https://schema.org/name": "Ekaterina Sakharova"
                }
            ],
            "https://schema.org/copyrightHolder'": "EMBL - European Bioinformatics Institute",
            "https://schema.org/license'": "https://www.apache.org/licenses/LICENSE-2.0",
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-06-00",
            "https://schema.org/dateModified": "2022-08-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential",
            "https://schema.org/copyrightNotice": " Copyright < 2022 EMBL - European Bioinformatics Institute This file has been modified by UNLOCK - Unlocking Microbial Potential "
        },
        {
            "class": "CommandLineTool",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "label": "pypolca",
            "doc": "pypolca is a Standalone Python re-implementation of the POLCA genome polisher from the MaSuRCA genome assembly and analysis toolkit.",
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/pypolca:0.3.1--pyhdfd78af_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "0.3.1"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/pypolca",
                                "file:///home/bart/git/cwl/tools/pypolca/doi.org/10.1099/mgen.0.001254"
                            ],
                            "package": "fastqc"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "pypolca",
                "run"
            ],
            "inputs": [
                {
                    "type": "File",
                    "doc": "Path to assembly contigs or scaffolds.",
                    "label": "Assembly Contigs/Scaffolds",
                    "inputBinding": {
                        "prefix": "--assembly"
                    },
                    "id": "#pypolca.cwl/assembly"
                },
                {
                    "type": "boolean",
                    "doc": "Equivalent to --min_alt 4 --min_ratio 3.",
                    "label": "Careful mode",
                    "inputBinding": {
                        "prefix": "--careful"
                    },
                    "default": true,
                    "id": "#pypolca.cwl/careful"
                },
                {
                    "type": "File",
                    "doc": "Path to polishing forward reads. Can be FASTQ or FASTQ gzipped.",
                    "label": "Forward reads",
                    "inputBinding": {
                        "prefix": "--reads1"
                    },
                    "id": "#pypolca.cwl/forward_reads"
                },
                {
                    "type": "string",
                    "doc": "Identifier for output files.",
                    "label": "Identifier",
                    "inputBinding": {
                        "prefix": "-p"
                    },
                    "default": "polca",
                    "id": "#pypolca.cwl/identifier"
                },
                {
                    "type": "string",
                    "doc": "Memory per thread to use in samtools sort, set to 2G or more for large genomes. default 2G",
                    "label": "Memory limit per thread",
                    "inputBinding": {
                        "prefix": "-m"
                    },
                    "default": "4G",
                    "id": "#pypolca.cwl/memory_limit"
                },
                {
                    "type": "int",
                    "default": 2,
                    "doc": "Minimum alt allele count to make a change. Default 2",
                    "label": "Minimum Alt Allele Count",
                    "inputBinding": {
                        "prefix": "--min_alt"
                    },
                    "id": "#pypolca.cwl/min_alt"
                },
                {
                    "type": "float",
                    "default": 2.0,
                    "doc": "Minimum alt allele to ref allele ratio to make a change.",
                    "label": "Minimum Alt Allele to Ref Allele Ratio",
                    "inputBinding": {
                        "prefix": "--min_ratio"
                    },
                    "id": "#pypolca.cwl/min_ratio"
                },
                {
                    "type": "boolean",
                    "doc": "Do not polish, just create vcf file, evaluate the assembly and exit.",
                    "label": "Skip polishing",
                    "inputBinding": {
                        "prefix": "-n"
                    },
                    "default": false,
                    "id": "#pypolca.cwl/no_polish"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "Path to polishing reverse reads. Can be FASTQ or FASTQ gzipped. Optional. Only use -1 if you have single end reads.",
                    "label": "Reverse reads",
                    "inputBinding": {
                        "prefix": "--reads2"
                    },
                    "id": "#pypolca.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Number of threads. Default 2",
                    "label": "Threads",
                    "inputBinding": {
                        "prefix": "--threads"
                    },
                    "default": 4,
                    "id": "#pypolca.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "output_pypolca/*.log"
                    },
                    "id": "#pypolca.cwl/log"
                },
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "output_pypolca/logs"
                    },
                    "id": "#pypolca.cwl/logs_dir"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "output_pypolca/*.fasta"
                    },
                    "id": "#pypolca.cwl/polished_genome"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "output_pypolca/*.report"
                    },
                    "id": "#pypolca.cwl/report"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "output_pypolca/*.vcf"
                    },
                    "id": "#pypolca.cwl/vcf"
                }
            ],
            "id": "#pypolca.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2024-04-19",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "baseCommand": [
                "metaquast.py"
            ],
            "label": "metaQUAST: Quality Assessment Tool for Metagenome Assemblies",
            "doc": "Runs the Quality Assessment Tool for Metagenome Assemblies application\n\nNecessary to install the pre-release to prevent issues:\nhttps://github.com/ablab/quast/releases/tag/quast_5.1.0rc1\n\nThe working installation followed the method in http://quast.sourceforge.net/docs/manual.html:\n$ wget https://github.com/ablab/quast/releases/download/quast_5.1.0rc1/quast-5.1.0rc1.tar.gz\n$ tar -xzf quast-5.1.0rc1.tar.gz\n$ cd quast-5.1.0rc1/\n$ ./setup.py install_full\n",
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/quast:5.2.0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "5.2.0"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/quast",
                                "file:///home/bart/git/cwl/tools/quast/doi.org/10.1093/bioinformatics/btv697"
                            ],
                            "package": "quast"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "InitialWorkDirRequirement",
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "metaQUAST_results",
                            "writable": true
                        }
                    ]
                }
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.assembly)"
                },
                {
                    "valueFrom": "metaQUAST_results",
                    "prefix": "--output-dir"
                },
                "${\n  if (inputs.blastdb){\n    return [\"--blast-db\", inputs.blastdb.path];\n  } else {\n    return [\"--blast-db\", \"/usr/bin/silva_138.1/\"];\n  }\n}\n"
            ],
            "inputs": [
                {
                    "type": "File",
                    "doc": "The input assembly in fasta format",
                    "id": "#metaquast.cwl/assembly"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "Reference BLASTdb",
                    "doc": "Reference BLASTdb. The path should point to a directory containing .nsq file or to .nsq file itself. (default is silva)",
                    "inputBinding": {
                        "prefix": "--blast-db"
                    },
                    "id": "#metaquast.cwl/blastdb"
                },
                {
                    "type": "boolean",
                    "label": "Run space efficient",
                    "doc": "Uses less diskspace. Removes aux files with some details for advanced analysis. (default true)",
                    "inputBinding": {
                        "prefix": "--space-efficient"
                    },
                    "default": true,
                    "id": "#metaquast.cwl/space_efficient"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "--threads"
                    },
                    "id": "#metaquast.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/basic_stats"
                    },
                    "id": "#metaquast.cwl/basicStats"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/combined_reference"
                    },
                    "id": "#metaquast.cwl/meta_combined_ref"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/quast_downloaded_references"
                    },
                    "id": "#metaquast.cwl/meta_downloaded_ref"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/icarus.html"
                    },
                    "id": "#metaquast.cwl/meta_icarus"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/icarus_viewers"
                    },
                    "id": "#metaquast.cwl/meta_icarusDir"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/summary"
                    },
                    "id": "#metaquast.cwl/meta_summary"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/krona_charts"
                    },
                    "id": "#metaquast.cwl/metaquast_krona"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/metaquast.log"
                    },
                    "id": "#metaquast.cwl/metaquast_log"
                },
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "metaQUAST_results"
                    },
                    "id": "#metaquast.cwl/metaquast_outdir"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/report.html"
                    },
                    "id": "#metaquast.cwl/metaquast_report"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/not_aligned"
                    },
                    "id": "#metaquast.cwl/not_aligned"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/quast.log"
                    },
                    "id": "#metaquast.cwl/quastLog"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/report.*"
                    },
                    "id": "#metaquast.cwl/quastReport"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/icarus_viewers"
                    },
                    "id": "#metaquast.cwl/quast_icarusDir"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/icarus.html"
                    },
                    "id": "#metaquast.cwl/quast_icarusHtml"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/runs_per_reference"
                    },
                    "id": "#metaquast.cwl/runs_per_reference"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "metaQUAST_results/transposed_report.*"
                    },
                    "id": "#metaquast.cwl/transposedReport"
                }
            ],
            "id": "#metaquast.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0002-5516-8391",
                    "https://schema.org/email": "mailto:german.royvalgarcia@wur.nl",
                    "https://schema.org/name": "Germ\u00e1n Royval"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2021-02-09",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "ExpressionTool",
            "requirements": [
                {
                    "class": "InitialWorkDirRequirement",
                    "listing": [
                        "$(inputs.bam_file)",
                        "$(inputs.bai)"
                    ]
                }
            ],
            "inputs": [
                {
                    "type": "File",
                    "id": "#expression_bam_index.cwl/bam_file"
                },
                {
                    "type": "File",
                    "id": "#expression_bam_index.cwl/bam_index"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "id": "#expression_bam_index.cwl/hybrid_bamindex"
                }
            ],
            "expression": "${ var ret = inputs.bam_file; ret[\"secondaryFiles\"] = [\n    inputs.bam_index,\n]; return { \"hybrid_bamindex\": ret } ; }\n",
            "doc": "Creates an \"hybrid\" output with the index file as secondary file.\n\nAdapted from:\nhttps://github.com/vetscience/Assemblosis/blob/master/Run/expressiontoolbam.cwl\n\nLICENSE:\nBSD 3-Clause License\n\nCopyright (c) 2017, The University lf Melbourne\nAll rights reserved.\n\nRedistribution and use in source and binary forms, with or without\nmodification, are permitted provided that the following conditions are met:\n\n* Redistributions of source code must retain the above copyright notice, this\n  list of conditions and the following disclaimer.\n\n* Redistributions in binary form must reproduce the above copyright notice,\n  this list of conditions and the following disclaimer in the documentation\n  and/or other materials provided with the distribution.\n\n* Neither the name of the copyright holder nor the names of its\n  contributors may be used to endorse or promote products derived from\n  this software without specific prior written permission.\n\nTHIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\"\nAND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE\nIMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE\nDISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE\nFOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL\nDAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR\nSERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER\nCAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,\nOR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE\nOF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.",
            "id": "#expression_bam_index.cwl",
            "http://schema.org/citation": "https://m-unlock.nl",
            "http://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "http://schema.org/dateModified": "2024-10-07",
            "http://schema.org/dateCreated": "2020-00-00",
            "http://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "http://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "sam to sorted bam",
            "doc": "samtools view -@ $2 -hu $1 | samtools sort -@ $2 -o $3.bam\n",
            "requirements": [
                {
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "minimap_run",
                            "writable": true
                        },
                        {
                            "entryname": "script.sh",
                            "entry": "#!/bin/bash\nsamtools view -@ $2 -hu $3 | samtools sort -@ $2 -o $1.sorted.bam"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "baseCommand": [
                "bash",
                "-x",
                "script.sh"
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/samtools:1.19.2--h50ea8bc_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.19.2"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/samtools",
                                "file:///home/bart/git/cwl/tools/samtools/doi.org/10.1093/gigascience/giab008"
                            ],
                            "package": "samtools"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#sam_to_sorted-bam.cwl/identifier"
                },
                {
                    "type": "File",
                    "doc": "unsorted sam file",
                    "label": "unsorted sam file",
                    "inputBinding": {
                        "position": 3
                    },
                    "id": "#sam_to_sorted-bam.cwl/sam"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "number of cpu threads to use",
                    "label": "cpu threads",
                    "default": 1,
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#sam_to_sorted-bam.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier).sorted.bam"
                    },
                    "id": "#sam_to_sorted-bam.cwl/sortedbam"
                }
            ],
            "id": "#sam_to_sorted-bam.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/dateModified": "2022-02-22",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "samtools idxstats",
            "doc": "samtools idxstats - reports alignment summary statistics",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/samtools:1.19.2--h50ea8bc_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.19.2"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/samtools",
                                "file:///home/bart/git/cwl/tools/samtools/doi.org/10.1093/gigascience/giab008"
                            ],
                            "package": "samtools"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "File",
                    "label": "Bam file",
                    "doc": "(sorted) Bam file",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#samtools_idxstats.cwl/bam_file"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#samtools_idxstats.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Number of threads to use",
                    "default": 2,
                    "inputBinding": {
                        "position": 0,
                        "prefix": "--threads"
                    },
                    "id": "#samtools_idxstats.cwl/threads"
                }
            ],
            "baseCommand": [
                "samtools",
                "idxstats"
            ],
            "stdout": "$(inputs.identifier)_contigReadCounts.tsv",
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_contigReadCounts.tsv"
                    },
                    "id": "#samtools_idxstats.cwl/contigReadCounts"
                }
            ],
            "id": "#samtools_idxstats.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2021-00-00",
            "https://schema.org/dateModified": "2022-02-22",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "samtools index",
            "doc": "samtools index - creates an index file for bam file",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/samtools:1.19.2--h50ea8bc_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.19.2"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/samtools",
                                "file:///home/bart/git/cwl/tools/samtools/doi.org/10.1093/gigascience/giab008"
                            ],
                            "package": "samtools"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "File",
                    "label": "Bam file",
                    "doc": "(sorted) Bam file",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#samtools_index.cwl/bam_file"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Number of threads to use",
                    "default": 2,
                    "inputBinding": {
                        "position": 0,
                        "prefix": "-@"
                    },
                    "id": "#samtools_index.cwl/threads"
                }
            ],
            "baseCommand": [
                "samtools",
                "index"
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.bam_file.basename).bai",
                    "position": 2
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.bam_file.basename).bai"
                    },
                    "id": "#samtools_index.cwl/bam_index"
                }
            ],
            "id": "#samtools_index.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2022-02-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Genome conversion",
            "doc": "Runs Genome conversion tool from SAPP\n",
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/sapp:2.0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "17.0.3"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/openjdk"
                            ],
                            "package": "openjdk"
                        },
                        {
                            "version": [
                                "2.0"
                            ],
                            "specs": [
                                "https://sapp.gitlab.io/docs/index.html",
                                "file:///home/bart/git/cwl/tools/sapp/doi.org/10.1101/184747"
                            ],
                            "package": "sapp"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Codon table used for this organism",
                    "label": "Codon table",
                    "inputBinding": {
                        "prefix": "-codon"
                    },
                    "id": "#conversion.cwl/codon_table"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "Reference genome file used in EMBL format",
                    "label": "Reference genome",
                    "inputBinding": {
                        "prefix": "-input"
                    },
                    "id": "#conversion.cwl/embl"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "Reference genome file used in fasta format",
                    "label": "Reference genome",
                    "inputBinding": {
                        "prefix": "-input"
                    },
                    "id": "#conversion.cwl/fasta"
                },
                {
                    "type": "string",
                    "doc": "Name of the sample being analysed",
                    "label": "Sample name",
                    "inputBinding": {
                        "prefix": "-id"
                    },
                    "id": "#conversion.cwl/identifier"
                }
            ],
            "baseCommand": [
                "java",
                "-Xmx5g",
                "-jar",
                "/SAPP-2.0.jar"
            ],
            "arguments": [
                {
                    "valueFrom": "${\n  if (inputs.embl) {\n    return '-embl2rdf';\n  }\n  if (inputs.fasta) {\n    return '-fasta2rdf';\n  }\n}\n"
                },
                {
                    "valueFrom": "${\n  if (inputs.fasta) {\n    return '-genome';\n  } \n  return null;\n}\n"
                },
                {
                    "prefix": "-output",
                    "valueFrom": "$(inputs.identifier).ttl"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier).ttl"
                    },
                    "id": "#conversion.cwl/output"
                }
            ],
            "id": "#conversion.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "SAPP conversion -kofamscan",
            "doc": "SAPP conversion from eggNOG-mapper output to GBOL RDF file (TTL)\n",
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/sapp:2.0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "17.0.3"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/openjdk"
                            ],
                            "package": "openjdk"
                        },
                        {
                            "version": [
                                "2.0"
                            ],
                            "specs": [
                                "https://sapp.gitlab.io/docs/index.html",
                                "file:///home/bart/git/cwl/tools/sapp/conversion/doi.org/10.1101/184747"
                            ],
                            "package": "sapp"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "string",
                    "label": "Output prefix",
                    "doc": "Output filename prefix. Suffix '.eggNOG.ttl' will be added.",
                    "id": "#conversion_eggnog.cwl/output_prefix"
                },
                {
                    "type": "File",
                    "label": "RDF",
                    "doc": "Input RDF file",
                    "inputBinding": {
                        "prefix": "-input"
                    },
                    "id": "#conversion_eggnog.cwl/rdf"
                },
                {
                    "type": "File",
                    "label": "KofamScan tsv",
                    "doc": "KofamScan detail-tsv output",
                    "inputBinding": {
                        "prefix": "-resultFile"
                    },
                    "id": "#conversion_eggnog.cwl/resultfile"
                }
            ],
            "arguments": [
                {
                    "prefix": "-output",
                    "valueFrom": "$(inputs.output_prefix).eggNOG.ttl"
                }
            ],
            "baseCommand": [
                "java",
                "-Xmx5G",
                "-jar",
                "/SAPP-2.0.jar",
                "-eggnog"
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_prefix).eggNOG.ttl"
                    },
                    "id": "#conversion_eggnog.cwl/eggnog_ttl"
                }
            ],
            "id": "#conversion_eggnog.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2023-09-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "SAPP conversion -interproscan",
            "doc": "SAPP conversion from InterProScan 5 output to GBOL RDF file (TTL)\n",
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/sapp:2.0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "17.0.3"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/openjdk"
                            ],
                            "package": "openjdk"
                        },
                        {
                            "version": [
                                "2.0"
                            ],
                            "specs": [
                                "https://sapp.gitlab.io/docs/index.html",
                                "file:///home/bart/git/cwl/tools/sapp/conversion/doi.org/10.1101/184747"
                            ],
                            "package": "sapp"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "string",
                    "label": "Output prefix",
                    "doc": "Output filename prefix. Suffix '.InterProScan.ttl' will be added.",
                    "id": "#conversion_interproscan.cwl/output_prefix"
                },
                {
                    "type": "File",
                    "label": "RDF",
                    "doc": "Input RDF file",
                    "inputBinding": {
                        "prefix": "-input"
                    },
                    "id": "#conversion_interproscan.cwl/rdf"
                },
                {
                    "type": "File",
                    "label": "KofamScan tsv",
                    "doc": "KofamScan detail-tsv output",
                    "inputBinding": {
                        "prefix": "-resultFile"
                    },
                    "id": "#conversion_interproscan.cwl/resultfile"
                }
            ],
            "arguments": [
                {
                    "prefix": "-output",
                    "valueFrom": "$(inputs.output_prefix).InterProScan.ttl"
                }
            ],
            "baseCommand": [
                "java",
                "-Xmx5G",
                "-jar",
                "/SAPP-2.0.jar",
                "-interpro"
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_prefix).InterProScan.ttl"
                    },
                    "id": "#conversion_interproscan.cwl/interproscan_ttl"
                }
            ],
            "id": "#conversion_interproscan.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2023-09-00",
            "https://schema.org/dateModified": "2024-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "SAPP conversion -kofamscan",
            "doc": "SAPP conversion from KoFamScan/KoFamKOALA output to GBOL RDF file (TTL)\n",
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/sapp:2.0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "17.0.3"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/openjdk"
                            ],
                            "package": "openjdk"
                        },
                        {
                            "version": [
                                "2.0"
                            ],
                            "specs": [
                                "https://sapp.gitlab.io/docs/index.html",
                                "file:///home/bart/git/cwl/tools/sapp/conversion/doi.org/10.1101/184747"
                            ],
                            "package": "sapp"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Limit",
                    "doc": "Limit the number of hits per locus tag (0=no limit) (optional). Default 0",
                    "inputBinding": {
                        "prefix": "-limit"
                    },
                    "default": 0,
                    "id": "#conversion_kofamscan.cwl/limit"
                },
                {
                    "type": "string",
                    "label": "Output prefix",
                    "doc": "Output filename prefix. Suffix '.KoFamKOALA.ttl' will be added.",
                    "id": "#conversion_kofamscan.cwl/output_prefix"
                },
                {
                    "type": "File",
                    "label": "RDF",
                    "doc": "Input RDF file",
                    "inputBinding": {
                        "prefix": "-input"
                    },
                    "id": "#conversion_kofamscan.cwl/rdf"
                },
                {
                    "type": "File",
                    "label": "KofamScan tsv",
                    "doc": "KofamScan detail-tsv output",
                    "inputBinding": {
                        "prefix": "-resultFile"
                    },
                    "id": "#conversion_kofamscan.cwl/resultfile"
                }
            ],
            "arguments": [
                {
                    "prefix": "-output",
                    "valueFrom": "$(inputs.output_prefix).KoFamKOALA.ttl"
                }
            ],
            "baseCommand": [
                "java",
                "-Xmx5G",
                "-jar",
                "/SAPP-2.0.jar",
                "-kofamscan"
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_prefix).KoFamKOALA.ttl"
                    },
                    "id": "#conversion_kofamscan.cwl/kofamscan_ttl"
                }
            ],
            "id": "#conversion_kofamscan.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2023-09-00",
            "https://schema.org/dateModified": "2024-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Genome conversion",
            "doc": "Runs Genome conversion tool from SAPP\n",
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/sapp:2.0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "17.0.3"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/openjdk"
                            ],
                            "package": "openjdk"
                        },
                        {
                            "version": [
                                "2.0"
                            ],
                            "specs": [
                                "https://sapp.gitlab.io/docs/index.html",
                                "file:///home/bart/git/cwl/tools/sapp/doi.org/10.1101/184747"
                            ],
                            "package": "sapp"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "File",
                    "doc": "RDF file in any format with the right extension",
                    "label": "RDF input file (hdt, ttl, nt, etc...)",
                    "inputBinding": {
                        "prefix": "-i"
                    },
                    "id": "#toHDT.cwl/input"
                },
                {
                    "type": "string",
                    "doc": "Name of the output file with the right extension",
                    "label": "RDF output file (hdt, ttl, nt, etc...)",
                    "inputBinding": {
                        "prefix": "-o"
                    },
                    "id": "#toHDT.cwl/output"
                }
            ],
            "baseCommand": [
                "java",
                "-Xmx5g",
                "-jar",
                "/SAPP-2.0.jar",
                "-convert"
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output)"
                    },
                    "id": "#toHDT.cwl/hdt_output"
                }
            ],
            "id": "#toHDT.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "SemiBin2",
            "doc": "Single-sample Metagenomic binning with semi-supervised deep learning using information from reference genomes.",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "networkAccess": true,
                    "class": "NetworkAccess"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/semibin:2.0.2--pyhdfd78af_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.0.2"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/semibin",
                                "file:///home/bart/git/cwl/tools/semibin/doi.org/10.1038/s41467-022-29843-y"
                            ],
                            "package": "semibin"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "SemiBin2",
                "single_easy_bin"
            ],
            "inputs": [
                {
                    "type": "File",
                    "doc": "Input assembly in fasta format",
                    "label": "Input assembly",
                    "inputBinding": {
                        "prefix": "--input-fasta"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/assembly"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "Mapped reads to assembly in sorted BAM format",
                    "label": "BAM file",
                    "inputBinding": {
                        "prefix": "--input-bam"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/bam_file"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "doc": "Built-in models (human_gut/dog_gut/ocean/soil/cat_gut/human_oral/mouse_gut/pig_gut/built_environment/wastewater/chicken_caecum/global)",
                    "label": "Environment",
                    "inputBinding": {
                        "prefix": "--environment"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/environment"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "inputBinding": {
                        "prefix": "--tag-output"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "Contig depth file from MetaBAT2",
                    "label": "MetaBAT2 depths",
                    "inputBinding": {
                        "prefix": "--depth-metabat2"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/metabat2_depth_file"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "doc": "Reference Database data directory (usually, MMseqs2 GTDB)",
                    "label": "Reference Database",
                    "inputBinding": {
                        "prefix": "--reference-db"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/reference_database"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "An alternative binning algorithm for assemblies from long-read datasets.",
                    "label": "Long read assembly",
                    "inputBinding": {
                        "prefix": "--sequencing-type=long_read"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/sequencing_type_longread"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Number of threads to use",
                    "default": 1,
                    "inputBinding": {
                        "prefix": "--threads"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/threads"
                }
            ],
            "arguments": [
                {
                    "prefix": "-o",
                    "valueFrom": "$(inputs.identifier)_SemiBin"
                },
                {
                    "prefix": "--compression",
                    "valueFrom": "none"
                }
            ],
            "outputs": [
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Coverage data",
                    "doc": "Coverage data generated from depth file.",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_SemiBin/\"*_cov.csv\""
                    },
                    "id": "#semibin2_single_easy_bin.cwl/coverage"
                },
                {
                    "type": "File",
                    "label": "Training data",
                    "doc": "Data used in the training of deep learning model",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_SemiBin/data.csv"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/data"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Training data",
                    "doc": "Data used in the training of deep learning model, not generated when using MetaBAT2 depth file.",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_SemiBin/data_split.csv"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/data_split"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Bins info",
                    "doc": "Info on (reclustered) bins (contig,nbs,n50 etc..)",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_SemiBin/recluster_bins_info.tsv"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/info"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "MMseqs annotation",
                    "doc": "MMseqs contig annotation",
                    "outputBinding": {
                        "glob": "mmseqs_contig_annotation"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/mmseqs_contig_annotation"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Deep learning model",
                    "doc": "Saved semi-supervised deep learning model.",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_SemiBin/model.h5"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/model"
                },
                {
                    "type": "Directory",
                    "label": "Bins",
                    "doc": "Directory of all reconstructed bins before reclustering.",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_SemiBin/output_bins"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/output_bins"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "Reclustered Bins",
                    "doc": "Directory of all reconstructed bins after reclustering",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_SemiBin/output_recluster_bins"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/recluster_bins"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "Markers",
                    "doc": "Directory with HMM marker hits",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_SemiBin/sample0"
                    },
                    "id": "#semibin2_single_easy_bin.cwl/sample0"
                }
            ],
            "id": "#semibin2_single_easy_bin.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-09-00",
            "https://schema.org/dateModified": "2024-01-14",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "SMETANA",
            "doc": "Species METabolic interaction ANAlysis (SMETANA) is a python-based command line tool to analyse microbial communities.\nIt takes as input a microbial communtity (from a collection of genome-scale metabolic models in SBML format) and \ncomputes of several metrics that describe the potential for cross-feeding interactions between community members.\n    \n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker-private/smetana:1.2.0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.2.0"
                            ],
                            "specs": [
                                "https://pypi.org/project/smetana",
                                "file:///home/bart/git/cwl/tools/smetana/doi.org/10.1073/pnas.1421834112"
                            ],
                            "package": "python3"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "smetana"
            ],
            "outputs": [
                {
                    "label": "SMETANA output",
                    "doc": "SMETANA detailed tsv output",
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_SMETANA*"
                    },
                    "id": "#smetana.cwl/detailed_output_tsv"
                }
            ],
            "inputs": [
                {
                    "label": "Metabolic model",
                    "doc": "Multiple Metabolic models (xml format)",
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "inputBinding": {
                        "position": 10
                    },
                    "id": "#smetana.cwl/GEM"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Runs MIP/MRO and is much faster, recommended when analysing multiple communities.",
                    "label": "Detailed mode",
                    "inputBinding": {
                        "prefix": "--detailed",
                        "position": 11
                    },
                    "id": "#smetana.cwl/detailed"
                },
                {
                    "type": "string",
                    "label": "Flavor",
                    "doc": "Expected SBML flavor of the input files (cobra or fbc2)",
                    "inputBinding": {
                        "prefix": "--flavor",
                        "position": 3
                    },
                    "default": "fbc2",
                    "id": "#smetana.cwl/flavor"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Calculates all inter-species interactions (much slower), check Algorithms for details.",
                    "label": "Global mode",
                    "inputBinding": {
                        "prefix": "--global",
                        "position": 11
                    },
                    "default": false,
                    "id": "#smetana.cwl/global"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "Identifier used",
                    "id": "#smetana.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Media database",
                    "doc": "Media database file",
                    "inputBinding": {
                        "prefix": "-m",
                        "position": 1
                    },
                    "id": "#smetana.cwl/media"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Media database",
                    "doc": "Media database file",
                    "inputBinding": {
                        "prefix": "--mediadb",
                        "position": 2
                    },
                    "id": "#smetana.cwl/mediadb"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "solver",
                    "doc": "Set the solver to be used. [cplex|glpk|gurobi|glpk_exact]. default; glpk",
                    "inputBinding": {
                        "prefix": "--solver",
                        "position": 4
                    },
                    "id": "#smetana.cwl/solver"
                }
            ],
            "arguments": [
                "--verbose",
                {
                    "prefix": "--output",
                    "valueFrom": "$(inputs.identifier)_SMETANA"
                }
            ],
            "id": "#smetana.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-06-00",
            "https://schema.org/dateModified": "2023-02-20",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "SPAdes assembler",
            "doc": "SPAdes is a versatile toolkit designed for assembly and analysis of sequencing data. \nSPAdes is primarily developed for Illumina sequencing data, but can be used for IonTorrent as well. \nMost of SPAdes pipelines support hybrid mode, i.e. allow using long reads (PacBio and Oxford Nanopore) as a supplementary data.\n",
            "requirements": [
                {
                    "listing": [
                        {
                            "entryname": "input_spades.json",
                            "entry": "[\n  {\n    orientation: \"fr\",\n    type: \"paired-end\",\n    right reads: $( inputs.forward_reads.map( function(x) {return  x.path} ) ),\n    left reads: $( inputs.reverse_reads.map( function(x) {return  x.path} ) )\n  }            \n  ${\n    var pacbio=\"\"\n      if (inputs.pacbio_reads != null) {\n       pacbio+=',{ type: \"pacbio\", single reads: [\"' + inputs.pacbio_reads.map( function(x) {return  x.path} ).join('\",\"') + '\"] }' \n    }\n    return pacbio;\n  }\n  ${\n    var nanopore=\"\"\n      if (inputs.nanopore_reads != null) {\n       nanopore+=',{ type: \"nanopore\", single reads: [\"' + inputs.nanopore_reads.map( function(x) {return  x.path} ).join('\",\"') + '\"] }'\n      //  nanopore+=',{ type: \"nanopore\", single reads: [\"' + inputs.nanopore_reads.join('\",\"') + '\"] }'\n    }\n    return nanopore;\n  }\n]"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/spades:3.15.5--h95f258a_1",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "3.15.5"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/spades",
                                "file:///home/bart/git/cwl/tools/spades/doi.org/10.1002/cpbi.102"
                            ],
                            "package": "spades"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "spades.py",
                "--dataset",
                "input_spades.json"
            ],
            "arguments": [
                {
                    "valueFrom": "$(runtime.outdir)/output",
                    "prefix": "-o"
                },
                {
                    "valueFrom": "$(inputs.memory / 1000)",
                    "prefix": "--memory"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "this flag is required for IonTorrent data",
                    "label": "iontorrent data",
                    "inputBinding": {
                        "prefix": "--iontorrent"
                    },
                    "id": "#spades.cwl/IonTorrent"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "this flag is required for biosyntheticSPAdes mode",
                    "label": "biosynthetic spades mode",
                    "inputBinding": {
                        "prefix": "--bio"
                    },
                    "id": "#spades.cwl/biosyntheticSPAdes"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "doc": "The file containing the forward reads",
                    "label": "Forward reads",
                    "id": "#spades.cwl/forward_reads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "this flag is highly recommended for high-coverage isolate and multi-cell data",
                    "label": "high-coverage mode",
                    "inputBinding": {
                        "prefix": "--isolate"
                    },
                    "id": "#spades.cwl/isolate"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "doc": "List of k-mer sizes (must be odd and less than 128). Separate with comma, no space. Default 'auto'",
                    "label": "Kmer sizes",
                    "inputBinding": {
                        "prefix": "-k"
                    },
                    "id": "#spades.cwl/kmer_sizes"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Memory used in megabytes",
                    "id": "#spades.cwl/memory"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "this flag is required for metagenomic sample data",
                    "label": "metagenomics sample",
                    "inputBinding": {
                        "prefix": "--meta"
                    },
                    "id": "#spades.cwl/metagenome"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "doc": "Fastq file with Oxford NanoPore reads",
                    "label": "NanoPore reads",
                    "id": "#spades.cwl/nanopore_reads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Runs only assembling (without read error correction)",
                    "label": "Only assembler",
                    "inputBinding": {
                        "prefix": "--only-assembler"
                    },
                    "id": "#spades.cwl/only_assembler"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "doc": "Fastq file with PacBio CLR reads",
                    "label": "PacBio CLR reads",
                    "id": "#spades.cwl/pacbio_reads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "runs plasmidSPAdes pipeline for plasmid detection",
                    "label": "plasmid spades run",
                    "inputBinding": {
                        "prefix": "--plasmid"
                    },
                    "id": "#spades.cwl/plasmid"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "doc": "The file containing the reverse reads",
                    "label": "Reverse reads",
                    "id": "#spades.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "this flag is required for RNA-Seq data",
                    "label": "rnaseq data",
                    "inputBinding": {
                        "prefix": "--rna"
                    },
                    "id": "#spades.cwl/rna"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "number of threads to use",
                    "label": "threads",
                    "inputBinding": {
                        "prefix": "--threads"
                    },
                    "default": 10,
                    "id": "#spades.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "output/assembly_graph.fastg"
                    },
                    "id": "#spades.cwl/assembly_graph"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "output/assembly_graph_with_scaffolds.gfa"
                    },
                    "id": "#spades.cwl/assembly_graph_with_scaffolds"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "output/contigs.fasta"
                    },
                    "id": "#spades.cwl/contigs"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "output/contigs.paths"
                    },
                    "id": "#spades.cwl/contigs_assembly_paths"
                },
                {
                    "label": "contigs before repeat resolution",
                    "type": "File",
                    "outputBinding": {
                        "glob": "output/before_rr.fasta"
                    },
                    "id": "#spades.cwl/contigs_before_rr"
                },
                {
                    "label": "internal configuration file",
                    "type": "File",
                    "outputBinding": {
                        "glob": "output/dataset.info"
                    },
                    "id": "#spades.cwl/internal_config"
                },
                {
                    "label": "internal YAML data set file",
                    "type": "File",
                    "outputBinding": {
                        "glob": "output/input_dataset.yaml"
                    },
                    "id": "#spades.cwl/internal_dataset"
                },
                {
                    "label": "SPAdes log",
                    "type": "File",
                    "outputBinding": {
                        "glob": "output/spades.log"
                    },
                    "id": "#spades.cwl/log"
                },
                {
                    "label": "information about SPAdes parameters in this run",
                    "type": "File",
                    "outputBinding": {
                        "glob": "output/params.txt"
                    },
                    "id": "#spades.cwl/params"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "output/scaffolds.fasta"
                    },
                    "id": "#spades.cwl/scaffolds"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "output/scaffolds.paths"
                    },
                    "id": "#spades.cwl/scaffolds_assembly_paths"
                }
            ],
            "id": "#spades.cwl",
            "http://schema.org/author": [
                {
                    "class": "http://schema.org/Person",
                    "http://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "http://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "http://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "http://schema.org/Person",
                    "http://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "http://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "http://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "http://schema.org/Person",
                    "http://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "http://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "http://schema.org/name": "Bart Nijsse"
                }
            ],
            "http://schema.org/citation": "https://m-unlock.nl",
            "http://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "http://schema.org/dateCreated": "2020-00-00",
            "http://schema.org/dateModified": "2024-05-00",
            "http://schema.org/license": "https://spdx.org/licenses/CC0-1.0.html",
            "http://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Fasta statistics",
            "doc": "Fasta statistics like N50, total length, etc..",
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/raw_n50:idba-1.1.3",
                    "class": "DockerRequirement"
                }
            ],
            "baseCommand": [
                "raw_n50"
            ],
            "stdout": "$(inputs.identifier)_stats.txt",
            "inputs": [
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#raw_n50.cwl/identifier"
                },
                {
                    "type": "File",
                    "label": "Input fasta",
                    "doc": "Input multi fasta file",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#raw_n50.cwl/input_fasta"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_stats.txt"
                    },
                    "id": "#raw_n50.cwl/output"
                }
            ],
            "id": "#raw_n50.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-00-06",
            "https://schema.org/dateModified": "2024-03-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "ScatterFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                },
                {
                    "class": "SubworkflowFeatureRequirement"
                }
            ],
            "label": "Illumina read quality control, trimming and contamination filter.",
            "doc": "**Workflow for Illumina paired read quality control, trimming and filtering.**<br />\nMultiple paired datasets will be merged into single paired dataset.<br />\nSummary:\n- FastQC on raw data files<br />\n- fastp for read quality trimming<br />\n- BBduk for phiX and (optional) rRNA filtering<br />\n- Kraken2 for taxonomic classification of reads (optional)<br />\n- Bracken Bayesian Reestimation of Abundance with KrakEN (optional)<br />\n- BBmap for (contamination) filtering using given references (optional)<br />\n- FastQC on filtered (merged) data<br />\n\nOther UNLOCK workflows on WorkflowHub: https://workflowhub.eu/projects/16/workflows?view=default<br><br>\n\n**All tool CWL files and other workflows can be found here:**<br>\n  Tools: https://gitlab.com/m-unlock/cwl<br>\n  Workflows: https://gitlab.com/m-unlock/cwl/workflows<br>\n\n**How to setup and use an UNLOCK workflow:**<br>\nhttps://m-unlock.gitlab.io/docs/setup/setup.html<br>\n",
            "outputs": [
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Filtered forward read",
                    "doc": "Filtered forward read",
                    "outputSource": "#workflow_illumina_quality.cwl/out_fwd_reads/fwd_out",
                    "id": "#workflow_illumina_quality.cwl/QC_forward_reads"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Filtered reverse read",
                    "doc": "Filtered reverse read",
                    "outputSource": "#workflow_illumina_quality.cwl/out_rev_reads/rev_out",
                    "id": "#workflow_illumina_quality.cwl/QC_reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "Kraken2 folder",
                    "doc": "Folder with Kraken2 output files",
                    "outputSource": "#workflow_illumina_quality.cwl/kraken2_files_to_folder/results",
                    "id": "#workflow_illumina_quality.cwl/kraken2_folder"
                },
                {
                    "type": "Directory",
                    "label": "Filtering reports folder",
                    "doc": "Folder containing all reports of filtering and quality control",
                    "outputSource": "#workflow_illumina_quality.cwl/reports_files_to_folder/results",
                    "id": "#workflow_illumina_quality.cwl/reports_folder"
                }
            ],
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "string"
                    },
                    "label": "Bracken levels",
                    "doc": "Taxonomy levels in bracken estimate abundances on. Default runs through; [D,P,C,O,F,G,S,S1]",
                    "default": [
                        "P",
                        "C",
                        "O",
                        "F",
                        "G",
                        "S"
                    ],
                    "id": "#workflow_illumina_quality.cwl/bracken_levels"
                },
                {
                    "type": "int",
                    "label": "Input read length",
                    "doc": "Read length Needed for Bracken analysis. Need to be available in kraken2 database folder. (default 150)",
                    "default": 150,
                    "id": "#workflow_illumina_quality.cwl/bracken_read_length"
                },
                {
                    "type": "boolean",
                    "doc": "Remove exact duplicate reads with fastp. (default false)",
                    "label": "Deduplicate reads",
                    "default": false,
                    "id": "#workflow_illumina_quality.cwl/deduplicate"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Output Destination",
                    "doc": "Optional output destination only used for cwl-prov reporting.",
                    "id": "#workflow_illumina_quality.cwl/destination"
                },
                {
                    "type": "boolean",
                    "label": "Don't output reads.",
                    "doc": "Do not output filtered reads. (default false)",
                    "default": false,
                    "id": "#workflow_illumina_quality.cwl/do_not_output_filtered_reads"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "doc": "References fasta file(s) for filtering",
                    "label": "Filter reference file(s)",
                    "loadListing": "no_listing",
                    "id": "#workflow_illumina_quality.cwl/filter_references"
                },
                {
                    "type": "boolean",
                    "doc": "Optionally remove rRNA sequences from the reads (default false)",
                    "label": "filter rRNA",
                    "default": false,
                    "id": "#workflow_illumina_quality.cwl/filter_rrna"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "doc": "Forward sequence fastq file(s) locally",
                    "label": "Forward reads",
                    "loadListing": "no_listing",
                    "id": "#workflow_illumina_quality.cwl/forward_reads"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow.",
                    "label": "identifier used",
                    "id": "#workflow_illumina_quality.cwl/identifier"
                },
                {
                    "type": "boolean",
                    "doc": "Keep with reads mapped to the given reference. Default false",
                    "label": "Keep mapped reads",
                    "default": false,
                    "id": "#workflow_illumina_quality.cwl/keep_reference_mapped_reads"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Kraken2 confidence threshold",
                    "doc": "Confidence score threshold must be between [0, 1]. (default 0.0)",
                    "id": "#workflow_illumina_quality.cwl/kraken2_confidence"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "Directory"
                        }
                    ],
                    "label": "Kraken2 database",
                    "doc": "Kraken2 database location, multiple databases is possible",
                    "default": [],
                    "loadListing": "no_listing",
                    "id": "#workflow_illumina_quality.cwl/kraken2_database"
                },
                {
                    "type": "int",
                    "doc": "Maximum memory usage in MegaBytes. (default 8000)",
                    "label": "Maximum memory in MB",
                    "default": 8000,
                    "id": "#workflow_illumina_quality.cwl/memory"
                },
                {
                    "type": "boolean",
                    "label": "Kraken2 standard report",
                    "doc": "Also output Kraken2 standard report with per read classification. These can be large. (default false)",
                    "default": false,
                    "id": "#workflow_illumina_quality.cwl/output_kraken2_standard_report"
                },
                {
                    "type": "boolean",
                    "doc": "Prepare references to a single fasta file and unique headers.\nWhen false a single fasta file as reference is expected with unique headers. (default true)\n",
                    "label": "Prepare references",
                    "default": true,
                    "id": "#workflow_illumina_quality.cwl/prepare_reference"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "doc": "Reverse sequence fastq file(s) locally",
                    "label": "Reverse reads",
                    "loadListing": "no_listing",
                    "id": "#workflow_illumina_quality.cwl/reverse_reads"
                },
                {
                    "type": "boolean",
                    "label": "Run Bracken",
                    "doc": "Skip Bracken analysis. (default false)",
                    "default": false,
                    "id": "#workflow_illumina_quality.cwl/skip_bracken"
                },
                {
                    "type": "boolean",
                    "doc": "Skip kraken2 on filtered data. (default false)",
                    "label": "Skip kraken2 filtered",
                    "default": false,
                    "id": "#workflow_illumina_quality.cwl/skip_kraken2_filtered"
                },
                {
                    "type": "boolean",
                    "doc": "Skip kraken2 on unfiltered data. (default false)",
                    "label": "Skip kraken2 unfiltered",
                    "default": false,
                    "id": "#workflow_illumina_quality.cwl/skip_kraken2_unfiltered"
                },
                {
                    "type": "boolean",
                    "doc": "Skip FastQC analyses of filtered input reads (default false)",
                    "label": "Skip QC filtered",
                    "default": false,
                    "id": "#workflow_illumina_quality.cwl/skip_qc_filtered"
                },
                {
                    "type": "boolean",
                    "doc": "Skip FastQC analyses of raw input reads (default false)",
                    "label": "Skip QC unfiltered",
                    "default": false,
                    "id": "#workflow_illumina_quality.cwl/skip_qc_unfiltered"
                },
                {
                    "label": "Input URLs used for this run",
                    "doc": "A provenance element to capture the original source of the input data",
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "string"
                        }
                    ],
                    "id": "#workflow_illumina_quality.cwl/source"
                },
                {
                    "type": "int",
                    "doc": "Number of threads to use for computational processes. (default 2)",
                    "label": "Number of threads",
                    "default": 2,
                    "id": "#workflow_illumina_quality.cwl/threads"
                }
            ],
            "steps": [
                {
                    "label": "fastp",
                    "doc": "Read quality filtering and (barcode) trimming.",
                    "run": "#fastp.cwl",
                    "in": [
                        {
                            "source": "#workflow_illumina_quality.cwl/deduplicate",
                            "id": "#workflow_illumina_quality.cwl/fastp/deduplicate"
                        },
                        {
                            "source": [
                                "#workflow_illumina_quality.cwl/fastq_merge_fwd/output",
                                "#workflow_illumina_quality.cwl/fastq_fwd_array_to_file/file"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#workflow_illumina_quality.cwl/fastp/forward_reads"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/identifier",
                            "id": "#workflow_illumina_quality.cwl/fastp/identifier"
                        },
                        {
                            "source": [
                                "#workflow_illumina_quality.cwl/fastq_merge_rev/output",
                                "#workflow_illumina_quality.cwl/fastq_rev_array_to_file/file"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#workflow_illumina_quality.cwl/fastp/reverse_reads"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/threads",
                            "id": "#workflow_illumina_quality.cwl/fastp/threads"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/fastp/out_forward_reads",
                        "#workflow_illumina_quality.cwl/fastp/out_reverse_reads",
                        "#workflow_illumina_quality.cwl/fastp/html_report",
                        "#workflow_illumina_quality.cwl/fastp/json_report"
                    ],
                    "id": "#workflow_illumina_quality.cwl/fastp"
                },
                {
                    "label": "Fwd reads array to file",
                    "doc": "Forward file of single file array to file object",
                    "when": "$(inputs.forward_reads.length === 1)",
                    "run": "#array_to_file_tool.cwl",
                    "in": [
                        {
                            "source": "#workflow_illumina_quality.cwl/forward_reads",
                            "id": "#workflow_illumina_quality.cwl/fastq_fwd_array_to_file/files"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/forward_reads",
                            "id": "#workflow_illumina_quality.cwl/fastq_fwd_array_to_file/forward_reads"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/fastq_fwd_array_to_file/file"
                    ],
                    "id": "#workflow_illumina_quality.cwl/fastq_fwd_array_to_file"
                },
                {
                    "label": "Merge forward reads",
                    "doc": "Merge multiple forward fastq reads to a single file",
                    "when": "$(inputs.forward_reads.length > 1)",
                    "run": "#concatenate.cwl",
                    "in": [
                        {
                            "source": "#workflow_illumina_quality.cwl/forward_reads",
                            "id": "#workflow_illumina_quality.cwl/fastq_merge_fwd/forward_reads"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/forward_reads",
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_illumina_quality.cwl/fastq_merge_fwd/infiles"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/identifier",
                            "valueFrom": "$(self)_illumina_merged_1.fq.gz",
                            "id": "#workflow_illumina_quality.cwl/fastq_merge_fwd/outname"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/fastq_merge_fwd/output"
                    ],
                    "id": "#workflow_illumina_quality.cwl/fastq_merge_fwd"
                },
                {
                    "label": "Merge reverse reads",
                    "doc": "Merge multiple reverse fastq reads to a single file",
                    "when": "$(inputs.reverse_reads.length > 1)",
                    "run": "#concatenate.cwl",
                    "in": [
                        {
                            "source": "#workflow_illumina_quality.cwl/reverse_reads",
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_illumina_quality.cwl/fastq_merge_rev/infiles"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/identifier",
                            "valueFrom": "$(self)_illumina_merged_2.fq.gz",
                            "id": "#workflow_illumina_quality.cwl/fastq_merge_rev/outname"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/reverse_reads",
                            "id": "#workflow_illumina_quality.cwl/fastq_merge_rev/reverse_reads"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/fastq_merge_rev/output"
                    ],
                    "id": "#workflow_illumina_quality.cwl/fastq_merge_rev"
                },
                {
                    "label": "Rev reads array to file",
                    "doc": "Forward file of single file array to file object",
                    "when": "$(inputs.reverse_reads.length === 1)",
                    "run": "#array_to_file_tool.cwl",
                    "in": [
                        {
                            "source": "#workflow_illumina_quality.cwl/reverse_reads",
                            "id": "#workflow_illumina_quality.cwl/fastq_rev_array_to_file/files"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/reverse_reads",
                            "id": "#workflow_illumina_quality.cwl/fastq_rev_array_to_file/reverse_reads"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/fastq_rev_array_to_file/file"
                    ],
                    "id": "#workflow_illumina_quality.cwl/fastq_rev_array_to_file"
                },
                {
                    "label": "FastQC after",
                    "doc": "Quality assessment and report of reads",
                    "run": "#fastqc.cwl",
                    "when": "$(inputs.skip_qc_filtered == false)",
                    "in": [
                        {
                            "source": [
                                "#workflow_illumina_quality.cwl/phix_filter/out_forward_reads",
                                "#workflow_illumina_quality.cwl/phix_filter/out_reverse_reads"
                            ],
                            "id": "#workflow_illumina_quality.cwl/fastqc_illumina_after/fastq"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/skip_qc_filtered",
                            "id": "#workflow_illumina_quality.cwl/fastqc_illumina_after/skip_qc_filtered"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/threads",
                            "id": "#workflow_illumina_quality.cwl/fastqc_illumina_after/threads"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/fastqc_illumina_after/html_files",
                        "#workflow_illumina_quality.cwl/fastqc_illumina_after/zip_files"
                    ],
                    "id": "#workflow_illumina_quality.cwl/fastqc_illumina_after"
                },
                {
                    "label": "FastQC before",
                    "doc": "Quality assessment and report of reads",
                    "run": "#fastqc.cwl",
                    "when": "$(inputs.skip_qc_unfiltered == false)",
                    "in": [
                        {
                            "source": [
                                "#workflow_illumina_quality.cwl/forward_reads",
                                "#workflow_illumina_quality.cwl/reverse_reads"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_illumina_quality.cwl/fastqc_illumina_before/fastq"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/skip_qc_unfiltered",
                            "id": "#workflow_illumina_quality.cwl/fastqc_illumina_before/skip_qc_unfiltered"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/threads",
                            "id": "#workflow_illumina_quality.cwl/fastqc_illumina_before/threads"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/fastqc_illumina_before/html_files",
                        "#workflow_illumina_quality.cwl/fastqc_illumina_before/zip_files"
                    ],
                    "id": "#workflow_illumina_quality.cwl/fastqc_illumina_before"
                },
                {
                    "label": "Kraken2 unfiltered",
                    "doc": "Taxonomic classification on unfiltered files",
                    "when": "$(!inputs.skip_kraken2_filtered && inputs.kraken2_database !== null && inputs.kraken2_database.length !== 0)",
                    "run": "#workflow_kraken2-bracken.cwl",
                    "scatter": "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/kraken2_database",
                    "in": [
                        {
                            "source": "#workflow_illumina_quality.cwl/bracken_levels",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/bracken_levels"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/identifier",
                            "valueFrom": "$(self+\"_illumina_filtered\")",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/identifier"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/phix_filter/out_forward_reads",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/illumina_forward_reads"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/phix_filter/out_reverse_reads",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/illumina_reverse_reads"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/kraken2_confidence",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/kraken2_confidence"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/kraken2_database",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/kraken2_database"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/output_kraken2_standard_report",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/output_standard_report"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/bracken_read_length",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/read_length"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/skip_kraken2_filtered",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/skip_kraken2_filtered"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/threads",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/threads"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/kraken2_folder",
                        "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/bracken_folder"
                    ],
                    "id": "#workflow_illumina_quality.cwl/illumina_kraken2_filtered"
                },
                {
                    "label": "Kraken2 unfiltered",
                    "doc": "Taxonomic classification on unfiltered files",
                    "when": "$(!inputs.skip_kraken2_unfiltered && inputs.kraken2_database !== null && inputs.kraken2_database.length !== 0)",
                    "run": "#workflow_kraken2-bracken.cwl",
                    "scatter": "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/kraken2_database",
                    "in": [
                        {
                            "source": "#workflow_illumina_quality.cwl/bracken_levels",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/bracken_levels"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/identifier",
                            "valueFrom": "$(self+\"_illumina_unfiltered\")",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/identifier"
                        },
                        {
                            "source": [
                                "#workflow_illumina_quality.cwl/rrna_filter/out_forward_reads",
                                "#workflow_illumina_quality.cwl/fastp/out_forward_reads"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/illumina_forward_reads"
                        },
                        {
                            "source": [
                                "#workflow_illumina_quality.cwl/rrna_filter/out_reverse_reads",
                                "#workflow_illumina_quality.cwl/fastp/out_reverse_reads"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/illumina_reverse_reads"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/kraken2_confidence",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/kraken2_confidence"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/kraken2_database",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/kraken2_database"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/output_kraken2_standard_report",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/output_standard_report"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/bracken_read_length",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/read_length"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/skip_bracken",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/skip_bracken"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/skip_kraken2_unfiltered",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/skip_kraken2_unfiltered"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/threads",
                            "id": "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/threads"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/kraken2_folder",
                        "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/bracken_folder"
                    ],
                    "id": "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered"
                },
                {
                    "label": "Kraken2 folder",
                    "doc": "Kraken2 files to single folder",
                    "when": "$((!inputs.skip_kraken2_unfiltered || !inputs.skip_kraken2_filtered) && (!inputs.kraken2_database !== null && inputs.kraken2_database.length !== 0))",
                    "in": [
                        {
                            "default": "Illumina_Kraken2-Bracken",
                            "id": "#workflow_illumina_quality.cwl/kraken2_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/kraken2_folder",
                                "#workflow_illumina_quality.cwl/illumina_kraken2_unfiltered/bracken_folder",
                                "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/kraken2_folder",
                                "#workflow_illumina_quality.cwl/illumina_kraken2_filtered/bracken_folder"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_illumina_quality.cwl/kraken2_files_to_folder/folders"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/kraken2_database",
                            "id": "#workflow_illumina_quality.cwl/kraken2_files_to_folder/kraken2_database"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/skip_kraken2_filtered",
                            "id": "#workflow_illumina_quality.cwl/kraken2_files_to_folder/skip_kraken2_filtered"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/skip_kraken2_unfiltered",
                            "id": "#workflow_illumina_quality.cwl/kraken2_files_to_folder/skip_kraken2_unfiltered"
                        }
                    ],
                    "run": "#files_to_folder.cwl",
                    "out": [
                        "#workflow_illumina_quality.cwl/kraken2_files_to_folder/results"
                    ],
                    "id": "#workflow_illumina_quality.cwl/kraken2_files_to_folder"
                },
                {
                    "label": "Output fwd reads",
                    "doc": "Step for to output filtered reads because that is an option.",
                    "when": "$(!inputs.do_not_output_filtered_reads)",
                    "run": {
                        "class": "ExpressionTool",
                        "requirements": [
                            {
                                "class": "InlineJavascriptRequirement"
                            }
                        ],
                        "inputs": [
                            {
                                "type": "File",
                                "id": "#workflow_illumina_quality.cwl/out_fwd_reads/run/fwd_in"
                            }
                        ],
                        "outputs": [
                            {
                                "type": "File",
                                "id": "#workflow_illumina_quality.cwl/out_fwd_reads/run/fwd_out"
                            }
                        ],
                        "expression": "${ return {'fwd_out': inputs.fwd_in}; }\n"
                    },
                    "in": [
                        {
                            "source": "#workflow_illumina_quality.cwl/do_not_output_filtered_reads",
                            "id": "#workflow_illumina_quality.cwl/out_fwd_reads/do_not_output_filtered_reads"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/phix_filter/out_forward_reads",
                            "id": "#workflow_illumina_quality.cwl/out_fwd_reads/fwd_in"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/out_fwd_reads/fwd_out"
                    ],
                    "id": "#workflow_illumina_quality.cwl/out_fwd_reads"
                },
                {
                    "label": "Output rev reads",
                    "doc": "Step to output filtered reads because that is an option.",
                    "when": "$(!inputs.do_not_output_filtered_reads)",
                    "run": {
                        "class": "ExpressionTool",
                        "requirements": [
                            {
                                "class": "InlineJavascriptRequirement"
                            }
                        ],
                        "inputs": [
                            {
                                "type": "File",
                                "id": "#workflow_illumina_quality.cwl/out_rev_reads/run/rev_in"
                            }
                        ],
                        "outputs": [
                            {
                                "type": "File",
                                "id": "#workflow_illumina_quality.cwl/out_rev_reads/run/rev_out"
                            }
                        ],
                        "expression": "${ return {'rev_out': inputs.rev_in}; }\n"
                    },
                    "in": [
                        {
                            "source": "#workflow_illumina_quality.cwl/do_not_output_filtered_reads",
                            "id": "#workflow_illumina_quality.cwl/out_rev_reads/do_not_output_filtered_reads"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/phix_filter/out_reverse_reads",
                            "id": "#workflow_illumina_quality.cwl/out_rev_reads/rev_in"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/out_rev_reads/rev_out"
                    ],
                    "id": "#workflow_illumina_quality.cwl/out_rev_reads"
                },
                {
                    "label": "PhiX filter (bbduk)",
                    "doc": "Filters illumina spike-in PhiX sequences from reads using bbduk",
                    "run": "#bbduk_filter.cwl",
                    "in": [
                        {
                            "source": [
                                "#workflow_illumina_quality.cwl/reference_filter_illumina/out_forward_reads",
                                "#workflow_illumina_quality.cwl/rrna_filter/out_forward_reads",
                                "#workflow_illumina_quality.cwl/fastp/out_forward_reads"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#workflow_illumina_quality.cwl/phix_filter/forward_reads"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/identifier",
                            "valueFrom": "$(self+\"_illumina_filtered\")",
                            "id": "#workflow_illumina_quality.cwl/phix_filter/identifier"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/memory",
                            "id": "#workflow_illumina_quality.cwl/phix_filter/memory"
                        },
                        {
                            "valueFrom": "/opt/conda/opt/bbmap-39.06-0/resources/phix174_ill.ref.fa.gz",
                            "id": "#workflow_illumina_quality.cwl/phix_filter/reference"
                        },
                        {
                            "source": [
                                "#workflow_illumina_quality.cwl/reference_filter_illumina/out_reverse_reads",
                                "#workflow_illumina_quality.cwl/rrna_filter/out_reverse_reads",
                                "#workflow_illumina_quality.cwl/fastp/out_reverse_reads"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#workflow_illumina_quality.cwl/phix_filter/reverse_reads"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/threads",
                            "id": "#workflow_illumina_quality.cwl/phix_filter/threads"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/phix_filter/out_forward_reads",
                        "#workflow_illumina_quality.cwl/phix_filter/out_reverse_reads",
                        "#workflow_illumina_quality.cwl/phix_filter/summary",
                        "#workflow_illumina_quality.cwl/phix_filter/stats_file"
                    ],
                    "id": "#workflow_illumina_quality.cwl/phix_filter"
                },
                {
                    "label": "Prepare references",
                    "doc": "Prepare references to a single fasta file and unique headers",
                    "when": "$(inputs.prepare_reference && inputs.fasta_input !== null && inputs.fasta_input.length !== 0)",
                    "run": "#workflow_prepare_fasta_db.cwl",
                    "in": [
                        {
                            "source": "#workflow_illumina_quality.cwl/filter_references",
                            "id": "#workflow_illumina_quality.cwl/prepare_fasta_db/fasta_input"
                        },
                        {
                            "default": true,
                            "id": "#workflow_illumina_quality.cwl/prepare_fasta_db/make_headers_unique"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/identifier",
                            "id": "#workflow_illumina_quality.cwl/prepare_fasta_db/output_name"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/prepare_reference",
                            "id": "#workflow_illumina_quality.cwl/prepare_fasta_db/prepare_reference"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/prepare_fasta_db/fasta_db"
                    ],
                    "id": "#workflow_illumina_quality.cwl/prepare_fasta_db"
                },
                {
                    "label": "Reference array to file",
                    "doc": "Array to file object when the reference does not need to be prepared",
                    "when": "$(inputs.prepare_reference == false && inputs.files.length !== 0)",
                    "run": "#array_to_file_tool.cwl",
                    "in": [
                        {
                            "source": "#workflow_illumina_quality.cwl/filter_references",
                            "id": "#workflow_illumina_quality.cwl/reference_array_to_file/files"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/prepare_reference",
                            "id": "#workflow_illumina_quality.cwl/reference_array_to_file/prepare_reference"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/reference_array_to_file/file"
                    ],
                    "id": "#workflow_illumina_quality.cwl/reference_array_to_file"
                },
                {
                    "label": "Reference read mapping",
                    "doc": "Map reads against references using BBMap",
                    "when": "$(inputs.filter_references !== null && inputs.filter_references.length !== 0)",
                    "run": "#bbmap_filter-reads.cwl",
                    "in": [
                        {
                            "source": "#workflow_illumina_quality.cwl/filter_references",
                            "id": "#workflow_illumina_quality.cwl/reference_filter_illumina/filter_references"
                        },
                        {
                            "source": [
                                "#workflow_illumina_quality.cwl/rrna_filter/out_forward_reads",
                                "#workflow_illumina_quality.cwl/fastp/out_forward_reads"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#workflow_illumina_quality.cwl/reference_filter_illumina/forward_reads"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/identifier",
                            "valueFrom": "$(self+\"_ref-filter\")",
                            "id": "#workflow_illumina_quality.cwl/reference_filter_illumina/identifier"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/memory",
                            "id": "#workflow_illumina_quality.cwl/reference_filter_illumina/memory"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/reference_array_to_file/file",
                            "id": "#workflow_illumina_quality.cwl/reference_filter_illumina/non_prepared_reference"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/keep_reference_mapped_reads",
                            "id": "#workflow_illumina_quality.cwl/reference_filter_illumina/output_mapped"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/prepare_fasta_db/fasta_db",
                            "id": "#workflow_illumina_quality.cwl/reference_filter_illumina/prepared_reference"
                        },
                        {
                            "valueFrom": "${ var ref = null; if (inputs.prepared_reference) { ref = inputs.prepared_reference; } else if (inputs.non_prepared_reference) { ref = inputs.non_prepared_reference; } console.log(ref); return ref; }",
                            "id": "#workflow_illumina_quality.cwl/reference_filter_illumina/reference"
                        },
                        {
                            "source": [
                                "#workflow_illumina_quality.cwl/rrna_filter/out_reverse_reads",
                                "#workflow_illumina_quality.cwl/fastp/out_reverse_reads"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#workflow_illumina_quality.cwl/reference_filter_illumina/reverse_reads"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/threads",
                            "id": "#workflow_illumina_quality.cwl/reference_filter_illumina/threads"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/reference_filter_illumina/out_forward_reads",
                        "#workflow_illumina_quality.cwl/reference_filter_illumina/out_reverse_reads",
                        "#workflow_illumina_quality.cwl/reference_filter_illumina/log",
                        "#workflow_illumina_quality.cwl/reference_filter_illumina/stats",
                        "#workflow_illumina_quality.cwl/reference_filter_illumina/covstats"
                    ],
                    "id": "#workflow_illumina_quality.cwl/reference_filter_illumina"
                },
                {
                    "label": "Reports to folder",
                    "doc": "Preparation of fastp output files to a specific output folder",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "default": "Illumina_quality_reports",
                            "id": "#workflow_illumina_quality.cwl/reports_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_illumina_quality.cwl/fastqc_illumina_before/html_files",
                                "#workflow_illumina_quality.cwl/fastqc_illumina_before/zip_files",
                                "#workflow_illumina_quality.cwl/fastqc_illumina_after/html_files",
                                "#workflow_illumina_quality.cwl/fastqc_illumina_after/zip_files",
                                "#workflow_illumina_quality.cwl/fastp/html_report",
                                "#workflow_illumina_quality.cwl/fastp/json_report",
                                "#workflow_illumina_quality.cwl/reference_filter_illumina/stats",
                                "#workflow_illumina_quality.cwl/reference_filter_illumina/covstats",
                                "#workflow_illumina_quality.cwl/reference_filter_illumina/log",
                                "#workflow_illumina_quality.cwl/phix_filter/summary",
                                "#workflow_illumina_quality.cwl/phix_filter/stats_file",
                                "#workflow_illumina_quality.cwl/rrna_filter/summary",
                                "#workflow_illumina_quality.cwl/rrna_filter/stats_file"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_illumina_quality.cwl/reports_files_to_folder/files"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/reports_files_to_folder/results"
                    ],
                    "id": "#workflow_illumina_quality.cwl/reports_files_to_folder"
                },
                {
                    "label": "rRNA filter (bbduk)",
                    "doc": "Filters rRNA sequences from reads using bbduk",
                    "when": "$(inputs.filter_rrna)",
                    "run": "#bbduk_filter.cwl",
                    "in": [
                        {
                            "source": "#workflow_illumina_quality.cwl/filter_rrna",
                            "id": "#workflow_illumina_quality.cwl/rrna_filter/filter_rrna"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/fastp/out_forward_reads",
                            "id": "#workflow_illumina_quality.cwl/rrna_filter/forward_reads"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/identifier",
                            "valueFrom": "$(self+\"_rRNA-filter\")",
                            "id": "#workflow_illumina_quality.cwl/rrna_filter/identifier"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/memory",
                            "id": "#workflow_illumina_quality.cwl/rrna_filter/memory"
                        },
                        {
                            "valueFrom": "/opt/conda/opt/bbmap-39.06-0/resources/riboKmers.fa.gz",
                            "id": "#workflow_illumina_quality.cwl/rrna_filter/reference"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/fastp/out_reverse_reads",
                            "id": "#workflow_illumina_quality.cwl/rrna_filter/reverse_reads"
                        },
                        {
                            "source": "#workflow_illumina_quality.cwl/threads",
                            "id": "#workflow_illumina_quality.cwl/rrna_filter/threads"
                        }
                    ],
                    "out": [
                        "#workflow_illumina_quality.cwl/rrna_filter/out_forward_reads",
                        "#workflow_illumina_quality.cwl/rrna_filter/out_reverse_reads",
                        "#workflow_illumina_quality.cwl/rrna_filter/summary",
                        "#workflow_illumina_quality.cwl/rrna_filter/stats_file"
                    ],
                    "id": "#workflow_illumina_quality.cwl/rrna_filter"
                }
            ],
            "id": "#workflow_illumina_quality.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-06-14",
            "https://schema.org/dateModified": "2024-05-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "ScatterFeatureRequirement"
                }
            ],
            "label": "Kracken2 + Bracken",
            "doc": "Run Kraken2 Analysis + Krona visualization followed by Bracken. Currently only on illumina reads.\n",
            "outputs": [
                {
                    "type": "Directory",
                    "label": "Bracken folder",
                    "doc": "Folder with Bracken output files",
                    "outputSource": "#workflow_kraken2-bracken.cwl/files_to_folder_bracken/results",
                    "id": "#workflow_kraken2-bracken.cwl/bracken_folder"
                },
                {
                    "type": "Directory",
                    "label": "Kraken2 folder",
                    "doc": "Folder with Kraken2 output files",
                    "outputSource": "#workflow_kraken2-bracken.cwl/files_to_folder_kraken/results",
                    "id": "#workflow_kraken2-bracken.cwl/kraken2_folder"
                }
            ],
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "string"
                    },
                    "label": "Bracken levels",
                    "doc": "Taxonomy levels in bracken estimate abundances on. Default runs through; [P,C,O,F,G,S]",
                    "default": [
                        "P",
                        "C",
                        "O",
                        "F",
                        "G",
                        "S"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/bracken_levels"
                },
                {
                    "type": "int",
                    "label": "Bracken reads threshold",
                    "doc": "Number of reads required PRIOR to abundance estimation to perform reestimation in bracken. Default 0",
                    "default": 0,
                    "id": "#workflow_kraken2-bracken.cwl/bracken_reads_threshold"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Output Destination",
                    "doc": "Optional output destination only used for cwl-prov reporting.",
                    "id": "#workflow_kraken2-bracken.cwl/destination"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#workflow_kraken2-bracken.cwl/identifier"
                },
                {
                    "type": "File",
                    "doc": "Forward sequence fastq file(s) locally",
                    "label": "Forward reads",
                    "loadListing": "no_listing",
                    "id": "#workflow_kraken2-bracken.cwl/illumina_forward_reads"
                },
                {
                    "type": "File",
                    "doc": "Reverse sequence fastq file(s) locally",
                    "label": "Reverse reads",
                    "loadListing": "no_listing",
                    "id": "#workflow_kraken2-bracken.cwl/illumina_reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Kraken2 confidence threshold",
                    "doc": "Confidence score threshold (default 0.0) must be between [0, 1]",
                    "id": "#workflow_kraken2-bracken.cwl/kraken2_confidence"
                },
                {
                    "type": "Directory",
                    "label": "Kraken2 database",
                    "doc": "Kraken2 database location",
                    "loadListing": "no_listing",
                    "id": "#workflow_kraken2-bracken.cwl/kraken2_database"
                },
                {
                    "type": "boolean",
                    "label": "Kraken2 standard report",
                    "doc": "Also output Kraken2 standard report with per read classification. These can be large. (default true)",
                    "default": true,
                    "id": "#workflow_kraken2-bracken.cwl/output_standard_report"
                },
                {
                    "type": "int",
                    "label": "Read length",
                    "doc": "Read length to use in bracken",
                    "id": "#workflow_kraken2-bracken.cwl/read_length"
                },
                {
                    "type": "boolean",
                    "label": "Run Bracken",
                    "doc": "Skip Bracken analysis. Default false.",
                    "default": false,
                    "id": "#workflow_kraken2-bracken.cwl/skip_bracken"
                },
                {
                    "label": "Input URLs used for this run",
                    "doc": "A provenance element to capture the original source of the input data",
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "string"
                        }
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/source"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Number of threads to use for computational processes",
                    "label": "Number of threads",
                    "default": 2,
                    "id": "#workflow_kraken2-bracken.cwl/threads"
                }
            ],
            "steps": [
                {
                    "label": "Bracken folder",
                    "doc": "Bracken files to single folder",
                    "in": [
                        {
                            "default": "Bracken_Illumina",
                            "id": "#workflow_kraken2-bracken.cwl/files_to_folder_bracken/destination"
                        },
                        {
                            "source": [
                                "#workflow_kraken2-bracken.cwl/illumina_bracken/output_report"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_kraken2-bracken.cwl/files_to_folder_bracken/files"
                        }
                    ],
                    "run": "#files_to_folder.cwl",
                    "out": [
                        "#workflow_kraken2-bracken.cwl/files_to_folder_bracken/results"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/files_to_folder_bracken"
                },
                {
                    "label": "Kraken2 folder",
                    "doc": "Kraken2 files to single folder",
                    "in": [
                        {
                            "default": "Kraken2_Illumina",
                            "id": "#workflow_kraken2-bracken.cwl/files_to_folder_kraken/destination"
                        },
                        {
                            "source": [
                                "#workflow_kraken2-bracken.cwl/illumina_kraken2/sample_report",
                                "#workflow_kraken2-bracken.cwl/illumina_kraken2/sample_report",
                                "#workflow_kraken2-bracken.cwl/kraken2krona_txt/krona_txt",
                                "#workflow_kraken2-bracken.cwl/krona/krona_html",
                                "#workflow_kraken2-bracken.cwl/kraken2_compress/outfile"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_kraken2-bracken.cwl/files_to_folder_kraken/files"
                        }
                    ],
                    "run": "#files_to_folder.cwl",
                    "out": [
                        "#workflow_kraken2-bracken.cwl/files_to_folder_kraken/results"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/files_to_folder_kraken"
                },
                {
                    "label": "Illumina bracken",
                    "doc": "Bracken runs on Illumina reads",
                    "run": "#bracken.cwl",
                    "when": "$(!inputs.skip_bracken)",
                    "scatter": "#workflow_kraken2-bracken.cwl/illumina_bracken/level",
                    "in": [
                        {
                            "source": "#workflow_kraken2-bracken.cwl/kraken2_database",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_bracken/database"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/identifier",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_bracken/identifier"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/illumina_kraken2/sample_report",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_bracken/kraken_report"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/bracken_levels",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_bracken/level"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/read_length",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_bracken/read_length"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/skip_bracken",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_bracken/skip_bracken"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/bracken_reads_threshold",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_bracken/threshold"
                        }
                    ],
                    "out": [
                        "#workflow_kraken2-bracken.cwl/illumina_bracken/output_report"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/illumina_bracken"
                },
                {
                    "label": "Kraken2",
                    "doc": "bla",
                    "run": "#kraken2.cwl",
                    "in": [
                        {
                            "source": "#workflow_kraken2-bracken.cwl/kraken2_confidence",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2/confidence"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/kraken2_database",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2/database"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/illumina_forward_reads",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2/forward_reads"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/identifier",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2/identifier"
                        },
                        {
                            "default": true,
                            "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2/paired_end"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/illumina_reverse_reads",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2/reverse_reads"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/threads",
                            "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2/threads"
                        }
                    ],
                    "out": [
                        "#workflow_kraken2-bracken.cwl/illumina_kraken2/sample_report",
                        "#workflow_kraken2-bracken.cwl/illumina_kraken2/standard_report"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/illumina_kraken2"
                },
                {
                    "label": "Compress kraken2",
                    "doc": "Compress large kraken2 report file",
                    "when": "$(inputs.kraken2_standard_report)",
                    "run": "#pigz.cwl",
                    "in": [
                        {
                            "source": "#workflow_kraken2-bracken.cwl/illumina_kraken2/standard_report",
                            "id": "#workflow_kraken2-bracken.cwl/kraken2_compress/inputfile"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/output_standard_report",
                            "id": "#workflow_kraken2-bracken.cwl/kraken2_compress/kraken2_standard_report"
                        },
                        {
                            "source": "#workflow_kraken2-bracken.cwl/threads",
                            "id": "#workflow_kraken2-bracken.cwl/kraken2_compress/threads"
                        }
                    ],
                    "out": [
                        "#workflow_kraken2-bracken.cwl/kraken2_compress/outfile"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/kraken2_compress"
                },
                {
                    "label": "kraken2krona",
                    "doc": "Convert kraken2 report to krona format",
                    "run": "#kreport2krona.cwl",
                    "in": [
                        {
                            "source": "#workflow_kraken2-bracken.cwl/illumina_kraken2/sample_report",
                            "id": "#workflow_kraken2-bracken.cwl/kraken2krona_txt/report"
                        }
                    ],
                    "out": [
                        "#workflow_kraken2-bracken.cwl/kraken2krona_txt/krona_txt"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/kraken2krona_txt"
                },
                {
                    "label": "Krona",
                    "doc": "Krona visualization of Kraken2",
                    "run": "#krona_ktImportText.cwl",
                    "in": [
                        {
                            "source": "#workflow_kraken2-bracken.cwl/kraken2krona_txt/krona_txt",
                            "id": "#workflow_kraken2-bracken.cwl/krona/input"
                        }
                    ],
                    "out": [
                        "#workflow_kraken2-bracken.cwl/krona/krona_html"
                    ],
                    "id": "#workflow_kraken2-bracken.cwl/krona"
                }
            ],
            "id": "#workflow_kraken2-bracken.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2024-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "ScatterFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                },
                {
                    "class": "SubworkflowFeatureRequirement"
                }
            ],
            "label": "Long Read Quality Control and Filtering",
            "doc": "**Workflow for long read quality control and contamination filtering.**\n- NanoPlot before and after filtering (read quality control)\n- Filtlong filter on quality and length\n- Kraken2 taxonomic read classification (before and after filtering)\n- Minimap2 read filtering based on given references\n\nOther UNLOCK workflows on WorkflowHub: https://workflowhub.eu/projects/16/workflows?view=default<br><br>\n\n**All tool CWL files and other workflows can be found here:**<br>\n  Tools: https://gitlab.com/m-unlock/cwl/-/tree/master/cwl <br>\n  Workflows: https://gitlab.com/m-unlock/cwl/-/tree/master/cwl/workflows<br>\n\n**How to setup and use an UNLOCK workflow:**<br>\nhttps://m-unlock.gitlab.io/docs/setup/setup.html<br>\n",
            "outputs": [
                {
                    "type": "File",
                    "label": "Filtered long reads",
                    "doc": "Filtered long reads",
                    "outputSource": [
                        "#workflow_longread_quality.cwl/reference_filter_longreads/fastq",
                        "#workflow_longread_quality.cwl/filtlong/output_reads",
                        "#workflow_longread_quality.cwl/merge_longreads_fastq/output",
                        "#workflow_longread_quality.cwl/longreads_array_to_file/file"
                    ],
                    "pickValue": "first_non_null",
                    "id": "#workflow_longread_quality.cwl/filtered_reads"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Filtlong log",
                    "doc": "Log file from filtlong longread filtering",
                    "outputSource": "#workflow_longread_quality.cwl/filtlong/log",
                    "id": "#workflow_longread_quality.cwl/filtlong_log"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "Kraken2 folder",
                    "doc": "Folder with Kraken2 output files",
                    "outputSource": "#workflow_longread_quality.cwl/kraken2_files_to_folder/results",
                    "id": "#workflow_longread_quality.cwl/kraken2_folder"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "NanoPlot filtered",
                    "doc": "Folder with quality plots from Nanoplot after filtering",
                    "outputSource": "#workflow_longread_quality.cwl/nanoplot_filtered_files_to_folder/results",
                    "id": "#workflow_longread_quality.cwl/nanoplot_filtered_folder"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "NanoPlot unfiltered",
                    "doc": "Folder with quality plots from Nanoplot before filtering",
                    "outputSource": "#workflow_longread_quality.cwl/nanoplot_unfiltered_files_to_folder/results",
                    "id": "#workflow_longread_quality.cwl/nanoplot_unfiltered_folder"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Reference filter log",
                    "doc": "Log file from minimap2 from reference filter.",
                    "outputSource": "#workflow_longread_quality.cwl/reference_filter_longreads/log",
                    "id": "#workflow_longread_quality.cwl/reference_filter_longreads_log"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Output Destination",
                    "doc": "Optional Output destination used for cwl-prov reporting.",
                    "id": "#workflow_longread_quality.cwl/destination"
                },
                {
                    "type": "boolean",
                    "doc": "Input fastq is generated by albacore, MinKNOW or guppy  with additional information concerning channel and time. \nUsed to creating more informative quality plots (default false)\n",
                    "label": "Fastq rich (ONT)",
                    "default": false,
                    "id": "#workflow_longread_quality.cwl/fastq_rich"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "doc": "Contamination references fasta file for contamination filtering. Gzipped or not (Not mixed)",
                    "label": "Contamination reference file",
                    "id": "#workflow_longread_quality.cwl/filter_references"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#workflow_longread_quality.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "doc": "Maximum read length threshold (default 90)",
                    "label": "Maximum read length threshold",
                    "default": 90,
                    "id": "#workflow_longread_quality.cwl/keep_percent"
                },
                {
                    "type": "boolean",
                    "doc": "Keep only reads mapped to the given reference (default false)",
                    "label": "Keep mapped reads",
                    "default": false,
                    "id": "#workflow_longread_quality.cwl/keep_reference_mapped_reads"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Kraken2 confidence threshold",
                    "doc": "Confidence score threshold (default 0.0) must be between [0, 1]",
                    "id": "#workflow_longread_quality.cwl/kraken2_confidence"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "Directory"
                        }
                    ],
                    "label": "Kraken2 database",
                    "doc": "Kraken2 database location, multiple databases is possible",
                    "default": [],
                    "id": "#workflow_longread_quality.cwl/kraken2_database"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "doc": "Weight given to the length score (default 10)",
                    "label": "Length weigth",
                    "default": 10,
                    "id": "#workflow_longread_quality.cwl/length_weight"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "doc": "Long read sequence file locally fastq format",
                    "label": "Long reads",
                    "id": "#workflow_longread_quality.cwl/longreads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Maximum memory usage in megabytes",
                    "label": "Maximum memory in MB",
                    "default": 4000,
                    "id": "#workflow_longread_quality.cwl/memory"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Minimum read length threshold (default 1000)",
                    "label": "Minimum read length",
                    "default": 1000,
                    "id": "#workflow_longread_quality.cwl/minimum_length"
                },
                {
                    "type": "boolean",
                    "label": "Kraken2 standard report",
                    "doc": "Also output Kraken2 standard report with per read classification. These can be large. (default false)",
                    "default": false,
                    "id": "#workflow_longread_quality.cwl/output_kraken2_standard_report"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Prepare reference with unique headers (default true)",
                    "label": "Prepare references",
                    "default": true,
                    "id": "#workflow_longread_quality.cwl/prepare_reference"
                },
                {
                    "type": [
                        {
                            "type": "enum",
                            "symbols": [
                                "#workflow_longread_quality.cwl/readtype/Nanopore",
                                "#workflow_longread_quality.cwl/readtype/PacBio"
                            ]
                        }
                    ],
                    "doc": "Type of read PacBio or Nanopore.",
                    "label": "Read type",
                    "id": "#workflow_longread_quality.cwl/readtype"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Skip kraken2 on filtered data. Default false",
                    "label": "Skip kraken2 filtered",
                    "default": false,
                    "id": "#workflow_longread_quality.cwl/skip_kraken2_filtered"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Skip kraken2 on unfiltered data. Default false",
                    "label": "Skip kraken2 unfiltered",
                    "default": false,
                    "id": "#workflow_longread_quality.cwl/skip_kraken2_unfiltered"
                },
                {
                    "type": "boolean",
                    "doc": "Skip NanoPlot analyses of filter input data (default false)",
                    "label": "Skip NanoPlot after",
                    "default": false,
                    "id": "#workflow_longread_quality.cwl/skip_qc_filtered"
                },
                {
                    "type": "boolean",
                    "doc": "Skip NanoPlot analyses of unfiltered input data (default false)",
                    "label": "Skip NanoPlot before",
                    "default": false,
                    "id": "#workflow_longread_quality.cwl/skip_qc_unfiltered"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Skip quality trimming",
                    "doc": "Skip quality trimming tools (default false)",
                    "default": false,
                    "id": "#workflow_longread_quality.cwl/skip_quality_filter"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "CWL base step number",
                    "doc": "Step number for order of steps",
                    "default": 1,
                    "id": "#workflow_longread_quality.cwl/step"
                },
                {
                    "type": "int",
                    "doc": "Number of threads to use for computational processes",
                    "label": "Number of threads",
                    "default": 2,
                    "id": "#workflow_longread_quality.cwl/threads"
                }
            ],
            "steps": [
                {
                    "label": "Filtlong",
                    "doc": "Filter longreads on quality and length",
                    "run": "#filtlong.cwl",
                    "when": "$(inputs.skip_quality_filter == false)",
                    "in": [
                        {
                            "source": "#workflow_longread_quality.cwl/identifier",
                            "id": "#workflow_longread_quality.cwl/filtlong/identifier"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/keep_percent",
                            "id": "#workflow_longread_quality.cwl/filtlong/keep_percent"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/length_weight",
                            "id": "#workflow_longread_quality.cwl/filtlong/length_weight"
                        },
                        {
                            "source": [
                                "#workflow_longread_quality.cwl/merge_longreads_fastq/output",
                                "#workflow_longread_quality.cwl/longreads_array_to_file/file"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#workflow_longread_quality.cwl/filtlong/long_reads"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/minimum_length",
                            "id": "#workflow_longread_quality.cwl/filtlong/minimum_length"
                        },
                        {
                            "valueFrom": "$(inputs.identifier)_$(inputs.readtype)_filtered",
                            "id": "#workflow_longread_quality.cwl/filtlong/output_filename"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/readtype",
                            "id": "#workflow_longread_quality.cwl/filtlong/readtype"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/skip_quality_filter",
                            "id": "#workflow_longread_quality.cwl/filtlong/skip_quality_filter"
                        }
                    ],
                    "out": [
                        "#workflow_longread_quality.cwl/filtlong/output_reads",
                        "#workflow_longread_quality.cwl/filtlong/log"
                    ],
                    "id": "#workflow_longread_quality.cwl/filtlong"
                },
                {
                    "label": "Kraken2 folder",
                    "doc": "Kraken2 files to single folder",
                    "when": "$((!inputs.skip_kraken2_unfiltered || !inputs.skip_kraken2_filtered) && inputs.kraken2_database !== null && inputs.kraken2_database.length !== 0)",
                    "in": [
                        {
                            "source": "#workflow_longread_quality.cwl/readtype",
                            "valueFrom": "$(\"Kraken2_\"+self)",
                            "id": "#workflow_longread_quality.cwl/kraken2_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_longread_quality.cwl/longreads_kraken2_before/sample_report",
                                "#workflow_longread_quality.cwl/longreads_kraken2_after/sample_report",
                                "#workflow_longread_quality.cwl/longreads_kraken2_krona/krona_html",
                                "#workflow_longread_quality.cwl/longreads_kraken2_compress/outfile"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_longread_quality.cwl/kraken2_files_to_folder/files"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/kraken2_database",
                            "id": "#workflow_longread_quality.cwl/kraken2_files_to_folder/kraken2_database"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/readtype",
                            "id": "#workflow_longread_quality.cwl/kraken2_files_to_folder/readtype"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/skip_kraken2_filtered",
                            "id": "#workflow_longread_quality.cwl/kraken2_files_to_folder/skip_kraken2_filtered"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/skip_kraken2_unfiltered",
                            "id": "#workflow_longread_quality.cwl/kraken2_files_to_folder/skip_kraken2_unfiltered"
                        }
                    ],
                    "run": "#files_to_folder.cwl",
                    "out": [
                        "#workflow_longread_quality.cwl/kraken2_files_to_folder/results"
                    ],
                    "id": "#workflow_longread_quality.cwl/kraken2_files_to_folder"
                },
                {
                    "label": "Array to file",
                    "doc": "Pick first file of longreads when only 1 file is given",
                    "when": "$(inputs.longreads.length === 1)",
                    "run": "#array_to_file_tool.cwl",
                    "in": [
                        {
                            "source": "#workflow_longread_quality.cwl/longreads",
                            "id": "#workflow_longread_quality.cwl/longreads_array_to_file/files"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/longreads",
                            "id": "#workflow_longread_quality.cwl/longreads_array_to_file/longreads"
                        }
                    ],
                    "out": [
                        "#workflow_longread_quality.cwl/longreads_array_to_file/file"
                    ],
                    "id": "#workflow_longread_quality.cwl/longreads_array_to_file"
                },
                {
                    "label": "Kraken2 filtered",
                    "doc": "Taxonomic classification of FASTQ reads after filtering",
                    "when": "$(!inputs.skip_kraken2_filtered && inputs.database !== null && inputs.database.length !== 0 && (inputs.reffilt_reads !== null || inputs.filtlong_reads !== null))",
                    "run": "#kraken2.cwl",
                    "scatter": "#workflow_longread_quality.cwl/longreads_kraken2_after/database",
                    "in": [
                        {
                            "source": "#workflow_longread_quality.cwl/kraken2_confidence",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_after/confidence"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/kraken2_database",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_after/database"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/filtlong/output_reads",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_after/filtlong_reads"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/identifier",
                            "valueFrom": "$(self+\"_\"+inputs.readtype)_filtered",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_after/identifier"
                        },
                        {
                            "valueFrom": "${ if (inputs.reffilt_reads !== null) { var fastq = inputs.reffilt_reads; } else if (inputs.filtlong_reads !== null) { var fastq = inputs.filtlong_reads; } else { var fastq = null } return fastq; }",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_after/nanopore_reads"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/readtype",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_after/readtype"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/reference_filter_longreads/fastq",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_after/reffilt_reads"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/skip_kraken2_filtered",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_after/skip_kraken2_filtered"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/threads",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_after/threads"
                        }
                    ],
                    "out": [
                        "#workflow_longread_quality.cwl/longreads_kraken2_after/sample_report",
                        "#workflow_longread_quality.cwl/longreads_kraken2_after/standard_report"
                    ],
                    "id": "#workflow_longread_quality.cwl/longreads_kraken2_after"
                },
                {
                    "label": "Kraken2 unfiltered",
                    "doc": "Taxonomic classification of FASTQ reads before filtering",
                    "when": "$(!inputs.skip_kraken2_unfiltered && inputs.database !== null && inputs.database.length !== 0)",
                    "run": "#kraken2.cwl",
                    "scatter": "#workflow_longread_quality.cwl/longreads_kraken2_before/database",
                    "in": [
                        {
                            "source": "#workflow_longread_quality.cwl/kraken2_confidence",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_before/confidence"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/kraken2_database",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_before/database"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/identifier",
                            "valueFrom": "$(self+\"_\"+inputs.readtype)_unfiltered",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_before/identifier"
                        },
                        {
                            "source": [
                                "#workflow_longread_quality.cwl/merge_longreads_fastq/output",
                                "#workflow_longread_quality.cwl/longreads_array_to_file/file"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_before/nanopore_reads"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/readtype",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_before/readtype"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/skip_kraken2_unfiltered",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_before/skip_kraken2_unfiltered"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/threads",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_before/threads"
                        }
                    ],
                    "out": [
                        "#workflow_longread_quality.cwl/longreads_kraken2_before/sample_report",
                        "#workflow_longread_quality.cwl/longreads_kraken2_before/standard_report"
                    ],
                    "id": "#workflow_longread_quality.cwl/longreads_kraken2_before"
                },
                {
                    "label": "Compress kraken2",
                    "doc": "Compress large kraken2 report file",
                    "when": "$(inputs.kraken2_database !== null && inputs.kraken2_database.length !== 0  && inputs.output_kraken2_standard_report)",
                    "run": "#pigz.cwl",
                    "scatter": "#workflow_longread_quality.cwl/longreads_kraken2_compress/inputfile",
                    "in": [
                        {
                            "source": [
                                "#workflow_longread_quality.cwl/longreads_kraken2_before/standard_report",
                                "#workflow_longread_quality.cwl/longreads_kraken2_after/standard_report"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_compress/inputfile"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/kraken2_database",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_compress/kraken2_database"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/output_kraken2_standard_report",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_compress/output_kraken2_standard_report"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/threads",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_compress/threads"
                        }
                    ],
                    "out": [
                        "#workflow_longread_quality.cwl/longreads_kraken2_compress/outfile"
                    ],
                    "id": "#workflow_longread_quality.cwl/longreads_kraken2_compress"
                },
                {
                    "label": "Krona Kraken2",
                    "doc": "Visualization of kraken2 text with Krona after kronatools conversion",
                    "when": "$(inputs.kraken2_database !== null && inputs.kraken2_database.length !== 0)",
                    "run": "#krona_ktImportText.cwl",
                    "scatter": "#workflow_longread_quality.cwl/longreads_kraken2_krona/input",
                    "in": [
                        {
                            "source": [
                                "#workflow_longread_quality.cwl/longreads_kraken2krona_txt/krona_txt"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_krona/input"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/kraken2_database",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2_krona/kraken2_database"
                        }
                    ],
                    "out": [
                        "#workflow_longread_quality.cwl/longreads_kraken2_krona/krona_html"
                    ],
                    "id": "#workflow_longread_quality.cwl/longreads_kraken2_krona"
                },
                {
                    "label": "Krakentools kreport2krona.py",
                    "doc": "Creates a Krona chart from text files listing quantities and lineages.",
                    "when": "$(inputs.kraken2_database !== null && inputs.kraken2_database.length !== 0)",
                    "run": "#kreport2krona.cwl",
                    "scatter": "#workflow_longread_quality.cwl/longreads_kraken2krona_txt/report",
                    "in": [
                        {
                            "source": "#workflow_longread_quality.cwl/kraken2_database",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2krona_txt/kraken2_database"
                        },
                        {
                            "source": [
                                "#workflow_longread_quality.cwl/longreads_kraken2_before/sample_report",
                                "#workflow_longread_quality.cwl/longreads_kraken2_after/sample_report"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_longread_quality.cwl/longreads_kraken2krona_txt/report"
                        }
                    ],
                    "out": [
                        "#workflow_longread_quality.cwl/longreads_kraken2krona_txt/krona_txt"
                    ],
                    "id": "#workflow_longread_quality.cwl/longreads_kraken2krona_txt"
                },
                {
                    "label": "Merge fastq files",
                    "when": "$(inputs.longreads.length > 1)",
                    "run": "#concatenate.cwl",
                    "in": [
                        {
                            "source": "#workflow_longread_quality.cwl/identifier",
                            "id": "#workflow_longread_quality.cwl/merge_longreads_fastq/identifier"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/longreads",
                            "id": "#workflow_longread_quality.cwl/merge_longreads_fastq/infiles"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/longreads",
                            "id": "#workflow_longread_quality.cwl/merge_longreads_fastq/longreads"
                        },
                        {
                            "valueFrom": "$(inputs.identifier)_$(inputs.readtype)_merged_raw.fastq.gz",
                            "id": "#workflow_longread_quality.cwl/merge_longreads_fastq/outname"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/readtype",
                            "id": "#workflow_longread_quality.cwl/merge_longreads_fastq/readtype"
                        }
                    ],
                    "out": [
                        "#workflow_longread_quality.cwl/merge_longreads_fastq/output"
                    ],
                    "id": "#workflow_longread_quality.cwl/merge_longreads_fastq"
                },
                {
                    "label": "Nanoplot filtered folder",
                    "doc": "Nanoplot plots and files to single folder",
                    "when": "$(inputs.skip_nanoplot_filtered == false && inputs.log !== null)",
                    "in": [
                        {
                            "source": "#workflow_longread_quality.cwl/readtype",
                            "valueFrom": "$(self+\"_quality_reports_filtered\")",
                            "id": "#workflow_longread_quality.cwl/nanoplot_filtered_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/log",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/WeightedHistogramReadlength",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/WeightedLogTransformed_HistogramReadlength",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/Non_weightedHistogramReadlength",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/Non_weightedLogTransformed_HistogramReadlength",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/LengthvsQualityScatterPlot",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/Yield_By_Length",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/report",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/logfile",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/nanostats",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/pickle",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/raw_data",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/CumulativeYieldPlot_Gigabases",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/CumulativeYieldPlot_NumberOfReads",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/NumberOfReads_Over_Time",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/ActivePores_Over_Time",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/TimeLengthViolinPlot",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/TimeQualityViolinPlot"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_longread_quality.cwl/nanoplot_filtered_files_to_folder/files"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/log",
                            "id": "#workflow_longread_quality.cwl/nanoplot_filtered_files_to_folder/log"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/readtype",
                            "id": "#workflow_longread_quality.cwl/nanoplot_filtered_files_to_folder/readtype"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/skip_qc_filtered",
                            "id": "#workflow_longread_quality.cwl/nanoplot_filtered_files_to_folder/skip_nanoplot_filtered"
                        }
                    ],
                    "run": "#files_to_folder.cwl",
                    "out": [
                        "#workflow_longread_quality.cwl/nanoplot_filtered_files_to_folder/results"
                    ],
                    "id": "#workflow_longread_quality.cwl/nanoplot_filtered_files_to_folder"
                },
                {
                    "label": "NanoPlot before",
                    "doc": "NanoPlot Quality assessment and report of reads after filtering",
                    "run": "#nanoplot.cwl",
                    "when": "$(inputs.skip_nanoplot_filtered == false && inputs.fastq_files !== null)",
                    "in": [
                        {
                            "valueFrom": "${ if (inputs.reffilt_reads !== null) { var fastq = [inputs.reffilt_reads]; } else if (inputs.filtlong_reads !== null) { var fastq = [inputs.filtlong_reads]; } else { var fastq = null } return fastq; }",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/fastq_files"
                        },
                        {
                            "valueFrom": "$(inputs.identifier)_$(inputs.readtype)_filtered_",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/file_prefix"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/filtlong/output_reads",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/filtlong_reads"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/identifier",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/identifier"
                        },
                        {
                            "valueFrom": "$(inputs.identifier)_$(inputs.readtype)_filtered",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/plot_title"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/readtype",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/readtype"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/reference_filter_longreads/fastq",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/reffilt_reads"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/skip_qc_filtered",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/skip_nanoplot_filtered"
                        },
                        {
                            "default": true,
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/store_pickle"
                        },
                        {
                            "default": true,
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/store_raw"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/threads",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/threads"
                        }
                    ],
                    "out": [
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/log",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/WeightedHistogramReadlength",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/WeightedLogTransformed_HistogramReadlength",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/Non_weightedHistogramReadlength",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/Non_weightedLogTransformed_HistogramReadlength",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/LengthvsQualityScatterPlot",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/Yield_By_Length",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/report",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/logfile",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/nanostats",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/pickle",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/raw_data",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/CumulativeYieldPlot_Gigabases",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/CumulativeYieldPlot_NumberOfReads",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/NumberOfReads_Over_Time",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/ActivePores_Over_Time",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/TimeLengthViolinPlot",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_filtered/TimeQualityViolinPlot"
                    ],
                    "id": "#workflow_longread_quality.cwl/nanoplot_longreads_filtered"
                },
                {
                    "label": "NanoPlot unfiltered",
                    "doc": "NanoPlot Quality assessment and report of reads before filtering",
                    "run": "#nanoplot.cwl",
                    "when": "$(inputs.skip_nanoplot_unfiltered == false)",
                    "in": [
                        {
                            "source": "#workflow_longread_quality.cwl/longreads_array_to_file/file",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/array_to_file"
                        },
                        {
                            "valueFrom": "${ if (inputs.array_to_file !== null && inputs.fastq_rich == false) { var fastq = [inputs.array_to_file]; } else if (inputs.merged_files !== null && inputs.fastq_rich == false) { var fastq = [inputs.merged_files]; } else { var fastq = null } return fastq; }",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/fastq_files"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/fastq_rich",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/fastq_rich"
                        },
                        {
                            "valueFrom": "$(inputs.identifier)_$(inputs.readtype)_unfiltered_",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/file_prefix"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/identifier",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/identifier"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/merge_longreads_fastq/output",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/merged_files"
                        },
                        {
                            "valueFrom": "$(inputs.identifier)_$(inputs.readtype)_unfiltered",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/plot_title"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/readtype",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/readtype"
                        },
                        {
                            "valueFrom": "${ if (inputs.array_to_file !== null && inputs.fastq_rich) { var fastq = [inputs.array_to_file]; } else if (inputs.merged_files !== null && inputs.fastq_rich) { var fastq = [inputs.merged_files]; } else { var fastq = null } return fastq; }",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/rich_fastq_files"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/skip_qc_unfiltered",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/skip_nanoplot_unfiltered"
                        },
                        {
                            "default": true,
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/store_pickle"
                        },
                        {
                            "default": true,
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/store_raw"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/threads",
                            "id": "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/threads"
                        }
                    ],
                    "out": [
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/log",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/WeightedHistogramReadlength",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/WeightedLogTransformed_HistogramReadlength",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/Non_weightedHistogramReadlength",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/Non_weightedLogTransformed_HistogramReadlength",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/LengthvsQualityScatterPlot",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/Yield_By_Length",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/report",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/logfile",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/nanostats",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/pickle",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/raw_data",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/CumulativeYieldPlot_Gigabases",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/CumulativeYieldPlot_NumberOfReads",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/NumberOfReads_Over_Time",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/ActivePores_Over_Time",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/TimeLengthViolinPlot",
                        "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/TimeQualityViolinPlot"
                    ],
                    "id": "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered"
                },
                {
                    "label": "Nanoplot unfiltered folder",
                    "doc": "Nanoplot plots and files to single folder",
                    "when": "$(inputs.skip_nanoplot_unfiltered == false)",
                    "in": [
                        {
                            "source": "#workflow_longread_quality.cwl/readtype",
                            "valueFrom": "$(self+\"_quality_reports_unfiltered\")",
                            "id": "#workflow_longread_quality.cwl/nanoplot_unfiltered_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/log",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/WeightedHistogramReadlength",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/WeightedLogTransformed_HistogramReadlength",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/Non_weightedHistogramReadlength",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/Non_weightedLogTransformed_HistogramReadlength",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/LengthvsQualityScatterPlot",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/Yield_By_Length",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/report",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/logfile",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/nanostats",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/pickle",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/raw_data",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/CumulativeYieldPlot_Gigabases",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/CumulativeYieldPlot_NumberOfReads",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/NumberOfReads_Over_Time",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/ActivePores_Over_Time",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/TimeLengthViolinPlot",
                                "#workflow_longread_quality.cwl/nanoplot_longreads_unfiltered/TimeQualityViolinPlot"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_longread_quality.cwl/nanoplot_unfiltered_files_to_folder/files"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/readtype",
                            "id": "#workflow_longread_quality.cwl/nanoplot_unfiltered_files_to_folder/readtype"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/skip_qc_unfiltered",
                            "id": "#workflow_longread_quality.cwl/nanoplot_unfiltered_files_to_folder/skip_nanoplot_unfiltered"
                        }
                    ],
                    "run": "#files_to_folder.cwl",
                    "out": [
                        "#workflow_longread_quality.cwl/nanoplot_unfiltered_files_to_folder/results"
                    ],
                    "id": "#workflow_longread_quality.cwl/nanoplot_unfiltered_files_to_folder"
                },
                {
                    "label": "Prepare references",
                    "doc": "Prepare references to a single fasta file and unique headers",
                    "when": "$(inputs.fasta_input !== null && inputs.fasta_input.length !== 0)",
                    "run": "#workflow_prepare_fasta_db.cwl",
                    "in": [
                        {
                            "source": "#workflow_longread_quality.cwl/filter_references",
                            "id": "#workflow_longread_quality.cwl/prepare_fasta_db/fasta_input"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/prepare_reference",
                            "id": "#workflow_longread_quality.cwl/prepare_fasta_db/make_headers_unique"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/identifier",
                            "id": "#workflow_longread_quality.cwl/prepare_fasta_db/output_name"
                        }
                    ],
                    "out": [
                        "#workflow_longread_quality.cwl/prepare_fasta_db/fasta_db"
                    ],
                    "id": "#workflow_longread_quality.cwl/prepare_fasta_db"
                },
                {
                    "label": "Reference mapping",
                    "doc": "Removal of contaminated reads using minimap2 mapping",
                    "when": "$(inputs.filter_references !== null && inputs.filter_references.length !== 0)",
                    "run": "#minimap2_to_fastq.cwl",
                    "in": [
                        {
                            "source": "#workflow_longread_quality.cwl/filter_references",
                            "id": "#workflow_longread_quality.cwl/reference_filter_longreads/filter_references"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/identifier",
                            "valueFrom": "$(self+\"_\"+inputs.readtype)",
                            "id": "#workflow_longread_quality.cwl/reference_filter_longreads/identifier"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/keep_reference_mapped_reads",
                            "id": "#workflow_longread_quality.cwl/reference_filter_longreads/output_mapped"
                        },
                        {
                            "default": "map-ont",
                            "id": "#workflow_longread_quality.cwl/reference_filter_longreads/preset"
                        },
                        {
                            "source": [
                                "#workflow_longread_quality.cwl/filtlong/output_reads",
                                "#workflow_longread_quality.cwl/merge_longreads_fastq/output",
                                "#workflow_longread_quality.cwl/longreads_array_to_file/file"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#workflow_longread_quality.cwl/reference_filter_longreads/reads"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/readtype",
                            "id": "#workflow_longread_quality.cwl/reference_filter_longreads/readtype"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/prepare_fasta_db/fasta_db",
                            "id": "#workflow_longread_quality.cwl/reference_filter_longreads/reference"
                        },
                        {
                            "source": "#workflow_longread_quality.cwl/threads",
                            "id": "#workflow_longread_quality.cwl/reference_filter_longreads/threads"
                        }
                    ],
                    "out": [
                        "#workflow_longread_quality.cwl/reference_filter_longreads/fastq",
                        "#workflow_longread_quality.cwl/reference_filter_longreads/log"
                    ],
                    "id": "#workflow_longread_quality.cwl/reference_filter_longreads"
                }
            ],
            "id": "#workflow_longread_quality.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-06-14",
            "https://schema.org/dateModified": "2024-04-18",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "label": "Metagenomic GEM construction from assembly",
            "doc": "Workflow for Metagenomics from bins to metabolic model.<br>\nSummary\n  - Prodigal gene prediction\n  - CarveMe genome scale metabolic model reconstruction\n  - MEMOTE for metabolic model testing\n  - SMETANA Species METabolic interaction ANAlysis\n\nOther UNLOCK workflows on WorkflowHub: https://workflowhub.eu/projects/16/workflows?view=default<br><br>\n\n**All tool CWL files and other workflows can be found here:**<br>\n  Tools: https://gitlab.com/m-unlock/cwl<br>\n  Workflows: https://gitlab.com/m-unlock/cwl/workflows<br>\n\n**How to setup and use an UNLOCK workflow:**<br>\nhttps://m-unlock.gitlab.io/docs/setup/setup.html<br>\n\nNote: This workflow uses private containers due to IBM CPLEX requirement.\n\nDocker files can be found here: https://gitlab.com/m-unlock/docker/-/tree/main/cwl/builder/docker\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "ScatterFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                },
                {
                    "class": "SubworkflowFeatureRequirement"
                }
            ],
            "outputs": [
                {
                    "label": "CarveMe GEMs folder",
                    "doc": "CarveMe metabolic models folder",
                    "type": "Directory",
                    "outputSource": "#workflow_metagenomics_GEM.cwl/carveme_files_to_folder/results",
                    "id": "#workflow_metagenomics_GEM.cwl/carveme_gems_folder"
                },
                {
                    "label": "GEMstats",
                    "doc": "CarveMe GEM statistics",
                    "type": "File",
                    "outputSource": "#workflow_metagenomics_GEM.cwl/gemstats/carveme_GEMstats",
                    "id": "#workflow_metagenomics_GEM.cwl/gemstats_out"
                },
                {
                    "label": "MEMOTE outputs folder",
                    "doc": "MEMOTE outputs folder",
                    "type": "Directory",
                    "outputSource": "#workflow_metagenomics_GEM.cwl/memote_files_to_folder/results",
                    "id": "#workflow_metagenomics_GEM.cwl/memote_folder"
                },
                {
                    "label": "Protein files folder",
                    "doc": "Prodigal predicted proteins (compressed) fasta files",
                    "type": "Directory",
                    "outputSource": "#workflow_metagenomics_GEM.cwl/prodigal_files_to_folder/results",
                    "id": "#workflow_metagenomics_GEM.cwl/protein_fasta_folder"
                },
                {
                    "label": "SMETANA output",
                    "doc": "SMETANA detailed output table",
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputSource": "#workflow_metagenomics_GEM.cwl/smetana/detailed_output_tsv",
                    "id": "#workflow_metagenomics_GEM.cwl/smetana_output"
                }
            ],
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "doc": "Bin/genome fasta files",
                    "label": "Genome/bin",
                    "id": "#workflow_metagenomics_GEM.cwl/bins"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "CarveMe solver",
                    "doc": "CarveMe solver (default scip), possible to use cplex in private container (not provided in public container)",
                    "default": "scip",
                    "id": "#workflow_metagenomics_GEM.cwl/carveme_solver"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Output Destination (prov only)",
                    "doc": "Not used in this workflow. Output destination used for cwl-prov reporting only.",
                    "id": "#workflow_metagenomics_GEM.cwl/destination"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Gap fill",
                    "doc": "Gap fill model for given media",
                    "id": "#workflow_metagenomics_GEM.cwl/gapfill"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "Identifier used",
                    "id": "#workflow_metagenomics_GEM.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Media database",
                    "doc": "Media database file",
                    "id": "#workflow_metagenomics_GEM.cwl/mediadb"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "MEMOTE solver",
                    "doc": "MEMOTE solver Choice (cplex, glpk, gurobi, glpk_exact); by default glpk",
                    "default": "glpk",
                    "id": "#workflow_metagenomics_GEM.cwl/memote_solver"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "enum",
                            "symbols": [
                                "#workflow_metagenomics_GEM.cwl/mode/single",
                                "#workflow_metagenomics_GEM.cwl/mode/meta"
                            ]
                        }
                    ],
                    "label": "Prodigal mode",
                    "doc": "Prodigal mode, single or meta",
                    "default": "single",
                    "id": "#workflow_metagenomics_GEM.cwl/mode"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Run SMETANA",
                    "doc": "Run SMETANA (Species METabolic interaction ANAlysis) (default false)",
                    "default": false,
                    "id": "#workflow_metagenomics_GEM.cwl/run_smetana"
                },
                {
                    "type": "string",
                    "doc": "Solver to be used in SMETANA (now only run with cplex)",
                    "default": "cplex",
                    "id": "#workflow_metagenomics_GEM.cwl/smetana_solver"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Number of threads to use for computational processes",
                    "label": "number of threads",
                    "default": 2,
                    "id": "#workflow_metagenomics_GEM.cwl/threads"
                }
            ],
            "steps": [
                {
                    "label": "CarveMe proprietary solver",
                    "doc": "Genome-scale metabolic models reconstruction with CarveMe using proprietary solver (cplex)",
                    "run": "#carveme.cwl",
                    "when": "$(inputs.carveme_solver == \"cplex\")",
                    "scatter": [
                        "#workflow_metagenomics_GEM.cwl/carveme/protein_file"
                    ],
                    "in": [
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/carveme_solver",
                            "id": "#workflow_metagenomics_GEM.cwl/carveme/carveme_solver"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/gapfill",
                            "id": "#workflow_metagenomics_GEM.cwl/carveme/gapfill"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/mediadb",
                            "id": "#workflow_metagenomics_GEM.cwl/carveme/mediadb"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/prodigal/predicted_proteins_faa",
                            "id": "#workflow_metagenomics_GEM.cwl/carveme/protein_file"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_GEM.cwl/carveme/carveme_gem"
                    ],
                    "requirements": [
                        {
                            "dockerPull": "docker-registry.wur.nl/m-unlock/docker-private/carveme:1.6.1",
                            "class": "DockerRequirement"
                        }
                    ],
                    "id": "#workflow_metagenomics_GEM.cwl/carveme"
                },
                {
                    "label": "CarveMe opensource solver",
                    "doc": "Genome-scale metabolic models reconstruction with CarveMe using opensource solver (scip)",
                    "run": "#carveme.cwl",
                    "when": "$(inputs.carveme_solver == \"scip\")",
                    "scatter": [
                        "#workflow_metagenomics_GEM.cwl/carveme_base/protein_file"
                    ],
                    "in": [
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/carveme_solver",
                            "id": "#workflow_metagenomics_GEM.cwl/carveme_base/carveme_solver"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/gapfill",
                            "id": "#workflow_metagenomics_GEM.cwl/carveme_base/gapfill"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/mediadb",
                            "id": "#workflow_metagenomics_GEM.cwl/carveme_base/mediadb"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/prodigal/predicted_proteins_faa",
                            "id": "#workflow_metagenomics_GEM.cwl/carveme_base/protein_file"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_GEM.cwl/carveme_base/carveme_gem"
                    ],
                    "id": "#workflow_metagenomics_GEM.cwl/carveme_base"
                },
                {
                    "doc": "Preparation of workflow output files to a specific output folder",
                    "label": "CarveMe GEMs to folder",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "CarveMe_GEMs",
                            "id": "#workflow_metagenomics_GEM.cwl/carveme_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_GEM.cwl/compress_carveme/outfile"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#workflow_metagenomics_GEM.cwl/carveme_files_to_folder/files"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_GEM.cwl/carveme_files_to_folder/results"
                    ],
                    "id": "#workflow_metagenomics_GEM.cwl/carveme_files_to_folder"
                },
                {
                    "label": "Compress GEM from CarveMe",
                    "doc": "Compress CarveMe GEM",
                    "run": "#pigz.cwl",
                    "scatter": "#workflow_metagenomics_GEM.cwl/compress_carveme/inputfile",
                    "in": [
                        {
                            "source": [
                                "#workflow_metagenomics_GEM.cwl/carveme/carveme_gem",
                                "#workflow_metagenomics_GEM.cwl/carveme_base/carveme_gem"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_GEM.cwl/compress_carveme/inputfile"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/threads",
                            "id": "#workflow_metagenomics_GEM.cwl/compress_carveme/threads"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_GEM.cwl/compress_carveme/outfile"
                    ],
                    "id": "#workflow_metagenomics_GEM.cwl/compress_carveme"
                },
                {
                    "label": "Compress proteins",
                    "doc": "Compress prodigal protein files",
                    "run": "#pigz.cwl",
                    "scatter": "#workflow_metagenomics_GEM.cwl/compress_prodigal/inputfile",
                    "in": [
                        {
                            "source": [
                                "#workflow_metagenomics_GEM.cwl/prodigal/predicted_proteins_faa"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#workflow_metagenomics_GEM.cwl/compress_prodigal/inputfile"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/threads",
                            "id": "#workflow_metagenomics_GEM.cwl/compress_prodigal/threads"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_GEM.cwl/compress_prodigal/outfile"
                    ],
                    "id": "#workflow_metagenomics_GEM.cwl/compress_prodigal"
                },
                {
                    "label": "GEM stats",
                    "doc": "CarveMe GEM statistics",
                    "run": "#GEMstats.cwl",
                    "in": [
                        {
                            "source": [
                                "#workflow_metagenomics_GEM.cwl/carveme/carveme_gem",
                                "#workflow_metagenomics_GEM.cwl/carveme_base/carveme_gem"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_GEM.cwl/gemstats/carveme_gems"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/identifier",
                            "id": "#workflow_metagenomics_GEM.cwl/gemstats/identifier"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_GEM.cwl/gemstats/carveme_GEMstats"
                    ],
                    "id": "#workflow_metagenomics_GEM.cwl/gemstats"
                },
                {
                    "doc": "Preparation of workflow output files to a specific output folder",
                    "label": "MEMOTE output",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "MEMOTE",
                            "id": "#workflow_metagenomics_GEM.cwl/memote_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_GEM.cwl/memote_report_snapshot/report_html",
                                "#workflow_metagenomics_GEM.cwl/memote_run/run_json",
                                "#workflow_metagenomics_GEM.cwl/memote_report_snapshot_base/report_html",
                                "#workflow_metagenomics_GEM.cwl/memote_run_base/run_json"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_GEM.cwl/memote_files_to_folder/files"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_GEM.cwl/memote_files_to_folder/results"
                    ],
                    "id": "#workflow_metagenomics_GEM.cwl/memote_files_to_folder"
                },
                {
                    "label": "MEMOTE report snapshot proprietary",
                    "doc": "Take a snapshot of a model's state and generate a report.",
                    "run": "#memote.cwl",
                    "when": "$(inputs.solver == \"cplex\")",
                    "requirements": [
                        {
                            "dockerPull": "docker-registry.wur.nl/m-unlock/docker-private/memote:0.13.0",
                            "class": "DockerRequirement"
                        }
                    ],
                    "scatter": [
                        "#workflow_metagenomics_GEM.cwl/memote_report_snapshot/GEM"
                    ],
                    "in": [
                        {
                            "source": [
                                "#workflow_metagenomics_GEM.cwl/carveme/carveme_gem",
                                "#workflow_metagenomics_GEM.cwl/carveme_base/carveme_gem"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot/GEM"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot/report_snapshot"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot/skip_test_find_metabolites_consumed_with_closed_bounds"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot/skip_test_find_metabolites_not_consumed_with_open_bounds"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot/skip_test_find_metabolites_not_produced_with_open_bounds"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot/skip_test_find_metabolites_produced_with_closed_bounds"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/memote_solver",
                            "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot/solver"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_GEM.cwl/memote_report_snapshot/report_html"
                    ],
                    "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot"
                },
                {
                    "label": "MEMOTE report snapshot opensource",
                    "doc": "Take a snapshot of a model's state and generate a report.",
                    "run": "#memote.cwl",
                    "when": "$(inputs.solver == \"glpk\")",
                    "scatter": [
                        "#workflow_metagenomics_GEM.cwl/memote_report_snapshot_base/GEM"
                    ],
                    "in": [
                        {
                            "source": [
                                "#workflow_metagenomics_GEM.cwl/carveme/carveme_gem",
                                "#workflow_metagenomics_GEM.cwl/carveme_base/carveme_gem"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot_base/GEM"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot_base/report_snapshot"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot_base/skip_test_find_metabolites_consumed_with_closed_bounds"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot_base/skip_test_find_metabolites_not_consumed_with_open_bounds"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot_base/skip_test_find_metabolites_not_produced_with_open_bounds"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot_base/skip_test_find_metabolites_produced_with_closed_bounds"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/memote_solver",
                            "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot_base/solver"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_GEM.cwl/memote_report_snapshot_base/report_html"
                    ],
                    "id": "#workflow_metagenomics_GEM.cwl/memote_report_snapshot_base"
                },
                {
                    "label": "MEMOTE proprietary solver",
                    "doc": "MEMOTE run analsis",
                    "run": "#memote.cwl",
                    "when": "$(inputs.solver == \"cplex\")",
                    "requirements": [
                        {
                            "dockerPull": "docker-registry.wur.nl/m-unlock/docker-private/memote:0.13.0",
                            "class": "DockerRequirement"
                        }
                    ],
                    "scatter": [
                        "#workflow_metagenomics_GEM.cwl/memote_run/GEM"
                    ],
                    "in": [
                        {
                            "source": [
                                "#workflow_metagenomics_GEM.cwl/carveme/carveme_gem",
                                "#workflow_metagenomics_GEM.cwl/carveme_base/carveme_gem"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_GEM.cwl/memote_run/GEM"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_run/run"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_run/skip_test_find_metabolites_consumed_with_closed_bounds"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_run/skip_test_find_metabolites_not_consumed_with_open_bounds"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_run/skip_test_find_metabolites_not_produced_with_open_bounds"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_run/skip_test_find_metabolites_produced_with_closed_bounds"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/memote_solver",
                            "id": "#workflow_metagenomics_GEM.cwl/memote_run/solver"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_GEM.cwl/memote_run/run_json"
                    ],
                    "id": "#workflow_metagenomics_GEM.cwl/memote_run"
                },
                {
                    "label": "MEMOTE opensource solver",
                    "doc": "MEMOTE run analsis",
                    "run": "#memote.cwl",
                    "when": "$(inputs.solver == \"glpk\")",
                    "scatter": [
                        "#workflow_metagenomics_GEM.cwl/memote_run_base/GEM"
                    ],
                    "in": [
                        {
                            "source": [
                                "#workflow_metagenomics_GEM.cwl/carveme/carveme_gem",
                                "#workflow_metagenomics_GEM.cwl/carveme_base/carveme_gem"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_GEM.cwl/memote_run_base/GEM"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_run_base/run"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_run_base/skip_test_find_metabolites_consumed_with_closed_bounds"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_run_base/skip_test_find_metabolites_not_consumed_with_open_bounds"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_run_base/skip_test_find_metabolites_not_produced_with_open_bounds"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_GEM.cwl/memote_run_base/skip_test_find_metabolites_produced_with_closed_bounds"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/memote_solver",
                            "id": "#workflow_metagenomics_GEM.cwl/memote_run_base/solver"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_GEM.cwl/memote_run_base/run_json"
                    ],
                    "id": "#workflow_metagenomics_GEM.cwl/memote_run_base"
                },
                {
                    "label": "prodigal",
                    "doc": "prodigal gene/protein prediction",
                    "run": "#prodigal.cwl",
                    "scatter": [
                        "#workflow_metagenomics_GEM.cwl/prodigal/input_fasta"
                    ],
                    "in": [
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/identifier",
                            "id": "#workflow_metagenomics_GEM.cwl/prodigal/identifier"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/bins",
                            "id": "#workflow_metagenomics_GEM.cwl/prodigal/input_fasta"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/mode",
                            "id": "#workflow_metagenomics_GEM.cwl/prodigal/mode"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_GEM.cwl/prodigal/predicted_proteins_faa"
                    ],
                    "id": "#workflow_metagenomics_GEM.cwl/prodigal"
                },
                {
                    "doc": "Preparation of workflow output files to a specific output folder",
                    "label": "Prodigal proteins to folder",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "Prodigal_proteins",
                            "id": "#workflow_metagenomics_GEM.cwl/prodigal_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_GEM.cwl/compress_prodigal/outfile"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#workflow_metagenomics_GEM.cwl/prodigal_files_to_folder/files"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_GEM.cwl/prodigal_files_to_folder/results"
                    ],
                    "id": "#workflow_metagenomics_GEM.cwl/prodigal_files_to_folder"
                },
                {
                    "label": "SMETANA",
                    "doc": "Species METabolic interaction ANAlysis",
                    "when": "$(inputs.run_smetana && inputs.solver == \"cplex\")",
                    "run": "#smetana.cwl",
                    "in": [
                        {
                            "source": [
                                "#workflow_metagenomics_GEM.cwl/carveme/carveme_gem",
                                "#workflow_metagenomics_GEM.cwl/carveme_base/carveme_gem"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_GEM.cwl/smetana/GEM"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/identifier",
                            "id": "#workflow_metagenomics_GEM.cwl/smetana/identifier"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/run_smetana",
                            "id": "#workflow_metagenomics_GEM.cwl/smetana/run_smetana"
                        },
                        {
                            "source": "#workflow_metagenomics_GEM.cwl/smetana_solver",
                            "id": "#workflow_metagenomics_GEM.cwl/smetana/solver"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_GEM.cwl/smetana/detailed_output_tsv"
                    ],
                    "id": "#workflow_metagenomics_GEM.cwl/smetana"
                }
            ],
            "id": "#workflow_metagenomics_GEM.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:Changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/dateModified": "2023-03-03",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "ScatterFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                },
                {
                    "class": "SubworkflowFeatureRequirement"
                }
            ],
            "label": "(Hybrid) Metagenomics workflow",
            "doc": "**Workflow (hybrid) metagenomic assembly and binning  **<br>\n  - Workflow Illumina Quality: https://workflowhub.eu/workflows/336?version=1\t\n    - FastQC (control)\n    - fastp (quality trimming)\n    - kraken2 (taxonomy)\n    - bbmap contamination filter\n  - Workflow Longread Quality:\t\n    - NanoPlot (control)\n    - filtlong (quality trimming)\n    - kraken2 (taxonomy)\n    - minimap2 contamination filter\n  - Kraken2 taxonomic classification of FASTQ reads\n  - SPAdes/Flye (Assembly)\n  - Pilon/Medaka/PyPolCA (Assembly polishing)\n  - QUAST (Assembly quality report)\n\n  (optional)\n  - Workflow binnning https://workflowhub.eu/workflows/64?version=11\n    - Metabat2/MaxBin2/SemiBin\n    - DAS Tool\n    - CheckM\n    - BUSCO\n    - GTDB-Tk\n\n  (optional)\n  - Workflow Genome-scale metabolic models https://workflowhub.eu/workflows/372\n    - CarveMe (GEM generation)\n    - MEMOTE (GEM test suite)\n    - SMETANA (Species METabolic interaction ANAlysis)\n\nOther UNLOCK workflows on WorkflowHub: https://workflowhub.eu/projects/16/workflows?view=default<br><br>\n\n**All tool CWL files and other workflows can be found here:**<br>\n  Tools: https://gitlab.com/m-unlock/cwl/-/tree/master/cwl <br>\n  Workflows: https://gitlab.com/m-unlock/cwl/-/tree/master/cwl/workflows<br>\n\n**How to setup and use an UNLOCK workflow:**<br>\nhttps://m-unlock.gitlab.io/docs/setup/setup.html<br>\n",
            "outputs": [
                {
                    "label": "Assembly output",
                    "doc": "Output from different assembly steps",
                    "type": "Directory",
                    "outputSource": "#main/assembly_files_to_folder/results",
                    "id": "#main/assembly_output"
                },
                {
                    "label": "Binning output",
                    "doc": "Binning outputfolders",
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputSource": "#main/binning_files_to_folder/results",
                    "id": "#main/binning_output"
                },
                {
                    "label": "Community GEM output",
                    "doc": "Community GEM output folder",
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputSource": "#main/GEM_files_to_folder/results",
                    "id": "#main/gem_output"
                },
                {
                    "label": "Read filtering output",
                    "doc": "Read filtering stats + filtered reads",
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputSource": "#main/readfilter_files_to_folder/results",
                    "id": "#main/read_filtering_output"
                },
                {
                    "label": "Read filtering output",
                    "doc": "Read filtering stats + filtered reads",
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputSource": "#main/keep_readfilter_files_to_folder/results",
                    "id": "#main/read_filtering_output_keep"
                }
            ],
            "inputs": [
                {
                    "type": "boolean",
                    "label": "Annotate bins",
                    "doc": "Annotate bins. Default false",
                    "default": false,
                    "id": "#main/annotate_bins"
                },
                {
                    "type": "boolean",
                    "label": "Annotate unbinned",
                    "doc": "Annotate unbinned contigs. Will be treated as metagenome. Default false",
                    "default": false,
                    "id": "#main/annotate_unbinned"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "enum",
                            "symbols": [
                                "#main/assembly_choice/spades",
                                "#main/assembly_choice/medaka",
                                "#main/assembly_choice/flye",
                                "#main/assembly_choice/pilon",
                                "#main/assembly_choice/pypolca"
                            ]
                        }
                    ],
                    "label": "Assembly choice",
                    "doc": "User's choice of assembly for post-assembly (binning) processes ('spades', 'medaka', 'flye', 'pilon', 'pypolca'). Optional. Only one choice allowed.",
                    "id": "#main/assembly_choice"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "Bakta DB",
                    "doc": "Bakta Database directory (required when annotating bins)",
                    "id": "#main/bakta_db"
                },
                {
                    "type": "boolean",
                    "label": "Run binning workflow",
                    "doc": "Run with contig binning workflow (default false)",
                    "default": false,
                    "id": "#main/binning"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "BUSCO dataset",
                    "doc": "Path to the BUSCO dataset downloaded location",
                    "loadListing": "no_listing",
                    "id": "#main/busco_data"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "CarveMe solver",
                    "doc": "CarveMe solver (default scip), possible to use cplex in private container (not provided in public container)",
                    "id": "#main/carveme_solver"
                },
                {
                    "type": "boolean",
                    "doc": "Remove exact duplicate reads Illumina reads with fastp (default false)",
                    "label": "Deduplicate reads",
                    "default": false,
                    "id": "#main/deduplicate"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Output Destination (prov only)",
                    "doc": "Not used in this workflow. Output destination used for cwl-prov reporting only.",
                    "id": "#main/destination"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "record",
                            "name": "#main/eggnog_dbs/eggnog_dbs",
                            "fields": [
                                {
                                    "type": [
                                        "null",
                                        "Directory"
                                    ],
                                    "doc": "Directory containing all data files for the eggNOG database.",
                                    "name": "#main/eggnog_dbs/eggnog_dbs/data_dir"
                                },
                                {
                                    "type": [
                                        "null",
                                        "File"
                                    ],
                                    "doc": "eggNOG database file",
                                    "name": "#main/eggnog_dbs/eggnog_dbs/db"
                                },
                                {
                                    "type": [
                                        "null",
                                        "File"
                                    ],
                                    "doc": "eggNOG database file for diamond blast search",
                                    "name": "#main/eggnog_dbs/eggnog_dbs/diamond_db"
                                }
                            ]
                        }
                    ],
                    "id": "#main/eggnog_dbs"
                },
                {
                    "type": "boolean",
                    "doc": "Input fastq is generated by albacore, MinKNOW or guppy  with additional information concerning channel and time. \nUsed to creating more informative quality plots (default false)\n",
                    "label": "Fastq rich (ONT)",
                    "default": false,
                    "id": "#main/fastq_rich"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "doc": "Reference fasta file(s) used for pre-filtering. Can be gzipped (not mixed)",
                    "label": "Reference file(s)",
                    "loadListing": "no_listing",
                    "id": "#main/filter_references"
                },
                {
                    "type": "boolean",
                    "label": "Deterministic Flye",
                    "doc": "Perform disjointig assembly single-threaded in Flye assembler. Default false",
                    "default": false,
                    "id": "#main/flye_deterministic"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Gap fill",
                    "doc": "Gap fill model for given media",
                    "id": "#main/gapfill"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Genome Size",
                    "doc": "Estimated genome size (for example, 5m or 2.6g)",
                    "id": "#main/genome_size"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "doc": "Directory containing the GTDBTK repository",
                    "label": "gtdbtk data directory",
                    "loadListing": "no_listing",
                    "id": "#main/gtdbtk_data"
                },
                {
                    "type": "string",
                    "label": "Identifier",
                    "doc": "Identifier for this dataset used in this workflow (required)",
                    "id": "#main/identifier"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "doc": "Illumina Forward sequence file(s)",
                    "label": "Forward reads",
                    "loadListing": "no_listing",
                    "id": "#main/illumina_forward_reads"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "label": "Reverse reads",
                    "doc": "Illumina Reverse sequence file(s)",
                    "loadListing": "no_listing",
                    "id": "#main/illumina_reverse_reads"
                },
                {
                    "type": "string",
                    "label": "InterProScan applications",
                    "doc": "Comma separated list of analyses:\nFunFam,SFLD,PANTHER,Gene3D,Hamap,PRINTS,ProSiteProfiles,Coils,SUPERFAMILY,SMART,CDD,PIRSR,ProSitePatterns,AntiFam,Pfam,MobiDBLite,PIRSF,NCBIfam\ndefault Pfam,SFLD,SMART,AntiFam,NCBIfam\n",
                    "default": "Pfam",
                    "id": "#main/interproscan_applications"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "InterProScan 5 directory",
                    "doc": "Directory of the (full) InterProScan 5 program. Used for annotating bins. (optional)",
                    "id": "#main/interproscan_directory"
                },
                {
                    "type": "boolean",
                    "doc": "Keep filtered reads in the final output (default false)",
                    "label": "Keep filtered reads",
                    "default": false,
                    "id": "#main/keep_filtered_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "SAPP kofamscan limit",
                    "doc": "Limit max number of entries of kofamscan hits per locus in SAPP. Default 5",
                    "default": 5,
                    "id": "#main/kofamscan_limit_sapp"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Kraken2 confidence threshold",
                    "doc": "Confidence score threshold must be in [0, 1] (default 0.0)",
                    "id": "#main/kraken2_confidence"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "Directory"
                        }
                    ],
                    "doc": "Database location of kraken2",
                    "label": "Kraken2 database",
                    "default": [],
                    "loadListing": "no_listing",
                    "id": "#main/kraken2_database"
                },
                {
                    "type": "float",
                    "doc": "Keep only this percentage of the best reads (measured by bases) (default 90)",
                    "label": "Keep percentage",
                    "default": 90,
                    "id": "#main/longread_keep_percent"
                },
                {
                    "type": "float",
                    "doc": "Weight given to the length score (default 10)",
                    "label": "Length weigth",
                    "default": 10,
                    "id": "#main/longread_length_weight"
                },
                {
                    "type": "int",
                    "doc": "Minimum read length threshold (default 1000)",
                    "label": "Minimum read length",
                    "default": 1000,
                    "id": "#main/longread_minimum_length"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Media database",
                    "doc": "Media database file",
                    "id": "#main/mediadb"
                },
                {
                    "type": "int",
                    "doc": "Maximum memory usage in megabytes (default 8GB)",
                    "label": "Memory usage (MB)",
                    "default": 8000,
                    "id": "#main/memory"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "MEMOTE solver",
                    "doc": "MEMOTE solver Choice (cplex, glpk, gurobi, glpk_exact); by default glpk",
                    "id": "#main/memote_solver"
                },
                {
                    "type": "boolean",
                    "default": true,
                    "doc": "Metagenome option for assemblers (default true)",
                    "label": "When working with metagenomes",
                    "id": "#main/metagenome"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "label": "Oxford Nanopore reads",
                    "doc": "File(s) with Oxford Nanopore reads in FASTQ format",
                    "loadListing": "no_listing",
                    "id": "#main/nanopore_reads"
                },
                {
                    "type": "boolean",
                    "label": "Only spades assembler",
                    "doc": "Run spades in only assembler mode (without read error correction) (default false)",
                    "default": false,
                    "id": "#main/only_assembler_mode_spades"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "ONT Basecalling model for MEDAKA",
                    "doc": "Used in MEDAKA\nBasecalling model used with guppy default r941_min_high.\nAvailable: r103_fast_g507, r103_fast_snp_g507, r103_fast_variant_g507, r103_hac_g507, r103_hac_snp_g507, r103_hac_variant_g507, r103_min_high_g345, r103_min_high_g360, r103_prom_high_g360, r103_prom_snp_g3210, r103_prom_variant_g3210, r103_sup_g507, r103_sup_snp_g507, r103_sup_variant_g507, r1041_e82_400bps_fast_g615, r1041_e82_400bps_fast_variant_g615, r1041_e82_400bps_hac_g615, r1041_e82_400bps_hac_variant_g615, r1041_e82_400bps_sup_g615, r1041_e82_400bps_sup_variant_g615, r104_e81_fast_g5015, r104_e81_fast_variant_g5015, r104_e81_hac_g5015, r104_e81_hac_variant_g5015, r104_e81_sup_g5015, r104_e81_sup_g610, r104_e81_sup_variant_g610, r10_min_high_g303, r10_min_high_g340, r941_e81_fast_g514, r941_e81_fast_variant_g514, r941_e81_hac_g514, r941_e81_hac_variant_g514, r941_e81_sup_g514, r941_e81_sup_variant_g514, r941_min_fast_g303, r941_min_fast_g507, r941_min_fast_snp_g507, r941_min_fast_variant_g507, r941_min_hac_g507, r941_min_hac_snp_g507, r941_min_hac_variant_g507, r941_min_high_g303, r941_min_high_g330, r941_min_high_g340_rle, r941_min_high_g344, r941_min_high_g351, r941_min_high_g360, r941_min_sup_g507, r941_min_sup_snp_g507, r941_min_sup_variant_g507, r941_prom_fast_g303, r941_prom_fast_g507, r941_prom_fast_snp_g507, r941_prom_fast_variant_g507, r941_prom_hac_g507, r941_prom_hac_snp_g507, r941_prom_hac_variant_g507, r941_prom_high_g303, r941_prom_high_g330, r941_prom_high_g344, r941_prom_high_g360, r941_prom_high_g4011, r941_prom_snp_g303, r941_prom_snp_g322, r941_prom_snp_g360, r941_prom_sup_g507, r941_prom_sup_snp_g507, r941_prom_sup_variant_g507, r941_prom_variant_g303, r941_prom_variant_g322, r941_prom_variant_g360, r941_sup_plant_g610, r941_sup_plant_variant_g610\n(required for Medaka)\n",
                    "id": "#main/ont_basecall_model"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "label": "PacBio reads",
                    "doc": "File(s) with PacBio reads in FASTQ format",
                    "loadListing": "no_listing",
                    "id": "#main/pacbio_reads"
                },
                {
                    "type": "string",
                    "label": "Pilon fix list",
                    "doc": "A comma-separated list of categories of issues to try to fix:\n  \"snps\": try to fix individual base errors;\n  \"indels\": try to fix small indels;\n  \"gaps\": try to fill gaps;\n  \"local\": try to detect and fix local misassemblies;\n  \"all\": all of the above (default);\n  \"bases\": shorthand for \"snps\" and \"indels\" (for back compatibility);\n  default; snps,gaps,local (conservative)\n",
                    "default": "snps,gaps,local",
                    "id": "#main/pilon_fixlist"
                },
                {
                    "type": "boolean",
                    "label": "Run GEM workflow",
                    "doc": "Run the community GEnomescale Metabolic models workflow on bins. (default false)\nNOTE: Uses by default private docker containers \n",
                    "default": false,
                    "id": "#main/run_GEM"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Whether to use BinSPreader for bin refinement",
                    "default": false,
                    "id": "#main/run_binspreader"
                },
                {
                    "type": "boolean",
                    "label": "Run eggNOG-mapper",
                    "doc": "Run with eggNOG-mapper annotation. Requires eggnog database files. Default false",
                    "default": false,
                    "id": "#main/run_eggnog"
                },
                {
                    "type": "boolean",
                    "label": "Use Flye",
                    "doc": "Run with Flye assembler (default false)",
                    "default": false,
                    "id": "#main/run_flye"
                },
                {
                    "type": "boolean",
                    "label": "Run InterProScan",
                    "doc": "Run with eggNOG-mapper annotation. Requires InterProScan v5 program files. Default false",
                    "default": false,
                    "id": "#main/run_interproscan"
                },
                {
                    "type": "boolean",
                    "label": "Run kofamscan",
                    "doc": "Run with KEGG KO KoFamKOALA annotation. Default false",
                    "default": false,
                    "id": "#main/run_kofamscan"
                },
                {
                    "type": "boolean",
                    "label": "Use Medaka",
                    "doc": "Run with Mekada assembly polishing with nanopore reads (default false)",
                    "default": false,
                    "id": "#main/run_medaka"
                },
                {
                    "type": "boolean",
                    "label": "Use Pilon",
                    "doc": "Run with Pilon illumina assembly polishing (default false)",
                    "default": false,
                    "id": "#main/run_pilon"
                },
                {
                    "type": "boolean",
                    "label": "Use PyPolCA",
                    "doc": "Run with PyPolCA assembly polishing for Long-reads with illumina data (default false)",
                    "default": false,
                    "id": "#main/run_pypolca"
                },
                {
                    "type": "boolean",
                    "label": "Run SMETANA",
                    "doc": "Run SMETANA (Species METabolic interaction ANAlysis) (default false)",
                    "default": false,
                    "id": "#main/run_smetana"
                },
                {
                    "type": "boolean",
                    "label": "Use SPAdes",
                    "doc": "Run with SPAdes assembler (default true)",
                    "default": true,
                    "id": "#main/run_spades"
                },
                {
                    "type": "string",
                    "doc": "SemiBin built-in models; human_gut/dog_gut/ocean/soil/cat_gut/human_oral/mouse_gut/pig_gut/built_environment/wastewater/chicken_caecum/global (default global)",
                    "label": "SemiBin Environment",
                    "default": "global",
                    "id": "#main/semibin_environment"
                },
                {
                    "type": "boolean",
                    "label": "Skip bakta CRISPR",
                    "doc": "Skip bakta CRISPR array prediction using PILER-CR. Default false",
                    "default": false,
                    "id": "#main/skip_bakta_crispr"
                },
                {
                    "type": "boolean",
                    "label": "Run Bracken",
                    "doc": "Skip Bracken analysis. Default false.",
                    "default": false,
                    "id": "#main/skip_bracken"
                },
                {
                    "type": "boolean",
                    "doc": "Skip quality analyses of unfiltered input reads (default false)",
                    "label": "Skip QC unfiltered",
                    "default": false,
                    "id": "#main/skip_qc_unfiltered"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "doc": "Solver to be used in SMETANA (now only run with cplex)",
                    "id": "#main/smetana_solver"
                },
                {
                    "type": "int",
                    "doc": "Number of threads to use for each computational processe (default 2)",
                    "label": "Number of threads",
                    "default": 2,
                    "id": "#main/threads"
                },
                {
                    "type": "boolean",
                    "doc": "Continue with reads mapped to the given reference (default false)",
                    "label": "Keep mapped reads",
                    "default": false,
                    "id": "#main/use_reference_mapped_reads"
                }
            ],
            "steps": [
                {
                    "doc": "Preparation of GEM workflow output files and folders to a specific output folder",
                    "label": "GEM workflow output to folder",
                    "when": "$(inputs.binning && inputs.run_GEM)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "source": "#main/binning",
                            "id": "#main/GEM_files_to_folder/binning"
                        },
                        {
                            "valueFrom": "$(\"metaGEM\")",
                            "id": "#main/GEM_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/workflow_GEM/smetana_output",
                                "#main/workflow_GEM/gemstats_out"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/GEM_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#main/workflow_GEM/carveme_gems_folder",
                                "#main/workflow_GEM/protein_fasta_folder",
                                "#main/workflow_GEM/memote_folder"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/GEM_files_to_folder/folders"
                        },
                        {
                            "source": "#main/run_GEM",
                            "id": "#main/GEM_files_to_folder/run_GEM"
                        }
                    ],
                    "out": [
                        "#main/GEM_files_to_folder/results"
                    ],
                    "id": "#main/GEM_files_to_folder"
                },
                {
                    "doc": "Preparation of Flye output files to a specific output folder",
                    "label": "Flye output folder",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "$(\"Assembly\")",
                            "id": "#main/assembly_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/spades_files_to_folder/results",
                                "#main/flye_files_to_folder/results",
                                "#main/medaka_files_to_folder/results",
                                "#main/pilon_files_to_folder/results",
                                "#main/pypolca_files_to_folder/results"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/assembly_files_to_folder/folders"
                        }
                    ],
                    "out": [
                        "#main/assembly_files_to_folder/results"
                    ],
                    "id": "#main/assembly_files_to_folder"
                },
                {
                    "label": "Minimap2",
                    "doc": "Illumina read mapping using Minimap2 on assembled scaffolds",
                    "when": "$(inputs.binning && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)",
                    "run": "#minimap2_to_sorted-bam_PE.cwl",
                    "in": [
                        {
                            "source": "#main/assembly_choice",
                            "id": "#main/assembly_read_mapping_illumina/assembly_choice"
                        },
                        {
                            "source": "#main/binning",
                            "id": "#main/assembly_read_mapping_illumina/binning"
                        },
                        {
                            "source": "#main/workflow_quality_illumina/QC_forward_reads",
                            "id": "#main/assembly_read_mapping_illumina/forward_reads"
                        },
                        {
                            "source": "#main/identifier",
                            "id": "#main/assembly_read_mapping_illumina/identifier"
                        },
                        {
                            "default": "sr",
                            "id": "#main/assembly_read_mapping_illumina/preset"
                        },
                        {
                            "source": [
                                "#main/workflow_pypolca/polished_genome",
                                "#main/workflow_pilon/pilon_polished_assembly",
                                "#main/medaka/polished_assembly",
                                "#main/flye/assembly",
                                "#main/spades/scaffolds"
                            ],
                            "valueFrom": "${\n  // Mapping of user choices to assembly step outputs\n  var assemblyOutputs = {\n    // Priority: follow the order below\n    \"pypolca\": self[0],\n    \"pilon\": self[1],\n    \"medaka\": self[2],\n    \"flye\": self[3],\n    \"spades\": self[4]\n  }\n  // Check if user has made a choice \n  if (inputs.assembly_choice) {\n    return assemblyOutputs[inputs.assembly_choice];\n  } else {\n    // Default logic if no specific choice is made: first non-null\n    var assemblies = [\n      self[0],\n      self[1],\n      self[2],\n      self[3],\n      self[4]\n    ].filter(function (assembly) { return assembly !== null; });\n    return assemblies[0]; // Return the first non-null assembly\n  }\n}\n",
                            "id": "#main/assembly_read_mapping_illumina/reference"
                        },
                        {
                            "source": "#main/workflow_quality_illumina/QC_reverse_reads",
                            "id": "#main/assembly_read_mapping_illumina/reverse_reads"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/assembly_read_mapping_illumina/threads"
                        }
                    ],
                    "out": [
                        "#main/assembly_read_mapping_illumina/sorted_bam"
                    ],
                    "id": "#main/assembly_read_mapping_illumina"
                },
                {
                    "doc": "Preparation of binning output files and folders to a specific output folder",
                    "label": "Binning output to folder",
                    "when": "$(inputs.binning)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "source": "#main/binning",
                            "id": "#main/binning_files_to_folder/binning"
                        },
                        {
                            "valueFrom": "$(\"Binning\")",
                            "id": "#main/binning_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/contig_read_counts/contigReadCounts",
                                "#main/workflow_binning/bins_read_stats",
                                "#main/workflow_binning/bins_summary_table",
                                "#main/workflow_binning/eukrep_fasta",
                                "#main/workflow_binning/eukrep_stats_file"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/binning_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#main/workflow_binning/das_tool_output",
                                "#main/workflow_binning/metabat2_output",
                                "#main/workflow_binning/metabat2_binspreader_output",
                                "#main/workflow_binning/maxbin2_binspreader_output",
                                "#main/workflow_binning/semibin_binspreader_output",
                                "#main/workflow_binning/maxbin2_output",
                                "#main/workflow_binning/semibin_output",
                                "#main/workflow_binning/checkm_output",
                                "#main/workflow_binning/gtdbtk_output",
                                "#main/workflow_binning/busco_output",
                                "#main/workflow_binning/annotation_output"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/binning_files_to_folder/folders"
                        }
                    ],
                    "out": [
                        "#main/binning_files_to_folder/results"
                    ],
                    "id": "#main/binning_files_to_folder"
                },
                {
                    "label": "SPAdes compressed",
                    "doc": "Compress the large Spades assembly output files",
                    "when": "$(inputs.run_spades && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)",
                    "run": "#pigz.cwl",
                    "scatter": [
                        "#main/compress_spades/inputfile"
                    ],
                    "scatterMethod": "dotproduct",
                    "in": [
                        {
                            "source": "#main/illumina_forward_reads",
                            "id": "#main/compress_spades/forward_reads"
                        },
                        {
                            "source": [
                                "#main/spades/contigs",
                                "#main/spades/scaffolds",
                                "#main/spades/assembly_graph",
                                "#main/spades/contigs_before_rr",
                                "#main/spades/contigs_assembly_paths",
                                "#main/spades/scaffolds_assembly_paths"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/compress_spades/inputfile"
                        },
                        {
                            "source": "#main/run_spades",
                            "id": "#main/compress_spades/run_spades"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/compress_spades/threads"
                        }
                    ],
                    "out": [
                        "#main/compress_spades/outfile"
                    ],
                    "id": "#main/compress_spades"
                },
                {
                    "label": "Samtools idxstats",
                    "doc": "Reports alignment summary statistics",
                    "when": "$(inputs.binning && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)",
                    "run": "#samtools_idxstats.cwl",
                    "in": [
                        {
                            "source": "#main/assembly_read_mapping_illumina/sorted_bam",
                            "id": "#main/contig_read_counts/bam_file"
                        },
                        {
                            "source": "#main/binning",
                            "id": "#main/contig_read_counts/binning"
                        },
                        {
                            "source": "#main/illumina_forward_reads",
                            "id": "#main/contig_read_counts/forward_reads"
                        },
                        {
                            "source": "#main/identifier",
                            "id": "#main/contig_read_counts/identifier"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/contig_read_counts/threads"
                        }
                    ],
                    "out": [
                        "#main/contig_read_counts/contigReadCounts"
                    ],
                    "id": "#main/contig_read_counts"
                },
                {
                    "label": "Flye assembly",
                    "doc": "De novo assembly of single-molecule reads with Flye",
                    "when": "$(inputs.run_flye && ((inputs.nanopore_reads !== null && inputs.nanopore_reads.length !== 0) || (inputs.pacbio_reads !== null && inputs.pacbio_reads.length !== 0)))",
                    "run": "#flye.cwl",
                    "in": [
                        {
                            "source": "#main/flye_deterministic",
                            "id": "#main/flye/deterministic"
                        },
                        {
                            "source": "#main/genome_size",
                            "id": "#main/flye/genome_size"
                        },
                        {
                            "source": "#main/metagenome",
                            "id": "#main/flye/metagenome"
                        },
                        {
                            "source": "#main/workflow_quality_nanopore/filtered_reads",
                            "id": "#main/flye/nano_raw"
                        },
                        {
                            "source": "#main/nanopore_reads",
                            "id": "#main/flye/nanopore_reads"
                        },
                        {
                            "source": "#main/workflow_quality_pacbio/filtered_reads",
                            "id": "#main/flye/pacbio_raw"
                        },
                        {
                            "source": "#main/pacbio_reads",
                            "id": "#main/flye/pacbio_reads"
                        },
                        {
                            "source": "#main/run_flye",
                            "id": "#main/flye/run_flye"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/flye/threads"
                        }
                    ],
                    "out": [
                        "#main/flye/00_assembly",
                        "#main/flye/10_consensus",
                        "#main/flye/20_repeat",
                        "#main/flye/30_contigger",
                        "#main/flye/40_polishing",
                        "#main/flye/assembly",
                        "#main/flye/assembly_info",
                        "#main/flye/flye_log",
                        "#main/flye/params",
                        "#main/flye/assembly_graph"
                    ],
                    "id": "#main/flye"
                },
                {
                    "doc": "Preparation of Flye output files to a specific output folder",
                    "label": "Flye output folder",
                    "when": "$(inputs.run_flye)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "$(\"Flye_Assembly\")",
                            "id": "#main/flye_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/flye/assembly",
                                "#main/flye/assembly_info",
                                "#main/flye/flye_log",
                                "#main/flye/params",
                                "#main/flye/assembly_graph"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/flye_files_to_folder/files"
                        },
                        {
                            "source": "#main/run_flye",
                            "id": "#main/flye_files_to_folder/run_flye"
                        }
                    ],
                    "out": [
                        "#main/flye_files_to_folder/results"
                    ],
                    "id": "#main/flye_files_to_folder"
                },
                {
                    "doc": "Preparation of read filtering output files to a specific output folder",
                    "label": "Read filtering output folder",
                    "when": "$(inputs.keep_filtered_reads)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "$(\"Read_filtering_and_classification\")",
                            "id": "#main/keep_readfilter_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/workflow_quality_nanopore/filtered_reads",
                                "#main/workflow_quality_pacbio/filtered_reads",
                                "#main/workflow_quality_illumina/QC_forward_reads",
                                "#main/workflow_quality_illumina/QC_reverse_reads",
                                "#main/workflow_quality_nanopore/filtlong_log",
                                "#main/workflow_quality_nanopore/reference_filter_longreads_log",
                                "#main/workflow_quality_pacbio/filtlong_log",
                                "#main/workflow_quality_pacbio/reference_filter_longreads_log"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/keep_readfilter_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#main/workflow_quality_pacbio/kraken2_folder",
                                "#main/workflow_quality_nanopore/kraken2_folder",
                                "#main/workflow_quality_illumina/kraken2_folder",
                                "#main/workflow_quality_illumina/reports_folder",
                                "#main/workflow_quality_nanopore/nanoplot_unfiltered_folder",
                                "#main/workflow_quality_nanopore/nanoplot_filtered_folder",
                                "#main/workflow_quality_pacbio/nanoplot_unfiltered_folder",
                                "#main/workflow_quality_pacbio/nanoplot_filtered_folder"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/keep_readfilter_files_to_folder/folders"
                        },
                        {
                            "source": "#main/keep_filtered_reads",
                            "id": "#main/keep_readfilter_files_to_folder/keep_filtered_reads"
                        }
                    ],
                    "out": [
                        "#main/keep_readfilter_files_to_folder/results"
                    ],
                    "id": "#main/keep_readfilter_files_to_folder"
                },
                {
                    "label": "Medaka polishing of assembly",
                    "doc": "Medaka for (ont reads) polishing of a assembled genome",
                    "when": "$(inputs.run_medaka && inputs.nanopore_reads !== null && inputs.nanopore_reads.length !== 0)",
                    "run": "#medaka_consensus_py.cwl",
                    "in": [
                        {
                            "source": "#main/ont_basecall_model",
                            "id": "#main/medaka/basecall_model"
                        },
                        {
                            "source": [
                                "#main/flye/assembly",
                                "#main/spades/scaffolds"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#main/medaka/draft_assembly"
                        },
                        {
                            "source": "#main/nanopore_reads",
                            "id": "#main/medaka/nanopore_reads"
                        },
                        {
                            "source": "#main/workflow_quality_nanopore/filtered_reads",
                            "id": "#main/medaka/reads"
                        },
                        {
                            "source": "#main/run_medaka",
                            "id": "#main/medaka/run_medaka"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/medaka/threads"
                        }
                    ],
                    "out": [
                        "#main/medaka/polished_assembly",
                        "#main/medaka/gaps_in_draft_coords"
                    ],
                    "id": "#main/medaka"
                },
                {
                    "doc": "Preparation of Medaka output files to a specific output folder",
                    "label": "Medaka output folder",
                    "when": "$(inputs.run_medaka && inputs.nanopore_reads !== null && inputs.nanopore_reads.length !== 0)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "$(\"Medaka_assembly_polishing\")",
                            "id": "#main/medaka_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/medaka/polished_assembly",
                                "#main/medaka/gaps_in_draft_coords"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/medaka_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#main/metaquast_medaka_files_to_folder/results"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/medaka_files_to_folder/folders"
                        },
                        {
                            "source": "#main/nanopore_reads",
                            "id": "#main/medaka_files_to_folder/nanopore_reads"
                        },
                        {
                            "source": "#main/run_medaka",
                            "id": "#main/medaka_files_to_folder/run_medaka"
                        }
                    ],
                    "out": [
                        "#main/medaka_files_to_folder/results"
                    ],
                    "id": "#main/medaka_files_to_folder"
                },
                {
                    "label": "assembly evaluation",
                    "doc": "evaluation of polished assembly with metaQUAST",
                    "when": "$(inputs.run_medaka && inputs.nanopore_reads !== null && inputs.nanopore_reads.length !== 0)",
                    "run": "#metaquast.cwl",
                    "in": [
                        {
                            "source": "#main/medaka/polished_assembly",
                            "id": "#main/metaquast_medaka/assembly"
                        },
                        {
                            "source": "#main/nanopore_reads",
                            "id": "#main/metaquast_medaka/nanopore_reads"
                        },
                        {
                            "source": "#main/run_medaka",
                            "id": "#main/metaquast_medaka/run_medaka"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/metaquast_medaka/threads"
                        }
                    ],
                    "out": [
                        "#main/metaquast_medaka/metaquast_outdir",
                        "#main/metaquast_medaka/meta_combined_ref",
                        "#main/metaquast_medaka/meta_icarusDir",
                        "#main/metaquast_medaka/metaquast_krona",
                        "#main/metaquast_medaka/not_aligned",
                        "#main/metaquast_medaka/meta_downloaded_ref",
                        "#main/metaquast_medaka/runs_per_reference",
                        "#main/metaquast_medaka/meta_summary",
                        "#main/metaquast_medaka/meta_icarus",
                        "#main/metaquast_medaka/metaquast_log",
                        "#main/metaquast_medaka/metaquast_report",
                        "#main/metaquast_medaka/basicStats",
                        "#main/metaquast_medaka/quast_icarusDir",
                        "#main/metaquast_medaka/quast_icarusHtml",
                        "#main/metaquast_medaka/quastReport",
                        "#main/metaquast_medaka/quastLog",
                        "#main/metaquast_medaka/transposedReport"
                    ],
                    "id": "#main/metaquast_medaka"
                },
                {
                    "doc": "Preparation of metaQUAST output files to a specific output folder",
                    "label": "Medaka metaQUAST output folder",
                    "when": "$(inputs.run_medaka && inputs.nanopore_reads !== null && inputs.nanopore_reads.length !== 0)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "$(\"QUAST_Medaka_assembly_quality\")",
                            "id": "#main/metaquast_medaka_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/metaquast_medaka/metaquast_report",
                                "#main/metaquast_medaka/quastReport"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/metaquast_medaka_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#main/metaquast_medaka/metaquast_krona",
                                "#main/metaquast_medaka/not_aligned",
                                "#main/metaquast_medaka/runs_per_reference"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/metaquast_medaka_files_to_folder/folders"
                        },
                        {
                            "source": "#main/nanopore_reads",
                            "id": "#main/metaquast_medaka_files_to_folder/nanopore_reads"
                        },
                        {
                            "source": "#main/run_medaka",
                            "id": "#main/metaquast_medaka_files_to_folder/run_medaka"
                        }
                    ],
                    "out": [
                        "#main/metaquast_medaka_files_to_folder/results"
                    ],
                    "id": "#main/metaquast_medaka_files_to_folder"
                },
                {
                    "label": "Illumina assembly evaluation",
                    "doc": "Illumina evaluation of pilon polished assembly with metaQUAST",
                    "when": "$(inputs.run_pilon && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)",
                    "run": "#metaquast.cwl",
                    "in": [
                        {
                            "source": "#main/workflow_pilon/pilon_polished_assembly",
                            "id": "#main/metaquast_pilon/assembly"
                        },
                        {
                            "source": "#main/illumina_forward_reads",
                            "id": "#main/metaquast_pilon/forward_reads"
                        },
                        {
                            "source": "#main/run_pilon",
                            "id": "#main/metaquast_pilon/run_pilon"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/metaquast_pilon/threads"
                        }
                    ],
                    "out": [
                        "#main/metaquast_pilon/metaquast_outdir",
                        "#main/metaquast_pilon/meta_combined_ref",
                        "#main/metaquast_pilon/meta_icarusDir",
                        "#main/metaquast_pilon/metaquast_krona",
                        "#main/metaquast_pilon/not_aligned",
                        "#main/metaquast_pilon/meta_downloaded_ref",
                        "#main/metaquast_pilon/runs_per_reference",
                        "#main/metaquast_pilon/meta_summary",
                        "#main/metaquast_pilon/meta_icarus",
                        "#main/metaquast_pilon/metaquast_log",
                        "#main/metaquast_pilon/metaquast_report",
                        "#main/metaquast_pilon/basicStats",
                        "#main/metaquast_pilon/quast_icarusDir",
                        "#main/metaquast_pilon/quast_icarusHtml",
                        "#main/metaquast_pilon/quastReport",
                        "#main/metaquast_pilon/quastLog",
                        "#main/metaquast_pilon/transposedReport"
                    ],
                    "id": "#main/metaquast_pilon"
                },
                {
                    "doc": "Preparation of QUAST output files to a specific output folder",
                    "label": "Illumina metaQUAST output folder",
                    "when": "$(inputs.run_pilon && inputs.illumina_forward_reads !== null && inputs.illumina_forward_reads.length !== 0)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "$(\"QUAST_Illumina_polished_assembly_quality\")",
                            "id": "#main/metaquast_pilon_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/metaquast_pilon/metaquast_report",
                                "#main/metaquast_pilon/quastReport"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/metaquast_pilon_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#main/metaquast_pilon/metaquast_krona",
                                "#main/metaquast_pilon/not_aligned"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/metaquast_pilon_files_to_folder/folders"
                        },
                        {
                            "source": "#main/illumina_forward_reads",
                            "id": "#main/metaquast_pilon_files_to_folder/illumina_forward_reads"
                        },
                        {
                            "source": "#main/run_pilon",
                            "id": "#main/metaquast_pilon_files_to_folder/run_pilon"
                        }
                    ],
                    "out": [
                        "#main/metaquast_pilon_files_to_folder/results"
                    ],
                    "id": "#main/metaquast_pilon_files_to_folder"
                },
                {
                    "label": "Pypolca polished assembly evaluation with QUAST",
                    "doc": "Run Evaluation of PyPolCA polished assembly with metaQUAST",
                    "when": "$(inputs.run_pypolca && inputs.assembly !== null && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)",
                    "run": "#metaquast.cwl",
                    "in": [
                        {
                            "source": "#main/workflow_pypolca/polished_genome",
                            "id": "#main/metaquast_pypolca/assembly"
                        },
                        {
                            "source": "#main/illumina_forward_reads",
                            "id": "#main/metaquast_pypolca/forward_reads"
                        },
                        {
                            "source": "#main/run_pypolca",
                            "id": "#main/metaquast_pypolca/run_pypolca"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/metaquast_pypolca/threads"
                        }
                    ],
                    "out": [
                        "#main/metaquast_pypolca/metaquast_outdir",
                        "#main/metaquast_pypolca/meta_combined_ref",
                        "#main/metaquast_pypolca/meta_icarusDir",
                        "#main/metaquast_pypolca/metaquast_krona",
                        "#main/metaquast_pypolca/not_aligned",
                        "#main/metaquast_pypolca/meta_downloaded_ref",
                        "#main/metaquast_pypolca/runs_per_reference",
                        "#main/metaquast_pypolca/meta_summary",
                        "#main/metaquast_pypolca/meta_icarus",
                        "#main/metaquast_pypolca/metaquast_log",
                        "#main/metaquast_pypolca/metaquast_report",
                        "#main/metaquast_pypolca/basicStats",
                        "#main/metaquast_pypolca/quast_icarusDir",
                        "#main/metaquast_pypolca/quast_icarusHtml",
                        "#main/metaquast_pypolca/quastReport",
                        "#main/metaquast_pypolca/quastLog",
                        "#main/metaquast_pypolca/transposedReport"
                    ],
                    "id": "#main/metaquast_pypolca"
                },
                {
                    "doc": "Preparation of PyPolCA metaQUAST output files to a specific output folder",
                    "label": "PyPolca metaQUAST output folder",
                    "when": "$(inputs.run_pypolca && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "$(\"QUAST_PyPolCA_polished_assembly_quality\")",
                            "id": "#main/metaquast_pypolca_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/metaquast_pypolca/metaquast_report",
                                "#main/metaquast_pypolca/quastReport"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/metaquast_pypolca_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#main/metaquast_pypolca/metaquast_krona",
                                "#main/metaquast_pypolca/not_aligned"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/metaquast_pypolca_files_to_folder/folders"
                        },
                        {
                            "source": "#main/illumina_forward_reads",
                            "id": "#main/metaquast_pypolca_files_to_folder/forward_reads"
                        },
                        {
                            "source": "#main/run_pypolca",
                            "id": "#main/metaquast_pypolca_files_to_folder/run_pypolca"
                        }
                    ],
                    "out": [
                        "#main/metaquast_pypolca_files_to_folder/results"
                    ],
                    "id": "#main/metaquast_pypolca_files_to_folder"
                },
                {
                    "doc": "Preparation of pilon output files to a specific output folder",
                    "label": "Pilon output folder",
                    "when": "$(inputs.run_pilon && inputs.illumina_forward_reads !== null && inputs.illumina_forward_reads.length !== 0)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "$(\"Illumina_polished_assembly\")",
                            "id": "#main/pilon_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/workflow_pilon/vcf",
                                "#main/workflow_pilon/pilon_polished_assembly",
                                "#main/workflow_pilon/log"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/pilon_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#main/metaquast_pilon_files_to_folder/results"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/pilon_files_to_folder/folders"
                        },
                        {
                            "source": "#main/illumina_forward_reads",
                            "id": "#main/pilon_files_to_folder/illumina_forward_reads"
                        },
                        {
                            "source": "#main/run_pilon",
                            "id": "#main/pilon_files_to_folder/run_pilon"
                        }
                    ],
                    "out": [
                        "#main/pilon_files_to_folder/results"
                    ],
                    "id": "#main/pilon_files_to_folder"
                },
                {
                    "label": "Prepare references",
                    "doc": "Prepare references to a single fasta file and unique headers",
                    "when": "$(inputs.fasta_files !== null && inputs.fasta_files.length !== 0)",
                    "run": "#prepare_fasta_db.cwl",
                    "in": [
                        {
                            "source": "#main/filter_references",
                            "id": "#main/prepare_fasta_db/fasta_files"
                        },
                        {
                            "valueFrom": "filter-reference_prepared.fa.gz",
                            "id": "#main/prepare_fasta_db/output_file_name"
                        }
                    ],
                    "out": [
                        "#main/prepare_fasta_db/fasta_db"
                    ],
                    "id": "#main/prepare_fasta_db"
                },
                {
                    "doc": "Preparation of PyPolCA output files to a specific output folder",
                    "label": "PyPolca output folder",
                    "when": "$(inputs.run_pypolca && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "$(\"PyPolCA_polished_assembly\")",
                            "id": "#main/pypolca_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/workflow_pypolca/polished_genome",
                                "#main/workflow_pypolca/vcf",
                                "#main/workflow_pypolca/report",
                                "#main/workflow_pypolca/log"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/pypolca_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#main/metaquast_pypolca_files_to_folder/results",
                                "#main/workflow_pypolca/logs_dir"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/pypolca_files_to_folder/folders"
                        },
                        {
                            "source": "#main/illumina_forward_reads",
                            "id": "#main/pypolca_files_to_folder/forward_reads"
                        },
                        {
                            "source": "#main/run_pypolca",
                            "id": "#main/pypolca_files_to_folder/run_pypolca"
                        }
                    ],
                    "out": [
                        "#main/pypolca_files_to_folder/results"
                    ],
                    "id": "#main/pypolca_files_to_folder"
                },
                {
                    "doc": "Preparation of read filtering output files to a specific output folder",
                    "label": "Read filtering output folder",
                    "when": "$(inputs.keep_filtered_reads == false)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "$(\"Read_filtering_and_classification\")",
                            "id": "#main/readfilter_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/workflow_quality_nanopore/filtlong_log",
                                "#main/workflow_quality_nanopore/reference_filter_longreads_log",
                                "#main/workflow_quality_pacbio/filtlong_log",
                                "#main/workflow_quality_pacbio/reference_filter_longreads_log"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/readfilter_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#main/workflow_quality_pacbio/kraken2_folder",
                                "#main/workflow_quality_nanopore/kraken2_folder",
                                "#main/workflow_quality_illumina/kraken2_folder",
                                "#main/workflow_quality_illumina/reports_folder",
                                "#main/workflow_quality_nanopore/nanoplot_unfiltered_folder",
                                "#main/workflow_quality_nanopore/nanoplot_filtered_folder",
                                "#main/workflow_quality_pacbio/nanoplot_unfiltered_folder",
                                "#main/workflow_quality_pacbio/nanoplot_filtered_folder"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/readfilter_files_to_folder/folders"
                        },
                        {
                            "source": "#main/keep_filtered_reads",
                            "id": "#main/readfilter_files_to_folder/keep_filtered_reads"
                        }
                    ],
                    "out": [
                        "#main/readfilter_files_to_folder/results"
                    ],
                    "id": "#main/readfilter_files_to_folder"
                },
                {
                    "doc": "Genome assembly using SPAdes with illumina and or long reads",
                    "label": "SPAdes assembly",
                    "when": "$(inputs.run_spades && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)",
                    "run": "#spades.cwl",
                    "in": [
                        {
                            "source": [
                                "#main/workflow_quality_illumina/QC_forward_reads"
                            ],
                            "linkMerge": "merge_nested",
                            "id": "#main/spades/forward_reads"
                        },
                        {
                            "source": "#main/memory",
                            "id": "#main/spades/memory"
                        },
                        {
                            "source": "#main/metagenome",
                            "id": "#main/spades/metagenome"
                        },
                        {
                            "source": "#main/workflow_quality_nanopore/filtered_reads",
                            "valueFrom": "${ var reads = null; if (self !== null) { reads = [self]; } return reads; }",
                            "id": "#main/spades/nanopore_reads"
                        },
                        {
                            "source": "#main/only_assembler_mode_spades",
                            "id": "#main/spades/only_assembler"
                        },
                        {
                            "source": "#main/workflow_quality_pacbio/filtered_reads",
                            "valueFrom": "${ var reads = null; if (self !== null) { reads = [self]; } return reads; }",
                            "id": "#main/spades/pacbio_reads"
                        },
                        {
                            "source": [
                                "#main/workflow_quality_illumina/QC_reverse_reads"
                            ],
                            "linkMerge": "merge_nested",
                            "id": "#main/spades/reverse_reads"
                        },
                        {
                            "source": "#main/run_spades",
                            "id": "#main/spades/run_spades"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/spades/threads"
                        }
                    ],
                    "out": [
                        "#main/spades/contigs",
                        "#main/spades/scaffolds",
                        "#main/spades/assembly_graph",
                        "#main/spades/assembly_graph_with_scaffolds",
                        "#main/spades/contigs_assembly_paths",
                        "#main/spades/scaffolds_assembly_paths",
                        "#main/spades/contigs_before_rr",
                        "#main/spades/params",
                        "#main/spades/log",
                        "#main/spades/internal_config",
                        "#main/spades/internal_dataset"
                    ],
                    "id": "#main/spades"
                },
                {
                    "doc": "Preparation of SPAdes output files to a specific output folder",
                    "label": "SPADES output to folder",
                    "when": "$(inputs.run_spades)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "$(\"SPAdes_Assembly\")",
                            "id": "#main/spades_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/compress_spades/outfile",
                                "#main/spades/params",
                                "#main/spades/log",
                                "#main/spades/internal_config",
                                "#main/spades/internal_dataset",
                                "#main/spades/assembly_graph_with_scaffolds"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/spades_files_to_folder/files"
                        },
                        {
                            "source": "#main/run_spades",
                            "id": "#main/spades_files_to_folder/run_spades"
                        }
                    ],
                    "out": [
                        "#main/spades_files_to_folder/results"
                    ],
                    "id": "#main/spades_files_to_folder"
                },
                {
                    "label": "GEM workflow",
                    "doc": "CarveMe community genomescale metabolic models workflow from bins",
                    "when": "$(inputs.binning && inputs.run_GEM)",
                    "run": "#workflow_metagenomics_GEM.cwl",
                    "in": [
                        {
                            "source": "#main/binning",
                            "id": "#main/workflow_GEM/binning"
                        },
                        {
                            "source": "#main/workflow_binning/bins",
                            "id": "#main/workflow_GEM/bins"
                        },
                        {
                            "source": "#main/carveme_solver",
                            "id": "#main/workflow_GEM/carveme_solver"
                        },
                        {
                            "source": "#main/gapfill",
                            "id": "#main/workflow_GEM/gapfill"
                        },
                        {
                            "source": "#main/identifier",
                            "id": "#main/workflow_GEM/identifier"
                        },
                        {
                            "source": "#main/mediadb",
                            "id": "#main/workflow_GEM/mediadb"
                        },
                        {
                            "source": "#main/memote_solver",
                            "id": "#main/workflow_GEM/memote_solver"
                        },
                        {
                            "source": "#main/run_GEM",
                            "id": "#main/workflow_GEM/run_GEM"
                        },
                        {
                            "source": "#main/run_smetana",
                            "id": "#main/workflow_GEM/run_smetana"
                        },
                        {
                            "source": "#main/smetana_solver",
                            "id": "#main/workflow_GEM/smetana_solver"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/workflow_GEM/threads"
                        }
                    ],
                    "out": [
                        "#main/workflow_GEM/carveme_gems_folder",
                        "#main/workflow_GEM/protein_fasta_folder",
                        "#main/workflow_GEM/memote_folder",
                        "#main/workflow_GEM/smetana_output",
                        "#main/workflow_GEM/gemstats_out"
                    ],
                    "id": "#main/workflow_GEM"
                },
                {
                    "label": "Binning workflow",
                    "doc": "Binning workflow to create bins",
                    "when": "$(inputs.binning && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)",
                    "run": "#workflow_metagenomics_binning.cwl",
                    "in": [
                        {
                            "source": "#main/annotate_bins",
                            "id": "#main/workflow_binning/annotate_bins"
                        },
                        {
                            "source": "#main/annotate_unbinned",
                            "id": "#main/workflow_binning/annotate_unbinned"
                        },
                        {
                            "source": [
                                "#main/workflow_pypolca/polished_genome",
                                "#main/workflow_pilon/pilon_polished_assembly",
                                "#main/medaka/polished_assembly",
                                "#main/flye/assembly",
                                "#main/spades/scaffolds"
                            ],
                            "valueFrom": "${\n  // Mapping of user choices to assembly step outputs\n  var assemblyOutputs = {\n    // Priority: follow order below\n    \"pypolca\": self[0],\n    \"pilon\": self[1],\n    \"medaka\": self[2],\n    \"flye\": self[3],\n    \"spades\": self[4]\n  }\n  // Check if user has made a choice \n  if (inputs.assembly_choice) {\n    return assemblyOutputs[inputs.assembly_choice];\n  } else {\n    // Default logic if no specific choice is made: first non-null\n    var assemblies = [\n      self[0],\n      self[1],\n      self[2],\n      self[3],\n      self[4]\n    ].filter(function (assembly) { return assembly !== null; });\n    return assemblies[0]; // Return the first non-null assembly\n  }\n}\n",
                            "id": "#main/workflow_binning/assembly"
                        },
                        {
                            "source": "#main/assembly_choice",
                            "id": "#main/workflow_binning/assembly_choice"
                        },
                        {
                            "source": [
                                "#main/flye/assembly_graph",
                                "#main/spades/assembly_graph_with_scaffolds"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "first_non_null",
                            "id": "#main/workflow_binning/assembly_graph"
                        },
                        {
                            "source": "#main/bakta_db",
                            "id": "#main/workflow_binning/bakta_db"
                        },
                        {
                            "source": "#main/assembly_read_mapping_illumina/sorted_bam",
                            "id": "#main/workflow_binning/bam_file"
                        },
                        {
                            "source": "#main/binning",
                            "id": "#main/workflow_binning/binning"
                        },
                        {
                            "source": "#main/busco_data",
                            "id": "#main/workflow_binning/busco_data"
                        },
                        {
                            "source": "#main/eggnog_dbs",
                            "id": "#main/workflow_binning/eggnog_dbs"
                        },
                        {
                            "source": "#main/illumina_forward_reads",
                            "id": "#main/workflow_binning/forward_reads"
                        },
                        {
                            "source": "#main/gtdbtk_data",
                            "id": "#main/workflow_binning/gtdbtk_data"
                        },
                        {
                            "source": "#main/identifier",
                            "id": "#main/workflow_binning/identifier"
                        },
                        {
                            "source": "#main/interproscan_applications",
                            "id": "#main/workflow_binning/interproscan_applications"
                        },
                        {
                            "source": "#main/interproscan_directory",
                            "id": "#main/workflow_binning/interproscan_directory"
                        },
                        {
                            "source": "#main/kofamscan_limit_sapp",
                            "id": "#main/workflow_binning/kofamscan_limit_sapp"
                        },
                        {
                            "source": "#main/memory",
                            "id": "#main/workflow_binning/memory"
                        },
                        {
                            "source": "#main/run_GEM",
                            "id": "#main/workflow_binning/run_GEM"
                        },
                        {
                            "source": "#main/run_binspreader",
                            "id": "#main/workflow_binning/run_binspreader"
                        },
                        {
                            "source": "#main/run_eggnog",
                            "id": "#main/workflow_binning/run_eggnog"
                        },
                        {
                            "source": "#main/run_interproscan",
                            "id": "#main/workflow_binning/run_interproscan"
                        },
                        {
                            "source": "#main/run_kofamscan",
                            "id": "#main/workflow_binning/run_kofamscan"
                        },
                        {
                            "source": "#main/semibin_environment",
                            "id": "#main/workflow_binning/semibin_environment"
                        },
                        {
                            "source": "#main/skip_bakta_crispr",
                            "id": "#main/workflow_binning/skip_bakta_crispr"
                        },
                        {
                            "valueFrom": "${\n  if (inputs.run_GEM && inputs.annotate_bins){\n    return true\n  }\n  else {return false}\n}\n",
                            "id": "#main/workflow_binning/sub_workflow_bin_proteins"
                        },
                        {
                            "valueFrom": "${\n  if (inputs.run_GEM && inputs.annotate_bins != false){return true}\n  else {return false}\n}\n",
                            "id": "#main/workflow_binning/sub_workflow_bins"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/workflow_binning/threads"
                        }
                    ],
                    "out": [
                        "#main/workflow_binning/bins",
                        "#main/workflow_binning/das_tool_output",
                        "#main/workflow_binning/maxbin2_output",
                        "#main/workflow_binning/semibin_output",
                        "#main/workflow_binning/metabat2_output",
                        "#main/workflow_binning/metabat2_binspreader_output",
                        "#main/workflow_binning/maxbin2_binspreader_output",
                        "#main/workflow_binning/semibin_binspreader_output",
                        "#main/workflow_binning/checkm_output",
                        "#main/workflow_binning/gtdbtk_output",
                        "#main/workflow_binning/busco_output",
                        "#main/workflow_binning/bins_summary_table",
                        "#main/workflow_binning/bins_read_stats",
                        "#main/workflow_binning/eukrep_fasta",
                        "#main/workflow_binning/eukrep_stats_file",
                        "#main/workflow_binning/annotation_output"
                    ],
                    "id": "#main/workflow_binning"
                },
                {
                    "label": "Pilon worklow",
                    "doc": "Illumina reads assembly polishing with Pilon",
                    "when": "$(inputs.run_pilon && inputs.illumina_forward_reads !== null && inputs.illumina_forward_reads.length !== 0)",
                    "run": "#workflow_pilon_mapping.cwl",
                    "in": [
                        {
                            "source": [
                                "#main/medaka/polished_assembly",
                                "#main/flye/assembly",
                                "#main/spades/scaffolds"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#main/workflow_pilon/assembly"
                        },
                        {
                            "source": "#main/pilon_fixlist",
                            "id": "#main/workflow_pilon/fixlist"
                        },
                        {
                            "source": "#main/identifier",
                            "valueFrom": "$(self)_scaffolds",
                            "id": "#main/workflow_pilon/identifier"
                        },
                        {
                            "source": "#main/workflow_quality_illumina/QC_forward_reads",
                            "id": "#main/workflow_pilon/illumina_forward_reads"
                        },
                        {
                            "source": "#main/workflow_quality_illumina/QC_reverse_reads",
                            "id": "#main/workflow_pilon/illumina_reverse_reads"
                        },
                        {
                            "source": "#main/memory",
                            "id": "#main/workflow_pilon/memory"
                        },
                        {
                            "source": "#main/run_pilon",
                            "id": "#main/workflow_pilon/run_pilon"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/workflow_pilon/threads"
                        }
                    ],
                    "out": [
                        "#main/workflow_pilon/pilon_polished_assembly",
                        "#main/workflow_pilon/vcf",
                        "#main/workflow_pilon/log"
                    ],
                    "id": "#main/workflow_pilon"
                },
                {
                    "label": "Run PyPolCA assemlby polishing",
                    "doc": "PyPolCA polishing of longreads assembly with illumina reads",
                    "when": "$(inputs.run_pypolca && inputs.forward_reads !== null && inputs.forward_reads.length !== 0)",
                    "run": "#pypolca.cwl",
                    "in": [
                        {
                            "source": [
                                "#main/medaka/polished_assembly",
                                "#main/flye/assembly",
                                "#main/spades/scaffolds"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#main/workflow_pypolca/assembly"
                        },
                        {
                            "source": "#main/workflow_quality_illumina/QC_forward_reads",
                            "id": "#main/workflow_pypolca/forward_reads"
                        },
                        {
                            "source": "#main/identifier",
                            "id": "#main/workflow_pypolca/identifier"
                        },
                        {
                            "source": "#main/workflow_quality_illumina/QC_reverse_reads",
                            "id": "#main/workflow_pypolca/reverse_reads"
                        },
                        {
                            "source": "#main/run_pypolca",
                            "id": "#main/workflow_pypolca/run_pypolca"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/workflow_pypolca/threads"
                        }
                    ],
                    "out": [
                        "#main/workflow_pypolca/polished_genome",
                        "#main/workflow_pypolca/vcf",
                        "#main/workflow_pypolca/report",
                        "#main/workflow_pypolca/log",
                        "#main/workflow_pypolca/logs_dir"
                    ],
                    "id": "#main/workflow_pypolca"
                },
                {
                    "label": "Quality and filtering workflow",
                    "doc": "Quality, filtering and taxonomic classification of Illumina reads",
                    "when": "$(inputs.forward_reads !== null && inputs.forward_reads.length !== 0)",
                    "run": "#workflow_illumina_quality.cwl",
                    "in": [
                        {
                            "source": "#main/deduplicate",
                            "id": "#main/workflow_quality_illumina/deduplicate"
                        },
                        {
                            "linkMerge": "merge_flattened",
                            "source": [
                                "#main/prepare_fasta_db/fasta_db"
                            ],
                            "pickValue": "all_non_null",
                            "id": "#main/workflow_quality_illumina/filter_references"
                        },
                        {
                            "source": "#main/illumina_forward_reads",
                            "id": "#main/workflow_quality_illumina/forward_reads"
                        },
                        {
                            "source": "#main/identifier",
                            "id": "#main/workflow_quality_illumina/identifier"
                        },
                        {
                            "source": "#main/use_reference_mapped_reads",
                            "id": "#main/workflow_quality_illumina/keep_reference_mapped_reads"
                        },
                        {
                            "source": "#main/kraken2_confidence",
                            "id": "#main/workflow_quality_illumina/kraken2_confidence"
                        },
                        {
                            "source": "#main/kraken2_database",
                            "id": "#main/workflow_quality_illumina/kraken2_database"
                        },
                        {
                            "source": "#main/memory",
                            "id": "#main/workflow_quality_illumina/memory"
                        },
                        {
                            "default": false,
                            "id": "#main/workflow_quality_illumina/prepare_reference"
                        },
                        {
                            "source": "#main/illumina_reverse_reads",
                            "id": "#main/workflow_quality_illumina/reverse_reads"
                        },
                        {
                            "source": "#main/skip_bracken",
                            "id": "#main/workflow_quality_illumina/skip_bracken"
                        },
                        {
                            "source": "#main/skip_qc_unfiltered",
                            "id": "#main/workflow_quality_illumina/skip_qc_unfiltered"
                        },
                        {
                            "default": 1,
                            "id": "#main/workflow_quality_illumina/step"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/workflow_quality_illumina/threads"
                        }
                    ],
                    "out": [
                        "#main/workflow_quality_illumina/QC_reverse_reads",
                        "#main/workflow_quality_illumina/QC_forward_reads",
                        "#main/workflow_quality_illumina/reports_folder",
                        "#main/workflow_quality_illumina/kraken2_folder"
                    ],
                    "id": "#main/workflow_quality_illumina"
                },
                {
                    "label": "Oxford Nanopore quality workflow",
                    "doc": "Quality, filtering and taxonomic classification workflow for Oxford Nanopore reads",
                    "when": "$(inputs.longreads !== null && inputs.longreads.length !== 0)",
                    "run": "#workflow_longread_quality.cwl",
                    "in": [
                        {
                            "source": "#main/fastq_rich",
                            "id": "#main/workflow_quality_nanopore/fastq_rich"
                        },
                        {
                            "linkMerge": "merge_flattened",
                            "source": [
                                "#main/prepare_fasta_db/fasta_db"
                            ],
                            "pickValue": "all_non_null",
                            "id": "#main/workflow_quality_nanopore/filter_references"
                        },
                        {
                            "source": "#main/identifier",
                            "id": "#main/workflow_quality_nanopore/identifier"
                        },
                        {
                            "source": "#main/longread_keep_percent",
                            "id": "#main/workflow_quality_nanopore/keep_percent"
                        },
                        {
                            "source": "#main/use_reference_mapped_reads",
                            "id": "#main/workflow_quality_nanopore/keep_reference_mapped_reads"
                        },
                        {
                            "source": "#main/kraken2_confidence",
                            "id": "#main/workflow_quality_nanopore/kraken2_confidence"
                        },
                        {
                            "source": "#main/kraken2_database",
                            "id": "#main/workflow_quality_nanopore/kraken2_database"
                        },
                        {
                            "source": "#main/longread_length_weight",
                            "id": "#main/workflow_quality_nanopore/length_weight"
                        },
                        {
                            "source": "#main/nanopore_reads",
                            "id": "#main/workflow_quality_nanopore/longreads"
                        },
                        {
                            "source": "#main/longread_minimum_length",
                            "id": "#main/workflow_quality_nanopore/minimum_length"
                        },
                        {
                            "default": false,
                            "id": "#main/workflow_quality_nanopore/prepare_reference"
                        },
                        {
                            "default": "Nanopore",
                            "id": "#main/workflow_quality_nanopore/readtype"
                        },
                        {
                            "source": "#main/skip_qc_unfiltered",
                            "id": "#main/workflow_quality_nanopore/skip_qc_unfiltered"
                        },
                        {
                            "default": 1,
                            "id": "#main/workflow_quality_nanopore/step"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/workflow_quality_nanopore/threads"
                        }
                    ],
                    "out": [
                        "#main/workflow_quality_nanopore/filtered_reads",
                        "#main/workflow_quality_nanopore/filtlong_log",
                        "#main/workflow_quality_nanopore/reference_filter_longreads_log",
                        "#main/workflow_quality_nanopore/nanoplot_unfiltered_folder",
                        "#main/workflow_quality_nanopore/nanoplot_filtered_folder",
                        "#main/workflow_quality_nanopore/kraken2_folder"
                    ],
                    "id": "#main/workflow_quality_nanopore"
                },
                {
                    "label": "PacBio quality and filtering workflow",
                    "doc": "Quality, filtering and taxonomic classification for PacBio reads",
                    "when": "$(inputs.longreads !== null && inputs.longreads.length !== 0)",
                    "run": "#workflow_longread_quality.cwl",
                    "in": [
                        {
                            "linkMerge": "merge_flattened",
                            "source": [
                                "#main/prepare_fasta_db/fasta_db"
                            ],
                            "id": "#main/workflow_quality_pacbio/filter_references"
                        },
                        {
                            "source": "#main/identifier",
                            "id": "#main/workflow_quality_pacbio/identifier"
                        },
                        {
                            "source": "#main/longread_keep_percent",
                            "id": "#main/workflow_quality_pacbio/keep_percent"
                        },
                        {
                            "source": "#main/use_reference_mapped_reads",
                            "id": "#main/workflow_quality_pacbio/keep_reference_mapped_reads"
                        },
                        {
                            "source": "#main/kraken2_confidence",
                            "id": "#main/workflow_quality_pacbio/kraken2_confidence"
                        },
                        {
                            "source": "#main/kraken2_database",
                            "id": "#main/workflow_quality_pacbio/kraken2_database"
                        },
                        {
                            "source": "#main/longread_length_weight",
                            "id": "#main/workflow_quality_pacbio/length_weight"
                        },
                        {
                            "source": "#main/pacbio_reads",
                            "id": "#main/workflow_quality_pacbio/longreads"
                        },
                        {
                            "source": "#main/longread_minimum_length",
                            "id": "#main/workflow_quality_pacbio/minimum_length"
                        },
                        {
                            "default": false,
                            "id": "#main/workflow_quality_pacbio/prepare_reference"
                        },
                        {
                            "default": "PacBio",
                            "id": "#main/workflow_quality_pacbio/readtype"
                        },
                        {
                            "source": "#main/skip_qc_unfiltered",
                            "id": "#main/workflow_quality_pacbio/skip_qc_unfiltered"
                        },
                        {
                            "default": 1,
                            "id": "#main/workflow_quality_pacbio/step"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/workflow_quality_pacbio/threads"
                        }
                    ],
                    "out": [
                        "#main/workflow_quality_pacbio/filtered_reads",
                        "#main/workflow_quality_pacbio/filtlong_log",
                        "#main/workflow_quality_pacbio/reference_filter_longreads_log",
                        "#main/workflow_quality_pacbio/nanoplot_unfiltered_folder",
                        "#main/workflow_quality_pacbio/nanoplot_filtered_folder",
                        "#main/workflow_quality_pacbio/kraken2_folder"
                    ],
                    "id": "#main/workflow_quality_pacbio"
                }
            ],
            "id": "#main",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/dateModified": "2024-07-08",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "ScatterFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                },
                {
                    "class": "SubworkflowFeatureRequirement"
                }
            ],
            "label": "Metagenomic Binning from Assembly",
            "doc": "Workflow for Metagenomics binning from assembly.<br>\n\nMinimal inputs are: Identifier, assembly (fasta) and a associated sorted BAM file\n\nSummary\n  - MetaBAT2 (binning)\n  - MaxBin2 (binning)\n  - SemiBin2 (binning)\n  - BinSPreader (bin refinement)\n  - DAS Tool (bin merging)\n  - binette (bin merging) In Development\n  - EukRep (eukaryotic classification)\n  - CheckM (bin completeness and contamination)\n  - BUSCO (bin completeness)\n  - GTDB-Tk (bin taxonomic classification)\n\nOther UNLOCK workflows on WorkflowHub: https://workflowhub.eu/projects/16/workflows?view=default<br><br>\n\n**All tool CWL files and other workflows can be found here:**<br>\n  Tools: https://gitlab.com/m-unlock/cwl<br>\n  Workflows: https://gitlab.com/m-unlock/cwl/workflows<br>\n\n**How to setup and use an UNLOCK workflow:**<br>\nhttps://m-unlock.gitlab.io/docs/setup/setup.html<br>\n",
            "outputs": [
                {
                    "label": "Annotation",
                    "doc": "Bin annotation",
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputSource": "#workflow_metagenomics_binning.cwl/annotation_files_to_folder_bins/results",
                    "id": "#workflow_metagenomics_binning.cwl/annotation_output"
                },
                {
                    "label": "Bin files",
                    "doc": "Bins files in fasta format. To be be used in other workflows.",
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputSource": "#workflow_metagenomics_binning.cwl/das_tool_bins/files",
                    "id": "#workflow_metagenomics_binning.cwl/bins"
                },
                {
                    "label": "Assembly/Bin read stats",
                    "doc": "General assembly and bin coverage",
                    "type": "File",
                    "outputSource": "#workflow_metagenomics_binning.cwl/bin_readstats/binReadStats",
                    "id": "#workflow_metagenomics_binning.cwl/bins_read_stats"
                },
                {
                    "label": "Bins summary",
                    "doc": "Summary of info about the bins",
                    "type": "File",
                    "outputSource": "#workflow_metagenomics_binning.cwl/bins_summary/bins_summary_table",
                    "id": "#workflow_metagenomics_binning.cwl/bins_summary_table"
                },
                {
                    "label": "BUSCO",
                    "doc": "BUSCO output directory",
                    "type": "Directory",
                    "outputSource": "#workflow_metagenomics_binning.cwl/busco_files_to_folder/results",
                    "id": "#workflow_metagenomics_binning.cwl/busco_output"
                },
                {
                    "label": "CheckM",
                    "doc": "CheckM output directory",
                    "type": "Directory",
                    "outputSource": "#workflow_metagenomics_binning.cwl/checkm_files_to_folder/results",
                    "id": "#workflow_metagenomics_binning.cwl/checkm_output"
                },
                {
                    "label": "DAS Tool",
                    "doc": "DAS Tool output directory",
                    "type": "Directory",
                    "outputSource": "#workflow_metagenomics_binning.cwl/das_tool_files_to_folder/results",
                    "id": "#workflow_metagenomics_binning.cwl/das_tool_output"
                },
                {
                    "label": "EukRep fasta",
                    "doc": "EukRep eukaryotic classified contigs",
                    "type": "File",
                    "outputSource": "#workflow_metagenomics_binning.cwl/eukrep/euk_fasta_out",
                    "id": "#workflow_metagenomics_binning.cwl/eukrep_fasta"
                },
                {
                    "label": "EukRep stats",
                    "doc": "EukRep fasta statistics",
                    "type": "File",
                    "outputSource": "#workflow_metagenomics_binning.cwl/eukrep_stats/output",
                    "id": "#workflow_metagenomics_binning.cwl/eukrep_stats_file"
                },
                {
                    "label": "GTDB-Tk",
                    "doc": "GTDB-Tk output directory",
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputSource": "#workflow_metagenomics_binning.cwl/gtdbtk_files_to_folder/results",
                    "id": "#workflow_metagenomics_binning.cwl/gtdbtk_output"
                },
                {
                    "label": "MaxBin2 BinSPreader",
                    "doc": "MaxBin2 output directory",
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputSource": "#workflow_metagenomics_binning.cwl/maxbin2_binspreader_files_to_folder/results",
                    "id": "#workflow_metagenomics_binning.cwl/maxbin2_binspreader_output"
                },
                {
                    "label": "MaxBin2",
                    "doc": "MaxBin2 output directory\u00df",
                    "type": "Directory",
                    "outputSource": "#workflow_metagenomics_binning.cwl/maxbin2_files_to_folder/results",
                    "id": "#workflow_metagenomics_binning.cwl/maxbin2_output"
                },
                {
                    "label": "MetaBAT2 BinSPreader",
                    "doc": "MetaBAT2 output directory",
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputSource": "#workflow_metagenomics_binning.cwl/metabat2_binspreader_files_to_folder/results",
                    "id": "#workflow_metagenomics_binning.cwl/metabat2_binspreader_output"
                },
                {
                    "label": "MetaBAT2",
                    "doc": "MetaBAT2 output directory",
                    "type": "Directory",
                    "outputSource": "#workflow_metagenomics_binning.cwl/metabat2_files_to_folder/results",
                    "id": "#workflow_metagenomics_binning.cwl/metabat2_output"
                },
                {
                    "label": "SemiBin BinSPreader",
                    "doc": "MaxBin2 output directory",
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputSource": "#workflow_metagenomics_binning.cwl/semibin_binspreader_files_to_folder/results",
                    "id": "#workflow_metagenomics_binning.cwl/semibin_binspreader_output"
                },
                {
                    "label": "SemiBin",
                    "doc": "MaxBin2 output directory",
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputSource": "#workflow_metagenomics_binning.cwl/semibin_files_to_folder/results",
                    "id": "#workflow_metagenomics_binning.cwl/semibin_output"
                }
            ],
            "inputs": [
                {
                    "type": "boolean",
                    "label": "Annotate bins",
                    "doc": "Annotate bins. Default false",
                    "default": false,
                    "id": "#workflow_metagenomics_binning.cwl/annotate_bins"
                },
                {
                    "type": "boolean",
                    "label": "Annotate unbinned",
                    "doc": "Annotate unbinned contigs. Will be treated as metagenome. Default false",
                    "default": false,
                    "id": "#workflow_metagenomics_binning.cwl/annotate_unbinned"
                },
                {
                    "type": "File",
                    "doc": "Assembly in fasta format",
                    "label": "Assembly fasta",
                    "loadListing": "no_listing",
                    "id": "#workflow_metagenomics_binning.cwl/assembly"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "doc": "Assembly graph file from asssembler for BinSPreader",
                    "label": "BinSPreader graph file",
                    "id": "#workflow_metagenomics_binning.cwl/assembly_graph"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "Bakta DB",
                    "doc": "Bakta Database directory (required when annotating bins)",
                    "id": "#workflow_metagenomics_binning.cwl/bakta_db"
                },
                {
                    "type": "File",
                    "doc": "Mapping file in sorted bam format containing reads mapped to the assembly",
                    "label": "Bam file",
                    "loadListing": "no_listing",
                    "id": "#workflow_metagenomics_binning.cwl/bam_file"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "BUSCO dataset",
                    "doc": "Directory containing the BUSCO dataset location.",
                    "loadListing": "no_listing",
                    "id": "#workflow_metagenomics_binning.cwl/busco_data"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Output destination",
                    "doc": "Optional output destination path for cwl-prov reporting. (not used in the workflow itself)",
                    "id": "#workflow_metagenomics_binning.cwl/destination"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "record",
                            "name": "#workflow_metagenomics_binning.cwl/eggnog_dbs/eggnog_dbs",
                            "fields": [
                                {
                                    "type": [
                                        "null",
                                        "Directory"
                                    ],
                                    "doc": "Directory containing all data files for the eggNOG database.",
                                    "name": "#workflow_metagenomics_binning.cwl/eggnog_dbs/eggnog_dbs/data_dir"
                                },
                                {
                                    "type": [
                                        "null",
                                        "File"
                                    ],
                                    "doc": "eggNOG database file",
                                    "name": "#workflow_metagenomics_binning.cwl/eggnog_dbs/eggnog_dbs/db"
                                },
                                {
                                    "type": [
                                        "null",
                                        "File"
                                    ],
                                    "doc": "eggNOG database file for diamond blast search",
                                    "name": "#workflow_metagenomics_binning.cwl/eggnog_dbs/eggnog_dbs/diamond_db"
                                }
                            ]
                        }
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/eggnog_dbs"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "doc": "Directory containing the GTDB database. When none is given GTDB-Tk will be skipped.",
                    "label": "gtdbtk data directory",
                    "loadListing": "no_listing",
                    "id": "#workflow_metagenomics_binning.cwl/gtdbtk_data"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "Identifier used",
                    "id": "#workflow_metagenomics_binning.cwl/identifier"
                },
                {
                    "type": "string",
                    "label": "InterProScan applications",
                    "doc": "Comma separated list of analyses:\nFunFam,SFLD,PANTHER,Gene3D,Hamap,PRINTS,ProSiteProfiles,Coils,SUPERFAMILY,SMART,CDD,PIRSR,ProSitePatterns,AntiFam,Pfam,MobiDBLite,PIRSF,NCBIfam\ndefault Pfam,SFLD,SMART,AntiFam,NCBIfam\n",
                    "default": "Pfam",
                    "id": "#workflow_metagenomics_binning.cwl/interproscan_applications"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "InterProScan 5 directory",
                    "doc": "Directory of the (full) InterProScan 5 program. Used for annotating bins. (required when running with interproscan)",
                    "id": "#workflow_metagenomics_binning.cwl/interproscan_directory"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "SAPP kofamscan limit",
                    "doc": "Limit max number of entries of kofamscan hits per locus in SAPP. Default 5",
                    "default": 5,
                    "id": "#workflow_metagenomics_binning.cwl/kofamscan_limit_sapp"
                },
                {
                    "type": "int",
                    "doc": "Maximum memory usage in megabytes",
                    "label": "memory usage (MB)",
                    "default": 4000,
                    "id": "#workflow_metagenomics_binning.cwl/memory"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Whether to use BinSPreader for bin refinement",
                    "default": false,
                    "id": "#workflow_metagenomics_binning.cwl/run_binspreader"
                },
                {
                    "type": "boolean",
                    "label": "Run eggNOG-mapper",
                    "doc": "Run with eggNOG-mapper annotation. Requires eggnog database files. Default false",
                    "default": false,
                    "id": "#workflow_metagenomics_binning.cwl/run_eggnog"
                },
                {
                    "type": "boolean",
                    "label": "Run InterProScan",
                    "doc": "Run with eggNOG-mapper annotation. Requires InterProScan v5 program files. Default false",
                    "default": false,
                    "id": "#workflow_metagenomics_binning.cwl/run_interproscan"
                },
                {
                    "type": "boolean",
                    "label": "Run kofamscan",
                    "doc": "Run with KEGG KO KoFamKOALA annotation. Default false",
                    "default": false,
                    "id": "#workflow_metagenomics_binning.cwl/run_kofamscan"
                },
                {
                    "type": "boolean",
                    "doc": "Run with SemiBin2 binner. Default true",
                    "label": "Run SemiBin",
                    "default": true,
                    "id": "#workflow_metagenomics_binning.cwl/run_semibin"
                },
                {
                    "type": "string",
                    "doc": "Semibin2 Built-in models (global/human_gut/dog_gut/ocean/soil/cat_gut/human_oral/mouse_gut/pig_gut/built_environment/wastewater/chicken_caecum). Default; global",
                    "label": "SemiBin Environment",
                    "default": "global",
                    "id": "#workflow_metagenomics_binning.cwl/semibin_environment"
                },
                {
                    "type": "boolean",
                    "label": "Skip CRISPR",
                    "doc": "Skip CRISPR array prediction using PILER-CR",
                    "default": false,
                    "id": "#workflow_metagenomics_binning.cwl/skip_bakta_crispr"
                },
                {
                    "type": "boolean",
                    "label": "Sub workflow Run",
                    "doc": "Use this when you need the output the bin protein files as File[] for subsequent analysis workflow steps in another workflow such as wf_GEM.",
                    "default": false,
                    "id": "#workflow_metagenomics_binning.cwl/sub_workflow_bin_proteins"
                },
                {
                    "type": "boolean",
                    "label": "Sub workflow Run",
                    "doc": "Use this when you need the output bins as File[] for subsequent analysis workflow steps in another workflow.",
                    "default": false,
                    "id": "#workflow_metagenomics_binning.cwl/sub_workflow_bins"
                },
                {
                    "type": "int",
                    "doc": "Number of threads to use for computational processes",
                    "label": "Threads",
                    "default": 2,
                    "id": "#workflow_metagenomics_binning.cwl/threads"
                }
            ],
            "steps": [
                {
                    "doc": "Depths per bin",
                    "label": "Depths per bin",
                    "run": "#aggregateBinDepths.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/das_tool_bins/files",
                            "id": "#workflow_metagenomics_binning.cwl/aggregate_bin_depths/bins"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/aggregate_bin_depths/identifier"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/metabat2_contig_depths/depths",
                            "id": "#workflow_metagenomics_binning.cwl/aggregate_bin_depths/metabatdepthsFile"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/aggregate_bin_depths/binDepths"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/aggregate_bin_depths"
                },
                {
                    "doc": "Preparation of annotation output files to a specific output folder",
                    "label": "Annotation output folder",
                    "when": "$((inputs.annotate_bins || inputs.annotate_unbinned) && inputs.bakta_db !== null)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/annotate_bins",
                            "id": "#workflow_metagenomics_binning.cwl/annotation_files_to_folder_bins/annotate_bins"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/annotate_unbinned",
                            "id": "#workflow_metagenomics_binning.cwl/annotation_files_to_folder_bins/annotate_unbinned"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/bakta_db",
                            "id": "#workflow_metagenomics_binning.cwl/annotation_files_to_folder_bins/bakta_db"
                        },
                        {
                            "valueFrom": "Bin_annotation",
                            "id": "#workflow_metagenomics_binning.cwl/annotation_files_to_folder_bins/destination"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/compressed_other_files",
                                "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/sapp_hdt_file",
                                "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/sapp_hdt_file",
                                "#workflow_metagenomics_binning.cwl/annotation_output_to_array_bins/output"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_binning.cwl/annotation_files_to_folder_bins/files"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/bakta_folder_compressed",
                                "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/bakta_folder_compressed"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_binning.cwl/annotation_files_to_folder_bins/folders"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/annotation_files_to_folder_bins/results"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/annotation_files_to_folder_bins"
                },
                {
                    "when": "$(inputs.annotate_bins && inputs.bakta_db !== null)",
                    "run": "#merge_file_arrays.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/annotate_bins",
                            "id": "#workflow_metagenomics_binning.cwl/annotation_output_to_array_bins/annotate_bins"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/bakta_db",
                            "id": "#workflow_metagenomics_binning.cwl/annotation_output_to_array_bins/bakta_db"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/compressed_other_files"
                            ],
                            "id": "#workflow_metagenomics_binning.cwl/annotation_output_to_array_bins/input"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/annotation_output_to_array_bins/output"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/annotation_output_to_array_bins"
                },
                {
                    "doc": "Table general bin and assembly read mapping stats",
                    "label": "Bin and assembly read stats",
                    "run": "#assembly_bins_readstats.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/assembly",
                            "id": "#workflow_metagenomics_binning.cwl/bin_readstats/assembly"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/bam_file",
                            "id": "#workflow_metagenomics_binning.cwl/bin_readstats/bam_file"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/das_tool/contig2bin",
                            "id": "#workflow_metagenomics_binning.cwl/bin_readstats/binContigs"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/bin_readstats/identifier"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/bin_readstats/binReadStats"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/bin_readstats"
                },
                {
                    "doc": "Convert bins in a directory to an array of Files",
                    "label": "Bin folder to files",
                    "run": "#folder_to_files.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/annotate_bins",
                            "id": "#workflow_metagenomics_binning.cwl/binfolder_to_files/annotate_bins"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/remove_unbinned/output_folder",
                            "id": "#workflow_metagenomics_binning.cwl/binfolder_to_files/folder"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/binfolder_to_files/files"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/binfolder_to_files"
                },
                {
                    "doc": "Table of all bins and their statistics like size, contigs, completeness etc",
                    "label": "Bins summary",
                    "run": "#bins_summary.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/aggregate_bin_depths/binDepths",
                            "id": "#workflow_metagenomics_binning.cwl/bins_summary/bin_depths"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/das_tool/bin_dir",
                            "id": "#workflow_metagenomics_binning.cwl/bins_summary/bin_dir"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/busco/batch_summary",
                            "id": "#workflow_metagenomics_binning.cwl/bins_summary/busco_batch"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/checkm/checkm_out_table",
                            "id": "#workflow_metagenomics_binning.cwl/bins_summary/checkm"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/gtdbtk/gtdbtk_summary",
                            "id": "#workflow_metagenomics_binning.cwl/bins_summary/gtdbtk"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/bins_summary/identifier"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/bins_summary/bins_summary_table"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/bins_summary"
                },
                {
                    "doc": "BUSCO assembly completeness workflow",
                    "label": "BUSCO",
                    "run": "#busco.cwl",
                    "when": "$(inputs.bins.length !== 0)",
                    "in": [
                        {
                            "valueFrom": "$(true)",
                            "id": "#workflow_metagenomics_binning.cwl/busco/auto-lineage-prok"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/das_tool_bins/files",
                            "id": "#workflow_metagenomics_binning.cwl/busco/bins"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/busco_data",
                            "id": "#workflow_metagenomics_binning.cwl/busco/busco_data"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/busco/identifier"
                        },
                        {
                            "valueFrom": "geno",
                            "id": "#workflow_metagenomics_binning.cwl/busco/mode"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/remove_unbinned/output_folder",
                            "id": "#workflow_metagenomics_binning.cwl/busco/sequence_folder"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/threads",
                            "id": "#workflow_metagenomics_binning.cwl/busco/threads"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/busco/batch_summary"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/busco"
                },
                {
                    "doc": "Preparation of BUSCO output files to a specific output folder",
                    "label": "BUSCO output folder",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "BUSCO_Bin_Completeness",
                            "id": "#workflow_metagenomics_binning.cwl/busco_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/busco/batch_summary"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_binning.cwl/busco_files_to_folder/files"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/busco_files_to_folder/results"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/busco_files_to_folder"
                },
                {
                    "doc": "CheckM bin quality assessment",
                    "label": "CheckM",
                    "run": "#checkm_lineagewf.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/remove_unbinned/output_folder",
                            "id": "#workflow_metagenomics_binning.cwl/checkm/bin_dir"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/checkm/identifier"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/threads",
                            "id": "#workflow_metagenomics_binning.cwl/checkm/threads"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/checkm/checkm_out_table",
                        "#workflow_metagenomics_binning.cwl/checkm/checkm_out_folder"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/checkm"
                },
                {
                    "doc": "Preparation of CheckM output files to a specific output folder",
                    "label": "CheckM output",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "CheckM_Bin_Quality",
                            "id": "#workflow_metagenomics_binning.cwl/checkm_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/checkm/checkm_out_table"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#workflow_metagenomics_binning.cwl/checkm_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/checkm/checkm_out_folder"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#workflow_metagenomics_binning.cwl/checkm_files_to_folder/folders"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/checkm_files_to_folder/results"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/checkm_files_to_folder"
                },
                {
                    "doc": "Compress GTDB-Tk output folder",
                    "label": "Compress GTDB-Tk",
                    "when": "$(inputs.gtdbtk_data !== null)",
                    "run": "#compress_directory.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/gtdbtk_data",
                            "id": "#workflow_metagenomics_binning.cwl/compress_gtdbtk/gtdbtk_data"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/gtdbtk/gtdbtk_out_folder",
                            "id": "#workflow_metagenomics_binning.cwl/compress_gtdbtk/indir"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/compress_gtdbtk/outfile"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/compress_gtdbtk"
                },
                {
                    "doc": "DAS Tool",
                    "label": "DAS Tool integrate predictions from multiple binning tools",
                    "run": "#das_tool.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/assembly",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool/assembly"
                        },
                        {
                            "valueFrom": "${\n  var tables = [\n    inputs.run_binspreader ? inputs.metabat2_binspreader_refined_contig2bin : inputs.metabat2_contig2bin,\n    inputs.run_binspreader ? inputs.maxbin2_binspreader_refined_contig2bin : inputs.maxbin2_contig2bin,\n    inputs.run_binspreader ? inputs.semibin_binspreader_refined_contig2bin : inputs.semibin_contig2bin\n  ]        \n  return tables;\n}\n",
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool/bin_tables"
                        },
                        {
                            "valueFrom": "${\n  if (inputs.run_binspreader && inputs.run_semibin) {\n    return \"MetaBAT2_BinSPreader,MaxBin2_BinSPreader,SemiBin_BinSPreader\";\n  } else {\n    if (inputs.run_binspreader) {\n      return \"MetaBAT2_BinSPreader,MaxBin2_BinSPreader\";\n    } else {\n      if (inputs.run_semibin) {\n        return \"MetaBAT2,MaxBin2,SemiBin\";\n      } else {\n      return \"MetaBAT2,MaxBin2\";\n      }\n    }\n  }\n}\n",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool/binner_labels"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool/identifier"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/refined_contig2bin",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool/maxbin2_binspreader_refined_contig2bin"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/maxbin2_contig2bin/table",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool/maxbin2_contig2bin"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/metabat2_binspreader/refined_contig2bin",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool/metabat2_binspreader_refined_contig2bin"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/metabat2_contig2bin/table",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool/metabat2_contig2bin"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_binspreader",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool/run_binspreader"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_semibin",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool/run_semibin"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/semibin_binspreader/refined_contig2bin",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool/semibin_binspreader_refined_contig2bin"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/semibin_contig2bin/table",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool/semibin_contig2bin"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/threads",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool/threads"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/das_tool/bin_dir",
                        "#workflow_metagenomics_binning.cwl/das_tool/summary",
                        "#workflow_metagenomics_binning.cwl/das_tool/contig2bin",
                        "#workflow_metagenomics_binning.cwl/das_tool/log"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/das_tool"
                },
                {
                    "doc": "DAS Tool bins folder to File array for further analysis",
                    "label": "Bin folder to files[]",
                    "run": "#folder_to_files.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/das_tool/bin_dir",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool_bins/folder"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/das_tool_bins/files"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/das_tool_bins"
                },
                {
                    "doc": "Preparation of DAS Tool output files to a specific output folder.",
                    "label": "DAS Tool output folder",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "DAS_Tool_binner_integration",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/das_tool/log",
                                "#workflow_metagenomics_binning.cwl/das_tool/summary",
                                "#workflow_metagenomics_binning.cwl/das_tool/contig2bin",
                                "#workflow_metagenomics_binning.cwl/aggregate_bin_depths/binDepths"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/das_tool/bin_dir"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#workflow_metagenomics_binning.cwl/das_tool_files_to_folder/folders"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/das_tool_files_to_folder/results"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/das_tool_files_to_folder"
                },
                {
                    "doc": "EukRep, eukaryotic sequence classification",
                    "label": "EukRep",
                    "run": "#eukrep.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/assembly",
                            "id": "#workflow_metagenomics_binning.cwl/eukrep/assembly"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/eukrep/identifier"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/eukrep/euk_fasta_out"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/eukrep"
                },
                {
                    "doc": "EukRep fasta statistics",
                    "label": "EukRep stats",
                    "run": "#raw_n50.cwl",
                    "in": [
                        {
                            "valueFrom": "$(inputs.tmp_id)_EukRep",
                            "id": "#workflow_metagenomics_binning.cwl/eukrep_stats/identifier"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/eukrep/euk_fasta_out",
                            "id": "#workflow_metagenomics_binning.cwl/eukrep_stats/input_fasta"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/eukrep_stats/tmp_id"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/eukrep_stats/output"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/eukrep_stats"
                },
                {
                    "doc": "Extract unbinned fasta from bin directory. For analyis by subsequent tools.",
                    "label": "Extract unbinned file",
                    "run": "#file_from_folder_regex.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/das_tool/bin_dir",
                            "id": "#workflow_metagenomics_binning.cwl/extract_unbinned/folder"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "valueFrom": "$(self)_unbinned.fasta",
                            "id": "#workflow_metagenomics_binning.cwl/extract_unbinned/output_file_name"
                        },
                        {
                            "valueFrom": ".*unbinned.*",
                            "id": "#workflow_metagenomics_binning.cwl/extract_unbinned/regex"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/extract_unbinned/out_file"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/extract_unbinned"
                },
                {
                    "doc": "Taxomic assigment of bins with GTDB-Tk",
                    "label": "GTDBTK",
                    "when": "$(inputs.gtdbtk_data !== null && inputs.bins.length !== 0)",
                    "run": "#gtdbtk_classify_wf.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/remove_unbinned/output_folder",
                            "id": "#workflow_metagenomics_binning.cwl/gtdbtk/bin_dir"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/das_tool_bins/files",
                            "id": "#workflow_metagenomics_binning.cwl/gtdbtk/bins"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/gtdbtk_data",
                            "id": "#workflow_metagenomics_binning.cwl/gtdbtk/gtdbtk_data"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/gtdbtk/identifier"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/threads",
                            "id": "#workflow_metagenomics_binning.cwl/gtdbtk/threads"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/gtdbtk/gtdbtk_summary",
                        "#workflow_metagenomics_binning.cwl/gtdbtk/gtdbtk_out_folder"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/gtdbtk"
                },
                {
                    "doc": "Preparation of GTDB-Tk output files to a specific output folder",
                    "label": "GTBD-Tk output folder",
                    "when": "$(inputs.gtdbtk_data !== null)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "GTDB-Tk_Bin_Taxonomy",
                            "id": "#workflow_metagenomics_binning.cwl/gtdbtk_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/gtdbtk/gtdbtk_summary",
                                "#workflow_metagenomics_binning.cwl/compress_gtdbtk/outfile"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_binning.cwl/gtdbtk_files_to_folder/files"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/gtdbtk_data",
                            "id": "#workflow_metagenomics_binning.cwl/gtdbtk_files_to_folder/gtdbtk_data"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/gtdbtk_files_to_folder/results"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/gtdbtk_files_to_folder"
                },
                {
                    "doc": "Binning procedure using MaxBin2",
                    "label": "MaxBin2 binning",
                    "run": "#maxbin2.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/metabat2_contig_depths/depths",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2/abundances"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/assembly",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2/contigs"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2/identifier"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/threads",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2/threads"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/maxbin2/bins",
                        "#workflow_metagenomics_binning.cwl/maxbin2/summary",
                        "#workflow_metagenomics_binning.cwl/maxbin2/log"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/maxbin2"
                },
                {
                    "doc": "MaxBin2 Bin refinement using BinSPreader",
                    "label": "MaxBin2 BinSPreader",
                    "run": "#binspreader.cwl",
                    "when": "$(inputs.run_binspreader && inputs.assembly_graph !== null)",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/assembly_graph",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/assembly_graph"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/maxbin2_contig2bin/table",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/contig2bin"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/identifier"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_binspreader",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/run_binspreader"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/threads",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/threads"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/refined_contig2bin",
                        "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/bin_stats",
                        "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/bin_weights",
                        "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/edge_weights",
                        "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/graph_links"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/maxbin2_binspreader"
                },
                {
                    "doc": "Preparation of BinSpreader refined MaxBin2 bins output files to a specific output folder",
                    "label": "MaxBin2 BinSpreader output folder",
                    "when": "$(inputs.run_binspreader && inputs.assembly_graph !== null)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "BinRefiner_MaxBin2_BinSPreader",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_binspreader_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/refined_contig2bin",
                                "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/bin_stats",
                                "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/bin_weights",
                                "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/edge_weights",
                                "#workflow_metagenomics_binning.cwl/maxbin2_binspreader/graph_links"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_binspreader_files_to_folder/files"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_binspreader",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_binspreader_files_to_folder/run_binspreader"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/maxbin2_binspreader_files_to_folder/results"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/maxbin2_binspreader_files_to_folder"
                },
                {
                    "label": "MaxBin2 to contig to bins",
                    "doc": "List the contigs and their corresponding bin.",
                    "run": "#fasta_to_contig2bin.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/maxbin2_to_folder/results",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_contig2bin/bin_folder"
                        },
                        {
                            "valueFrom": "MaxBin2",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_contig2bin/binner_name"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/maxbin2_contig2bin/table"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/maxbin2_contig2bin"
                },
                {
                    "doc": "Preparation of maxbin2 output files to a specific output folder.",
                    "label": "MaxBin2 output folder",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "Binner_MaxBin2",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/maxbin2/summary",
                                "#workflow_metagenomics_binning.cwl/maxbin2/log",
                                "#workflow_metagenomics_binning.cwl/maxbin2_contig2bin/table"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/maxbin2_to_folder/results"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_files_to_folder/folders"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/maxbin2_files_to_folder/results"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/maxbin2_files_to_folder"
                },
                {
                    "doc": "Create folder with MaxBin2 bins",
                    "label": "MaxBin2 bins to folder",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "MaxBin2_bins",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_to_folder/destination"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/maxbin2/bins",
                            "id": "#workflow_metagenomics_binning.cwl/maxbin2_to_folder/files"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/maxbin2_to_folder/results"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/maxbin2_to_folder"
                },
                {
                    "doc": "Binning procedure using MetaBAT2",
                    "label": "MetaBAT2 binning",
                    "run": "#metabat2.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/assembly",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2/assembly"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/metabat2_contig_depths/depths",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2/depths"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2/identifier"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/threads",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2/threads"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/metabat2/bin_dir",
                        "#workflow_metagenomics_binning.cwl/metabat2/log"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/metabat2"
                },
                {
                    "doc": "MetaBAT2 Bin refinement using BinSPreader",
                    "label": "MetaBAT2 BinSPreader",
                    "run": "#binspreader.cwl",
                    "when": "$(inputs.run_binspreader && inputs.assembly_graph !== null)",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/assembly_graph",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_binspreader/assembly_graph"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/metabat2_contig2bin/table",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_binspreader/contig2bin"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_binspreader/identifier"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_binspreader",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_binspreader/run_binspreader"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/threads",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_binspreader/threads"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/metabat2_binspreader/refined_contig2bin",
                        "#workflow_metagenomics_binning.cwl/metabat2_binspreader/bin_stats",
                        "#workflow_metagenomics_binning.cwl/metabat2_binspreader/bin_weights",
                        "#workflow_metagenomics_binning.cwl/metabat2_binspreader/edge_weights",
                        "#workflow_metagenomics_binning.cwl/metabat2_binspreader/graph_links"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/metabat2_binspreader"
                },
                {
                    "doc": "Preparation of BinSpreader refined MetaBAT2 bins output files + unbinned contigs to a specific output folder",
                    "label": "MetaBAT2 BinSpreader output folder",
                    "when": "$(inputs.run_binspreader && inputs.assembly_graph !== null)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "BinRefiner_MetaBAT2_BinSPreader",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_binspreader_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/metabat2_binspreader/refined_contig2bin",
                                "#workflow_metagenomics_binning.cwl/metabat2_binspreader/bin_stats",
                                "#workflow_metagenomics_binning.cwl/metabat2_binspreader/bin_weights",
                                "#workflow_metagenomics_binning.cwl/metabat2_binspreader/edge_weights",
                                "#workflow_metagenomics_binning.cwl/metabat2_binspreader/graph_links"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_binspreader_files_to_folder/files"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_binspreader",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_binspreader_files_to_folder/run_binspreader"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/metabat2_binspreader_files_to_folder/results"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/metabat2_binspreader_files_to_folder"
                },
                {
                    "label": "MetaBAT2 to contig to bins",
                    "doc": "List the contigs and their corresponding bin.",
                    "run": "#fasta_to_contig2bin.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/metabat2_filter_bins/output_folder",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_contig2bin/bin_folder"
                        },
                        {
                            "valueFrom": "MetaBAT2",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_contig2bin/binner_name"
                        },
                        {
                            "valueFrom": "fa",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_contig2bin/extension"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/metabat2_contig2bin/table"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/metabat2_contig2bin"
                },
                {
                    "label": "contig depths",
                    "doc": "MetabatContigDepths to obtain the depth file used in the MetaBat2 and SemiBin2 binning process",
                    "run": "#metabatContigDepths.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/bam_file",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_contig_depths/bamFile"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_contig_depths/identifier"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/metabat2_contig_depths/depths"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/metabat2_contig_depths"
                },
                {
                    "doc": "Preparation of MetaBAT2 output files + unbinned contigs to a specific output folder",
                    "label": "MetaBAT2 output folder",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "Binner_MetaBAT2",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/metabat2/log",
                                "#workflow_metagenomics_binning.cwl/metabat2_contig_depths/depths",
                                "#workflow_metagenomics_binning.cwl/metabat2_contig2bin/table"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/metabat2/bin_dir"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_files_to_folder/folders"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/metabat2_files_to_folder/results"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/metabat2_files_to_folder"
                },
                {
                    "doc": "Only keep genome bin fasta files (exlude e.g TooShort.fa)",
                    "label": "Keep MetaBAT2 genome bins",
                    "run": "#folder_file_regex.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/metabat2/bin_dir",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_filter_bins/folder"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_filter_bins/output_as_folder"
                        },
                        {
                            "valueFrom": "MetaBAT2_bins",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_filter_bins/output_folder_name"
                        },
                        {
                            "valueFrom": "bin\\.[0-9]+\\.fa",
                            "id": "#workflow_metagenomics_binning.cwl/metabat2_filter_bins/regex"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/metabat2_filter_bins/output_folder"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/metabat2_filter_bins"
                },
                {
                    "doc": "Remove unbinned fasta from bin directory. For analyis by subsequent tools.",
                    "label": "Remove unbinned",
                    "run": "#folder_file_regex.cwl",
                    "in": [
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_binning.cwl/remove_unbinned/exclude"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/das_tool/bin_dir",
                            "id": "#workflow_metagenomics_binning.cwl/remove_unbinned/folder"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_binning.cwl/remove_unbinned/output_as_folder"
                        },
                        {
                            "valueFrom": "DAS_Tool_genome_bins",
                            "id": "#workflow_metagenomics_binning.cwl/remove_unbinned/output_folder_name"
                        },
                        {
                            "valueFrom": ".*unbinned.*",
                            "id": "#workflow_metagenomics_binning.cwl/remove_unbinned/regex"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/remove_unbinned/output_folder"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/remove_unbinned"
                },
                {
                    "doc": "Binning procedure using SemiBin2",
                    "label": "Semibin binning",
                    "run": "#semibin2_single_easy_bin.cwl",
                    "when": "$(inputs.run_semibin)",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/assembly",
                            "id": "#workflow_metagenomics_binning.cwl/semibin/assembly"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/semibin_environment",
                            "id": "#workflow_metagenomics_binning.cwl/semibin/environment"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "valueFrom": "$(self)_SemiBin",
                            "id": "#workflow_metagenomics_binning.cwl/semibin/identifier"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/metabat2_contig_depths/depths",
                            "id": "#workflow_metagenomics_binning.cwl/semibin/metabat2_depth_file"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_semibin",
                            "id": "#workflow_metagenomics_binning.cwl/semibin/run_semibin"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/threads",
                            "id": "#workflow_metagenomics_binning.cwl/semibin/threads"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/semibin/recluster_bins",
                        "#workflow_metagenomics_binning.cwl/semibin/output_bins",
                        "#workflow_metagenomics_binning.cwl/semibin/data",
                        "#workflow_metagenomics_binning.cwl/semibin/data_split",
                        "#workflow_metagenomics_binning.cwl/semibin/model",
                        "#workflow_metagenomics_binning.cwl/semibin/coverage"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/semibin"
                },
                {
                    "doc": "SemiBin2 Bin refinement using BinSPreader",
                    "label": "SemiBin2 BinSpreader",
                    "run": "#binspreader.cwl",
                    "when": "$(inputs.run_binspreader && inputs.assembly_graph !== null)",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/assembly_graph",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_binspreader/assembly_graph"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/semibin_contig2bin/table",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_binspreader/contig2bin"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/identifier",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_binspreader/identifier"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_binspreader",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_binspreader/run_binspreader"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/threads",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_binspreader/threads"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/semibin_binspreader/refined_contig2bin",
                        "#workflow_metagenomics_binning.cwl/semibin_binspreader/bin_stats",
                        "#workflow_metagenomics_binning.cwl/semibin_binspreader/bin_weights",
                        "#workflow_metagenomics_binning.cwl/semibin_binspreader/edge_weights",
                        "#workflow_metagenomics_binning.cwl/semibin_binspreader/graph_links"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/semibin_binspreader"
                },
                {
                    "doc": "Preparation of BinSpreader refined SemiBin bins output files to a specific output folder",
                    "label": "SemiBin BinSpreader output folder",
                    "when": "$(inputs.run_binspreader && inputs.assembly_graph !== null)",
                    "run": "#files_to_folder.cwl",
                    "in": [
                        {
                            "valueFrom": "BinRefiner_SemiBin_BinSPreader",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_binspreader_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/semibin_binspreader/refined_contig2bin",
                                "#workflow_metagenomics_binning.cwl/semibin_binspreader/bin_stats",
                                "#workflow_metagenomics_binning.cwl/semibin_binspreader/bin_weights",
                                "#workflow_metagenomics_binning.cwl/semibin_binspreader/edge_weights",
                                "#workflow_metagenomics_binning.cwl/semibin_binspreader/graph_links"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_binspreader_files_to_folder/files"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_binspreader",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_binspreader_files_to_folder/run_binspreader"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/semibin_binspreader_files_to_folder/results"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/semibin_binspreader_files_to_folder"
                },
                {
                    "label": "SemiBin to contig to bins",
                    "doc": "List the contigs and their corresponding bin.",
                    "run": "#fasta_to_contig2bin.cwl",
                    "when": "$(inputs.run_semibin)",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/semibin/output_bins",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_contig2bin/bin_folder"
                        },
                        {
                            "valueFrom": "SemiBin",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_contig2bin/binner_name"
                        },
                        {
                            "valueFrom": "fa",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_contig2bin/extension"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_semibin",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_contig2bin/run_semibin"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/semibin_contig2bin/table"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/semibin_contig2bin"
                },
                {
                    "doc": "Preparation of SemiBin output files to a specific output folder.",
                    "label": "SemiBin output folder",
                    "run": "#files_to_folder.cwl",
                    "when": "$(inputs.run_semibin)",
                    "in": [
                        {
                            "valueFrom": "Binner_SemiBin",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/semibin_contig2bin/table",
                                "#workflow_metagenomics_binning.cwl/semibin/data",
                                "#workflow_metagenomics_binning.cwl/semibin/data_split",
                                "#workflow_metagenomics_binning.cwl/semibin/model",
                                "#workflow_metagenomics_binning.cwl/semibin/coverage"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_files_to_folder/files"
                        },
                        {
                            "source": [
                                "#workflow_metagenomics_binning.cwl/semibin/output_bins",
                                "#workflow_metagenomics_binning.cwl/semibin/recluster_bins"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_files_to_folder/folders"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_semibin",
                            "id": "#workflow_metagenomics_binning.cwl/semibin_files_to_folder/run_semibin"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/semibin_files_to_folder/results"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/semibin_files_to_folder"
                },
                {
                    "doc": "Microbial annotation workflow of the predicted bins",
                    "label": "Annotate bins",
                    "when": "$(inputs.annotate_bins && inputs.bakta_db !== null)",
                    "run": "#workflow_microbial_annotation.cwl",
                    "scatter": [
                        "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/genome_fasta"
                    ],
                    "scatterMethod": "dotproduct",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/annotate_bins",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/annotate_bins"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/bakta_db",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/bakta_db"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/compress_output"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/eggnog_dbs",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/eggnog_dbs"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/binfolder_to_files/files",
                            "default": [],
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/genome_fasta"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/interproscan_applications",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/interproscan_applications"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/interproscan_directory",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/interproscan_directory"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/kofamscan_limit_sapp",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/kofamscan_limit_sapp"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_eggnog",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/run_eggnog"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_interproscan",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/run_interproscan"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_kofamscan",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/run_kofamscan"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/sapp_conversion"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/skip_bakta_crispr",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/skip_bakta_crispr"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/threads",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/threads"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/bakta_folder_compressed",
                        "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/compressed_other_files",
                        "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins/sapp_hdt_file"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_bins"
                },
                {
                    "doc": "Microbial annotation workflow of the predicted bins",
                    "label": "Annotate bins",
                    "when": "$(inputs.annotate_unbinned && inputs.genome_fasta !== null && inputs.bakta_db !== null)",
                    "run": "#workflow_microbial_annotation.cwl",
                    "in": [
                        {
                            "source": "#workflow_metagenomics_binning.cwl/annotate_unbinned",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/annotate_unbinned"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/bakta_db",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/bakta_db"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/compress_output"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/eggnog_dbs",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/eggnog_dbs"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/extract_unbinned/out_file",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/genome_fasta"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/interproscan_applications",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/interproscan_applications"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/interproscan_directory",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/interproscan_directory"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/kofamscan_limit_sapp",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/kofamscan_limit_sapp"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/metagenome"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_eggnog",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/run_eggnog"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_interproscan",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/run_interproscan"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/run_kofamscan",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/run_kofamscan"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/sapp_conversion"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/skip_bakta_crispr",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/skip_bakta_crispr"
                        },
                        {
                            "default": true,
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/skip_bakta_plot"
                        },
                        {
                            "source": "#workflow_metagenomics_binning.cwl/threads",
                            "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/threads"
                        }
                    ],
                    "out": [
                        "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/bakta_folder_compressed",
                        "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/compressed_other_files",
                        "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned/sapp_hdt_file"
                    ],
                    "id": "#workflow_metagenomics_binning.cwl/workflow_microbial_annotation_unbinned"
                }
            ],
            "id": "#workflow_metagenomics_binning.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/dateModified": "2024-10-02",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "ScatterFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                },
                {
                    "class": "SubworkflowFeatureRequirement"
                }
            ],
            "label": "Microbial (meta-) genome annotation",
            "doc": "Workflow for microbial genome annotation.",
            "outputs": [
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "outputSource": "#workflow_microbial_annotation.cwl/bakta_to_folder_compressed/results",
                    "id": "#workflow_microbial_annotation.cwl/bakta_folder_compressed"
                },
                {
                    "type": "Directory",
                    "outputSource": "#workflow_microbial_annotation.cwl/bakta_to_folder_uncompressed/results",
                    "id": "#workflow_microbial_annotation.cwl/bakta_to_folder_uncompressed"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "outputSource": "#workflow_microbial_annotation.cwl/compress_other/outfile",
                    "linkMerge": "merge_flattened",
                    "pickValue": "all_non_null",
                    "id": "#workflow_microbial_annotation.cwl/compressed_other_files"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputSource": "#workflow_microbial_annotation.cwl/workflow_sapp_conversion/hdt_file",
                    "id": "#workflow_microbial_annotation.cwl/sapp_hdt_file"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputSource": "#workflow_microbial_annotation.cwl/uncompressed_other/outfiles",
                    "id": "#workflow_microbial_annotation.cwl/uncompressed_other_files"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "Bakta DB",
                    "doc": "Bakta database directory (default bakta-db_v5.1-light built in the container) (optional)\n",
                    "id": "#workflow_microbial_annotation.cwl/bakta_db"
                },
                {
                    "type": "int",
                    "default": 11,
                    "doc": "Codon table 11/4. Default = 11",
                    "label": "Codon table",
                    "id": "#workflow_microbial_annotation.cwl/codon_table"
                },
                {
                    "type": "boolean",
                    "doc": "Compress output files. Default false",
                    "default": false,
                    "id": "#workflow_microbial_annotation.cwl/compress_output"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Output Destination (prov only)",
                    "doc": "Not used in this workflow. Output destination used in cwl-prov reporting only.",
                    "id": "#workflow_microbial_annotation.cwl/destination"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "record",
                            "name": "#workflow_microbial_annotation.cwl/eggnog_dbs/eggnog_dbs",
                            "fields": [
                                {
                                    "type": [
                                        "null",
                                        "Directory"
                                    ],
                                    "doc": "Directory containing all data files for the eggNOG database.",
                                    "name": "#workflow_microbial_annotation.cwl/eggnog_dbs/eggnog_dbs/data_dir"
                                },
                                {
                                    "type": [
                                        "null",
                                        "File"
                                    ],
                                    "doc": "eggNOG database file",
                                    "name": "#workflow_microbial_annotation.cwl/eggnog_dbs/eggnog_dbs/db"
                                },
                                {
                                    "type": [
                                        "null",
                                        "File"
                                    ],
                                    "doc": "eggNOG database file for diamond blast search",
                                    "name": "#workflow_microbial_annotation.cwl/eggnog_dbs/eggnog_dbs/diamond_db"
                                }
                            ]
                        }
                    ],
                    "id": "#workflow_microbial_annotation.cwl/eggnog_dbs"
                },
                {
                    "type": "File",
                    "label": "Genome fasta file",
                    "doc": "Genome fasta file used for annotation (required)",
                    "id": "#workflow_microbial_annotation.cwl/genome_fasta"
                },
                {
                    "type": "string",
                    "default": "Pfam",
                    "label": "Interproscan applications",
                    "doc": "Comma separated list of analyses:\nFunFam,SFLD,PANTHER,Gene3D,Hamap,PRINTS,ProSiteProfiles,Coils,SUPERFAMILY,SMART,CDD,PIRSR,ProSitePatterns,AntiFam,Pfam,MobiDBLite,PIRSF,NCBIfam\ndefault Pfam,SFLD,SMART,AntiFam,NCBIfam\n",
                    "id": "#workflow_microbial_annotation.cwl/interproscan_applications"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "InterProScan 5 directory",
                    "doc": "Directory of the (full) InterProScan 5 program. When not given InterProscan will not run. (optional)",
                    "id": "#workflow_microbial_annotation.cwl/interproscan_directory"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "SAPP kofamscan filter",
                    "doc": "Limit max number of entries of kofamscan hits per locus in SAPP. Default 5",
                    "default": 5,
                    "id": "#workflow_microbial_annotation.cwl/kofamscan_limit_sapp"
                },
                {
                    "type": "boolean",
                    "label": "metagenome",
                    "doc": "Run in metagenome mode. Affects only protein prediction. Default false",
                    "default": false,
                    "id": "#workflow_microbial_annotation.cwl/metagenome"
                },
                {
                    "type": "boolean",
                    "label": "Run eggNOG-mapper",
                    "doc": "Run with eggNOG-mapper annotation. Requires eggnog database files. Default false",
                    "default": false,
                    "id": "#workflow_microbial_annotation.cwl/run_eggnog"
                },
                {
                    "type": "boolean",
                    "label": "Run InterProScan",
                    "doc": "Run with eggNOG-mapper annotation. Requires InterProScan v5 program files. Default false",
                    "default": false,
                    "id": "#workflow_microbial_annotation.cwl/run_interproscan"
                },
                {
                    "type": "boolean",
                    "label": "Run kofamscan",
                    "doc": "Run with KEGG KO KoFamKOALA annotation. Default false",
                    "default": false,
                    "id": "#workflow_microbial_annotation.cwl/run_kofamscan"
                },
                {
                    "type": "boolean",
                    "doc": "Run SAPP (Semantic Annotation Platform with Provenance) on the annotations. Default true",
                    "default": true,
                    "id": "#workflow_microbial_annotation.cwl/sapp_conversion"
                },
                {
                    "type": "boolean",
                    "label": "Skip bakta CRISPR array prediction using PILER-CR",
                    "doc": "Skip CRISPR prediction",
                    "default": false,
                    "id": "#workflow_microbial_annotation.cwl/skip_bakta_crispr"
                },
                {
                    "type": "boolean",
                    "label": "Skip plot",
                    "doc": "Skip Bakta plotting",
                    "default": false,
                    "id": "#workflow_microbial_annotation.cwl/skip_bakta_plot"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 4,
                    "doc": "Number of threads to use for computational processes. Default 4",
                    "label": "Number of threads",
                    "id": "#workflow_microbial_annotation.cwl/threads"
                }
            ],
            "steps": [
                {
                    "label": "Bakta",
                    "doc": "Bacterial genome annotation tool",
                    "when": "$(inputs.bakta_db !== null)",
                    "run": "#bakta.cwl",
                    "in": [
                        {
                            "source": "#workflow_microbial_annotation.cwl/bakta_db",
                            "id": "#workflow_microbial_annotation.cwl/bakta/db"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/genome_fasta",
                            "id": "#workflow_microbial_annotation.cwl/bakta/fasta_file"
                        },
                        {
                            "default": true,
                            "id": "#workflow_microbial_annotation.cwl/bakta/keep_contig_headers"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/metagenome",
                            "id": "#workflow_microbial_annotation.cwl/bakta/meta"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/skip_bakta_crispr",
                            "id": "#workflow_microbial_annotation.cwl/bakta/skip_crispr"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/skip_bakta_plot",
                            "id": "#workflow_microbial_annotation.cwl/bakta/skip_plot"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/threads",
                            "id": "#workflow_microbial_annotation.cwl/bakta/threads"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/codon_table",
                            "id": "#workflow_microbial_annotation.cwl/bakta/translation_table"
                        }
                    ],
                    "out": [
                        "#workflow_microbial_annotation.cwl/bakta/hypo_sequences_cds",
                        "#workflow_microbial_annotation.cwl/bakta/hypo_annotation_tsv",
                        "#workflow_microbial_annotation.cwl/bakta/annotation_tsv",
                        "#workflow_microbial_annotation.cwl/bakta/summary_txt",
                        "#workflow_microbial_annotation.cwl/bakta/annotation_json",
                        "#workflow_microbial_annotation.cwl/bakta/annotation_gff3",
                        "#workflow_microbial_annotation.cwl/bakta/annotation_gbff",
                        "#workflow_microbial_annotation.cwl/bakta/annotation_embl",
                        "#workflow_microbial_annotation.cwl/bakta/sequences_fna",
                        "#workflow_microbial_annotation.cwl/bakta/sequences_ffn",
                        "#workflow_microbial_annotation.cwl/bakta/sequences_cds",
                        "#workflow_microbial_annotation.cwl/bakta/plot_png",
                        "#workflow_microbial_annotation.cwl/bakta/plot_svg"
                    ],
                    "id": "#workflow_microbial_annotation.cwl/bakta"
                },
                {
                    "label": "Compressed bakta folder",
                    "doc": "Move all compressed bakta files to a folder",
                    "run": "#files_to_folder.cwl",
                    "when": "$(inputs.compress_output)",
                    "in": [
                        {
                            "source": "#workflow_microbial_annotation.cwl/compress_output",
                            "id": "#workflow_microbial_annotation.cwl/bakta_to_folder_compressed/compress_output"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/genome_fasta",
                            "valueFrom": "$(\"Bakta_\"+self.nameroot)",
                            "id": "#workflow_microbial_annotation.cwl/bakta_to_folder_compressed/destination"
                        },
                        {
                            "source": [
                                "#workflow_microbial_annotation.cwl/compress_bakta/outfile",
                                "#workflow_microbial_annotation.cwl/bakta/plot_png"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_microbial_annotation.cwl/bakta_to_folder_compressed/files"
                        }
                    ],
                    "out": [
                        "#workflow_microbial_annotation.cwl/bakta_to_folder_compressed/results"
                    ],
                    "id": "#workflow_microbial_annotation.cwl/bakta_to_folder_compressed"
                },
                {
                    "$import": "#workflow_microbial_annotation.cwl/bakta_to_folder_uncompressed"
                },
                {
                    "label": "Compress Bakta",
                    "run": "#pigz.cwl",
                    "when": "$(inputs.compress_output)",
                    "scatter": [
                        "#workflow_microbial_annotation.cwl/compress_bakta/inputfile"
                    ],
                    "scatterMethod": "dotproduct",
                    "in": [
                        {
                            "source": "#workflow_microbial_annotation.cwl/compress_output",
                            "id": "#workflow_microbial_annotation.cwl/compress_bakta/compress_output"
                        },
                        {
                            "source": [
                                "#workflow_microbial_annotation.cwl/bakta/hypo_sequences_cds",
                                "#workflow_microbial_annotation.cwl/bakta/hypo_annotation_tsv",
                                "#workflow_microbial_annotation.cwl/bakta/annotation_tsv",
                                "#workflow_microbial_annotation.cwl/bakta/summary_txt",
                                "#workflow_microbial_annotation.cwl/bakta/annotation_json",
                                "#workflow_microbial_annotation.cwl/bakta/annotation_gff3",
                                "#workflow_microbial_annotation.cwl/bakta/annotation_gbff",
                                "#workflow_microbial_annotation.cwl/bakta/annotation_embl",
                                "#workflow_microbial_annotation.cwl/bakta/sequences_fna",
                                "#workflow_microbial_annotation.cwl/bakta/sequences_ffn",
                                "#workflow_microbial_annotation.cwl/bakta/sequences_cds",
                                "#workflow_microbial_annotation.cwl/bakta/plot_svg"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_microbial_annotation.cwl/compress_bakta/inputfile"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/threads",
                            "id": "#workflow_microbial_annotation.cwl/compress_bakta/threads"
                        }
                    ],
                    "out": [
                        "#workflow_microbial_annotation.cwl/compress_bakta/outfile"
                    ],
                    "id": "#workflow_microbial_annotation.cwl/compress_bakta"
                },
                {
                    "label": "Compressed other",
                    "doc": "Compress files when compression is true",
                    "when": "$(inputs.compress_output)",
                    "run": "#pigz.cwl",
                    "scatter": [
                        "#workflow_microbial_annotation.cwl/compress_other/inputfile"
                    ],
                    "scatterMethod": "dotproduct",
                    "in": [
                        {
                            "source": "#workflow_microbial_annotation.cwl/compress_output",
                            "id": "#workflow_microbial_annotation.cwl/compress_other/compress_output"
                        },
                        {
                            "source": [
                                "#workflow_microbial_annotation.cwl/kofamscan/output",
                                "#workflow_microbial_annotation.cwl/interproscan/json_annotations",
                                "#workflow_microbial_annotation.cwl/interproscan/tsv_annotations",
                                "#workflow_microbial_annotation.cwl/eggnogmapper/output_annotations",
                                "#workflow_microbial_annotation.cwl/eggnogmapper/output_orthologs"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_microbial_annotation.cwl/compress_other/inputfile"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/threads",
                            "id": "#workflow_microbial_annotation.cwl/compress_other/threads"
                        }
                    ],
                    "out": [
                        "#workflow_microbial_annotation.cwl/compress_other/outfile"
                    ],
                    "id": "#workflow_microbial_annotation.cwl/compress_other"
                },
                {
                    "label": "eggNOG-mapper",
                    "when": "$(inputs.run_eggnog && inputs.eggnog !== null && inputs.input_fasta.size > 1024)",
                    "run": "#eggnog-mapper.cwl",
                    "in": [
                        {
                            "source": "#workflow_microbial_annotation.cwl/threads",
                            "id": "#workflow_microbial_annotation.cwl/eggnogmapper/cpu"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/eggnog_dbs",
                            "id": "#workflow_microbial_annotation.cwl/eggnogmapper/eggnog_dbs"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/bakta/sequences_cds",
                            "id": "#workflow_microbial_annotation.cwl/eggnogmapper/input_fasta"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/run_eggnog",
                            "id": "#workflow_microbial_annotation.cwl/eggnogmapper/run_eggnog"
                        }
                    ],
                    "out": [
                        "#workflow_microbial_annotation.cwl/eggnogmapper/output_annotations",
                        "#workflow_microbial_annotation.cwl/eggnogmapper/output_orthologs"
                    ],
                    "id": "#workflow_microbial_annotation.cwl/eggnogmapper"
                },
                {
                    "label": "InterProScan 5",
                    "when": "$(inputs.run_interproscan && inputs.interproscan_directory !== null && inputs.protein_fasta.size > 1024)",
                    "run": "#interproscan_v5.cwl",
                    "in": [
                        {
                            "source": "#workflow_microbial_annotation.cwl/interproscan_applications",
                            "id": "#workflow_microbial_annotation.cwl/interproscan/applications"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/interproscan_directory",
                            "id": "#workflow_microbial_annotation.cwl/interproscan/interproscan_directory"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/bakta/sequences_cds",
                            "id": "#workflow_microbial_annotation.cwl/interproscan/protein_fasta"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/run_interproscan",
                            "id": "#workflow_microbial_annotation.cwl/interproscan/run_interproscan"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/threads",
                            "id": "#workflow_microbial_annotation.cwl/interproscan/threads"
                        }
                    ],
                    "out": [
                        "#workflow_microbial_annotation.cwl/interproscan/tsv_annotations",
                        "#workflow_microbial_annotation.cwl/interproscan/json_annotations"
                    ],
                    "id": "#workflow_microbial_annotation.cwl/interproscan"
                },
                {
                    "label": "KofamScan",
                    "when": "$(inputs.run_kofamscan && inputs.input_fasta.size > 1024)",
                    "run": "#kofamscan.cwl",
                    "in": [
                        {
                            "source": "#workflow_microbial_annotation.cwl/bakta/sequences_cds",
                            "id": "#workflow_microbial_annotation.cwl/kofamscan/input_fasta"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/run_kofamscan",
                            "id": "#workflow_microbial_annotation.cwl/kofamscan/run_kofamscan"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/threads",
                            "id": "#workflow_microbial_annotation.cwl/kofamscan/threads"
                        }
                    ],
                    "out": [
                        "#workflow_microbial_annotation.cwl/kofamscan/output"
                    ],
                    "id": "#workflow_microbial_annotation.cwl/kofamscan"
                },
                {
                    "label": "Uncompressed other",
                    "doc": "Gather files when compression is false",
                    "when": "$(inputs.compress_output == false)",
                    "run": {
                        "class": "ExpressionTool",
                        "requirements": [
                            {
                                "class": "InlineJavascriptRequirement"
                            }
                        ],
                        "inputs": [
                            {
                                "type": {
                                    "type": "array",
                                    "items": "File"
                                },
                                "id": "#workflow_microbial_annotation.cwl/uncompressed_other/run/files"
                            }
                        ],
                        "outputs": [
                            {
                                "type": {
                                    "type": "array",
                                    "items": "File"
                                },
                                "id": "#workflow_microbial_annotation.cwl/uncompressed_other/run/outfiles"
                            }
                        ],
                        "expression": "${return {'outfiles': inputs.files} }\n"
                    },
                    "in": [
                        {
                            "source": "#workflow_microbial_annotation.cwl/compress_output",
                            "id": "#workflow_microbial_annotation.cwl/uncompressed_other/compress_output"
                        },
                        {
                            "source": [
                                "#workflow_microbial_annotation.cwl/kofamscan/output",
                                "#workflow_microbial_annotation.cwl/interproscan/json_annotations",
                                "#workflow_microbial_annotation.cwl/interproscan/tsv_annotations",
                                "#workflow_microbial_annotation.cwl/eggnogmapper/output_annotations",
                                "#workflow_microbial_annotation.cwl/eggnogmapper/output_orthologs"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#workflow_microbial_annotation.cwl/uncompressed_other/files"
                        }
                    ],
                    "out": [
                        "#workflow_microbial_annotation.cwl/uncompressed_other/outfiles"
                    ],
                    "id": "#workflow_microbial_annotation.cwl/uncompressed_other"
                },
                {
                    "run": "#workflow_sapp_conversion.cwl",
                    "when": "$(inputs.sapp_conversion && inputs.embl_file.size > 1024)",
                    "in": [
                        {
                            "source": "#workflow_microbial_annotation.cwl/bakta/annotation_embl",
                            "id": "#workflow_microbial_annotation.cwl/workflow_sapp_conversion/embl_file"
                        },
                        {
                            "valueFrom": "$(inputs.embl_file.nameroot)",
                            "id": "#workflow_microbial_annotation.cwl/workflow_sapp_conversion/identifier"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/interproscan/json_annotations",
                            "id": "#workflow_microbial_annotation.cwl/workflow_sapp_conversion/interproscan_output"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/kofamscan_limit_sapp",
                            "id": "#workflow_microbial_annotation.cwl/workflow_sapp_conversion/kofamscan_limit"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/kofamscan/output",
                            "id": "#workflow_microbial_annotation.cwl/workflow_sapp_conversion/kofamscan_output"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/sapp_conversion",
                            "id": "#workflow_microbial_annotation.cwl/workflow_sapp_conversion/sapp_conversion"
                        },
                        {
                            "source": "#workflow_microbial_annotation.cwl/threads",
                            "id": "#workflow_microbial_annotation.cwl/workflow_sapp_conversion/threads"
                        }
                    ],
                    "out": [
                        "#workflow_microbial_annotation.cwl/workflow_sapp_conversion/hdt_file"
                    ],
                    "id": "#workflow_microbial_annotation.cwl/workflow_sapp_conversion"
                }
            ],
            "id": "#workflow_microbial_annotation.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/dateModified": "2024-10-02",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                },
                {
                    "class": "SubworkflowFeatureRequirement"
                }
            ],
            "label": "Metagenomics workflow",
            "doc": "Workflow pilon assembly polishing\nSteps:\n  - BBmap (Read mapping to assembly)\n  - Pilon\n",
            "outputs": [
                {
                    "label": "Pilon log",
                    "doc": "Pilon log",
                    "type": "File",
                    "outputSource": "#workflow_pilon_mapping.cwl/pilon/pilon_log",
                    "id": "#workflow_pilon_mapping.cwl/log"
                },
                {
                    "label": "Polished genome",
                    "type": "File",
                    "outputSource": "#workflow_pilon_mapping.cwl/pilon/pilon_polished_assembly",
                    "id": "#workflow_pilon_mapping.cwl/pilon_polished_assembly"
                },
                {
                    "label": "VCF file",
                    "doc": "Compressed VCF file containing the changes.",
                    "type": "File",
                    "outputSource": "#workflow_pilon_mapping.cwl/vcf_compress/outfile",
                    "id": "#workflow_pilon_mapping.cwl/vcf"
                }
            ],
            "inputs": [
                {
                    "type": "File",
                    "doc": "Assembly in fasta format",
                    "label": "Assembly",
                    "id": "#workflow_pilon_mapping.cwl/assembly"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Output Destination",
                    "doc": "Optional Output destination used for cwl-prov reporting.",
                    "id": "#workflow_pilon_mapping.cwl/destination"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Pilon fix list",
                    "doc": "A comma-separated list of categories of issues to try to fix",
                    "default": "all",
                    "id": "#workflow_pilon_mapping.cwl/fixlist"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#workflow_pilon_mapping.cwl/identifier"
                },
                {
                    "type": "File",
                    "doc": "forward sequence file locally",
                    "label": "forward reads",
                    "id": "#workflow_pilon_mapping.cwl/illumina_forward_reads"
                },
                {
                    "type": "File",
                    "doc": "reverse sequence file locally",
                    "label": "reverse reads",
                    "id": "#workflow_pilon_mapping.cwl/illumina_reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Maximum memory usage in megabytes",
                    "label": "Maximum memory in MB",
                    "default": 40000,
                    "id": "#workflow_pilon_mapping.cwl/memory"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "number of threads to use for computational processes",
                    "label": "number of threads",
                    "default": 2,
                    "id": "#workflow_pilon_mapping.cwl/threads"
                }
            ],
            "steps": [
                {
                    "label": "samtools index",
                    "doc": "Index file generation for sorted bam file",
                    "run": "#samtools_index.cwl",
                    "in": [
                        {
                            "source": "#workflow_pilon_mapping.cwl/sam_to_sorted_bam/sortedbam",
                            "id": "#workflow_pilon_mapping.cwl/bam_index/bam_file"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/threads",
                            "id": "#workflow_pilon_mapping.cwl/bam_index/threads"
                        }
                    ],
                    "out": [
                        "#workflow_pilon_mapping.cwl/bam_index/bam_index"
                    ],
                    "id": "#workflow_pilon_mapping.cwl/bam_index"
                },
                {
                    "label": "CWL hybrid bam/bai file",
                    "run": "#expression_bam_index.cwl",
                    "in": [
                        {
                            "source": "#workflow_pilon_mapping.cwl/sam_to_sorted_bam/sortedbam",
                            "id": "#workflow_pilon_mapping.cwl/expressiontool_bam_index/bam_file"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/bam_index/bam_index",
                            "id": "#workflow_pilon_mapping.cwl/expressiontool_bam_index/bam_index"
                        }
                    ],
                    "out": [
                        "#workflow_pilon_mapping.cwl/expressiontool_bam_index/hybrid_bamindex"
                    ],
                    "id": "#workflow_pilon_mapping.cwl/expressiontool_bam_index"
                },
                {
                    "label": "pilon",
                    "doc": "Pilon draft assembly polishing with the mapped reads",
                    "run": "#pilon.cwl",
                    "in": [
                        {
                            "source": "#workflow_pilon_mapping.cwl/assembly",
                            "id": "#workflow_pilon_mapping.cwl/pilon/assembly"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/expressiontool_bam_index/hybrid_bamindex",
                            "id": "#workflow_pilon_mapping.cwl/pilon/bam_file"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/fixlist",
                            "id": "#workflow_pilon_mapping.cwl/pilon/fixlist"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/identifier",
                            "id": "#workflow_pilon_mapping.cwl/pilon/identifier"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/memory",
                            "id": "#workflow_pilon_mapping.cwl/pilon/memory"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/threads",
                            "id": "#workflow_pilon_mapping.cwl/pilon/threads"
                        }
                    ],
                    "out": [
                        "#workflow_pilon_mapping.cwl/pilon/pilon_polished_assembly",
                        "#workflow_pilon_mapping.cwl/pilon/pilon_vcf",
                        "#workflow_pilon_mapping.cwl/pilon/pilon_log"
                    ],
                    "id": "#workflow_pilon_mapping.cwl/pilon"
                },
                {
                    "label": "BBMap read mapping",
                    "doc": "Illumina read mapping using BBmap on assembled contigs",
                    "run": "#bbmap.cwl",
                    "in": [
                        {
                            "source": "#workflow_pilon_mapping.cwl/illumina_forward_reads",
                            "id": "#workflow_pilon_mapping.cwl/readmapping_pilon/forward_reads"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/identifier",
                            "id": "#workflow_pilon_mapping.cwl/readmapping_pilon/identifier"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/memory",
                            "id": "#workflow_pilon_mapping.cwl/readmapping_pilon/memory"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/assembly",
                            "id": "#workflow_pilon_mapping.cwl/readmapping_pilon/reference"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/illumina_reverse_reads",
                            "id": "#workflow_pilon_mapping.cwl/readmapping_pilon/reverse_reads"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/threads",
                            "id": "#workflow_pilon_mapping.cwl/readmapping_pilon/threads"
                        }
                    ],
                    "out": [
                        "#workflow_pilon_mapping.cwl/readmapping_pilon/sam",
                        "#workflow_pilon_mapping.cwl/readmapping_pilon/stats",
                        "#workflow_pilon_mapping.cwl/readmapping_pilon/covstats",
                        "#workflow_pilon_mapping.cwl/readmapping_pilon/log"
                    ],
                    "id": "#workflow_pilon_mapping.cwl/readmapping_pilon"
                },
                {
                    "label": "sam conversion to sorted bam",
                    "doc": "Sam file conversion to a sorted bam file",
                    "run": "#sam_to_sorted-bam.cwl",
                    "in": [
                        {
                            "source": "#workflow_pilon_mapping.cwl/identifier",
                            "id": "#workflow_pilon_mapping.cwl/sam_to_sorted_bam/identifier"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/readmapping_pilon/sam",
                            "id": "#workflow_pilon_mapping.cwl/sam_to_sorted_bam/sam"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/threads",
                            "id": "#workflow_pilon_mapping.cwl/sam_to_sorted_bam/threads"
                        }
                    ],
                    "out": [
                        "#workflow_pilon_mapping.cwl/sam_to_sorted_bam/sortedbam"
                    ],
                    "id": "#workflow_pilon_mapping.cwl/sam_to_sorted_bam"
                },
                {
                    "run": "#pigz.cwl",
                    "in": [
                        {
                            "source": "#workflow_pilon_mapping.cwl/pilon/pilon_vcf",
                            "id": "#workflow_pilon_mapping.cwl/vcf_compress/inputfile"
                        },
                        {
                            "source": "#workflow_pilon_mapping.cwl/threads",
                            "id": "#workflow_pilon_mapping.cwl/vcf_compress/threads"
                        }
                    ],
                    "out": [
                        "#workflow_pilon_mapping.cwl/vcf_compress/outfile"
                    ],
                    "id": "#workflow_pilon_mapping.cwl/vcf_compress"
                }
            ],
            "id": "#workflow_pilon_mapping.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2022-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                }
            ],
            "label": "Prepare (multiple) fasta files to one file.",
            "doc": "Prepare (multiple) fasta files to one file. \nWith option to make unique headers to avoid same fasta headers, which can break some tools.\n",
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "label": "Fasta input",
                    "doc": "Fasta file(s) to prepare",
                    "id": "#workflow_prepare_fasta_db.cwl/fasta_input"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Make headers unique",
                    "doc": "Make fasta headers unique avoiding same fasta headers, which can break some tools.",
                    "default": false,
                    "id": "#workflow_prepare_fasta_db.cwl/make_headers_unique"
                },
                {
                    "type": "string",
                    "doc": "Output name for this dataset used",
                    "label": "identifier used",
                    "id": "#workflow_prepare_fasta_db.cwl/output_name"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "label": "Prepared fasta file",
                    "doc": "Prepared fasta file",
                    "outputSource": [
                        "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/file",
                        "#workflow_prepare_fasta_db.cwl/merge_input/output",
                        "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/fasta_db"
                    ],
                    "pickValue": "first_non_null",
                    "id": "#workflow_prepare_fasta_db.cwl/fasta_db"
                }
            ],
            "steps": [
                {
                    "label": "Array to file",
                    "doc": "Pick first file of filter_reference when make_headers_unique input is false",
                    "when": "$(inputs.make_headers_unique === false && inputs.fasta_input.length === 1)",
                    "run": "#array_to_file_tool.cwl",
                    "in": [
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/fasta_input"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/files"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/make_headers_unique",
                            "id": "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/make_headers_unique"
                        }
                    ],
                    "out": [
                        "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/file"
                    ],
                    "id": "#workflow_prepare_fasta_db.cwl/fasta_array_to_file"
                },
                {
                    "label": "Merge reference files",
                    "doc": "Only merge input when make unique is false.",
                    "when": "$(inputs.make_headers_unique === false && inputs.fasta_input.length > 1)",
                    "run": "#concatenate.cwl",
                    "in": [
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/fasta_input"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/infiles"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/make_headers_unique",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/make_headers_unique"
                        },
                        {
                            "valueFrom": "$(inputs.output_name)_filter-reference_merged.fa",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/outname"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/output_name",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/output_name"
                        }
                    ],
                    "out": [
                        "#workflow_prepare_fasta_db.cwl/merge_input/output"
                    ],
                    "id": "#workflow_prepare_fasta_db.cwl/merge_input"
                },
                {
                    "label": "Prepare references",
                    "doc": "Prepare references to a single fasta file and unique headers",
                    "when": "$(inputs.make_headers_unique)",
                    "run": "#prepare_fasta_db.cwl",
                    "in": [
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/fasta_files"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/fasta_input"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/make_headers_unique",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/make_headers_unique"
                        },
                        {
                            "valueFrom": "$(inputs.output_name)_filter-reference_uniq.fa.gz",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/output_file_name"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/output_name",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/output_name"
                        }
                    ],
                    "out": [
                        "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/fasta_db"
                    ],
                    "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db"
                }
            ],
            "id": "#workflow_prepare_fasta_db.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2023-01-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "StepInputExpressionRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                }
            ],
            "label": "SAPP conversion Workflow",
            "doc": "Workflow for converting annotation tool output into a GBOL RDF file (TTL/HDT) using SAPP.\nCurrent implemented tools:\n    - Bakta (embl)\n    - InterProScan\n    - eggNOG-mapper\n    - Kofamscan\n",
            "outputs": [
                {
                    "type": "File",
                    "doc": "Output directory",
                    "outputSource": "#workflow_sapp_conversion.cwl/compress_hdt/outfile",
                    "id": "#workflow_sapp_conversion.cwl/hdt_file"
                }
            ],
            "inputs": [
                {
                    "type": "int",
                    "doc": "The codon table used for gene prediction",
                    "label": "Codon table",
                    "default": 11,
                    "id": "#workflow_sapp_conversion.cwl/codon_table"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Output Destination",
                    "doc": "Output destination used for cwl-prov reporting.",
                    "id": "#workflow_sapp_conversion.cwl/destination"
                },
                {
                    "label": "eggnog-mapper output",
                    "doc": "eggnog-mapper output file. Annotations tsv file (optional)",
                    "type": [
                        "null",
                        "File"
                    ],
                    "id": "#workflow_sapp_conversion.cwl/eggnog_output"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "id": "#workflow_sapp_conversion.cwl/embl_file"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "FASTA input file",
                    "doc": "Genome sequence in FASTA format",
                    "id": "#workflow_sapp_conversion.cwl/genome_fasta"
                },
                {
                    "type": "string",
                    "doc": "Identifier of the sample being converted",
                    "label": "Identifier",
                    "id": "#workflow_sapp_conversion.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "InterProScan output",
                    "doc": "InterProScan output file. JSON or TSV (optional)",
                    "id": "#workflow_sapp_conversion.cwl/interproscan_output"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "SAPP kofamscan filter",
                    "doc": "Limit the number of hits per locus tag to be converted (0=no limit) (optional). Default 0",
                    "id": "#workflow_sapp_conversion.cwl/kofamscan_limit"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "kofamscan output",
                    "doc": "KoFamScan / KoFamKOALA output file. detail-tsv (optional)",
                    "id": "#workflow_sapp_conversion.cwl/kofamscan_output"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 4,
                    "id": "#workflow_sapp_conversion.cwl/threads"
                }
            ],
            "steps": [
                {
                    "run": "#pigz.cwl",
                    "in": [
                        {
                            "source": "#workflow_sapp_conversion.cwl/turtle_to_hdt/hdt_output",
                            "id": "#workflow_sapp_conversion.cwl/compress_hdt/inputfile"
                        },
                        {
                            "source": "#workflow_sapp_conversion.cwl/threads",
                            "id": "#workflow_sapp_conversion.cwl/compress_hdt/threads"
                        }
                    ],
                    "out": [
                        "#workflow_sapp_conversion.cwl/compress_hdt/outfile"
                    ],
                    "id": "#workflow_sapp_conversion.cwl/compress_hdt"
                },
                {
                    "when": "$(inputs.embl != null)",
                    "run": "#conversion.cwl",
                    "in": [
                        {
                            "source": "#workflow_sapp_conversion.cwl/codon_table",
                            "id": "#workflow_sapp_conversion.cwl/embl_conversion/codon_table"
                        },
                        {
                            "source": "#workflow_sapp_conversion.cwl/embl_file",
                            "id": "#workflow_sapp_conversion.cwl/embl_conversion/embl"
                        },
                        {
                            "source": "#workflow_sapp_conversion.cwl/identifier",
                            "id": "#workflow_sapp_conversion.cwl/embl_conversion/identifier"
                        }
                    ],
                    "out": [
                        "#workflow_sapp_conversion.cwl/embl_conversion/output"
                    ],
                    "id": "#workflow_sapp_conversion.cwl/embl_conversion"
                },
                {
                    "when": "$(inputs.fasta != null)",
                    "run": "#conversion.cwl",
                    "in": [
                        {
                            "source": "#workflow_sapp_conversion.cwl/codon_table",
                            "id": "#workflow_sapp_conversion.cwl/genome_conversion/codon_table"
                        },
                        {
                            "source": "#workflow_sapp_conversion.cwl/genome_fasta",
                            "id": "#workflow_sapp_conversion.cwl/genome_conversion/fasta"
                        },
                        {
                            "source": "#workflow_sapp_conversion.cwl/identifier",
                            "id": "#workflow_sapp_conversion.cwl/genome_conversion/identifier"
                        }
                    ],
                    "out": [
                        "#workflow_sapp_conversion.cwl/genome_conversion/output"
                    ],
                    "id": "#workflow_sapp_conversion.cwl/genome_conversion"
                },
                {
                    "run": "#conversion_eggnog.cwl",
                    "when": "$(inputs.resultfile !== null)",
                    "in": [
                        {
                            "source": "#workflow_sapp_conversion.cwl/identifier",
                            "id": "#workflow_sapp_conversion.cwl/sapp_eggnog/output_prefix"
                        },
                        {
                            "source": [
                                "#workflow_sapp_conversion.cwl/sapp_interproscan/interproscan_ttl",
                                "#workflow_sapp_conversion.cwl/sapp_kofamscan/kofamscan_ttl",
                                "#workflow_sapp_conversion.cwl/embl_conversion/output"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#workflow_sapp_conversion.cwl/sapp_eggnog/rdf"
                        },
                        {
                            "source": "#workflow_sapp_conversion.cwl/eggnog_output",
                            "id": "#workflow_sapp_conversion.cwl/sapp_eggnog/resultfile"
                        }
                    ],
                    "out": [
                        "#workflow_sapp_conversion.cwl/sapp_eggnog/eggnog_ttl"
                    ],
                    "id": "#workflow_sapp_conversion.cwl/sapp_eggnog"
                },
                {
                    "run": "#conversion_interproscan.cwl",
                    "when": "$(inputs.resultfile !== null)",
                    "in": [
                        {
                            "source": "#workflow_sapp_conversion.cwl/identifier",
                            "id": "#workflow_sapp_conversion.cwl/sapp_interproscan/output_prefix"
                        },
                        {
                            "source": [
                                "#workflow_sapp_conversion.cwl/sapp_kofamscan/kofamscan_ttl",
                                "#workflow_sapp_conversion.cwl/embl_conversion/output"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#workflow_sapp_conversion.cwl/sapp_interproscan/rdf"
                        },
                        {
                            "source": "#workflow_sapp_conversion.cwl/interproscan_output",
                            "id": "#workflow_sapp_conversion.cwl/sapp_interproscan/resultfile"
                        }
                    ],
                    "out": [
                        "#workflow_sapp_conversion.cwl/sapp_interproscan/interproscan_ttl"
                    ],
                    "id": "#workflow_sapp_conversion.cwl/sapp_interproscan"
                },
                {
                    "run": "#conversion_kofamscan.cwl",
                    "when": "$(inputs.resultfile !== null)",
                    "in": [
                        {
                            "source": "#workflow_sapp_conversion.cwl/kofamscan_limit",
                            "id": "#workflow_sapp_conversion.cwl/sapp_kofamscan/limit"
                        },
                        {
                            "source": "#workflow_sapp_conversion.cwl/identifier",
                            "id": "#workflow_sapp_conversion.cwl/sapp_kofamscan/output_prefix"
                        },
                        {
                            "source": "#workflow_sapp_conversion.cwl/embl_conversion/output",
                            "id": "#workflow_sapp_conversion.cwl/sapp_kofamscan/rdf"
                        },
                        {
                            "source": "#workflow_sapp_conversion.cwl/kofamscan_output",
                            "id": "#workflow_sapp_conversion.cwl/sapp_kofamscan/resultfile"
                        }
                    ],
                    "out": [
                        "#workflow_sapp_conversion.cwl/sapp_kofamscan/kofamscan_ttl"
                    ],
                    "id": "#workflow_sapp_conversion.cwl/sapp_kofamscan"
                },
                {
                    "run": "#toHDT.cwl",
                    "in": [
                        {
                            "source": [
                                "#workflow_sapp_conversion.cwl/sapp_eggnog/eggnog_ttl",
                                "#workflow_sapp_conversion.cwl/sapp_interproscan/interproscan_ttl",
                                "#workflow_sapp_conversion.cwl/sapp_kofamscan/kofamscan_ttl",
                                "#workflow_sapp_conversion.cwl/embl_conversion/output"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#workflow_sapp_conversion.cwl/turtle_to_hdt/input"
                        },
                        {
                            "source": "#workflow_sapp_conversion.cwl/identifier",
                            "valueFrom": "$(self).SAPP.hdt",
                            "id": "#workflow_sapp_conversion.cwl/turtle_to_hdt/output"
                        }
                    ],
                    "out": [
                        "#workflow_sapp_conversion.cwl/turtle_to_hdt/hdt_output"
                    ],
                    "id": "#workflow_sapp_conversion.cwl/turtle_to_hdt"
                }
            ],
            "id": "#workflow_sapp_conversion.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2021-00-00",
            "https://schema.org/dateModified": "2024-08-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        }
    ],
    "cwlVersion": "v1.2",
    "$schemas": [
        "http://edamontology.org/EDAM_1.18.owl",
        "https://schema.org/version/latest/schemaorg-current-http.rdf",
        "http://edamontology.org/EDAM_1.20.owl",
        "https://schema.org/version/latest/schemaorg-current-https.rdf",
        "http://edamontology.org/EDAM_1.16.owl"
    ],
    "$namespaces": {
        "s": "https://schema.org/"
    }
}
