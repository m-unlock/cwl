{
    "$graph": [
        {
            "class": "CommandLineTool",
            "label": "Concatenate multiple files",
            "baseCommand": [
                "cat"
            ],
            "stdout": "$(inputs.outname)",
            "hints": [
                {
                    "dockerPull": "debian:buster",
                    "class": "DockerRequirement"
                }
            ],
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#concatenate.cwl/infiles"
                },
                {
                    "type": "string",
                    "id": "#concatenate.cwl/outname"
                }
            ],
            "id": "#concatenate.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2021-00-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential",
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outname)"
                    },
                    "id": "#concatenate.cwl/output"
                }
            ]
        },
        {
            "class": "CommandLineTool",
            "label": "compress a file multithreaded with pigz",
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/pigz:2.8",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.8"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/pigz"
                            ],
                            "package": "pigz"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "pigz",
                "-c"
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.inputfile)"
                }
            ],
            "stdout": "$(inputs.inputfile.basename).gz",
            "inputs": [
                {
                    "type": "File",
                    "id": "#pigz.cwl/inputfile"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "-p"
                    },
                    "id": "#pigz.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.inputfile.basename).gz"
                    },
                    "id": "#pigz.cwl/outfile"
                }
            ],
            "id": "#pigz.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2020-00-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Prepare fasta DB",
            "doc": "Prepares fasta file for so it does not contain duplicate fasta headers.\nOnly looks at the first part of the header before any whitespace.\nAdds and incremental number in the header.\n\nExpects fasta file(s) or plaintext fasta(s). Not mixed!    \n",
            "requirements": [
                {
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "prepare_fasta_db",
                            "writable": true
                        },
                        {
                            "entryname": "script.sh",
                            "entry": "#!/bin/bash\necho -e \"\\\n#/usr/bin/python3\nimport sys\\n\\\nheaders = set()\\n\\\nc = 0\\n\\\nfor line in sys.stdin:\\n\\\n  splitline = line.split()\\n\\\n  if line[0] == '>':    \\n\\\n    if splitline[0] in headers:\\n\\\n      c += 1\\n\\\n      print(splitline[0]+'.x'+str(c)+' '+' '.join(splitline[1:]))\\n\\\n    else:\\n\\\n      print(line.strip())\\n\\\n    headers.add(splitline[0])\\n\\\n  else:\\n\\\n    print(line.strip())\" > ./dup.py\nout_name=$1\nshift\n\n# python container does not have the command 'file' anymore\n# if file $@ | grep gzip; then\nif [[ $@ == *.gz ]]; then\n  zcat $@ | python3 ./dup.py | gzip > $out_name\nelse\n  cat $@ | python3 ./dup.py | gzip > $out_name\nfi"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "python:3.10-bookworm",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "3.10.6"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/python"
                            ],
                            "package": "python3"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "bash",
                "script.sh"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "label": "fasta files",
                    "doc": "Fasta file(s) to be the prepared. Can also be gzipped (not mixed!)",
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#prepare_fasta_db.cwl/fasta_files"
                },
                {
                    "type": "string",
                    "label": "Output outfile",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#prepare_fasta_db.cwl/output_file_name"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_file_name)"
                    },
                    "id": "#prepare_fasta_db.cwl/fasta_db"
                }
            ],
            "id": "#prepare_fasta_db.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-07-00",
            "https://schema.org/dateModified": "2023-01-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "label": "Convert an array of 1 file to a file object",
            "doc": "Converts the array and returns the first file in the array. \nShould only be used when 1 file is in the array.\n",
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "id": "#array_to_file_tool.cwl/files"
                }
            ],
            "baseCommand": [
                "mv"
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.files[0].path)",
                    "position": 1
                },
                {
                    "valueFrom": "./",
                    "position": 2
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.files[0].basename)"
                    },
                    "id": "#array_to_file_tool.cwl/file"
                }
            ],
            "id": "#array_to_file_tool.cwl"
        },
        {
            "class": "ExpressionTool",
            "doc": "Transforms the input files to a mentioned directory\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "string",
                    "id": "#files_to_folder.cwl/destination"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "id": "#files_to_folder.cwl/files"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "Directory"
                        }
                    ],
                    "id": "#files_to_folder.cwl/folders"
                }
            ],
            "expression": "${\n  var array = []\n  if (inputs.files != null) {\n    array = array.concat(inputs.files)\n  }\n  if (inputs.folders != null) {\n    array = array.concat(inputs.folders)\n  }\n  var r = {\n     'results':\n       { \"class\": \"Directory\",\n         \"basename\": inputs.destination,\n         \"listing\": array\n       } \n     };\n   return r; \n }\n",
            "outputs": [
                {
                    "type": "Directory",
                    "id": "#files_to_folder.cwl/results"
                }
            ],
            "id": "#files_to_folder.cwl",
            "http://schema.org/citation": "https://m-unlock.nl",
            "http://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "http://schema.org/dateModified": "2024-10-07",
            "http://schema.org/dateCreated": "2020-00-00",
            "http://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "http://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Filtlong",
            "doc": "Filtlong is a tool for filtering long reads by quality. It can take a set of long reads and produce a smaller, better subset. \nIt uses both read length (longer is better) and read identity (higher is better) when choosing which reads pass the filter.\n",
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/filtlong:0.2.1--hd03093a_1",
                    "class": "DockerRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "filtlong",
                            "writable": true
                        },
                        {
                            "entryname": "script.sh",
                            "entry": "#!/bin/bash\noutname=$1\nlongreads=$2\nshift;shift;\nfiltlong $longreads $@ 2> >(tee -a $outname.filtlong.log>&2) | gzip > $outname.fastq.gz"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "baseCommand": [
                "bash",
                "-x",
                "script.sh"
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_filename).filtlong.log"
                    },
                    "id": "#filtlong.cwl/log"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.output_filename).fastq.gz"
                    },
                    "id": "#filtlong.cwl/output_reads"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Reference assembly",
                    "doc": "Reference assembly in FASTA format",
                    "inputBinding": {
                        "prefix": "--assembly",
                        "position": 13
                    },
                    "id": "#filtlong.cwl/assembly"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Forward reads",
                    "doc": "Forward reference Illumina reads in FASTQ format",
                    "inputBinding": {
                        "prefix": "-illumina_1",
                        "position": 11
                    },
                    "id": "#filtlong.cwl/forward_reads"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Keep percentage",
                    "doc": "Keep only this percentage of the best reads (measured by bases)",
                    "inputBinding": {
                        "prefix": "--keep_percent",
                        "position": 4
                    },
                    "id": "#filtlong.cwl/keep_percent"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Length weight",
                    "doc": "Weight given to the length score (default; 1)",
                    "inputBinding": {
                        "prefix": "--length_weight",
                        "position": 14
                    },
                    "id": "#filtlong.cwl/length_weight"
                },
                {
                    "type": "File",
                    "label": "Long reads",
                    "doc": "Long reads in fastq format",
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#filtlong.cwl/long_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Maximum length",
                    "doc": "Maximum read length threshold",
                    "inputBinding": {
                        "prefix": "--max_length",
                        "position": 6
                    },
                    "id": "#filtlong.cwl/maximum_length"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Mean quality weight",
                    "doc": "Weight given to the mean quality score (default; 1)",
                    "inputBinding": {
                        "prefix": "--mean_q_weight",
                        "position": 15
                    },
                    "id": "#filtlong.cwl/mean_q_weight"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Minimum mean quality",
                    "doc": "Minimum mean quality threshold",
                    "inputBinding": {
                        "prefix": "--min_mean_q",
                        "position": 7
                    },
                    "id": "#filtlong.cwl/min_mean_q"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Minimum window quality",
                    "doc": "Minimum window quality threshold",
                    "inputBinding": {
                        "prefix": "--min_window_q",
                        "position": 8
                    },
                    "id": "#filtlong.cwl/min_window_q"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Minimum length",
                    "doc": "Minimum read length threshold",
                    "inputBinding": {
                        "prefix": "--min_length",
                        "position": 5
                    },
                    "id": "#filtlong.cwl/minimum_length"
                },
                {
                    "type": "string",
                    "label": "Output filename",
                    "doc": "Output filename (fastq.gz will be added by default)",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#filtlong.cwl/output_filename"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Reverse reads",
                    "doc": "Reverse reference Illumina reads in FASTQ format",
                    "inputBinding": {
                        "prefix": "-illumina_2",
                        "position": 12
                    },
                    "id": "#filtlong.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Split",
                    "doc": "Split reads at this many (or more) consecutive non-k-mer-matching bases",
                    "inputBinding": {
                        "prefix": "--trim",
                        "position": 10
                    },
                    "id": "#filtlong.cwl/split"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "Target bases",
                    "doc": "Keep only the best reads up to this many total bases",
                    "inputBinding": {
                        "prefix": "--target_bases",
                        "position": 3
                    },
                    "id": "#filtlong.cwl/target_bases"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Trim",
                    "doc": "Trim non-k-mer-matching bases from start/end of reads",
                    "inputBinding": {
                        "prefix": "--trim",
                        "position": 9
                    },
                    "id": "#filtlong.cwl/trim"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Mean window weight",
                    "doc": "Weight given to the window quality score (default; 1)",
                    "inputBinding": {
                        "prefix": "--window_q_weight",
                        "position": 16
                    },
                    "id": "#filtlong.cwl/window_q_weight"
                }
            ],
            "id": "#filtlong.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2023-01-03",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "baseCommand": [
                "kraken2"
            ],
            "label": "Kraken2",
            "doc": "Kraken2 metagenomics taxomic read classification.\n\nUpdated databases available at: https://benlangmead.github.io/aws-indexes/k2 (e.g. PlusPF-8)\nOriginal db: https://ccb.jhu.edu/software/kraken2/index.shtml?t=downloads\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/kraken2:2.1.3--pl5321hdcf5f25_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.1.3"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/kraken2",
                                "file:///home/bart/git/cwl/tools/kraken2/doi.org/10.1186/s13059-019-1891-0"
                            ],
                            "package": "kraken2"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.identifier)_$(inputs.database.path.split( '/' ).pop())_kraken2.txt",
                    "prefix": "--output"
                },
                {
                    "valueFrom": "$(inputs.identifier)_$(inputs.database.path.split( '/' ).pop())_kraken2_report.txt",
                    "prefix": "--report"
                },
                "--report-zero-counts",
                "--use-names",
                "--memory-mapping"
            ],
            "inputs": [
                {
                    "type": "boolean",
                    "doc": "input data is gzip compressed",
                    "inputBinding": {
                        "position": 3,
                        "prefix": "--bzip2-compressed"
                    },
                    "default": false,
                    "id": "#kraken2.cwl/bzip2"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Confidence",
                    "doc": "Confidence score threshold (default 0.0) must be in [0, 1]",
                    "inputBinding": {
                        "position": 4,
                        "prefix": "--confidence"
                    },
                    "id": "#kraken2.cwl/confidence"
                },
                {
                    "type": "Directory",
                    "label": "Database",
                    "doc": "Database location of kraken2",
                    "inputBinding": {
                        "prefix": "--db"
                    },
                    "id": "#kraken2.cwl/database"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Forward reads",
                    "doc": "Illumina forward read file",
                    "inputBinding": {
                        "position": 100
                    },
                    "id": "#kraken2.cwl/forward_reads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "input data is gzip compressed",
                    "inputBinding": {
                        "position": 3,
                        "prefix": "--gzip-compressed"
                    },
                    "default": false,
                    "id": "#kraken2.cwl/gzip"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#kraken2.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Nanopore reads",
                    "doc": "Oxford Nanopore Technologies reads in FASTQ",
                    "inputBinding": {
                        "position": 102
                    },
                    "id": "#kraken2.cwl/nanopore_reads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Paired end",
                    "doc": "Data is paired end (separate files)",
                    "inputBinding": {
                        "position": 2,
                        "prefix": "--paired"
                    },
                    "default": false,
                    "id": "#kraken2.cwl/paired_end"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Reverse reads",
                    "doc": "Illumina reverse read file",
                    "inputBinding": {
                        "position": 101
                    },
                    "id": "#kraken2.cwl/reverse_reads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "inputBinding": {
                        "prefix": "--threads"
                    },
                    "id": "#kraken2.cwl/threads"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_$(inputs.database.path.split( '/' ).pop())_kraken2_report.txt"
                    },
                    "id": "#kraken2.cwl/sample_report"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_$(inputs.database.path.split( '/' ).pop())_kraken2.txt"
                    },
                    "id": "#kraken2.cwl/standard_report"
                }
            ],
            "id": "#kraken2.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0002-5516-8391",
                    "https://schema.org/email": "mailto:german.royvalgarcia@wur.nl",
                    "https://schema.org/name": "Germ\u00e1n Royval"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2021-11-25",
            "https://schema.org/dateModified": "2021-11-04",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "kreport2krona.py",
            "doc": "This program takes a Kraken report file and prints out a krona-compatible TEXT file\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/krakentools:1.2--pyh5e36f6f_0",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "1.2"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/krakentools"
                            ],
                            "package": "kronatools"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "kreport2krona.py"
            ],
            "arguments": [
                {
                    "valueFrom": "$(inputs.report.nameroot)_krona.txt",
                    "prefix": "--output"
                }
            ],
            "inputs": [
                {
                    "type": "boolean",
                    "label": "Intermediate Ranks",
                    "doc": "Include non-standard levels. Default false",
                    "inputBinding": {
                        "prefix": "--intermediate-ranks"
                    },
                    "default": false,
                    "id": "#kreport2krona.cwl/intermediate-ranks"
                },
                {
                    "type": "boolean",
                    "label": "No Intermediate Ranks",
                    "doc": "only output standard levels [D,P,C,O,F,G,S]. Default true",
                    "inputBinding": {
                        "prefix": "--no-intermediate-ranks"
                    },
                    "default": true,
                    "id": "#kreport2krona.cwl/no-intermediate-ranks"
                },
                {
                    "type": "File",
                    "label": "Report",
                    "doc": "Kraken report file",
                    "inputBinding": {
                        "prefix": "--report"
                    },
                    "id": "#kreport2krona.cwl/report"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.report.nameroot)_krona.txt"
                    },
                    "id": "#kreport2krona.cwl/krona_txt"
                }
            ],
            "id": "#kreport2krona.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2024-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/krona:2.8.1--pl5321hdfd78af_1",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.8.1"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/krona",
                                "file:///home/bart/git/cwl/tools/krona/doi.org/10.1186/1471-2105-12-385"
                            ],
                            "package": "krona"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": [
                "ktImportText"
            ],
            "label": "Krona ktImportText",
            "doc": "Creates a Krona chart from text files listing quantities and lineages.\ntext  Tab-delimited text file. Each line should be a number followed by a list of wedges to contribute to (starting from the highest level). \nIf no wedges are listed (and just a quantity is given), it will contribute to the top level. \nIf the same lineage is listed more than once, the values will be added. Quantities can be omitted if -q is specified.\nLines beginning with \"#\" will be ignored. By default, separate datasets will be created for each input.\n",
            "arguments": [
                {
                    "prefix": "-o",
                    "valueFrom": "$(inputs.input.nameroot).html"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Highest level",
                    "doc": "Name of the highest level. Default 'all'",
                    "inputBinding": {
                        "position": 1,
                        "prefix": "-n"
                    },
                    "id": "#krona_ktImportText.cwl/highest_level"
                },
                {
                    "type": "File",
                    "label": "Tab-delimited text file",
                    "inputBinding": {
                        "position": 10
                    },
                    "id": "#krona_ktImportText.cwl/input"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "No quantity",
                    "doc": "Fields do not have a field for quantity. Default false",
                    "inputBinding": {
                        "position": 2,
                        "prefix": "-q"
                    },
                    "default": false,
                    "id": "#krona_ktImportText.cwl/no_quantity"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.input.nameroot).html"
                    },
                    "id": "#krona_ktImportText.cwl/krona_html"
                }
            ],
            "id": "#krona_ktImportText.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2024-04-10",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "Minimap2 to (un)mapped long reads",
            "doc": "Get unmapped or mapped long reads in fastq.gz format using minimap2 and samtools. Mainly used for contamination removal.\n - requires pigz!\nminimap2 | samtools | pigz\n",
            "requirements": [
                {
                    "listing": [
                        {
                            "entry": "$({class: 'Directory', listing: []})",
                            "entryname": "minimap_run",
                            "writable": true
                        },
                        {
                            "entryname": "script.sh",
                            "entry": "#!/bin/bash\n#   $1 = mapped/unmapped (-F -f)\n# 1 $2 = ref\n# 2 $3 = fastq\n# 3 $4 = preset (map-ont)\n# 4 $5 = threads\n# 5 $6 = identifier\n\nminimap2 -a -t $5 -x $4 $2 $3 | samtools fastq -@ $5 -n $1 4 | pigz -p $5 > $6_filtered.fastq.gz"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                },
                {
                    "class": "InlineJavascriptRequirement"
                }
            ],
            "baseCommand": [
                "bash",
                "-x",
                "script.sh"
            ],
            "hints": [
                {
                    "dockerPull": "docker-registry.wur.nl/m-unlock/docker/minimap2:2.28",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "version": [
                                "2.28"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/minimap2",
                                "file:///home/bart/git/cwl/tools/minimap2/doi.org/10.1093/bioinformatics/bty191"
                            ],
                            "package": "minimap2"
                        },
                        {
                            "version": [
                                "2.8"
                            ],
                            "specs": [
                                "https://anaconda.org/conda-forge/pigz"
                            ],
                            "package": "pigz"
                        },
                        {
                            "version": [
                                "1.19.2"
                            ],
                            "specs": [
                                "https://anaconda.org/bioconda/samtools",
                                "file:///home/bart/git/cwl/tools/minimap2/doi.org/10.1093/gigascience/giab008"
                            ],
                            "package": "samtools"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "arguments": [
                "${\n  if (inputs.output_mapped){\n    return '-F';\n  } else {\n    return '-f';\n  }\n}\n"
            ],
            "inputs": [
                {
                    "type": "string",
                    "doc": "Output prefix (_filtered.fastq.gz will be added)",
                    "label": "identifier",
                    "inputBinding": {
                        "position": 5
                    },
                    "id": "#minimap2_to_fastq.cwl/identifier"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Keep only reads mapped to the reference (default = false / output only unmapped reads)",
                    "label": "Keep mapped",
                    "default": false,
                    "inputBinding": {
                        "position": 6
                    },
                    "id": "#minimap2_to_fastq.cwl/output_mapped"
                },
                {
                    "type": "string",
                    "doc": "- map-pb/map-ont - PacBio CLR/Nanopore vs reference mapping\n- map-hifi - PacBio HiFi reads vs reference mapping\n- ava-pb/ava-ont - PacBio/Nanopore read overlap\n- asm5/asm10/asm20 - asm-to-ref mapping, for ~0.1/1/5% sequence divergence\n- splice/splice:hq - long-read/Pacbio-CCS spliced alignment\n- sr - genomic short-read mapping\n",
                    "label": "Read type",
                    "inputBinding": {
                        "position": 3
                    },
                    "id": "#minimap2_to_fastq.cwl/preset"
                },
                {
                    "type": "File",
                    "doc": "Query sequence in FASTQ/FASTA format (can be gzipped).",
                    "label": "Reads",
                    "inputBinding": {
                        "position": 2
                    },
                    "id": "#minimap2_to_fastq.cwl/reads"
                },
                {
                    "type": "File",
                    "doc": "Target sequence in FASTQ/FASTA format (can be gzipped).",
                    "label": "Reference",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#minimap2_to_fastq.cwl/reference"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Maximum threads to use",
                    "label": "Threads",
                    "default": 4,
                    "inputBinding": {
                        "position": 4
                    },
                    "id": "#minimap2_to_fastq.cwl/threads"
                }
            ],
            "stderr": "$(inputs.identifier)_minimap2.log",
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_filtered.fastq.gz"
                    },
                    "id": "#minimap2_to_fastq.cwl/fastq"
                },
                {
                    "type": "File",
                    "id": "#minimap2_to_fastq.cwl/log",
                    "outputBinding": {
                        "glob": "$(inputs.identifier)_minimap2.log"
                    }
                }
            ],
            "id": "#minimap2_to_fastq.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0002-5516-8391",
                    "https://schema.org/email": "mailto:german.royvalgarcia@wur.nl",
                    "https://schema.org/name": "Germ\u00e1n Royval"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2022-03-00",
            "https://schema.org/dateModified": "2022-04-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "CommandLineTool",
            "label": "NanoPlot",
            "doc": "Plotting suite for long read sequencing data and alignments\n\nModified from:\n  https://github.com/common-workflow-library/bio-cwl-tools/blob/release/nanoplot/nanoplot.cwl\n",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "networkAccess": true,
                    "class": "NetworkAccess"
                }
            ],
            "hints": [
                {
                    "dockerPull": "quay.io/biocontainers/nanoplot:1.43.0--pyhdfd78af_1",
                    "class": "DockerRequirement"
                },
                {
                    "packages": [
                        {
                            "specs": [
                                "https://github.com/wdecoster/NanoPlot/releases",
                                "file:///home/bart/git/cwl/tools/nanoplot/doi.org/10.1093/bioinformatics/btad311"
                            ],
                            "version": [
                                "1.43.0"
                            ],
                            "package": "NanoPlot"
                        }
                    ],
                    "class": "SoftwareRequirement"
                }
            ],
            "baseCommand": "NanoPlot",
            "inputs": [
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--alength"
                    },
                    "id": "#nanoplot.cwl/aligned_length"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "format": "http://edamontology.org/format_2572",
                    "inputBinding": {
                        "prefix": "--bam"
                    },
                    "id": "#nanoplot.cwl/bam_files"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--barcoded"
                    },
                    "id": "#nanoplot.cwl/barcoded"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "inputBinding": {
                        "prefix": "--color"
                    },
                    "id": "#nanoplot.cwl/color"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "inputBinding": {
                        "prefix": "--colormap"
                    },
                    "id": "#nanoplot.cwl/colormap"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "format": "http://edamontology.org/format_3462",
                    "inputBinding": {
                        "prefix": "--cram"
                    },
                    "id": "#nanoplot.cwl/cram_files"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--downsample"
                    },
                    "id": "#nanoplot.cwl/downsample"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--dpi"
                    },
                    "id": "#nanoplot.cwl/dpi"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--drop_outliers"
                    },
                    "id": "#nanoplot.cwl/drop_outliers"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "format": "http://edamontology.org/format_1931",
                    "inputBinding": {
                        "prefix": "--fasta"
                    },
                    "id": "#nanoplot.cwl/fasta_files"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "inputBinding": {
                        "prefix": "--fastq"
                    },
                    "id": "#nanoplot.cwl/fastq_files"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "inputBinding": {
                        "prefix": "--prefix"
                    },
                    "default": "",
                    "id": "#nanoplot.cwl/file_prefix"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "inputBinding": {
                        "prefix": "--font_scale"
                    },
                    "id": "#nanoplot.cwl/font_scale"
                },
                {
                    "type": [
                        {
                            "type": "enum",
                            "symbols": [
                                "#nanoplot.cwl/format/eps",
                                "#nanoplot.cwl/format/jpeg",
                                "#nanoplot.cwl/format/jpg",
                                "#nanoplot.cwl/format/pdf",
                                "#nanoplot.cwl/format/pgf",
                                "#nanoplot.cwl/format/png",
                                "#nanoplot.cwl/format/ps",
                                "#nanoplot.cwl/format/raw",
                                "#nanoplot.cwl/format/rgba",
                                "#nanoplot.cwl/format/svg",
                                "#nanoplot.cwl/format/svgz",
                                "#nanoplot.cwl/format/tif",
                                "#nanoplot.cwl/format/tiff"
                            ]
                        },
                        "null"
                    ],
                    "inputBinding": {
                        "prefix": "--format"
                    },
                    "id": "#nanoplot.cwl/format"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--no-N50"
                    },
                    "id": "#nanoplot.cwl/hide_n50"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--hide_stats"
                    },
                    "id": "#nanoplot.cwl/hide_stats"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Add NanoPlot run info in the report.",
                    "inputBinding": {
                        "prefix": "--info_in_report"
                    },
                    "id": "#nanoplot.cwl/info_in_report"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--listcolormaps"
                    },
                    "id": "#nanoplot.cwl/listcolormaps"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--listcolors"
                    },
                    "id": "#nanoplot.cwl/listcolors"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--loglength"
                    },
                    "id": "#nanoplot.cwl/log_length"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--maxlength"
                    },
                    "id": "#nanoplot.cwl/max_length"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--minlength"
                    },
                    "id": "#nanoplot.cwl/min_length"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--minqual"
                    },
                    "id": "#nanoplot.cwl/min_quality"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "inputBinding": {
                        "prefix": "--fastq_minimal"
                    },
                    "id": "#nanoplot.cwl/minimal_fastq_files"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--percentqual"
                    },
                    "id": "#nanoplot.cwl/percent_quality"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "inputBinding": {
                        "prefix": "--title"
                    },
                    "id": "#nanoplot.cwl/plot_title"
                },
                {
                    "type": [
                        {
                            "type": "array",
                            "items": {
                                "type": "enum",
                                "symbols": [
                                    "#nanoplot.cwl/plots/kde",
                                    "#nanoplot.cwl/plots/hex",
                                    "#nanoplot.cwl/plots/dot"
                                ]
                            }
                        },
                        "null"
                    ],
                    "inputBinding": {
                        "prefix": "--plots"
                    },
                    "id": "#nanoplot.cwl/plots"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "enum",
                            "symbols": [
                                "#nanoplot.cwl/read_type/1D",
                                "#nanoplot.cwl/read_type/2D",
                                "#nanoplot.cwl/read_type/1D2"
                            ]
                        }
                    ],
                    "inputBinding": {
                        "prefix": "--readtype"
                    },
                    "id": "#nanoplot.cwl/read_type"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "inputBinding": {
                        "prefix": "--fastq_rich"
                    },
                    "id": "#nanoplot.cwl/rich_fastq_files"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--runtime_until"
                    },
                    "id": "#nanoplot.cwl/run_until"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--N50"
                    },
                    "id": "#nanoplot.cwl/show_n50"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Store the extracted data in a pickle file for future plotting.",
                    "inputBinding": {
                        "prefix": "--store"
                    },
                    "id": "#nanoplot.cwl/store_pickle"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Store the extracted data in tab separated file.",
                    "inputBinding": {
                        "prefix": "--raw"
                    },
                    "id": "#nanoplot.cwl/store_raw"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "inputBinding": {
                        "prefix": "--summary"
                    },
                    "id": "#nanoplot.cwl/summary_files"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "--threads"
                    },
                    "id": "#nanoplot.cwl/threads"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Output the stats file as a properly formatted TSV.",
                    "inputBinding": {
                        "prefix": "--tsv_stats"
                    },
                    "id": "#nanoplot.cwl/tsv_stats"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "inputBinding": {
                        "prefix": "--ubam"
                    },
                    "id": "#nanoplot.cwl/ubam_files"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--pickle"
                    },
                    "id": "#nanoplot.cwl/use_pickle_file"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "inputBinding": {
                        "prefix": "--verbose"
                    },
                    "default": true,
                    "id": "#nanoplot.cwl/verbose"
                }
            ],
            "outputs": [
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)ActivePores_Over_Time.*"
                    },
                    "id": "#nanoplot.cwl/ActivePores_Over_Time"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)ActivityMap_ReadsPerChannel.*"
                    },
                    "id": "#nanoplot.cwl/ActivityMap_ReadsPerChannel"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)CumulativeYieldPlot_Gigabases.*"
                    },
                    "id": "#nanoplot.cwl/CumulativeYieldPlot_Gigabases"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)CumulativeYieldPlot_NumberOfReads.*"
                    },
                    "id": "#nanoplot.cwl/CumulativeYieldPlot_NumberOfReads"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)LengthvsQualityScatterPlot_*.*"
                    },
                    "id": "#nanoplot.cwl/LengthvsQualityScatterPlot"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)Non_weightedHistogramReadlength.*"
                    },
                    "id": "#nanoplot.cwl/Non_weightedHistogramReadlength"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)Non_weightedLogTransformed_HistogramReadlength.*"
                    },
                    "id": "#nanoplot.cwl/Non_weightedLogTransformed_HistogramReadlength"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)NumberOfReads_Over_Time.*"
                    },
                    "id": "#nanoplot.cwl/NumberOfReads_Over_Time"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)TimeLengthViolinPlot.*"
                    },
                    "id": "#nanoplot.cwl/TimeLengthViolinPlot"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)TimeQualityViolinPlot.*"
                    },
                    "id": "#nanoplot.cwl/TimeQualityViolinPlot"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)WeightedHistogramReadlength.*"
                    },
                    "id": "#nanoplot.cwl/WeightedHistogramReadlength"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)WeightedLogTransformed_HistogramReadlength.*"
                    },
                    "id": "#nanoplot.cwl/WeightedLogTransformed_HistogramReadlength"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)Yield_By_Length.*"
                    },
                    "id": "#nanoplot.cwl/Yield_By_Length"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "outputBinding": {
                        "glob": "*.log"
                    },
                    "id": "#nanoplot.cwl/log"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)NanoPlot_*.log"
                    },
                    "id": "#nanoplot.cwl/logfile"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)NanoStats.txt"
                    },
                    "id": "#nanoplot.cwl/nanostats"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)NanoPlot-data.pickle"
                    },
                    "id": "#nanoplot.cwl/pickle"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)NanoPlot-data.tsv.gz"
                    },
                    "id": "#nanoplot.cwl/raw_data"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "outputBinding": {
                        "glob": "$(inputs.file_prefix)NanoPlot-report.html"
                    },
                    "id": "#nanoplot.cwl/report"
                }
            ],
            "id": "#nanoplot.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0002-2703-8936",
                    "https://schema.org/name": "Miguel Boland"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/name": "Michael R. Crusoe"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-04-04",
            "https://schema.org/dateModified": "2023-04-03",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "ScatterFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                },
                {
                    "class": "SubworkflowFeatureRequirement"
                }
            ],
            "label": "Long Read Quality Control and Filtering",
            "doc": "**Workflow for long read quality control and contamination filtering.**\n- NanoPlot before and after filtering (read quality control)\n- Filtlong filter on quality and length\n- Kraken2 taxonomic read classification (before and after filtering)\n- Minimap2 read filtering based on given references\n\nOther UNLOCK workflows on WorkflowHub: https://workflowhub.eu/projects/16/workflows?view=default<br><br>\n\n**All tool CWL files and other workflows can be found here:**<br>\n  Tools: https://gitlab.com/m-unlock/cwl/-/tree/master/cwl <br>\n  Workflows: https://gitlab.com/m-unlock/cwl/-/tree/master/cwl/workflows<br>\n\n**How to setup and use an UNLOCK workflow:**<br>\nhttps://m-unlock.gitlab.io/docs/setup/setup.html<br>\n",
            "outputs": [
                {
                    "type": "File",
                    "label": "Filtered long reads",
                    "doc": "Filtered long reads",
                    "outputSource": [
                        "#main/reference_filter_longreads/fastq",
                        "#main/filtlong/output_reads",
                        "#main/merge_longreads_fastq/output",
                        "#main/longreads_array_to_file/file"
                    ],
                    "pickValue": "first_non_null",
                    "id": "#main/filtered_reads"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Filtlong log",
                    "doc": "Log file from filtlong longread filtering",
                    "outputSource": "#main/filtlong/log",
                    "id": "#main/filtlong_log"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "Kraken2 folder",
                    "doc": "Folder with Kraken2 output files",
                    "outputSource": "#main/kraken2_files_to_folder/results",
                    "id": "#main/kraken2_folder"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "NanoPlot filtered",
                    "doc": "Folder with quality plots from Nanoplot after filtering",
                    "outputSource": "#main/nanoplot_filtered_files_to_folder/results",
                    "id": "#main/nanoplot_filtered_folder"
                },
                {
                    "type": [
                        "null",
                        "Directory"
                    ],
                    "label": "NanoPlot unfiltered",
                    "doc": "Folder with quality plots from Nanoplot before filtering",
                    "outputSource": "#main/nanoplot_unfiltered_files_to_folder/results",
                    "id": "#main/nanoplot_unfiltered_folder"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "label": "Reference filter log",
                    "doc": "Log file from minimap2 from reference filter.",
                    "outputSource": "#main/reference_filter_longreads/log",
                    "id": "#main/reference_filter_longreads_log"
                }
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "label": "Output Destination",
                    "doc": "Optional Output destination used for cwl-prov reporting.",
                    "id": "#main/destination"
                },
                {
                    "type": "boolean",
                    "doc": "Input fastq is generated by albacore, MinKNOW or guppy  with additional information concerning channel and time. \nUsed to creating more informative quality plots (default false)\n",
                    "label": "Fastq rich (ONT)",
                    "default": false,
                    "id": "#main/fastq_rich"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "File"
                        }
                    ],
                    "doc": "Contamination references fasta file for contamination filtering. Gzipped or not (Not mixed)",
                    "label": "Contamination reference file",
                    "id": "#main/filter_references"
                },
                {
                    "type": "string",
                    "doc": "Identifier for this dataset used in this workflow",
                    "label": "identifier used",
                    "id": "#main/identifier"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "doc": "Maximum read length threshold (default 90)",
                    "label": "Maximum read length threshold",
                    "default": 90,
                    "id": "#main/keep_percent"
                },
                {
                    "type": "boolean",
                    "doc": "Keep only reads mapped to the given reference (default false)",
                    "label": "Keep mapped reads",
                    "default": false,
                    "id": "#main/keep_reference_mapped_reads"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "label": "Kraken2 confidence threshold",
                    "doc": "Confidence score threshold (default 0.0) must be between [0, 1]",
                    "id": "#main/kraken2_confidence"
                },
                {
                    "type": [
                        "null",
                        {
                            "type": "array",
                            "items": "Directory"
                        }
                    ],
                    "label": "Kraken2 database",
                    "doc": "Kraken2 database location, multiple databases is possible",
                    "default": [],
                    "id": "#main/kraken2_database"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "doc": "Weight given to the length score (default 10)",
                    "label": "Length weigth",
                    "default": 10,
                    "id": "#main/length_weight"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "doc": "Long read sequence file locally fastq format",
                    "label": "Long reads",
                    "id": "#main/longreads"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Maximum memory usage in megabytes",
                    "label": "Maximum memory in MB",
                    "default": 4000,
                    "id": "#main/memory"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "doc": "Minimum read length threshold (default 1000)",
                    "label": "Minimum read length",
                    "default": 1000,
                    "id": "#main/minimum_length"
                },
                {
                    "type": "boolean",
                    "label": "Kraken2 standard report",
                    "doc": "Also output Kraken2 standard report with per read classification. These can be large. (default false)",
                    "default": false,
                    "id": "#main/output_kraken2_standard_report"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Prepare reference with unique headers (default true)",
                    "label": "Prepare references",
                    "default": true,
                    "id": "#main/prepare_reference"
                },
                {
                    "type": [
                        {
                            "type": "enum",
                            "symbols": [
                                "#main/readtype/Nanopore",
                                "#main/readtype/PacBio"
                            ]
                        }
                    ],
                    "doc": "Type of read PacBio or Nanopore.",
                    "label": "Read type",
                    "id": "#main/readtype"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Skip kraken2 on filtered data. Default false",
                    "label": "Skip kraken2 filtered",
                    "default": false,
                    "id": "#main/skip_kraken2_filtered"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "doc": "Skip kraken2 on unfiltered data. Default false",
                    "label": "Skip kraken2 unfiltered",
                    "default": false,
                    "id": "#main/skip_kraken2_unfiltered"
                },
                {
                    "type": "boolean",
                    "doc": "Skip NanoPlot analyses of filter input data (default false)",
                    "label": "Skip NanoPlot after",
                    "default": false,
                    "id": "#main/skip_qc_filtered"
                },
                {
                    "type": "boolean",
                    "doc": "Skip NanoPlot analyses of unfiltered input data (default false)",
                    "label": "Skip NanoPlot before",
                    "default": false,
                    "id": "#main/skip_qc_unfiltered"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Skip quality trimming",
                    "doc": "Skip quality trimming tools (default false)",
                    "default": false,
                    "id": "#main/skip_quality_filter"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "label": "CWL base step number",
                    "doc": "Step number for order of steps",
                    "default": 1,
                    "id": "#main/step"
                },
                {
                    "type": "int",
                    "doc": "Number of threads to use for computational processes",
                    "label": "Number of threads",
                    "default": 2,
                    "id": "#main/threads"
                }
            ],
            "steps": [
                {
                    "label": "Filtlong",
                    "doc": "Filter longreads on quality and length",
                    "run": "#filtlong.cwl",
                    "when": "$(inputs.skip_quality_filter == false)",
                    "in": [
                        {
                            "source": "#main/identifier",
                            "id": "#main/filtlong/identifier"
                        },
                        {
                            "source": "#main/keep_percent",
                            "id": "#main/filtlong/keep_percent"
                        },
                        {
                            "source": "#main/length_weight",
                            "id": "#main/filtlong/length_weight"
                        },
                        {
                            "source": [
                                "#main/merge_longreads_fastq/output",
                                "#main/longreads_array_to_file/file"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#main/filtlong/long_reads"
                        },
                        {
                            "source": "#main/minimum_length",
                            "id": "#main/filtlong/minimum_length"
                        },
                        {
                            "valueFrom": "$(inputs.identifier)_$(inputs.readtype)_filtered",
                            "id": "#main/filtlong/output_filename"
                        },
                        {
                            "source": "#main/readtype",
                            "id": "#main/filtlong/readtype"
                        },
                        {
                            "source": "#main/skip_quality_filter",
                            "id": "#main/filtlong/skip_quality_filter"
                        }
                    ],
                    "out": [
                        "#main/filtlong/output_reads",
                        "#main/filtlong/log"
                    ],
                    "id": "#main/filtlong"
                },
                {
                    "label": "Kraken2 folder",
                    "doc": "Kraken2 files to single folder",
                    "when": "$((!inputs.skip_kraken2_unfiltered || !inputs.skip_kraken2_filtered) && inputs.kraken2_database !== null && inputs.kraken2_database.length !== 0)",
                    "in": [
                        {
                            "source": "#main/readtype",
                            "valueFrom": "$(\"Kraken2_\"+self)",
                            "id": "#main/kraken2_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/longreads_kraken2_before/sample_report",
                                "#main/longreads_kraken2_after/sample_report",
                                "#main/longreads_kraken2_krona/krona_html",
                                "#main/longreads_kraken2_compress/outfile"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/kraken2_files_to_folder/files"
                        },
                        {
                            "source": "#main/kraken2_database",
                            "id": "#main/kraken2_files_to_folder/kraken2_database"
                        },
                        {
                            "source": "#main/readtype",
                            "id": "#main/kraken2_files_to_folder/readtype"
                        },
                        {
                            "source": "#main/skip_kraken2_filtered",
                            "id": "#main/kraken2_files_to_folder/skip_kraken2_filtered"
                        },
                        {
                            "source": "#main/skip_kraken2_unfiltered",
                            "id": "#main/kraken2_files_to_folder/skip_kraken2_unfiltered"
                        }
                    ],
                    "run": "#files_to_folder.cwl",
                    "out": [
                        "#main/kraken2_files_to_folder/results"
                    ],
                    "id": "#main/kraken2_files_to_folder"
                },
                {
                    "label": "Array to file",
                    "doc": "Pick first file of longreads when only 1 file is given",
                    "when": "$(inputs.longreads.length === 1)",
                    "run": "#array_to_file_tool.cwl",
                    "in": [
                        {
                            "source": "#main/longreads",
                            "id": "#main/longreads_array_to_file/files"
                        },
                        {
                            "source": "#main/longreads",
                            "id": "#main/longreads_array_to_file/longreads"
                        }
                    ],
                    "out": [
                        "#main/longreads_array_to_file/file"
                    ],
                    "id": "#main/longreads_array_to_file"
                },
                {
                    "label": "Kraken2 filtered",
                    "doc": "Taxonomic classification of FASTQ reads after filtering",
                    "when": "$(!inputs.skip_kraken2_filtered && inputs.database !== null && inputs.database.length !== 0 && (inputs.reffilt_reads !== null || inputs.filtlong_reads !== null))",
                    "run": "#kraken2.cwl",
                    "scatter": "#main/longreads_kraken2_after/database",
                    "in": [
                        {
                            "source": "#main/kraken2_confidence",
                            "id": "#main/longreads_kraken2_after/confidence"
                        },
                        {
                            "source": "#main/kraken2_database",
                            "id": "#main/longreads_kraken2_after/database"
                        },
                        {
                            "source": "#main/filtlong/output_reads",
                            "id": "#main/longreads_kraken2_after/filtlong_reads"
                        },
                        {
                            "source": "#main/identifier",
                            "valueFrom": "$(self+\"_\"+inputs.readtype)_filtered",
                            "id": "#main/longreads_kraken2_after/identifier"
                        },
                        {
                            "valueFrom": "${ if (inputs.reffilt_reads !== null) { var fastq = inputs.reffilt_reads; } else if (inputs.filtlong_reads !== null) { var fastq = inputs.filtlong_reads; } else { var fastq = null } return fastq; }",
                            "id": "#main/longreads_kraken2_after/nanopore_reads"
                        },
                        {
                            "source": "#main/readtype",
                            "id": "#main/longreads_kraken2_after/readtype"
                        },
                        {
                            "source": "#main/reference_filter_longreads/fastq",
                            "id": "#main/longreads_kraken2_after/reffilt_reads"
                        },
                        {
                            "source": "#main/skip_kraken2_filtered",
                            "id": "#main/longreads_kraken2_after/skip_kraken2_filtered"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/longreads_kraken2_after/threads"
                        }
                    ],
                    "out": [
                        "#main/longreads_kraken2_after/sample_report",
                        "#main/longreads_kraken2_after/standard_report"
                    ],
                    "id": "#main/longreads_kraken2_after"
                },
                {
                    "label": "Kraken2 unfiltered",
                    "doc": "Taxonomic classification of FASTQ reads before filtering",
                    "when": "$(!inputs.skip_kraken2_unfiltered && inputs.database !== null && inputs.database.length !== 0)",
                    "run": "#kraken2.cwl",
                    "scatter": "#main/longreads_kraken2_before/database",
                    "in": [
                        {
                            "source": "#main/kraken2_confidence",
                            "id": "#main/longreads_kraken2_before/confidence"
                        },
                        {
                            "source": "#main/kraken2_database",
                            "id": "#main/longreads_kraken2_before/database"
                        },
                        {
                            "source": "#main/identifier",
                            "valueFrom": "$(self+\"_\"+inputs.readtype)_unfiltered",
                            "id": "#main/longreads_kraken2_before/identifier"
                        },
                        {
                            "source": [
                                "#main/merge_longreads_fastq/output",
                                "#main/longreads_array_to_file/file"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#main/longreads_kraken2_before/nanopore_reads"
                        },
                        {
                            "source": "#main/readtype",
                            "id": "#main/longreads_kraken2_before/readtype"
                        },
                        {
                            "source": "#main/skip_kraken2_unfiltered",
                            "id": "#main/longreads_kraken2_before/skip_kraken2_unfiltered"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/longreads_kraken2_before/threads"
                        }
                    ],
                    "out": [
                        "#main/longreads_kraken2_before/sample_report",
                        "#main/longreads_kraken2_before/standard_report"
                    ],
                    "id": "#main/longreads_kraken2_before"
                },
                {
                    "label": "Compress kraken2",
                    "doc": "Compress large kraken2 report file",
                    "when": "$(inputs.kraken2_database !== null && inputs.kraken2_database.length !== 0  && inputs.output_kraken2_standard_report)",
                    "run": "#pigz.cwl",
                    "scatter": "#main/longreads_kraken2_compress/inputfile",
                    "in": [
                        {
                            "source": [
                                "#main/longreads_kraken2_before/standard_report",
                                "#main/longreads_kraken2_after/standard_report"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/longreads_kraken2_compress/inputfile"
                        },
                        {
                            "source": "#main/kraken2_database",
                            "id": "#main/longreads_kraken2_compress/kraken2_database"
                        },
                        {
                            "source": "#main/output_kraken2_standard_report",
                            "id": "#main/longreads_kraken2_compress/output_kraken2_standard_report"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/longreads_kraken2_compress/threads"
                        }
                    ],
                    "out": [
                        "#main/longreads_kraken2_compress/outfile"
                    ],
                    "id": "#main/longreads_kraken2_compress"
                },
                {
                    "label": "Krona Kraken2",
                    "doc": "Visualization of kraken2 text with Krona after kronatools conversion",
                    "when": "$(inputs.kraken2_database !== null && inputs.kraken2_database.length !== 0)",
                    "run": "#krona_ktImportText.cwl",
                    "scatter": "#main/longreads_kraken2_krona/input",
                    "in": [
                        {
                            "source": [
                                "#main/longreads_kraken2krona_txt/krona_txt"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/longreads_kraken2_krona/input"
                        },
                        {
                            "source": "#main/kraken2_database",
                            "id": "#main/longreads_kraken2_krona/kraken2_database"
                        }
                    ],
                    "out": [
                        "#main/longreads_kraken2_krona/krona_html"
                    ],
                    "id": "#main/longreads_kraken2_krona"
                },
                {
                    "label": "Krakentools kreport2krona.py",
                    "doc": "Creates a Krona chart from text files listing quantities and lineages.",
                    "when": "$(inputs.kraken2_database !== null && inputs.kraken2_database.length !== 0)",
                    "run": "#kreport2krona.cwl",
                    "scatter": "#main/longreads_kraken2krona_txt/report",
                    "in": [
                        {
                            "source": "#main/kraken2_database",
                            "id": "#main/longreads_kraken2krona_txt/kraken2_database"
                        },
                        {
                            "source": [
                                "#main/longreads_kraken2_before/sample_report",
                                "#main/longreads_kraken2_after/sample_report"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/longreads_kraken2krona_txt/report"
                        }
                    ],
                    "out": [
                        "#main/longreads_kraken2krona_txt/krona_txt"
                    ],
                    "id": "#main/longreads_kraken2krona_txt"
                },
                {
                    "label": "Merge fastq files",
                    "when": "$(inputs.longreads.length > 1)",
                    "run": "#concatenate.cwl",
                    "in": [
                        {
                            "source": "#main/identifier",
                            "id": "#main/merge_longreads_fastq/identifier"
                        },
                        {
                            "source": "#main/longreads",
                            "id": "#main/merge_longreads_fastq/infiles"
                        },
                        {
                            "source": "#main/longreads",
                            "id": "#main/merge_longreads_fastq/longreads"
                        },
                        {
                            "valueFrom": "$(inputs.identifier)_$(inputs.readtype)_merged_raw.fastq.gz",
                            "id": "#main/merge_longreads_fastq/outname"
                        },
                        {
                            "source": "#main/readtype",
                            "id": "#main/merge_longreads_fastq/readtype"
                        }
                    ],
                    "out": [
                        "#main/merge_longreads_fastq/output"
                    ],
                    "id": "#main/merge_longreads_fastq"
                },
                {
                    "label": "Nanoplot filtered folder",
                    "doc": "Nanoplot plots and files to single folder",
                    "when": "$(inputs.skip_nanoplot_filtered == false && inputs.log !== null)",
                    "in": [
                        {
                            "source": "#main/readtype",
                            "valueFrom": "$(self+\"_quality_reports_filtered\")",
                            "id": "#main/nanoplot_filtered_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/nanoplot_longreads_filtered/log",
                                "#main/nanoplot_longreads_filtered/WeightedHistogramReadlength",
                                "#main/nanoplot_longreads_filtered/WeightedLogTransformed_HistogramReadlength",
                                "#main/nanoplot_longreads_filtered/Non_weightedHistogramReadlength",
                                "#main/nanoplot_longreads_filtered/Non_weightedLogTransformed_HistogramReadlength",
                                "#main/nanoplot_longreads_filtered/LengthvsQualityScatterPlot",
                                "#main/nanoplot_longreads_filtered/Yield_By_Length",
                                "#main/nanoplot_longreads_filtered/report",
                                "#main/nanoplot_longreads_filtered/logfile",
                                "#main/nanoplot_longreads_filtered/nanostats",
                                "#main/nanoplot_longreads_filtered/pickle",
                                "#main/nanoplot_longreads_filtered/raw_data",
                                "#main/nanoplot_longreads_filtered/CumulativeYieldPlot_Gigabases",
                                "#main/nanoplot_longreads_filtered/CumulativeYieldPlot_NumberOfReads",
                                "#main/nanoplot_longreads_filtered/NumberOfReads_Over_Time",
                                "#main/nanoplot_longreads_filtered/ActivePores_Over_Time",
                                "#main/nanoplot_longreads_filtered/TimeLengthViolinPlot",
                                "#main/nanoplot_longreads_filtered/TimeQualityViolinPlot"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/nanoplot_filtered_files_to_folder/files"
                        },
                        {
                            "source": "#main/nanoplot_longreads_filtered/log",
                            "id": "#main/nanoplot_filtered_files_to_folder/log"
                        },
                        {
                            "source": "#main/readtype",
                            "id": "#main/nanoplot_filtered_files_to_folder/readtype"
                        },
                        {
                            "source": "#main/skip_qc_filtered",
                            "id": "#main/nanoplot_filtered_files_to_folder/skip_nanoplot_filtered"
                        }
                    ],
                    "run": "#files_to_folder.cwl",
                    "out": [
                        "#main/nanoplot_filtered_files_to_folder/results"
                    ],
                    "id": "#main/nanoplot_filtered_files_to_folder"
                },
                {
                    "label": "NanoPlot before",
                    "doc": "NanoPlot Quality assessment and report of reads after filtering",
                    "run": "#nanoplot.cwl",
                    "when": "$(inputs.skip_nanoplot_filtered == false && inputs.fastq_files !== null)",
                    "in": [
                        {
                            "valueFrom": "${ if (inputs.reffilt_reads !== null) { var fastq = [inputs.reffilt_reads]; } else if (inputs.filtlong_reads !== null) { var fastq = [inputs.filtlong_reads]; } else { var fastq = null } return fastq; }",
                            "id": "#main/nanoplot_longreads_filtered/fastq_files"
                        },
                        {
                            "valueFrom": "$(inputs.identifier)_$(inputs.readtype)_filtered_",
                            "id": "#main/nanoplot_longreads_filtered/file_prefix"
                        },
                        {
                            "source": "#main/filtlong/output_reads",
                            "id": "#main/nanoplot_longreads_filtered/filtlong_reads"
                        },
                        {
                            "source": "#main/identifier",
                            "id": "#main/nanoplot_longreads_filtered/identifier"
                        },
                        {
                            "valueFrom": "$(inputs.identifier)_$(inputs.readtype)_filtered",
                            "id": "#main/nanoplot_longreads_filtered/plot_title"
                        },
                        {
                            "source": "#main/readtype",
                            "id": "#main/nanoplot_longreads_filtered/readtype"
                        },
                        {
                            "source": "#main/reference_filter_longreads/fastq",
                            "id": "#main/nanoplot_longreads_filtered/reffilt_reads"
                        },
                        {
                            "source": "#main/skip_qc_filtered",
                            "id": "#main/nanoplot_longreads_filtered/skip_nanoplot_filtered"
                        },
                        {
                            "default": true,
                            "id": "#main/nanoplot_longreads_filtered/store_pickle"
                        },
                        {
                            "default": true,
                            "id": "#main/nanoplot_longreads_filtered/store_raw"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/nanoplot_longreads_filtered/threads"
                        }
                    ],
                    "out": [
                        "#main/nanoplot_longreads_filtered/log",
                        "#main/nanoplot_longreads_filtered/WeightedHistogramReadlength",
                        "#main/nanoplot_longreads_filtered/WeightedLogTransformed_HistogramReadlength",
                        "#main/nanoplot_longreads_filtered/Non_weightedHistogramReadlength",
                        "#main/nanoplot_longreads_filtered/Non_weightedLogTransformed_HistogramReadlength",
                        "#main/nanoplot_longreads_filtered/LengthvsQualityScatterPlot",
                        "#main/nanoplot_longreads_filtered/Yield_By_Length",
                        "#main/nanoplot_longreads_filtered/report",
                        "#main/nanoplot_longreads_filtered/logfile",
                        "#main/nanoplot_longreads_filtered/nanostats",
                        "#main/nanoplot_longreads_filtered/pickle",
                        "#main/nanoplot_longreads_filtered/raw_data",
                        "#main/nanoplot_longreads_filtered/CumulativeYieldPlot_Gigabases",
                        "#main/nanoplot_longreads_filtered/CumulativeYieldPlot_NumberOfReads",
                        "#main/nanoplot_longreads_filtered/NumberOfReads_Over_Time",
                        "#main/nanoplot_longreads_filtered/ActivePores_Over_Time",
                        "#main/nanoplot_longreads_filtered/TimeLengthViolinPlot",
                        "#main/nanoplot_longreads_filtered/TimeQualityViolinPlot"
                    ],
                    "id": "#main/nanoplot_longreads_filtered"
                },
                {
                    "label": "NanoPlot unfiltered",
                    "doc": "NanoPlot Quality assessment and report of reads before filtering",
                    "run": "#nanoplot.cwl",
                    "when": "$(inputs.skip_nanoplot_unfiltered == false)",
                    "in": [
                        {
                            "source": "#main/longreads_array_to_file/file",
                            "id": "#main/nanoplot_longreads_unfiltered/array_to_file"
                        },
                        {
                            "valueFrom": "${ if (inputs.array_to_file !== null && inputs.fastq_rich == false) { var fastq = [inputs.array_to_file]; } else if (inputs.merged_files !== null && inputs.fastq_rich == false) { var fastq = [inputs.merged_files]; } else { var fastq = null } return fastq; }",
                            "id": "#main/nanoplot_longreads_unfiltered/fastq_files"
                        },
                        {
                            "source": "#main/fastq_rich",
                            "id": "#main/nanoplot_longreads_unfiltered/fastq_rich"
                        },
                        {
                            "valueFrom": "$(inputs.identifier)_$(inputs.readtype)_unfiltered_",
                            "id": "#main/nanoplot_longreads_unfiltered/file_prefix"
                        },
                        {
                            "source": "#main/identifier",
                            "id": "#main/nanoplot_longreads_unfiltered/identifier"
                        },
                        {
                            "source": "#main/merge_longreads_fastq/output",
                            "id": "#main/nanoplot_longreads_unfiltered/merged_files"
                        },
                        {
                            "valueFrom": "$(inputs.identifier)_$(inputs.readtype)_unfiltered",
                            "id": "#main/nanoplot_longreads_unfiltered/plot_title"
                        },
                        {
                            "source": "#main/readtype",
                            "id": "#main/nanoplot_longreads_unfiltered/readtype"
                        },
                        {
                            "valueFrom": "${ if (inputs.array_to_file !== null && inputs.fastq_rich) { var fastq = [inputs.array_to_file]; } else if (inputs.merged_files !== null && inputs.fastq_rich) { var fastq = [inputs.merged_files]; } else { var fastq = null } return fastq; }",
                            "id": "#main/nanoplot_longreads_unfiltered/rich_fastq_files"
                        },
                        {
                            "source": "#main/skip_qc_unfiltered",
                            "id": "#main/nanoplot_longreads_unfiltered/skip_nanoplot_unfiltered"
                        },
                        {
                            "default": true,
                            "id": "#main/nanoplot_longreads_unfiltered/store_pickle"
                        },
                        {
                            "default": true,
                            "id": "#main/nanoplot_longreads_unfiltered/store_raw"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/nanoplot_longreads_unfiltered/threads"
                        }
                    ],
                    "out": [
                        "#main/nanoplot_longreads_unfiltered/log",
                        "#main/nanoplot_longreads_unfiltered/WeightedHistogramReadlength",
                        "#main/nanoplot_longreads_unfiltered/WeightedLogTransformed_HistogramReadlength",
                        "#main/nanoplot_longreads_unfiltered/Non_weightedHistogramReadlength",
                        "#main/nanoplot_longreads_unfiltered/Non_weightedLogTransformed_HistogramReadlength",
                        "#main/nanoplot_longreads_unfiltered/LengthvsQualityScatterPlot",
                        "#main/nanoplot_longreads_unfiltered/Yield_By_Length",
                        "#main/nanoplot_longreads_unfiltered/report",
                        "#main/nanoplot_longreads_unfiltered/logfile",
                        "#main/nanoplot_longreads_unfiltered/nanostats",
                        "#main/nanoplot_longreads_unfiltered/pickle",
                        "#main/nanoplot_longreads_unfiltered/raw_data",
                        "#main/nanoplot_longreads_unfiltered/CumulativeYieldPlot_Gigabases",
                        "#main/nanoplot_longreads_unfiltered/CumulativeYieldPlot_NumberOfReads",
                        "#main/nanoplot_longreads_unfiltered/NumberOfReads_Over_Time",
                        "#main/nanoplot_longreads_unfiltered/ActivePores_Over_Time",
                        "#main/nanoplot_longreads_unfiltered/TimeLengthViolinPlot",
                        "#main/nanoplot_longreads_unfiltered/TimeQualityViolinPlot"
                    ],
                    "id": "#main/nanoplot_longreads_unfiltered"
                },
                {
                    "label": "Nanoplot unfiltered folder",
                    "doc": "Nanoplot plots and files to single folder",
                    "when": "$(inputs.skip_nanoplot_unfiltered == false)",
                    "in": [
                        {
                            "source": "#main/readtype",
                            "valueFrom": "$(self+\"_quality_reports_unfiltered\")",
                            "id": "#main/nanoplot_unfiltered_files_to_folder/destination"
                        },
                        {
                            "source": [
                                "#main/nanoplot_longreads_unfiltered/log",
                                "#main/nanoplot_longreads_unfiltered/WeightedHistogramReadlength",
                                "#main/nanoplot_longreads_unfiltered/WeightedLogTransformed_HistogramReadlength",
                                "#main/nanoplot_longreads_unfiltered/Non_weightedHistogramReadlength",
                                "#main/nanoplot_longreads_unfiltered/Non_weightedLogTransformed_HistogramReadlength",
                                "#main/nanoplot_longreads_unfiltered/LengthvsQualityScatterPlot",
                                "#main/nanoplot_longreads_unfiltered/Yield_By_Length",
                                "#main/nanoplot_longreads_unfiltered/report",
                                "#main/nanoplot_longreads_unfiltered/logfile",
                                "#main/nanoplot_longreads_unfiltered/nanostats",
                                "#main/nanoplot_longreads_unfiltered/pickle",
                                "#main/nanoplot_longreads_unfiltered/raw_data",
                                "#main/nanoplot_longreads_unfiltered/CumulativeYieldPlot_Gigabases",
                                "#main/nanoplot_longreads_unfiltered/CumulativeYieldPlot_NumberOfReads",
                                "#main/nanoplot_longreads_unfiltered/NumberOfReads_Over_Time",
                                "#main/nanoplot_longreads_unfiltered/ActivePores_Over_Time",
                                "#main/nanoplot_longreads_unfiltered/TimeLengthViolinPlot",
                                "#main/nanoplot_longreads_unfiltered/TimeQualityViolinPlot"
                            ],
                            "linkMerge": "merge_flattened",
                            "pickValue": "all_non_null",
                            "id": "#main/nanoplot_unfiltered_files_to_folder/files"
                        },
                        {
                            "source": "#main/readtype",
                            "id": "#main/nanoplot_unfiltered_files_to_folder/readtype"
                        },
                        {
                            "source": "#main/skip_qc_unfiltered",
                            "id": "#main/nanoplot_unfiltered_files_to_folder/skip_nanoplot_unfiltered"
                        }
                    ],
                    "run": "#files_to_folder.cwl",
                    "out": [
                        "#main/nanoplot_unfiltered_files_to_folder/results"
                    ],
                    "id": "#main/nanoplot_unfiltered_files_to_folder"
                },
                {
                    "label": "Prepare references",
                    "doc": "Prepare references to a single fasta file and unique headers",
                    "when": "$(inputs.fasta_input !== null && inputs.fasta_input.length !== 0)",
                    "run": "#workflow_prepare_fasta_db.cwl",
                    "in": [
                        {
                            "source": "#main/filter_references",
                            "id": "#main/prepare_fasta_db/fasta_input"
                        },
                        {
                            "source": "#main/prepare_reference",
                            "id": "#main/prepare_fasta_db/make_headers_unique"
                        },
                        {
                            "source": "#main/identifier",
                            "id": "#main/prepare_fasta_db/output_name"
                        }
                    ],
                    "out": [
                        "#main/prepare_fasta_db/fasta_db"
                    ],
                    "id": "#main/prepare_fasta_db"
                },
                {
                    "label": "Reference mapping",
                    "doc": "Removal of contaminated reads using minimap2 mapping",
                    "when": "$(inputs.filter_references !== null && inputs.filter_references.length !== 0)",
                    "run": "#minimap2_to_fastq.cwl",
                    "in": [
                        {
                            "source": "#main/filter_references",
                            "id": "#main/reference_filter_longreads/filter_references"
                        },
                        {
                            "source": "#main/identifier",
                            "valueFrom": "$(self+\"_\"+inputs.readtype)",
                            "id": "#main/reference_filter_longreads/identifier"
                        },
                        {
                            "source": "#main/keep_reference_mapped_reads",
                            "id": "#main/reference_filter_longreads/output_mapped"
                        },
                        {
                            "default": "map-ont",
                            "id": "#main/reference_filter_longreads/preset"
                        },
                        {
                            "source": [
                                "#main/filtlong/output_reads",
                                "#main/merge_longreads_fastq/output",
                                "#main/longreads_array_to_file/file"
                            ],
                            "pickValue": "first_non_null",
                            "id": "#main/reference_filter_longreads/reads"
                        },
                        {
                            "source": "#main/readtype",
                            "id": "#main/reference_filter_longreads/readtype"
                        },
                        {
                            "source": "#main/prepare_fasta_db/fasta_db",
                            "id": "#main/reference_filter_longreads/reference"
                        },
                        {
                            "source": "#main/threads",
                            "id": "#main/reference_filter_longreads/threads"
                        }
                    ],
                    "out": [
                        "#main/reference_filter_longreads/fastq",
                        "#main/reference_filter_longreads/log"
                    ],
                    "id": "#main/reference_filter_longreads"
                }
            ],
            "id": "#main",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateCreated": "2020-06-14",
            "https://schema.org/dateModified": "2024-04-18",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                }
            ],
            "label": "Prepare (multiple) fasta files to one file.",
            "doc": "Prepare (multiple) fasta files to one file. \nWith option to make unique headers to avoid same fasta headers, which can break some tools.\n",
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "label": "Fasta input",
                    "doc": "Fasta file(s) to prepare",
                    "id": "#workflow_prepare_fasta_db.cwl/fasta_input"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "label": "Make headers unique",
                    "doc": "Make fasta headers unique avoiding same fasta headers, which can break some tools.",
                    "default": false,
                    "id": "#workflow_prepare_fasta_db.cwl/make_headers_unique"
                },
                {
                    "type": "string",
                    "doc": "Output name for this dataset used",
                    "label": "identifier used",
                    "id": "#workflow_prepare_fasta_db.cwl/output_name"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "label": "Prepared fasta file",
                    "doc": "Prepared fasta file",
                    "outputSource": [
                        "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/file",
                        "#workflow_prepare_fasta_db.cwl/merge_input/output",
                        "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/fasta_db"
                    ],
                    "pickValue": "first_non_null",
                    "id": "#workflow_prepare_fasta_db.cwl/fasta_db"
                }
            ],
            "steps": [
                {
                    "label": "Array to file",
                    "doc": "Pick first file of filter_reference when make_headers_unique input is false",
                    "when": "$(inputs.make_headers_unique === false && inputs.fasta_input.length === 1)",
                    "run": "#array_to_file_tool.cwl",
                    "in": [
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/fasta_input"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/files"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/make_headers_unique",
                            "id": "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/make_headers_unique"
                        }
                    ],
                    "out": [
                        "#workflow_prepare_fasta_db.cwl/fasta_array_to_file/file"
                    ],
                    "id": "#workflow_prepare_fasta_db.cwl/fasta_array_to_file"
                },
                {
                    "label": "Merge reference files",
                    "doc": "Only merge input when make unique is false.",
                    "when": "$(inputs.make_headers_unique === false && inputs.fasta_input.length > 1)",
                    "run": "#concatenate.cwl",
                    "in": [
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/fasta_input"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/infiles"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/make_headers_unique",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/make_headers_unique"
                        },
                        {
                            "valueFrom": "$(inputs.output_name)_filter-reference_merged.fa",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/outname"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/output_name",
                            "id": "#workflow_prepare_fasta_db.cwl/merge_input/output_name"
                        }
                    ],
                    "out": [
                        "#workflow_prepare_fasta_db.cwl/merge_input/output"
                    ],
                    "id": "#workflow_prepare_fasta_db.cwl/merge_input"
                },
                {
                    "label": "Prepare references",
                    "doc": "Prepare references to a single fasta file and unique headers",
                    "when": "$(inputs.make_headers_unique)",
                    "run": "#prepare_fasta_db.cwl",
                    "in": [
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/fasta_files"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/fasta_input",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/fasta_input"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/make_headers_unique",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/make_headers_unique"
                        },
                        {
                            "valueFrom": "$(inputs.output_name)_filter-reference_uniq.fa.gz",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/output_file_name"
                        },
                        {
                            "source": "#workflow_prepare_fasta_db.cwl/output_name",
                            "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/output_name"
                        }
                    ],
                    "out": [
                        "#workflow_prepare_fasta_db.cwl/prepare_fasta_db/fasta_db"
                    ],
                    "id": "#workflow_prepare_fasta_db.cwl/prepare_fasta_db"
                }
            ],
            "id": "#workflow_prepare_fasta_db.cwl",
            "https://schema.org/author": [
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0009-0001-1350-5644",
                    "https://schema.org/email": "mailto:changlin.ke@wur.nl",
                    "https://schema.org/name": "Changlin Ke"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-8172-8981",
                    "https://schema.org/email": "mailto:jasper.koehorst@wur.nl",
                    "https://schema.org/name": "Jasper Koehorst"
                },
                {
                    "class": "https://schema.org/Person",
                    "https://schema.org/identifier": "https://orcid.org/0000-0001-9524-5964",
                    "https://schema.org/email": "mailto:bart.nijsse@wur.nl",
                    "https://schema.org/name": "Bart Nijsse"
                }
            ],
            "https://schema.org/citation": "https://m-unlock.nl",
            "https://schema.org/codeRepository": "https://gitlab.com/m-unlock/cwl",
            "https://schema.org/dateModified": "2024-10-07",
            "https://schema.org/dateCreated": "2023-01-00",
            "https://schema.org/license": "https://spdx.org/licenses/Apache-2.0",
            "https://schema.org/copyrightHolder": "UNLOCK - Unlocking Microbial Potential"
        }
    ],
    "cwlVersion": "v1.2",
    "$namespaces": {
        "s": "https://schema.org/"
    }
}
