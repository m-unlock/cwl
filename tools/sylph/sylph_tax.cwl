#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool
label: sylph-tax

doc: sylph is a program that performs ultrafast (1) ANI querying or (2) metagenomic profiling for metagenomic shotgun samples.


hints:
  SoftwareRequirement:
    packages:
      sylph-tax:
        version: ["1.1.2"]
        specs: ["https://anaconda.org/bioconda/sylph-tax", "https://doi.org/10.1038/s41587-024-02412-y"]
  DockerRequirement:
    dockerPull: docker-registry.wur.nl/m-unlock/docker/sylph_sylph-tax:0.8.0_1.1.2

requirements:
  InlineJavascriptRequirement: {}

# sylph-tax taxprof /data/Hpylori_unknown_sylph-profile.tsv -t GTDB_r220 -o /data/output_prefix-

baseCommand: [sylph-tax]

arguments:
  - prefix: "--output-file"
    valueFrom: $(inputs.output_filename_prefix+"_sylph-profile.tsv")

inputs:
  database:
    type: File
    doc: Sylph database
    label: Database
    inputBinding:
      position: 0

  input_tsv:
    type: File?
    doc: Single end reads, for example long reads. (fastx/gzip)
    label: Single end reads
    inputBinding:
      position: 1

  output_filename_prefix:
    type: string
    doc: Name of the output file. (_sylph-profile.tsv will be appended)
    label: Output filename (base)

  
outputs:
  sylph_tax:
    type: File
    outputBinding: {glob: '*.sylphmpa'}

s:author:
  - class: s:Person
    s:identifier: https://orcid.org/0000-0001-9524-5964
    s:email: mailto:bart.nijsse@wur.nl
    s:name: Bart Nijsse

s:citation: https://m-unlock.nl
s:codeRepository: https://gitlab.com/m-unlock/cwl
s:dateModified: "2025-02-21"
s:dateCreated: "2025-02-21"
s:license: https://spdx.org/licenses/Apache-2.0 
s:copyrightHolder: "UNLOCK - Unlocking Microbial Potential"

$namespaces:
  s: https://schema.org/
