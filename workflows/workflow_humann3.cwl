#!/usr/bin/env cwl-runner
cwlVersion: v1.2
class: Workflow
requirements:
  InlineJavascriptRequirement: {}
  StepInputExpressionRequirement: {}
  MultipleInputFeatureRequirement: {}
  SubworkflowFeatureRequirement: {}
  ScatterFeatureRequirement: {}

label: HUMAnN 3 workflow
doc: | 
  Runs MetaPhlAn 4 and HUMAnN 3 pipeline. Including illumina quality control reads. (Only paired end) \
  Includes renormalizing and all regroupings to other functional categories (EC,KO.. etc)

outputs:
  humann_genefamilies_out:
    type: File
    label: Gene families
    doc: HUMAnN 3 Gene families abundances
    outputSource: humann/genefamilies_out
  humann_pathabundance_out:
    type: File
    label: Pathway abundances
    doc: HUMAnN 3 pathway abundances
    outputSource: humann/pathabundance_out
  humann_pathcoverage_out:
    type: File
    label: Pathway coverage
    doc: HUMAnN 3 pathway coverage
    outputSource: humann/pathcoverage_out
  humann_log_out:
    type: File
    label: HUMAnN 3 log
    doc: HUMAnN 3
    outputSource: humann/log_out
  humann_stdout_out:
    type: File
    label: HUMAnN 3 stdout
    doc: HUMAnN 3 standard out
    outputSource: humann/stdout_out
  humann_pathways_unpacked:
    type: File
    label: Pathways unpacked
    doc: HUMAnN 3 pathways gene abundances
    outputSource: humann_unpack_pathways/pathway_genes_abundances
  
  regrouped_tables:
    type: File[]
    label: Regrouped tables
    doc: Regrouped tabes unnormalized
    outputSource: humann_regroup_table/regrouped_table

  normalized_tables:
    type: Directory
    label: Normalized tables
    doc: Normalized tables Director
    outputSource: files_to_folder_normalized/results


  metaphlan_profile:
    type: File
    label: MetaPhlAn4 profile
    doc: MetaPhlAn4 profile tsv
    outputSource: metaphlan/profile
  metaphlan_bt2out:
    type: File
    label: MetaPhlAn4 bt2out
    doc: MetaPhlAn4 bowtie 2 output
    outputSource: metaphlan/bowtie2out

inputs:
  identifier:
    type: string
    doc: Identifier for this dataset used in this workflow
    label: identifier used
  threads:
    type: int
    doc: Number of threads to use for computational processes
    label: Number of threads
    default: 2
  memory:
    type: int
    doc: Maximum memory usage in megabytes (default 8000)
    label: Memory usage (MB)
    default: 8000

  forward_reads:
    type: File[]
    doc: Forward sequence fastq file
    label: Forward reads
    loadListing: no_listing
  reverse_reads:
    type: File[]
    doc: Reverse sequence fastq file
    label: Reverse reads
    loadListing: no_listing

  filter_references:
    type: File[]?
    doc: Reference fasta file(s) used for pre-filtering. Can be gzipped (not mixed)
    label: Reference file(s)
    loadListing: no_listing
  use_reference_mapped_reads:
    type: boolean
    doc: Continue with reads mapped to the given reference (default false)
    label: Keep mapped reads
    default: false
  deduplicate:
    type: boolean
    doc: Remove exact duplicate reads Illumina reads with fastp (default false)
    label: Deduplicate reads
    default: false

  ## MetaPhlAn4
  metaphlan4_bt2_database:
    type: Directory
    label: MetaPhlAn4 database
    doc: MetaPhlAn4 database location
    loadListing: no_listing

  ## HUMAnN3
  uniref_dbtype:
    type:
      - type: enum
        symbols:
          - uniref50
          - uniref90
    doc: uniref50 or uniref90. Match this with your selected database!
    label: UniRef database type
  humann3_nucleotide_database:
    type: Directory
    label: HUMAnN3 nucleotide database
    doc: HUMAnN3 nucleotide database location
    loadListing: no_listing
  humann3_protein_database:
    type: Directory
    label: HUMAnN3 protein database
    doc: HUMAnN3 protein database location
    loadListing: no_listing

  # Input provenance (to be ignored when prov is not used)
  destination:
    type: string?
    label: Output Destination
    doc: Optional output destination only used for cwl-prov reporting.
  source:
    label: Input URLs used for this run
    doc: A provenance element to capture the original source of the input data
    type: string[]?

steps:
#############################################
#### Preparation of reference files
  prepare_fasta_db:
    label: Prepare references
    doc: Prepare references to a single fasta file and unique headers
    when: $(inputs.fasta_files !== null && inputs.fasta_files.length !== 0)
    run: ../tools/bbmap/prepare_fasta_db.cwl
    in:
      fasta_files: filter_references
      output_file_name:
        valueFrom: "filter-reference_prepared.fa.gz"
    out: [fasta_db]

#############################################
#### Quality workflow ILLUMINA
  workflow_quality_illumina:
    label: Quality and filtering workflow
    doc: Quality, filtering and taxonomic classification of Illumina reads
    when: $(inputs.forward_reads !== null && inputs.forward_reads.length !== 0)
    run: workflow_illumina_quality.cwl
    in:
      identifier: identifier
      forward_reads: forward_reads
      reverse_reads: reverse_reads
      deduplicate: deduplicate
      prepare_reference:
        default: false
      filter_references:
        linkMerge: merge_flattened
        source: [prepare_fasta_db/fasta_db]
        pickValue: all_non_null
      keep_reference_mapped_reads: use_reference_mapped_reads
      memory: memory
      threads: threads
    out: [QC_reverse_reads, QC_forward_reads, reports_folder]

############################################
### Merge reads
  interleave_fastq:
    label: Interleave fastq
    doc: Interleave QC forward and reverse files for subsequent tools
    run: ../tools/fastq/interleave_fastq.cwl
    in:
      identifier:
        source: identifier
        valueFrom: $(self)_interleaved
      threads: threads
      forward_reads: workflow_quality_illumina/QC_forward_reads
      reverse_reads: workflow_quality_illumina/QC_reverse_reads
    out: [fastq_out]


############################################
### MetaPhlAn 4
  metaphlan:
    label: MetaPhlAn 4
    doc: Profiling the composition of microbial communities
    run: ../tools/metaphlan/metaphlan4.cwl
    in:
      identifier: identifier
      threads: threads
      bowtie2db: metaphlan4_bt2_database
      reads: interleave_fastq/fastq_out
      input_type: 
        default: "fastq"
    out: [profile, bowtie2out]
############################################
### HUMAnN
  humann:
    label: HUMAnN 3
    doc: HMP Unified Metabolic Analysis Network.
    run: ../tools/humann/humann.cwl
    in:
      uniref_dbtype: uniref_dbtype
      identifier:
        source: identifier
        valueFrom: $(self)_$(inputs.uniref_dbtype)
      threads: threads
      input_file: interleave_fastq/fastq_out
      taxonomic_profile: metaphlan/profile
      protein_database: humann3_protein_database
      nucleotide_database: humann3_nucleotide_database
    out: [genefamilies_out, pathabundance_out, pathcoverage_out, log_out, stdout_out]
############################################
### HUMAnN - unpack pathways
  humann_unpack_pathways:
    label: Unpack pathways
    doc: HUMAnN 3 Unpack pathways
    run: ../tools/humann/humann_unpack_pathways.cwl
    in:
      uniref_dbtype: uniref_dbtype
      identifier:
        source: identifier
        valueFrom: $(self)_$(inputs.uniref_dbtype)
      input_genes: humann/genefamilies_out
      input_pathways: humann/pathabundance_out
    out: [pathway_genes_abundances]
############################################
### HUMAnN - renormalize genefamilies
  humann_renorm_table_genefamilies:
    label: Renorm gene families
    doc: HUMAnN 3 renormalize genefamilies
    scatter: units
    scatterMethod: dotproduct
    run: ../tools/humann/humann_renorm_table.cwl
    in:
      input_table: humann/genefamilies_out
      units:
        default: ["cpm","relab"]
      mode:
        default: "community"
    out: [renormalized_table]
############################################
### HUMAnN - renormalize pathways
  humann_renorm_table_pathways:
    label: Renorm pathways
    doc: HUMAnN 3 renormalize pathways
    run: ../tools/humann/humann_renorm_table.cwl
    scatter: [units, input_table]
    scatterMethod: flat_crossproduct
    in:
      input_table:
        source: [humann/pathabundance_out, humann_unpack_pathways/pathway_genes_abundances]
        linkMerge: merge_flattened
        pickValue: all_non_null
      units:
        default: ["cpm","relab"]
      mode:
        default: "community"
    out: [renormalized_table]
############################################
### HUMAnN - regroup genefamilies
  humann_regroup_table:
    label: Regroup unnormalized
    doc: HUMAnN 3 regroup genefamily
    run: ../tools/humann/humann_regroup_table.cwl
    in:
      input_table: humann/genefamilies_out
      group:
        default: "all"
      add_unireftype:
        default: "N"
      uniref_type: uniref_dbtype
    out: [regrouped_table]
###########################################
## HUMAnN - regroup renormalized genefamilies
  humann_regroup_renorm_table:
    label: Regroup renormalized
    doc: HUMAnN 3 regroup renormalized genefamilies
    run: ../tools/humann/humann_regroup_table.cwl
    scatter: input_table
    scatterMethod: dotproduct
    in:
      input_table: 
        source: [humann_renorm_table_genefamilies/renormalized_table]
        linkMerge: merge_flattened
      group:
        default: "all"
      uniref_type: uniref_dbtype
      add_unireftype:
        default: "N"
    out: [regrouped_table]
  renorm_groups_to_array:
    run: ../tools/expressions/merge_file_arrays.cwl
    label: Merge file arrays
    doc: Merges arrays of files in an array to a array of files
    in:
      input: humann_regroup_renorm_table/regrouped_table
    out: 
      [output]

#############################################
#### Prepare output
  files_to_folder_normalized:
    label: Normalized tables
    doc: Normalized tables folder
    in:
      files:
        source: [humann_renorm_table_genefamilies/renormalized_table, humann_renorm_table_pathways/renormalized_table, renorm_groups_to_array/output]
        linkMerge: merge_flattened
      destination:
        default: "normalized_tables"
    run: ../tools/expressions/files_to_folder.cwl
    out:
      [results]

s:author:
  - class: s:Person
    s:identifier: https://orcid.org/0000-0001-8172-8981
    s:email: mailto:jasper.koehorst@wur.nl
    s:name: Jasper Koehorst
  - class: s:Person
    s:identifier: https://orcid.org/0000-0001-9524-5964
    s:email: mailto:bart.nijsse@wur.nl
    s:name: Bart Nijsse
  - class: s:Person
    s:identifier: https://orcid.org/0009-0001-1350-5644
    s:email: mailto:changlin.ke@wur.nl
    s:name: Changlin Ke

s:citation: https://m-unlock.nl
s:codeRepository: https://gitlab.com/m-unlock/cwl
s:dateModified: "2025-02-20"
s:dateCreated: "2024-05-21"
s:license: https://spdx.org/licenses/Apache-2.0 
s:copyrightHolder: "UNLOCK - Unlocking Microbial Potential"


$namespaces:
  s: https://schema.org/
