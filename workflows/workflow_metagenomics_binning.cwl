#!/usr/bin/env cwl-runner
cwlVersion: v1.2
class: Workflow
requirements:
  StepInputExpressionRequirement: {}
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  SubworkflowFeatureRequirement: {}
  ScatterFeatureRequirement: {}

label: Metagenomic Binning from Assembly
doc: |
  Workflow for Metagenomics binning from assembly.<br>

  Minimal inputs are: Identifier, assembly (fasta) and a associated sorted BAM file

  Summary
    - MetaBAT2 (binning)
    - MaxBin2 (binning)
    - SemiBin2 (binning)
    - BinSPreader (bin refinement)
    - DAS Tool (bin merging)
    - binette (bin merging) In Development
    - EukRep (eukaryotic classification)
    - CheckM (bin completeness and contamination)
    - BUSCO (bin completeness)
    - GTDB-Tk (bin taxonomic classification)

  Other UNLOCK workflows on WorkflowHub: https://workflowhub.eu/projects/16/workflows?view=default<br><br>
  
  **All tool CWL files and other workflows can be found here:**<br>
    Tools: https://gitlab.com/m-unlock/cwl<br>
    Workflows: https://gitlab.com/m-unlock/cwl/workflows<br>

  **How to setup and use an UNLOCK workflow:**<br>
  https://m-unlock.gitlab.io/docs/setup/setup.html<br>

outputs:
  # proteins:
  #   type: File[]
  #   outputSource: extract_proteins/out_file
  bins:
    label: Bin files
    doc: Bins files in fasta format. To be be used in other workflows.
    type: File[]?
    outputSource: das_tool_bins/files
  metabat2_output:
    label: MetaBAT2
    doc: MetaBAT2 output directory
    type: Directory
    outputSource: metabat2_files_to_folder/results
  metabat2_binspreader_output:
    label: MetaBAT2 BinSPreader
    doc: MetaBAT2 output directory
    type: Directory?
    outputSource: metabat2_binspreader_files_to_folder/results
  maxbin2_output:
    label: MaxBin2
    doc: MaxBin2 output directoryß
    type: Directory
    outputSource: maxbin2_files_to_folder/results
  maxbin2_binspreader_output:
    label: MaxBin2 BinSPreader
    doc: MaxBin2 output directory
    type: Directory?
    outputSource: maxbin2_binspreader_files_to_folder/results
  semibin_output:
    label: SemiBin
    doc: MaxBin2 output directory
    type: Directory?
    outputSource: semibin_files_to_folder/results
  semibin_binspreader_output:
    label: SemiBin BinSPreader
    doc: MaxBin2 output directory
    type: Directory?
    outputSource: semibin_binspreader_files_to_folder/results
  das_tool_output:
    label: DAS Tool
    doc: DAS Tool output directory
    type: Directory
    outputSource: das_tool_files_to_folder/results
  checkm_output:
    label: CheckM
    doc: CheckM output directory
    type: Directory
    outputSource: checkm_files_to_folder/results
  busco_output:
    label: BUSCO
    doc: BUSCO output directory
    type: Directory
    outputSource: busco_files_to_folder/results
  gtdbtk_output:
    label: GTDB-Tk
    doc: GTDB-Tk output directory
    type: Directory?
    outputSource: gtdbtk_files_to_folder/results
  annotation_output:
    label: Annotation
    doc: Bin annotation
    type: Directory?
    outputSource: annotation_files_to_folder_bins/results
  bins_summary_table:
    label: Bins summary
    doc: Summary of info about the bins
    type: File
    outputSource: bins_summary/bins_summary_table
  bins_read_stats:
    label: Assembly/Bin read stats
    doc: General assembly and bin coverage
    type: File
    outputSource: bin_readstats/binReadStats    
  eukrep_fasta:
    label: EukRep fasta
    doc: EukRep eukaryotic classified contigs
    type: File
    outputSource: eukrep/euk_fasta_out
  eukrep_stats_file:
    label: EukRep stats
    doc: EukRep fasta statistics
    type: File
    outputSource: eukrep_stats/output

inputs:
  identifier:
    type: string
    doc: Identifier for this dataset used in this workflow
    label: Identifier used
  assembly:
    type: File
    doc: Assembly in fasta format
    label: Assembly fasta
    loadListing: no_listing
  bam_file:
    type: File
    doc: Mapping file in sorted bam format containing reads mapped to the assembly
    label: Bam file
    loadListing: no_listing
  threads:
    type: int
    doc: Number of threads to use for computational processes
    label: Threads
    default: 2
  memory:
    type: int
    doc: Maximum memory usage in megabytes
    label: memory usage (MB)
    default: 4000

  gtdbtk_data:
    type: Directory?
    doc: Directory containing the GTDB database. When none is given GTDB-Tk will be skipped.
    label: gtdbtk data directory
    loadListing: no_listing
  busco_data:
    type: Directory?
    label: BUSCO dataset
    doc: Directory containing the BUSCO dataset location.
    loadListing: no_listing

  bakta_db:
    type: Directory?
    label: Bakta DB
    doc: Bakta Database directory (required when annotating bins)
  skip_bakta_crispr:
    type: boolean
    label: Skip CRISPR
    doc: Skip CRISPR array prediction using PILER-CR
    default: false

  interproscan_directory:
    type: Directory?
    label: InterProScan 5 directory
    doc: Directory of the (full) InterProScan 5 program. Used for annotating bins. (required when running with interproscan)
  eggnog_dbs:
    type:
      - 'null'
      - type: record
        name: eggnog_dbs
        fields:
          data_dir:
            type: Directory?
            doc: Directory containing all data files for the eggNOG database.
          db:
            type: File?
            doc: eggNOG database file
          diamond_db:
            type: File?
            doc: eggNOG database file for diamond blast search
  run_kofamscan:
    type: boolean
    label: Run kofamscan
    doc: Run with KEGG KO KoFamKOALA annotation. Default false
    default: false
  kofamscan_limit_sapp:
    type: int?
    label: SAPP kofamscan limit
    doc: Limit max number of entries of kofamscan hits per locus in SAPP. Default 5
    default: 5
  run_eggnog:
    type: boolean
    label: Run eggNOG-mapper
    doc: Run with eggNOG-mapper annotation. Requires eggnog database files. Default false
    default: false
  run_interproscan:
    type: boolean
    label: Run InterProScan
    doc: Run with eggNOG-mapper annotation. Requires InterProScan v5 program files. Default false
    default: false
  interproscan_applications:
    type: string
    label: InterProScan applications
    doc: |
          Comma separated list of analyses:
          FunFam,SFLD,PANTHER,Gene3D,Hamap,PRINTS,ProSiteProfiles,Coils,SUPERFAMILY,SMART,CDD,PIRSR,ProSitePatterns,AntiFam,Pfam,MobiDBLite,PIRSF,NCBIfam
          default Pfam,SFLD,SMART,AntiFam,NCBIfam
    default: 'Pfam'

  run_semibin:
    type: boolean
    doc: Run with SemiBin2 binner. Default true
    label: Run SemiBin
    default: true
  semibin_environment:
    type: string
    doc: Semibin2 Built-in models (global/human_gut/dog_gut/ocean/soil/cat_gut/human_oral/mouse_gut/pig_gut/built_environment/wastewater/chicken_caecum). Default; global
    label: SemiBin Environment
    default: global

  # bin refiner
  run_binspreader:
    type: boolean?
    doc: Whether to use BinSPreader for bin refinement
    default: false
  assembly_graph:
    type: File?
    doc: Assembly graph file from asssembler for BinSPreader
    label: BinSPreader graph file
  
  # run_binette:
  #   type: boolean?
  #   doc: Whether to use Binette for bin refinement
  #   default: false
  # run_dastool:
  #   type: boolean?
  #   doc: Whether to use DAS Tool for bin refinement
  #   default: true

  annotate_bins:
    type: boolean
    label: Annotate bins
    doc: Annotate bins. Default false
    default: false
  annotate_unbinned:
    type: boolean
    label: Annotate unbinned
    doc: Annotate unbinned contigs. Will be treated as metagenome. Default false
    default: false
  
  sub_workflow_bins:
    type: boolean
    label: Sub workflow Run
    doc: Use this when you need the output bins as File[] for subsequent analysis workflow steps in another workflow.
    default: false
  sub_workflow_bin_proteins:
    type: boolean
    label: Sub workflow Run
    doc: Use this when you need the output the bin protein files as File[] for subsequent analysis workflow steps in another workflow such as wf_GEM.
    default: false

  destination:
    type: string?
    label: Output destination
    doc: Optional output destination path for cwl-prov reporting. (not used in the workflow itself)

steps:
#############################################
#### Contig depths file retrieval
  metabat2_contig_depths:
    label: contig depths 
    doc: MetabatContigDepths to obtain the depth file used in the MetaBat2 and SemiBin2 binning process
    run: ../tools/metabat2/metabatContigDepths.cwl
    in:
      identifier: identifier
      bamFile: bam_file
    out: [depths]
#############################################
#### EukRep, eukaryotic sequence classification
  eukrep:
    doc: EukRep, eukaryotic sequence classification
    label: EukRep
    run: ../tools/eukrep/eukrep.cwl
    in:
      identifier: identifier
      assembly: assembly
    out: [euk_fasta_out]

  eukrep_stats:
    doc: EukRep fasta statistics
    label: EukRep stats
    run: ../tools/stats/raw_n50.cwl
    in:
      tmp_id: identifier
      identifier:
        valueFrom: $(inputs.tmp_id)_EukRep
      input_fasta: eukrep/euk_fasta_out
    out: [output]
#############################################
#### Assembly binning using metabat2
  metabat2:
    doc: Binning procedure using MetaBAT2
    label: MetaBAT2 binning
    run: ../tools/metabat2/metabat2.cwl
    in:
      identifier: identifier
      threads: threads

      assembly: assembly
      depths: metabat2_contig_depths/depths
    out: [bin_dir, log]

  metabat2_filter_bins:
    doc: Only keep genome bin fasta files (exlude e.g TooShort.fa)
    label: Keep MetaBAT2 genome bins
    run: ../tools/expressions/folder_file_regex.cwl
    in:
      folder: metabat2/bin_dir
      output_as_folder:
        default: true
      regex:
        valueFrom: "bin\\.[0-9]+\\.fa"
      output_folder_name:
        valueFrom: "MetaBAT2_bins"  
    out: [output_folder]

  metabat2_contig2bin:
    label: MetaBAT2 to contig to bins
    doc: List the contigs and their corresponding bin.
    run: ../tools/das_tool/fasta_to_contig2bin.cwl
    in:
      bin_folder: metabat2_filter_bins/output_folder
      binner_name:
        valueFrom: "MetaBAT2"
      extension:
        valueFrom: "fa"
    out: [table]

  metabat2_binspreader:
    doc: MetaBAT2 Bin refinement using BinSPreader
    label: MetaBAT2 BinSPreader
    run: ../tools/binspreader/binspreader.cwl
    when: $(inputs.run_binspreader && inputs.assembly_graph !== null)
    in:
      run_binspreader: run_binspreader
      
      identifier: identifier
      assembly_graph: assembly_graph
      contig2bin: metabat2_contig2bin/table
      threads: threads
    out: [refined_contig2bin, bin_stats, bin_weights, edge_weights, graph_links]

#############################################
#### Assembly binning using MaxBin2
  maxbin2:
    doc: Binning procedure using MaxBin2
    label: MaxBin2 binning
    run: ../tools/maxbin2/maxbin2.cwl
    in:
      identifier: identifier
      threads: threads
      contigs: assembly
      abundances: metabat2_contig_depths/depths
    out: [bins, summary, log]

  maxbin2_to_folder:
    doc: Create folder with MaxBin2 bins
    label: MaxBin2 bins to folder
    run: ../tools/expressions/files_to_folder.cwl
    in:
      files: maxbin2/bins
      destination: 
        valueFrom: "MaxBin2_bins"
    out: [results]

  maxbin2_contig2bin:
    label: MaxBin2 to contig to bins
    doc: List the contigs and their corresponding bin.
    run: ../tools/das_tool/fasta_to_contig2bin.cwl
    in:
      bin_folder: maxbin2_to_folder/results
      binner_name:
        valueFrom: "MaxBin2"
    out: [table]

  maxbin2_binspreader:
    doc: MaxBin2 Bin refinement using BinSPreader
    label: MaxBin2 BinSPreader
    run: ../tools/binspreader/binspreader.cwl
    when: $(inputs.run_binspreader && inputs.assembly_graph !== null)
    in:
      run_binspreader: run_binspreader
      
      identifier: identifier
      assembly_graph: assembly_graph
      contig2bin: maxbin2_contig2bin/table
      threads: threads
    out: [refined_contig2bin, bin_stats, bin_weights, edge_weights, graph_links]

#############################################
#### Assembly binning using Semibin2
  semibin:
    doc: Binning procedure using SemiBin2
    label: Semibin binning
    run: ../tools/semibin/semibin2_single_easy_bin.cwl
    when: $(inputs.run_semibin)
    in:
      run_semibin: run_semibin
      identifier:
        source: identifier
        valueFrom: $(self)_SemiBin
      threads: threads
      assembly: assembly
      # bam_file: bam_file
      metabat2_depth_file: metabat2_contig_depths/depths
      environment: semibin_environment
      # reference_database: semibin_reference_database
      # abundances: metabat2_contig_depths/depths
    # FIXME: for now we don't use recluster_bins
    out: [recluster_bins, output_bins, data, data_split, model, coverage]

  semibin_contig2bin:
    label: SemiBin to contig to bins
    doc: List the contigs and their corresponding bin.
    run: ../tools/das_tool/fasta_to_contig2bin.cwl
    when: $(inputs.run_semibin)
    in:
      run_semibin: run_semibin
      bin_folder: semibin/output_bins
      binner_name:
        valueFrom: "SemiBin"
      extension:
        valueFrom: "fa"
    out: [table]

  semibin_binspreader:
    doc: SemiBin2 Bin refinement using BinSPreader
    label: SemiBin2 BinSpreader
    run: ../tools/binspreader/binspreader.cwl
    when: $(inputs.run_binspreader && inputs.assembly_graph !== null)
    in:
      run_binspreader: run_binspreader
      
      identifier: identifier
      assembly_graph: assembly_graph
      contig2bin: semibin_contig2bin/table
      threads: threads
    out: [refined_contig2bin, bin_stats, bin_weights, edge_weights, graph_links]
#############################################
#### DAStool 
  das_tool:
    doc: DAS Tool
    label: DAS Tool integrate predictions from multiple binning tools
    run: ../tools/das_tool/das_tool.cwl
    in:
      run_semibin: run_semibin
      run_binspreader: run_binspreader
      identifier: identifier
      metabat2_binspreader_refined_contig2bin: metabat2_binspreader/refined_contig2bin
      maxbin2_binspreader_refined_contig2bin: maxbin2_binspreader/refined_contig2bin
      semibin_binspreader_refined_contig2bin: semibin_binspreader/refined_contig2bin
      metabat2_contig2bin: metabat2_contig2bin/table
      maxbin2_contig2bin: maxbin2_contig2bin/table
      semibin_contig2bin: semibin_contig2bin/table

      threads: threads
      assembly: assembly
      bin_tables:
        valueFrom: |
          ${
            var tables = [
              inputs.run_binspreader ? inputs.metabat2_binspreader_refined_contig2bin : inputs.metabat2_contig2bin,
              inputs.run_binspreader ? inputs.maxbin2_binspreader_refined_contig2bin : inputs.maxbin2_contig2bin,
              inputs.run_binspreader ? inputs.semibin_binspreader_refined_contig2bin : inputs.semibin_contig2bin
            ]        
            return tables;
          }
        linkMerge: merge_flattened
        pickValue: all_non_null
      binner_labels:
        valueFrom: |
          ${
            if (inputs.run_binspreader && inputs.run_semibin) {
              return "MetaBAT2_BinSPreader,MaxBin2_BinSPreader,SemiBin_BinSPreader";
            } else {
              if (inputs.run_binspreader) {
                return "MetaBAT2_BinSPreader,MaxBin2_BinSPreader";
              } else {
                if (inputs.run_semibin) {
                  return "MetaBAT2,MaxBin2,SemiBin";
                } else {
                return "MetaBAT2,MaxBin2";
                }
              }
            }
          }
    out: [bin_dir, summary, contig2bin, log]

  das_tool_bins:
    doc: DAS Tool bins folder to File array for further analysis
    label: Bin folder to files[]
    run: ../tools/expressions/folder_to_files.cwl
    in:
      folder: das_tool/bin_dir
    out: [files]

  remove_unbinned:
    doc: Remove unbinned fasta from bin directory. For analyis by subsequent tools. 
    label: Remove unbinned
    run: ../tools/expressions/folder_file_regex.cwl
    in:
      folder: das_tool/bin_dir
      output_as_folder:
        default: true
      regex:
        valueFrom: ".*unbinned.*"
      exclude:
        default: true
      output_folder_name:
        valueFrom: "DAS_Tool_genome_bins"
    out: [output_folder]

  extract_unbinned:
    doc: Extract unbinned fasta from bin directory. For analyis by subsequent tools. 
    label: Extract unbinned file
    run: ../tools/expressions/file_from_folder_regex.cwl
    in:
      folder: das_tool/bin_dir
      regex:
        valueFrom: ".*unbinned.*"
      output_file_name:
        source: identifier
        valueFrom: $(self)_unbinned.fasta
    out: [out_file]

#############################################
#### CheckM bin quality assessment
  checkm:
    doc: CheckM bin quality assessment
    label: CheckM
    run: ../tools/checkm/checkm_lineagewf.cwl
    in:
      identifier: identifier
      threads: threads
      bin_dir: remove_unbinned/output_folder
    out: [checkm_out_table, checkm_out_folder]

#############################################
#### BUSCO bin quality assessment
  busco:
    doc: BUSCO assembly completeness workflow
    label: BUSCO
    run: ../tools/busco/busco.cwl
    when: $(inputs.bins.length !== 0)
    in:
      bins: das_tool_bins/files
      identifier: identifier
      sequence_folder: remove_unbinned/output_folder
      mode:
        valueFrom: "geno"
      threads: threads
      busco_data: busco_data
      auto-lineage-prok:
        valueFrom: $(true)
    out: [batch_summary]

#############################################
#### GTDB-Tk Taxomic assigment of bins with
  gtdbtk:
    doc: Taxomic assigment of bins with GTDB-Tk
    label: GTDBTK
    when: $(inputs.gtdbtk_data !== null && inputs.bins.length !== 0)
    run: ../tools/gtdbtk/gtdbtk_classify_wf.cwl
    in:
      bins: das_tool_bins/files
      gtdbtk_data: gtdbtk_data
      identifier: identifier
      threads: threads
      bin_dir: remove_unbinned/output_folder
    out: [gtdbtk_summary, gtdbtk_out_folder]
  compress_gtdbtk:
    doc: Compress GTDB-Tk output folder
    label: Compress GTDB-Tk
    when: $(inputs.gtdbtk_data !== null)
    run: ../tools/bash/compress_directory.cwl
    in:
      gtdbtk_data: gtdbtk_data
      indir: gtdbtk/gtdbtk_out_folder
    out: [outfile]

#############################################
#### Aggregate bin depths
  aggregate_bin_depths:
    doc: Depths per bin
    label: Depths per bin
    run: ../tools/metabat2/aggregateBinDepths.cwl
    in:
      identifier: identifier
      metabatdepthsFile: metabat2_contig_depths/depths
      bins: das_tool_bins/files
    out: [binDepths]
#############################################
#### Bin assembly statistics
  bins_summary:
    doc: Table of all bins and their statistics like size, contigs, completeness etc
    label: Bins summary
    run: ../tools/metagenomics/bins_summary.cwl
    in:
      identifier: identifier
      bin_dir: das_tool/bin_dir
      bin_depths: aggregate_bin_depths/binDepths
      checkm: checkm/checkm_out_table
      gtdbtk: gtdbtk/gtdbtk_summary
      busco_batch: busco/batch_summary
    out: [bins_summary_table]
############################################
### General bin and assembly read mapping stats
  bin_readstats:
    doc: Table general bin and assembly read mapping stats
    label: Bin and assembly read stats
    run: ../tools/metagenomics/assembly_bins_readstats.cwl
    in:
      identifier: identifier
      binContigs: das_tool/contig2bin
      bam_file: bam_file
      assembly: assembly
    out: [binReadStats]

############################################
### Bin annotation workflow
  binfolder_to_files:
    doc: Convert bins in a directory to an array of Files
    label: Bin folder to files
    ## Disabled when condition as a work around
    # when: $(inputs.annotate_bins)
    run: ../tools/expressions/folder_to_files.cwl
    in:
      annotate_bins: annotate_bins
      folder: remove_unbinned/output_folder
    out: [files]

  workflow_microbial_annotation_bins:
    doc: Microbial annotation workflow of the predicted bins
    label: Annotate bins
    when: $(inputs.annotate_bins && inputs.bakta_db !== null)
    run: workflow_microbial_annotation.cwl
    scatter: [genome_fasta]
    scatterMethod: dotproduct
    in:
      annotate_bins: annotate_bins
      genome_fasta: 
        source: binfolder_to_files/files
        default: []
      bakta_db: bakta_db
      skip_bakta_crispr: skip_bakta_crispr
      interproscan_directory: interproscan_directory
      interproscan_applications: interproscan_applications
      eggnog_dbs: eggnog_dbs
      run_interproscan: run_interproscan
      run_eggnog: run_eggnog
      run_kofamscan: run_kofamscan
      kofamscan_limit_sapp: kofamscan_limit_sapp
     
      sapp_conversion:
        default: true
      threads: threads
      compress_output:
        default: true
    out: [bakta_folder_compressed, compressed_other_files, sapp_hdt_file]

  workflow_microbial_annotation_unbinned:
    doc: Microbial annotation workflow of the predicted bins
    label: Annotate bins
    when: $(inputs.annotate_unbinned && inputs.genome_fasta !== null && inputs.bakta_db !== null)
    run: workflow_microbial_annotation.cwl
    in:
      annotate_unbinned: annotate_unbinned
      genome_fasta: extract_unbinned/out_file
      bakta_db: bakta_db
      skip_bakta_crispr: skip_bakta_crispr
      interproscan_directory: interproscan_directory
      interproscan_applications: interproscan_applications
      eggnog_dbs: eggnog_dbs
      run_interproscan: run_interproscan
      run_eggnog: run_eggnog
      run_kofamscan: run_kofamscan
      kofamscan_limit_sapp: kofamscan_limit_sapp
      metagenome:
        default: true
      skip_bakta_plot:
        default: true
      sapp_conversion:
        default: true
      threads: threads
      compress_output:
        default: true
    out: [bakta_folder_compressed, compressed_other_files, sapp_hdt_file]

  # extract_proteins:
  #   doc: Extract protein fasta from bakta directories. For analyis by subsequent tools. 
  #   label: Extract proteins
  #   when: $(inputs.annotate_bins || inputs.annotate_unbinned)
  #   run: ../tools/expressions/file_from_folder_regex.cwl
  #   scatter: [folder]
  #   scatterMethod: dotproduct
  #   in:
  #     folder: workflow_microbial_annotation_bins/bakta_folder
  #     regex:
  #       valueFrom: ".*.faa"
  #     output_file_name:
  #       source: identifier
  #       valueFrom: $(self)_unbinned.fasta
  #   out: [out_file]

#############################################

# OUTPUT FOLDER PREPARATION #

#############################################
#### MetaBAT2 output folder
  metabat2_files_to_folder:
    doc: Preparation of MetaBAT2 output files + unbinned contigs to a specific output folder
    label: MetaBAT2 output folder
    run: ../tools/expressions/files_to_folder.cwl
    in:
      files:
        source: [metabat2/log, metabat2_contig_depths/depths, metabat2_contig2bin/table]
        linkMerge: merge_flattened
      folders:
        source: [metabat2/bin_dir]
        linkMerge: merge_flattened
      destination:
        valueFrom: "Binner_MetaBAT2"
    out:
      [results]

  metabat2_binspreader_files_to_folder:
    doc: Preparation of BinSpreader refined MetaBAT2 bins output files + unbinned contigs to a specific output folder
    label: MetaBAT2 BinSpreader output folder
    when: $(inputs.run_binspreader && inputs.assembly_graph !== null)
    run: ../tools/expressions/files_to_folder.cwl
    in:
      run_binspreader: run_binspreader
      files:
        source: [metabat2_binspreader/refined_contig2bin, metabat2_binspreader/bin_stats, metabat2_binspreader/bin_weights, metabat2_binspreader/edge_weights, metabat2_binspreader/graph_links]
        linkMerge: merge_flattened
      destination:
        valueFrom: "BinRefiner_MetaBAT2_BinSPreader"
    out:
      [results]
#############################################
#### MaxBin2 output folder
  maxbin2_files_to_folder:
    doc: Preparation of maxbin2 output files to a specific output folder.
    label: MaxBin2 output folder
    run: ../tools/expressions/files_to_folder.cwl
    in:
      files:
        source: [maxbin2/summary, maxbin2/log, maxbin2_contig2bin/table]
        linkMerge: merge_flattened
      folders:
        source: [maxbin2_to_folder/results]
        linkMerge: merge_flattened
      destination:
        valueFrom: "Binner_MaxBin2"
    out:
      [results]
  maxbin2_binspreader_files_to_folder:
    doc: Preparation of BinSpreader refined MaxBin2 bins output files to a specific output folder
    label: MaxBin2 BinSpreader output folder
    when: $(inputs.run_binspreader && inputs.assembly_graph !== null)
    run: ../tools/expressions/files_to_folder.cwl
    in:
      run_binspreader: run_binspreader

      files:
        source: [maxbin2_binspreader/refined_contig2bin, maxbin2_binspreader/bin_stats, maxbin2_binspreader/bin_weights, maxbin2_binspreader/edge_weights, maxbin2_binspreader/graph_links]
        linkMerge: merge_flattened
      destination:
        valueFrom: "BinRefiner_MaxBin2_BinSPreader"
    out:
      [results]
#############################################
#### SemiBin2 output folder
  semibin_files_to_folder:
    doc: Preparation of SemiBin output files to a specific output folder.
    label: SemiBin output folder
    run: ../tools/expressions/files_to_folder.cwl
    when: $(inputs.run_semibin)
    in:
      run_semibin: run_semibin
      files:
        source: [semibin_contig2bin/table, semibin/data, semibin/data_split, semibin/model, semibin/coverage]
        linkMerge: merge_flattened
        pickValue: all_non_null
      folders:
        source: [semibin/output_bins, semibin/recluster_bins]
        linkMerge: merge_flattened
        pickValue: all_non_null
      destination:
        valueFrom: "Binner_SemiBin"
    out:
      [results]
  semibin_binspreader_files_to_folder:
    doc: Preparation of BinSpreader refined SemiBin bins output files to a specific output folder
    label: SemiBin BinSpreader output folder
    when: $(inputs.run_binspreader && inputs.assembly_graph !== null)
    run: ../tools/expressions/files_to_folder.cwl
    in:
      run_binspreader: run_binspreader

      files:
        source: [semibin_binspreader/refined_contig2bin, semibin_binspreader/bin_stats, semibin_binspreader/bin_weights, semibin_binspreader/edge_weights, semibin_binspreader/graph_links]
        linkMerge: merge_flattened
      destination:
        valueFrom: "BinRefiner_SemiBin_BinSPreader"
    out:
      [results]
#############################################
#### DAS Tool output folder
  das_tool_files_to_folder:
    doc: Preparation of DAS Tool output files to a specific output folder.
    label: DAS Tool output folder
    run: ../tools/expressions/files_to_folder.cwl
    in:
      files:
        source: [das_tool/log, das_tool/summary, das_tool/contig2bin, aggregate_bin_depths/binDepths]
        linkMerge: merge_flattened
      folders:
        source: [das_tool/bin_dir]
        linkMerge: merge_flattened
      destination:
        valueFrom: "DAS_Tool_binner_integration"
    out:
      [results]
#############################################
#### CheckM output folder
  checkm_files_to_folder:
    doc: Preparation of CheckM output files to a specific output folder
    label: CheckM output
    run: ../tools/expressions/files_to_folder.cwl
    in:
      files: 
        source: [checkm/checkm_out_table]
        linkMerge: merge_flattened
      folders:
        source: [checkm/checkm_out_folder]
        linkMerge: merge_flattened
      destination:
        valueFrom: "CheckM_Bin_Quality"
    out:
      [results]
############################################
### BUSCO output folder
  busco_files_to_folder:
    doc: Preparation of BUSCO output files to a specific output folder
    label: BUSCO output folder
    run: ../tools/expressions/files_to_folder.cwl
    in:
      files:
        source: [busco/batch_summary]
        linkMerge: merge_flattened
        pickValue: all_non_null
      destination: 
        valueFrom: "BUSCO_Bin_Completeness"
    out:
      [results]
#############################################
#### GTDB-Tk output folder
  gtdbtk_files_to_folder:
    doc: Preparation of GTDB-Tk output files to a specific output folder
    label: GTBD-Tk output folder
    when: $(inputs.gtdbtk_data !== null)
    run: ../tools/expressions/files_to_folder.cwl
    in:
      gtdbtk_data: gtdbtk_data
      files:
        source: [gtdbtk/gtdbtk_summary, compress_gtdbtk/outfile]
        linkMerge: merge_flattened
        pickValue: all_non_null
      destination: 
        valueFrom: "GTDB-Tk_Bin_Taxonomy"
    out:
      [results]
#############################################
#### Annotation output folder
  annotation_output_to_array_bins:
    when: $(inputs.annotate_bins && inputs.bakta_db !== null)
    run: ../tools/expressions/merge_file_arrays.cwl
    in:
      annotate_bins: annotate_bins
      bakta_db: bakta_db
      input: 
        source: [workflow_microbial_annotation_bins/compressed_other_files]
    out: 
      [output]
  annotation_files_to_folder_bins:
    doc: Preparation of annotation output files to a specific output folder
    label: Annotation output folder
    when: $((inputs.annotate_bins || inputs.annotate_unbinned) && inputs.bakta_db !== null)
    run: ../tools/expressions/files_to_folder.cwl
    in:
      annotate_bins: annotate_bins
      annotate_unbinned: annotate_unbinned
      bakta_db: bakta_db
      folders: 
        source: [workflow_microbial_annotation_bins/bakta_folder_compressed, workflow_microbial_annotation_unbinned/bakta_folder_compressed]
        linkMerge: merge_flattened
        pickValue: all_non_null
      files:
        source: [workflow_microbial_annotation_unbinned/compressed_other_files, workflow_microbial_annotation_unbinned/sapp_hdt_file, workflow_microbial_annotation_bins/sapp_hdt_file, annotation_output_to_array_bins/output]
        linkMerge: merge_flattened
        pickValue: all_non_null
      destination: 
        valueFrom: "Bin_annotation"
    out:
      [results]

s:author:
  - class: s:Person
    s:identifier: https://orcid.org/0000-0001-8172-8981
    s:email: mailto:jasper.koehorst@wur.nl
    s:name: Jasper Koehorst
  - class: s:Person
    s:identifier: https://orcid.org/0000-0001-9524-5964
    s:email: mailto:bart.nijsse@wur.nl
    s:name: Bart Nijsse
  - class: s:Person
    s:identifier: https://orcid.org/0009-0001-1350-5644
    s:email: mailto:changlin.ke@wur.nl
    s:name: Changlin Ke

s:citation: https://m-unlock.nl
s:codeRepository: https://gitlab.com/m-unlock/cwl
s:dateCreated: "2020-00-00"
s:dateModified: "2024-10-02"
s:license: https://spdx.org/licenses/Apache-2.0
s:copyrightHolder: "UNLOCK - Unlocking Microbial Potential"

$namespaces:
  s: https://schema.org/