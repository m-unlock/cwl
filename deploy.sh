#!/bin/bash
#===========================================================================================
#title          :UNLOCK CWL deploy
#description    :Deploy script by copying CWL files to the corresponding iRODS webdav folder
#author         :Bart Nijsse & Jasper Koehorst
#date           :2021
#version        :0.0.2
#===========================================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Perform a git pull
cd $DIR && git pull

if [ $? -ne 0 ]; then
  echo "Git pull error"
  exit $?
fi

# List of names to sync inside the CWL folder
locs=("tools" "workflows" "packed")

###################################
# Mac volume mount location
DIRECTORY=/Volumes/unlock-icat.irods.surfsara.nl/unlock/infrastructure/cwl/

if [ -d "$DIRECTORY" ]; then
  BRANCH="$(git rev-parse --abbrev-ref HEAD)"
  echo "BRANCH: $BRANCH"
  if [[ "$BRANCH" == "dev" ]]; then
    DIRECTORY=$DIRECTORY/dev/
    echo "DIRECTORY: $DIRECTORY"
  fi
  echo "Copying files to "$DIRECTORY" from "$DIR
  # For loop over the locations to sync
  # rsync -vah --size-only $DIR/ $DIRECTORY/ --delete
  for loc in "${locs[@]}"
  do
    echo "Syncing $DIR/$loc"
    rsync -vah --size-only $DIR/$loc/ $DIRECTORY/$loc/ --delete
  done
  
  # TODO Check sync tests
  # echo rsync -vah --size-only $DIR/tests/ $DIRECTORY/tests --delete
  # rsync -vah --size-only $DIR/tests/ $DIRECTORY/tests --delete
else
  echo -e "\033[0;31mMAC WebDAV mount unaccessible!\033[0m"
fi

###################################
# Other mount locations
DIRECTORY=/run/user/1000/gvfs/dav:host=unlock-icat.irods.surfsara.nl,ssl=true/unlock/infrastructure/cwl/

if [ -d "$DIRECTORY" ]; then
  BRANCH="$(git rev-parse --abbrev-ref HEAD)"
  if [[ "$BRANCH" == "dev" ]]; then
    DIRECTORY=$DIRECTORY/dev/
  fi
  echo "Copying files to "$DIRECTORY
  rsync -vrh --size-only $DIR/cwl/ $DIRECTORY/ --delete
else
  echo -e "\033[0;31mUBUNTU WebDAV mount unaccessible!\033[0m"
fi

###################################
# Sync start for kubernetes nodes #
###################################

DIRECTORY=$DIR/../sync
if [ -d "$DIRECTORY" ]; then
  $DIR/../sync/sync.sh --cwl
fi
